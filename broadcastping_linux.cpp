#include "broadcastping_linux.h"

BroadcastPing_Linux::BroadcastPing_Linux(QObject *parent) :
    QObject(parent)
{
    pingedAddresses = new QVector<QString>;
    broadcastAddress.clear();
}


// ----------------------------------------------------------------------------
BroadcastPing_Linux::~BroadcastPing_Linux()
{
    delete pingedAddresses;
}

// ----------------------------------------------------------------------------
int BroadcastPing_Linux::startingPing()
{
    int sock_to = -1;
    char packet[sizeof(struct icmp)];
    char buffer[sizeof(struct icmp)];
    struct icmp* icmp = (struct icmp*) packet;

    // Посылаем.
    sock_to = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (sock_to < 0)
    {
         return -1;
    }

    // Широковещательный запрос.
    int broadcast = 1;
    setsockopt(sock_to, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast));

    memset(packet, 0, sizeof(packet));
    icmp->icmp_type  = 8;
    icmp->icmp_code  = 0;
    icmp->icmp_cksum = 0;
    icmp->icmp_cksum = in_cksum((unsigned short *)packet, sizeof(packet));

    struct sockaddr_in addr_to;
    addr_to.sin_family = AF_INET;
    addr_to.sin_addr.s_addr =  inet_addr(broadcastAddress.toLocal8Bit());
    addr_to.sin_port = htons(7);

    socklen_t addrLen = sizeof(sockaddr_in);

    sendto(sock_to, packet, sizeof(packet), 0, (sockaddr*)&addr_to, addrLen);

    // Ловим.
    int sock_from = -1;
    sock_from = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);

    if (sock_from == -1)
    {
        return -1;
    }

    while (true)
    {
        fd_set rdevents;
        timeval tv;
        FD_ZERO( &rdevents );
        FD_SET( sock_from, &rdevents );
        tv.tv_sec = 0;
        tv.tv_usec = 1000000;

        // ждем готовности сокета
        int rval = select( sock_from + 1, &rdevents, NULL, NULL, &tv );
        if (rval == 0)
        {
            break;
        }
        else if (rval == -1)    // ошибка select, скорее всего сокет закрыт
        {

            return -1;
        }
        else                    // есть готовность
        {
            int dataSize = recvfrom(sock_from, buffer, sizeof(buffer), 0, NULL, NULL);
            icmpAnalysis(buffer, dataSize);
        }
    }
    return 0;
}


// ----------------------------------------------------------------------------
void BroadcastPing_Linux::icmpAnalysis(char *buff, int size)
{
    struct ip *ip = NULL;
    struct icmp *icmp = NULL;
    int hl = 0;
    struct timeval tv;
    if( !buff)
        return;

    gettimeofday( &tv, 0);                       // time of codogramm receive

    ip = ( struct ip * )buff;
    if ( ip->ip_v != 4 )
    {
      /* error( 0, 0, "IP datagram not version 4\n" ); */
      return;
    }

    /* IP header length in bytes */
    hl = ip->ip_hl << 2;
    if ( size < hl + ICMP_MINLEN )
    {
      /* error( 0, 0, "short datagram (%d bytes) from %s\n",
       * len, inet_ntoa( ip->ip_src ) );
       */
      return;
    }

    /* ICMP packet */
    icmp = ( struct icmp * )( buff + hl );
    if ( icmp->icmp_type == 0)                  // ECHO REPLY
    {
        /* Error packets aren't processed, bad node state by timeout only */
        pingedAddresses->append(inet_ntoa(ip->ip_src));
    }
}

// ----------------------------------------------------------------------------
unsigned short BroadcastPing_Linux::in_cksum(unsigned short *addr, int len)
{
    register int sum = 0;
    unsigned short answer = 0;
    register unsigned short *w = addr;
    register int nleft = len;
    /*
     * Our algorithm is simple, using a 32 bit accumulator (sum), we add
     * sequential 16 bit words to it, and at the end, fold back all the
     * carry bits from the top 16 bits into the lower 16 bits.
     */
    while (nleft > 1)
    {
      sum += *w++;
      nleft -= 2;
    }
    /* mop up an odd byte, if necessary */
    if (nleft == 1)
    {
      *(unsigned char *) (&answer) = *(unsigned char *) w;
      sum += answer;
    }
    /* add back carry outs from top 16 bits to low 16 bits */
    sum = (sum >> 16) + (sum & 0xffff);       /* add hi 16 to low 16 */
    sum += (sum >> 16);                       /* add carry */
    answer = ~sum;                            /* truncate to 16 bits */
    return (answer);
}
