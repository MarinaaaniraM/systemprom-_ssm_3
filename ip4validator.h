#ifndef IP4VALIDATOR_H
#define IP4VALIDATOR_H


#include <QValidator>


// предполагается, что валидатор работает вместе с QLineEdit::setInputMask("000.000.000.000; ")
// да, я знаю, что валидатор и маска плохо работают вместе, но тут оно, похоже, пока живое

class IP4Validator : public QValidator
{
    Q_OBJECT


public:
    explicit IP4Validator(QObject *parent = 0);
    QValidator::State validate(QString &input, int &pos) const;


};

#endif // IP4VALIDATOR_H
