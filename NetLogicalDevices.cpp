#include <QUuid>
#include <QGraphicsScene>
#include <qmath.h>
#include <QThreadPool>
#include <QTimer>
#include "NetLogicalDevices.h"
#include "GlobalConstants.h"




NetDevice::NetDevice(QString deviceName, QObject *parent) :
    QObject(parent), name(deviceName)
{
    deviceTypeInterface = &PreDefinedDeviceTypeInterface::unknownDevice;

    graphicalItem = new NetDeviceGraphicalItem(this, deviceTypeInterface->offlineImage, deviceTypeInterface->onlineImage);

    connect(graphicalItem, &NetDeviceGraphicalItem::userRequestToEditDevice, this, &NetDevice::userRequestToEditDevice);

    // ========================================================================
    connect(graphicalItem, &NetDeviceGraphicalItem::getWebInterfaceOfdevice, this, &NetDevice::openWebInterface);
    // ========================================================================

    connect(graphicalItem, &NetDeviceGraphicalItem::itemIsSelected, this, &NetDevice::userSelectedDevice);
    connect(graphicalItem, &NetDeviceGraphicalItem::userRequestToConnectDevices, this, &NetDevice::userRequestToConnectDevices);
    connect(graphicalItem, &NetDeviceGraphicalItem::userRequestToDeleteDevice, this, &NetDevice::userRequestToDeleteDevice);
    connect(graphicalItem, &NetDeviceGraphicalItem::userDoubleClickedItem, this, &NetDevice::userDoubleClickedItem);
    connect(graphicalItem, &NetDeviceGraphicalItem::deviceOnlineStatusChanged, this, &NetDevice::deviceOnlineStatusChanged);

    deviceUUID = QUuid::createUuid().toString();

    QThreadPool::globalInstance()->setExpiryTimeout(-1); // мы сами удалим треды
}
/////////////////////////////////////////
NetDevice::NetDevice(QObject *parent)
{
    NetDevice(tr("Без имени"), parent);
}
/////////////////////////////////////////
bool NetDevice::isMultiLevel() const
{
    return multiLevel;
}
/////////////////////////////////////////
bool NetDevice::network_isBeingPinged() const
{
    return network_shouldUsePing;
}
/////////////////////////////////////////
void NetDevice::network_setPinging(const bool &value)
{
    if (network_shouldUsePing != value)
    {
        if (value)
            createThread();
        else
        {
            destroyThread();
            graphicalItem->setDeviceOffline(); // не пингуемый всегда в оффлайне
        }
    }

    network_shouldUsePing = value;
}
/////////////////////////////////////////
QString NetDevice::getMultiMapName() const
{
    return multiMapName;
}
/////////////////////////////////////////
NetDeviceGraphicalItem *NetDevice::getGraphicalItem()
{
    return graphicalItem;
}
/////////////////////////////////////////
const NetDeviceGraphicalItem *NetDevice::getGraphicalItem() const
{
    return graphicalItem;
}
/////////////////////////////////////////
const ServiceStructures::DeviceTypeInterface *NetDevice::getDTI() const
{
    return deviceTypeInterface;
}
/////////////////////////////////////////
QString NetDevice::getName() const
{
    return name;
}
/////////////////////////////////////////
QString NetDevice::getAboutDeviceText() const
{
    return aboutDevice;
}
/////////////////////////////////////////
QString NetDevice::getNetworkAddressString() const
{
    QString returner = network_address.toString();

    QStringList octets = returner.split('.');
    for (int i = 0; i < octets.size(); i++)
    {
        while (octets.at(i).size() < 3)
        {
            // дополним пробелами слева
            octets[i] = QString(" " + octets.at(i));
        }
    }

    return octets.join('.');
}
////////////////////////////////////////
QString NetDevice::getNetworkAddressStringFormatted() const
{
    QString returner = network_address.toString();
    returner.remove(' ');
    return returner;
}
////////////////////////////////////////
void NetDevice::setName(const QString &text)
{
    name = text;

    graphicalItem->setTextItemText(getName(), getNetworkAddressString());
}
///////////////////////////////////////
void NetDevice::setAboutDeviceText(const QString &text)
{
    aboutDevice = text;
}
//////////////////////////////////////
void NetDevice::setMultiMapName(const QString &mapName)
{
    multiMapName = mapName;
    multiLevel = !(mapName.isEmpty());

    graphicalItem->setMultiMap(multiLevel);
}
//////////////////////////////////////
void NetDevice::network_setNetworkAddress(const QHostAddress &address)
{
    network_address = address;

    graphicalItem->setTextItemText(getName(), getNetworkAddressStringFormatted());
}
//////////////////////////////////////
bool NetDevice::network_setNetworkAddress(const QString &address)
{
    QString toUse = address;
    toUse.remove(' ');

    if (network_address.setAddress(toUse))
    {
        graphicalItem->setTextItemText(getName(), getNetworkAddressStringFormatted());
        return true;
    }

    return false;
}
//////////////////////////////////////
void NetDevice::setDeviceTypeInterface(const ServiceStructures::DeviceTypeInterface *dti)
{
    deviceTypeInterface = dti;

    graphicalItem->setImages(deviceTypeInterface->offlineImage, deviceTypeInterface->onlineImage);
}
//////////////////////////////////////
QHostAddress NetDevice::network_getNetworkAddress() const
{
    return network_address;
}
//////////////////////////////////////
QString NetDevice::getUUID() const
{
    return deviceUUID;
}
//////////////////////////////////////
void NetDevice::setUUID(const QString &value)
{
    deviceUUID = value;
}
//////////////////////////////////////
void NetDevice::userRequestToEditDevice()
{
    emit requestToEditDevice(this);
}


// ============================================================================
void NetDevice::openWebInterface()
{
    QDesktopServices service;
    service.openUrl(QUrl("http://" + this->network_address.toString()));
}

// ============================================================================


//////////////////////////////////////
void NetDevice::userSelectedDevice()
{
    emit graphicalItemIsSelected(this);
}
//////////////////////////////////////
void NetDevice::userRequestToConnectDevices()
{
    emit requestToConnectDevices(this);
}
//////////////////////////////////////
void NetDevice::userRequestToDeleteDevice()
{
    emit requestToDeleteDevice(this);
}
//////////////////////////////////////
void NetDevice::userDoubleClickedItem()
{
    if (isMultiLevel())
        emit requestToOpenMultiMap(this);
}
//////////////////////////////////////
void NetDevice::deviceOnlineStatusChanged(const bool &isOnline)
{
    emit deviceOnlineStatusChanged_writeToLog(QString(tr("Устройство ") + getName() + tr(" по адресу ") +
                                                      getNetworkAddressStringFormatted() +
                                                      (isOnline ? "" : tr(" не")) + tr(" доступно")),
                                              ServiceStructures::LogMessageType::normal);
}
//////////////////////////////////////
void NetDevice::createThread()
{
    emit startPingingMe(this->getUUID(), this->getNetworkAddressStringFormatted());
}
//////////////////////////////////////
void NetDevice::destroyThread()
{
    emit stopPingingMe(this->getUUID());
}
//////////////////////////////////////
void NetDevice::resetThread()
{
    destroyThread();
    createThread();
}
//////////////////////////////////////
void NetDevice::suspendThread(const bool &suspend)
{
    if (suspend)
        destroyThread(); // даже если его не было, то ничего страшного не случится, т.к. тогда он 0
    else
    {
        if (network_shouldUsePing)
            createThread();
    }
}
//////////////////////////////////////
bool NetDevice::operator ==(const NetDevice &other) const
{
    bool returner = ((name == other.getName()) && (aboutDevice == other.getAboutDeviceText()) &&
                     (network_address == other.network_getNetworkAddress()) && (deviceUUID == other.getUUID()));

    return returner;
}
/////////////////////////////////////
NetDevice::~NetDevice()
{
    delete graphicalItem;
    destroyThread();
}





/* -----------------------------------------------------------
 * -----------------------------------------------------------
 * ----------------------------------------------------------- */



NetDeviceLink::NetDeviceLink(NetDevice *device1, NetDevice *device2, QObject *parent) :
    QObject(parent), firstDevice(device1), secondDevice(device2)
{
    createElements();
}
//////////////////////////////////////
void NetDeviceLink::paintElements(QGraphicsScene *sceneForPainting)
{
    sceneForPainting->addItem(pointItem_1);
    sceneForPainting->addItem(pointItem_2);

    pointItem_1->setPos(pointItem_1_pointToDraw);
    pointItem_2->setPos(pointItem_2_pointToDraw);

    lineItem_1 = new NetDeviceLineItem(firstDevicePos, pointItem_1_pointToDraw, this);
    lineItem_2 = new NetDeviceLineItem(pointItem_1_pointToDraw, pointItem_2_pointToDraw, this);
    lineItem_3 = new NetDeviceLineItem(pointItem_2_pointToDraw, secondDevicePos, this);

    sceneForPainting->addItem(lineItem_1);
    sceneForPainting->addItem(lineItem_2);
    sceneForPainting->addItem(lineItem_3);

    connectComponents();
}
//////////////////////////////////////
const NetDevice *NetDeviceLink::getFirstDevice() const
{
    return firstDevice;
}
//////////////////////////////////////
const NetDevice *NetDeviceLink::getSecondDevice() const
{
    return secondDevice;
}
//////////////////////////////////////
QPointF NetDeviceLink::getFirstPointPos() const
{
    return pointItem_1->scenePos();
}
//////////////////////////////////////
QPointF NetDeviceLink::getSecondPointPos() const
{
    return pointItem_2->scenePos();
}
//////////////////////////////////////
void NetDeviceLink::setFirstPointPos(const qreal &x, const qreal &y)
{
    pointItem_1->setPos(x, y);
}
//////////////////////////////////////
void NetDeviceLink::setSecondPointPos(const qreal &x, const qreal &y)
{
    pointItem_2->setPos(x, y);
}
//////////////////////////////////////
void NetDeviceLink::setFirstPointPosBeforePaint(const qreal &x, const qreal &y)
{
    pointItem_1_pointToDraw.setX(x);
    pointItem_1_pointToDraw.setY(y);
}
//////////////////////////////////////
void NetDeviceLink::setSecondPointPosBeforePaint(const qreal &x, const qreal &y)
{
    pointItem_2_pointToDraw.setX(x);
    pointItem_2_pointToDraw.setY(y);
}
//////////////////////////////////////
bool NetDeviceLink::operator ==(const NetDeviceLink &other) const
{
    return ((firstDevice == other.getFirstDevice()) && (secondDevice == other.getSecondDevice()) &&
            (pointItem_1->pos() == other.getFirstPointPos()) && (pointItem_2->pos() == other.getSecondPointPos()));
}
//////////////////////////////////////
void NetDeviceLink::createElements()
{
    firstDevicePos = firstDevice->getGraphicalItem()->scenePos(); // (x1, y1)
    secondDevicePos = secondDevice->getGraphicalItem()->scenePos(); // (x2, y2)

    // нужно скорректировать позицию девайсов, чтобы смотреть не на верхний левый угол, а на центр иконки
    firstDevicePos.setX(firstDevicePos.x() + firstDevice->getGraphicalItem()->boundingRect().width() / 2);
    firstDevicePos.setY(firstDevicePos.y() + firstDevice->getGraphicalItem()->boundingRect().height() / 2);
    secondDevicePos.setX(secondDevicePos.x() + secondDevice->getGraphicalItem()->boundingRect().width() / 2);
    secondDevicePos.setY(secondDevicePos.y() + secondDevice->getGraphicalItem()->boundingRect().height() / 2);

    qreal xMod = qFabs(firstDevicePos.x() - secondDevicePos.x()) / 3;
    qreal yMod = qFabs(firstDevicePos.y() - secondDevicePos.y()) / 3;

    QPointF pointPos_1;
    QPointF pointPos_2;

    if (secondDevicePos.x() > firstDevicePos.x())
    {
        // x2 > x1
        pointPos_1.setX(firstDevicePos.x() + xMod);
        pointPos_2.setX(firstDevicePos.x() + xMod * 2);
    }
    else
    {
        // x1 > x2
        pointPos_1.setX(firstDevicePos.x() - xMod);
        pointPos_2.setX(firstDevicePos.x() - xMod * 2);
    }

    if (secondDevicePos.y() > firstDevicePos.y())
    {
        // y2 > y1
        pointPos_1.setY(firstDevicePos.y() + yMod);
        pointPos_2.setY(firstDevicePos.y() + yMod * 2);
    }
    else
    {
        // y1 > y2
        pointPos_1.setY(firstDevicePos.y() - yMod);
        pointPos_2.setY(firstDevicePos.y() - yMod * 2);
    }

    pointItem_1 = new NetDevicePointItem(this);
    pointItem_2 = new NetDevicePointItem(this);

    pointItem_1_pointToDraw = pointPos_1;
    pointItem_2_pointToDraw = pointPos_2;
}
//////////////////////////////////////
void NetDeviceLink::userRequestedToDeleteLink()
{
    emit requestToDeleteDevice(this);
}
//////////////////////////////////////
void NetDeviceLink::connectComponents()
{
    connect(firstDevice->getGraphicalItem(), &NetDeviceGraphicalItem::positionChanged,
            lineItem_1, &NetDeviceLineItem::firstPointChanged);
    connect(pointItem_1, &NetDevicePointItem::positionChanged, lineItem_1, &NetDeviceLineItem::secondPointChanged);

    connect(pointItem_1, &NetDevicePointItem::positionChanged, lineItem_2, &NetDeviceLineItem::firstPointChanged);
    connect(pointItem_2, &NetDevicePointItem::positionChanged, lineItem_2, &NetDeviceLineItem::secondPointChanged);

    connect(pointItem_2, &NetDevicePointItem::positionChanged, lineItem_3, &NetDeviceLineItem::firstPointChanged);
    connect(secondDevice->getGraphicalItem(), &NetDeviceGraphicalItem::positionChanged,
            lineItem_3, &NetDeviceLineItem::secondPointChanged);


    connect(pointItem_1, &NetDevicePointItem::userRequestToDeleteLink, this, &NetDeviceLink::userRequestedToDeleteLink);
    connect(pointItem_2, &NetDevicePointItem::userRequestToDeleteLink, this, &NetDeviceLink::userRequestedToDeleteLink);
}
//////////////////////////////////////
NetDeviceLink::~NetDeviceLink()
{
    delete pointItem_1;
    delete pointItem_2;

    delete lineItem_1;
    delete lineItem_2;
    delete lineItem_3;
}































