#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QDir>
#include <QFile>
#include <QImage>
#include "xmloperator.h"
#include "GlobalConstants.h"





XMLOperator::XMLOperator(QObject *parent) :
    QObject(parent)
{
    //
}
////////////////////////////////////////
bool XMLOperator::getCustomDeviceTypeInterfaces(QList<ServiceStructures::DeviceTypeInterface> &obtained)
{
    if (!obtained.isEmpty())
        return false; // пихают нам тут всякую гадость!

    QFile settingsCustomDTIsFile(QString("./" + GlobalConstants::settingsDirName +
                                         "/" + GlobalConstants::customDeviceTypeInterfacesFileName));
    if (!settingsCustomDTIsFile.open(QFile::ReadOnly | QFile::Text))
        return false;

    QXmlStreamReader streamReader;
    streamReader.setDevice(&settingsCustomDTIsFile);

    if (streamReader.readNextStartElement())
    {
        if (!((streamReader.name() == "ssmss") && (streamReader.attributes().value("version") == "1.0")))
            return false;

        // мы здесь -> заголовок прочтен верно
        while (streamReader.readNextStartElement())
        {
            if ((streamReader.name() == "customDTI") && (streamReader.attributes().hasAttribute("name")) &&
                    (streamReader.attributes().hasAttribute("onImagePath")) &&
                    (streamReader.attributes().hasAttribute("offImagePath")))
            {
                obtained.append(constructDTIFromReadedInfo(
                                    streamReader.attributes().value("name").toString(),
                                    streamReader.attributes().value("onImagePath").toString(),
                                    streamReader.attributes().value("offImagePath").toString()));
            }
            else
                return false;
        }
    }
    else
        return false; // уж заголовок с версией-то мы должны были прочитать!

    return !streamReader.error();
}
////////////////////////////////////////
bool XMLOperator::saveCustomDeviceTypeInterfaces(QList<ServiceStructures::DeviceTypeInterface> &customDTIs)
{
    QDir curDir(QString(QDir::currentPath() + "/" + GlobalConstants::settingsDirName));
    if (!curDir.exists())
    {
        if (!QDir::current().mkdir(GlobalConstants::settingsDirName))
            return false;
    }

    QFile settingsCustomDTIsFile(QString("./" + GlobalConstants::settingsDirName +
                                         "/" + GlobalConstants::customDeviceTypeInterfacesFileName));
    if (!settingsCustomDTIsFile.open(QFile::WriteOnly | QFile::Text))
        return false;

    QXmlStreamWriter streamWriter;
    streamWriter.setAutoFormatting(true);
    streamWriter.setDevice(&settingsCustomDTIsFile);

    streamWriter.writeStartDocument();
    streamWriter.writeDTD("<!DOCTYPE ssmss>");
    streamWriter.writeStartElement("ssmss");
    streamWriter.writeAttribute("version", "1.0");

    for (int i = 0; i < customDTIs.size(); i++)
    {
        streamWriter.writeStartElement("customDTI");
        {
            streamWriter.writeAttribute("name", customDTIs.at(i).name);
            streamWriter.writeAttribute("onImagePath", customDTIs.at(i).onlineImagePath);
            streamWriter.writeAttribute("offImagePath", customDTIs.at(i).offlineImagePath);
        }
        streamWriter.writeEndElement();
    }

    streamWriter.writeEndDocument();
    settingsCustomDTIsFile.close();

    return !streamWriter.hasError();
}
/////////////////////////////////////////
bool XMLOperator::saveTabWidget(QString type, const QString &fileName, const QRectF &sceneRect, QList<NetDevice *> &deviceList, QList<NetDeviceLink *> &deviceLinkList)
{
    QString fileNameTested = fileName;

    if (!fileName.endsWith(".ssmm"))
        fileNameTested += ".ssmm";

    QFile targetFile(fileNameTested);

    if (!targetFile.open(QFile::WriteOnly | QFile::Text))
        return false;

    QXmlStreamWriter streamWriter;
    streamWriter.setAutoFormatting(true);
    streamWriter.setDevice(&targetFile);

    streamWriter.writeStartDocument();
    streamWriter.writeDTD("<!DOCTYPE ssmm>");
    streamWriter.writeStartElement("ssmm");
    streamWriter.writeAttribute("version", "1.0");

    streamWriter.writeStartElement("SceneRect");
    {
        streamWriter.writeAttribute("width", QString::number(sceneRect.width()));
        streamWriter.writeAttribute("height", QString::number(sceneRect.height()));
    }
    streamWriter.writeEndElement();

    // ========================================================================
    streamWriter.writeStartElement("Ping");
    streamWriter.writeAttribute("type", type);
    streamWriter.writeEndElement();
    // ========================================================================

    streamWriter.writeStartElement("Devices");
    foreach(NetDevice *netDevice, deviceList)
    {
        writeNetDeviceData(&streamWriter, netDevice);
    }
    streamWriter.writeEndElement();

    streamWriter.writeStartElement("Links");
    foreach(NetDeviceLink *deviceLink, deviceLinkList)
    {
        writeNetDeviceLinkData(&streamWriter, deviceLink);
    }
    streamWriter.writeEndElement();

    streamWriter.writeEndDocument();
    targetFile.close();
    return !streamWriter.hasError();
}
////////////////////////////////////////
TabWidget *XMLOperator::getTabWidget(QString* type, const QString &fileName, const QList<ServiceStructures::DeviceTypeInterface> *usersDTI)
{
    QFile readedFile(fileName);
    TabWidget *returner;

    if (!readedFile.open(QFile::ReadOnly | QFile::Text))
        return 0;

    QXmlStreamReader streamReader;
    streamReader.setDevice(&readedFile);

    if (streamReader.readNextStartElement())
    {
        if (!((streamReader.name() == "ssmm") && (streamReader.attributes().value("version") == "1.0")))
            return 0;

        // мы здесь -> заголовок прочтен верно
        if (streamReader.readNextStartElement())
        {
            if ((streamReader.name() == "SceneRect") && (streamReader.attributes().hasAttribute("width")) &&
                    (streamReader.attributes().hasAttribute("height")))
            {
                bool ok1, ok2;
                qreal width, height;

                width = streamReader.attributes().value("width").toDouble(&ok1);
                height = streamReader.attributes().value("height").toDouble(&ok2);

                if (ok1 && ok2)
                    returner = new TabWidget(usersDTI, QRectF(0, 0, width, height));
                else
                    return 0;
            }
            else
                return 0;
        }
        else
            return 0; // SceneRect быть обязан

        returner->setFileName(fileName);

        if (!readXMLEndElements(&streamReader))
        {
            delete returner;
            return 0;
        }

        // ====================================================================
        if (streamReader.readNextStartElement())
        {
            if (streamReader.name() == "Ping")
            {
                *type = streamReader.attributes().value("type").toString();
            }
            else
                return 0;
        }
        else
            return 0;

        returner->setFileName(fileName);

        if (!readXMLEndElements(&streamReader))
        {
            delete returner;
            return 0;
        }
        // ====================================================================

        // а теперь необходимо пройтись по элементам карты
        while (streamReader.readNextStartElement())
        {
            if (streamReader.name() == "Devices")
            {
                if (!getNetDeviceData(&streamReader, usersDTI, returner))
                {
                    delete returner;
                    return 0;
                }
            }
            else if (streamReader.name() == "Links")
            {
                if (!getNetDeviceLinkData(&streamReader, returner))
                {
                    delete returner;
                    return 0;
                }
            }
            else
            {
                // и что мы, блджд, прочитали?
                delete returner;
                return 0;
            }
        }
    }
    else
        return 0; // уж заголовок с версией-то мы должны были прочитать!

    if (streamReader.error())
    {
        delete returner;
        return 0;
    }

    return returner;
}
////////////////////////////////////////
ServiceStructures::DeviceTypeInterface XMLOperator::constructDTIFromReadedInfo(const QString &name, const QString &onImagePath, const QString &offImagePath)
{
    ServiceStructures::DeviceTypeInterface returner;
    returner.name = name;
    returner.offlineImagePath = offImagePath;
    returner.onlineImagePath = onImagePath;

    QImage onImage(onImagePath, "PNG");
    if (onImage.isNull())
    {
        // не удалось загрузить пикчу
        onImage = QImage("://Resources/defaultDeviceTypeInterfaceImages/unknownDeviceOn.png");
    }
    returner.onlineImage = onImage;

    QImage offImage(offImagePath, "PNG");
    if (offImage.isNull())
    {
        // не удалось загрузить пикчу
        offImage = QImage("://Resources/defaultDeviceTypeInterfaceImages/unknownDeviceOn.png");
    }
    returner.offlineImage = offImage;

    return returner;
}
////////////////////////////////////////
void XMLOperator::writeNetDeviceData(QXmlStreamWriter *streamWriter, NetDevice *deviceToWrite)
{
    streamWriter->writeStartElement("NetDevice");
    {
        streamWriter->writeAttribute("multiLevel", (deviceToWrite->isMultiLevel() ? "true" : "false"));
        streamWriter->writeAttribute("shouldUsePing", (deviceToWrite->network_isBeingPinged() ? "true" : "false"));
        streamWriter->writeAttribute("name", deviceToWrite->getName());
        streamWriter->writeAttribute("aboutDevice", deviceToWrite->getAboutDeviceText());
        streamWriter->writeAttribute("UUID", deviceToWrite->getUUID());
        streamWriter->writeAttribute("multiMapName", deviceToWrite->getMultiMapName());
        streamWriter->writeAttribute("networkAddress", deviceToWrite->getNetworkAddressString());

        streamWriter->writeStartElement("DTI");
        {
            streamWriter->writeAttribute("isCustom",
                        (PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.contains(*(deviceToWrite->getDTI())) ? "false" : "true"));
            streamWriter->writeAttribute("name", deviceToWrite->getDTI()->name);
            streamWriter->writeAttribute("onImagePath", deviceToWrite->getDTI()->onlineImagePath);
            streamWriter->writeAttribute("offImagePath", deviceToWrite->getDTI()->offlineImagePath);
        }
        streamWriter->writeEndElement();

        streamWriter->writeStartElement("Graphics");
        {
            streamWriter->writeAttribute("x",
                                   QString::number(deviceToWrite->getGraphicalItem()->scenePos().x()));
            streamWriter->writeAttribute("y",
                                   QString::number(deviceToWrite->getGraphicalItem()->scenePos().y()));
        }
        streamWriter->writeEndElement();
    }
    streamWriter->writeEndElement(); // NetDevice
}
////////////////////////////////////////
bool XMLOperator::getNetDeviceData(QXmlStreamReader *streamReader, const QList<ServiceStructures::DeviceTypeInterface> *usersDTI, TabWidget *tabWidget)
{
    while (streamReader->readNextStartElement())
    {
        if ((streamReader->name() == "NetDevice") && (streamReader->attributes().hasAttribute("multiLevel")) &&
                (streamReader->attributes().hasAttribute("shouldUsePing")) && (streamReader->attributes().hasAttribute("name")) &&
                (streamReader->attributes().hasAttribute("aboutDevice")) && (streamReader->attributes().hasAttribute("UUID")) &&
                (streamReader->attributes().hasAttribute("multiMapName")) && (streamReader->attributes().hasAttribute("networkAddress")))
        {
            bool ok;

            NetDevice *netDevice = new NetDevice(streamReader->attributes().value("name").toString());

            if (readedAttributeToBool(streamReader->attributes().value("multiLevel"), ok))
            {
                if (!ok)
                {
                    delete netDevice;
                    return false;
                }

                netDevice->setMultiMapName(streamReader->attributes().value("multiMapName").toString());
            }

            netDevice->setAboutDeviceText(streamReader->attributes().value("aboutDevice").toString());
            netDevice->setUUID(streamReader->attributes().value("UUID").toString());
            if (!netDevice->network_setNetworkAddress(streamReader->attributes().value("networkAddress").toString()))
            {
                delete netDevice;
                return false;
            }

            if (readedAttributeToBool(streamReader->attributes().value("shouldUsePing"), ok))
            {
                if (!ok)
                {
                    delete netDevice;
                    return false;
                }

                netDevice->network_setPinging(true);
            }


            if (!readNetDeviceData_DTI(streamReader, netDevice, usersDTI))
                return false;

            tabWidget->obtainNewDevice(netDevice);

            if (!readNetDeviceData_Pos(streamReader, netDevice, tabWidget))
                return false;

            if (!readXMLEndElements(streamReader))
                return false;
        }
        else
            return false;
    }

    return true;
}
////////////////////////////////////////
void XMLOperator::writeNetDeviceLinkData(QXmlStreamWriter *streamWriter, NetDeviceLink *linkToWrite)
{
    streamWriter->writeStartElement("NetDeviceLink");
    {
        streamWriter->writeStartElement("Devices");
        {
            streamWriter->writeStartElement("FirstDevice");
            {
                streamWriter->writeAttribute("UUID", linkToWrite->getFirstDevice()->getUUID());
            }
            streamWriter->writeEndElement();

            streamWriter->writeStartElement("SecondDevice");
            {
                streamWriter->writeAttribute("UUID", linkToWrite->getSecondDevice()->getUUID());
            }
            streamWriter->writeEndElement();
        }
        streamWriter->writeEndElement();

        streamWriter->writeStartElement("Points");
        {
            streamWriter->writeStartElement("FirstPoint");
            {
                streamWriter->writeAttribute("x",
                                    QString::number(linkToWrite->getFirstPointPos().x()));
                streamWriter->writeAttribute("y",
                                    QString::number(linkToWrite->getFirstPointPos().y()));
            }
            streamWriter->writeEndElement();

            streamWriter->writeStartElement("SecondPoint");
            {
                streamWriter->writeAttribute("x",
                                    QString::number(linkToWrite->getSecondPointPos().x()));
                streamWriter->writeAttribute("y",
                                    QString::number(linkToWrite->getSecondPointPos().y()));
            }
            streamWriter->writeEndElement();
        }
        streamWriter->writeEndElement();
    }
    streamWriter->writeEndElement();
}
////////////////////////////////////////
bool XMLOperator::readedAttributeToBool(const QStringRef &value, bool &ok)
{
    QString testStr = value.toString();
    ok = false;

    if (testStr == "true")
    {
        ok = true;
        return true;
    }
    else
        if (testStr == "false")
        {
            ok = true;
            return false;
        }

    return false;
}
////////////////////////////////////////
bool XMLOperator::readNetDeviceData_DTI(QXmlStreamReader *streamReader, NetDevice *device, const QList<ServiceStructures::DeviceTypeInterface> *usersDTI)
{
    if (streamReader->readNextStartElement())
    {
        if ((streamReader->name() == "DTI") && (streamReader->attributes().hasAttribute("isCustom")) &&
                (streamReader->attributes().hasAttribute("name")) && (streamReader->attributes().hasAttribute("onImagePath")) &&
                (streamReader->attributes().hasAttribute("offImagePath")))
        {
            bool ok, isCustom;

            isCustom = readedAttributeToBool(streamReader->attributes().value("isCustom"), ok);
            if (ok)
            {
                ServiceStructures::DeviceTypeInterface dti;

                if (isCustom)
                {
                    dti = ServiceStructures::findInList(usersDTI, streamReader->attributes().value("name").toString(),
                                                        streamReader->attributes().value("onImagePath").toString(),
                                                        streamReader->attributes().value("offImagePath").toString());
                    if (dti.name.isEmpty())
                    {
                        delete device;
                        return false;
                    }

                    device->setDeviceTypeInterface(&(usersDTI->at(usersDTI->indexOf(constructDTIFromReadedInfo(
                                                                                     dti.name, dti.onlineImagePath, dti.offlineImagePath)))));
                }
                else
                {
                    //QList<ServiceStructures::DeviceTypeInterface> preDefinedList = PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList;
                    dti = ServiceStructures::findInList(&PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList,
                                                        streamReader->attributes().value("name").toString());

                    if (dti.name.isEmpty())
                    {
                        delete device;
                        return false;
                    }

                    device->setDeviceTypeInterface(&(PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.at(
                                                       PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.indexOf(dti))));
                }

                if (!readXMLEndElements(streamReader))
                {
                    delete device;
                    return false;
                }

                return true;
            }
        }
    }

    delete device;
    return false;
}
////////////////////////////////////////
bool XMLOperator::readNetDeviceData_Pos(QXmlStreamReader *streamReader, NetDevice *device, TabWidget *tabWidget)
{
    if (streamReader->readNextStartElement())
    {
        if ((streamReader->name() == "Graphics") && (streamReader->attributes().hasAttribute("x")) &&
                (streamReader->attributes().hasAttribute("y")))
        {
            bool ok1, ok2;
            qreal x = streamReader->attributes().value("x").toDouble(&ok1);
            qreal y = streamReader->attributes().value("y").toDouble(&ok2);

            if (ok1 && ok2)
            {
                tabWidget->setDevicePos(device, x, y);

                if (!readXMLEndElements(streamReader))
                {
                    return false;
                }

                return true;
            }
        }
    }

    return false;
}
////////////////////////////////////////
bool XMLOperator::getNetDeviceLinkData(QXmlStreamReader *streamReader, TabWidget *tabWidget)
{
    while(streamReader->readNextStartElement())
    {
        if (streamReader->name() == "NetDeviceLink")
        {
            if (!streamReader->readNextStartElement())
                return false;

            if (!(streamReader->name() == "Devices"))
                return false;

            if (!streamReader->readNextStartElement())
                return false;

            NetDeviceLink *netDeviceLink;

            if ((streamReader->name() == "FirstDevice") && (streamReader->attributes().hasAttribute("UUID")))
            {
                bool ok;

                NetDevice *device1 = tabWidget->getDeviceByUUID(streamReader->attributes().value("UUID").toString(), ok);
                if (!ok)
                    return false;

                if (!readXMLEndElements(streamReader))
                    return false;

                if (!streamReader->readNextStartElement())
                    return false;

                if ((streamReader->name() == "SecondDevice") && (streamReader->attributes().hasAttribute("UUID")))
                {
                    NetDevice *device2 = tabWidget->getDeviceByUUID(streamReader->attributes().value("UUID").toString(), ok);
                    if (!ok)
                        return false;

                    if (!readXMLEndElements(streamReader))
                        return false;

                    netDeviceLink = new NetDeviceLink(device1, device2);
                }
                else
                    return false;
            }
            else
                return false;

            if (!readXMLEndElements(streamReader))
            {
                delete netDeviceLink;
                return false;
            }

            if (!streamReader->readNextStartElement())
            {
                delete netDeviceLink;
                return false;
            }

            if (!(streamReader->name() == "Points"))
            {
                delete netDeviceLink;
                return false;
            }

            if (!streamReader->readNextStartElement())
            {
                delete netDeviceLink;
                return false;
            }

            if ((streamReader->name() == "FirstPoint") && (streamReader->attributes().hasAttribute("x")) &&
                    (streamReader->attributes().hasAttribute("y")))
            {
                bool ok1, ok2;
                qreal x = streamReader->attributes().value("x").toDouble(&ok1);
                qreal y = streamReader->attributes().value("y").toDouble(&ok2);

                if (ok1 && ok2)
                {
                    netDeviceLink->setFirstPointPosBeforePaint(x, y);
                }
                else
                {
                    delete netDeviceLink;
                    return false;
                }
            }
            else
            {
                delete netDeviceLink;
                return false;
            }

            if (!readXMLEndElements(streamReader))
            {
                delete netDeviceLink;
                return false;
            }

            if (!streamReader->readNextStartElement())
            {
                delete netDeviceLink;
                return false;
            }

            if ((streamReader->name() == "SecondPoint") && (streamReader->attributes().hasAttribute("x")) &&
                    (streamReader->attributes().hasAttribute("y")))
            {
                bool ok1, ok2;
                qreal x = streamReader->attributes().value("x").toDouble(&ok1);
                qreal y = streamReader->attributes().value("y").toDouble(&ok2);

                if (ok1 && ok2)
                {
                    netDeviceLink->setSecondPointPosBeforePaint(x, y);
                }
                else
                {
                    delete netDeviceLink;
                    return false;
                }
            }
            else
            {
                delete netDeviceLink;
                return false;
            }

            if (!readXMLEndElements(streamReader))
            {
                delete netDeviceLink;
                return false;
            }

            tabWidget->obtainNewDeviceLink(netDeviceLink);
        }
        else
            return false;

        if (!readXMLEndElements(streamReader, 2))
            return false;
    }

    return true;
}
////////////////////////////////////////
bool XMLOperator::readXMLEndElements(QXmlStreamReader *streamReader, const unsigned int &repeatCount, const bool &skipWhitescpaces)
{
    unsigned int i = repeatCount;

    while(i)
    {
        QXmlStreamReader::TokenType tokenType = streamReader->readNext();
        if (tokenType != QXmlStreamReader::TokenType::EndElement)
        {
            if ((streamReader->isWhitespace()) && skipWhitescpaces)
            {
                // прочитали пустые пробелы, созданные aoutoformat'тингом. просто опустим
                continue;
            }

            return false;
        }

        i--;
    }

    return true;
}



































