#ifndef VERSIONINFO_H
#define VERSIONINFO_H


#include "ServiceStructures.h"


//-------------------- version ------------------------
const ServiceStructures::VersionInfo currentVersion = {3, 0, 0, 140306};
//-----------------------------------------------------



#endif // VERSIONINFO_H
