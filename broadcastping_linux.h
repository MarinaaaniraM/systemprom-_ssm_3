#ifndef BROADCASTPING_LINUX_H
#define BROADCASTPING_LINUX_H

#include <QObject>
#include <QDebug>
#include <QString>
#include <QVector>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <unistd.h>
#include <linux/types.h>
#include <arpa/inet.h>
#include <memory.h>
#include <fcntl.h>
#include <sys/time.h>

class BroadcastPing_Linux : public QObject
{
    Q_OBJECT
public:
    explicit BroadcastPing_Linux(QObject *parent = 0);
    ~BroadcastPing_Linux();

    void setBroadcastAddress(QString addr)  // Широковещательный адрес
    {
        broadcastAddress.clear();
        broadcastAddress.append(addr);
    }
    QVector<QString>* getEnableAddresses()
    {
        return pingedAddresses;
    }
    int startingPing();
private:
    QString broadcastAddress;
    QVector<QString>* pingedAddresses;      // Запсываем адреса, ответившие на пинг

    unsigned short in_cksum(unsigned short *addr, int len);
    void icmpAnalysis(char* buff, int size);

};

#endif // BROADCASTPING_LINUX_H
