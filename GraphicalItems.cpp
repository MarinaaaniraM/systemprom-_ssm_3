#include <QPainter>
#include <QImage>
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include "GraphicalItems.h"
#include "NetLogicalDevices.h"






NetDeviceGraphicalItem::NetDeviceGraphicalItem(NetDevice *device, QImage offlineImage, QImage onlineImage, QGraphicsItem *parent) :
    QObject(), QGraphicsPixmapItem(parent), netDevice(device), originalOffImage(offlineImage), originalOnImage(onlineImage)
{
    if (netDevice->isMultiLevel())
    {
        multiMap = true;
        addMultiMapIconOnImage(offlineImage);
        addMultiMapIconOnImage(onlineImage);
    }

    offImage = QPixmap::fromImage(offlineImage);
    onImage = QPixmap::fromImage(onlineImage);

    setDeviceOffline(); // по умолчанию мы выключены

    setFlags(flags() | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemSendsGeometryChanges);
    // добавили флаги, чтобы итем можно было двигать мышью

    createActions();

    setupTextItem();
}
///////////////////////////////////////////////
void NetDeviceGraphicalItem::setMultiMap(const bool &multimap)
{
    multiMap = ((netDevice->isMultiLevel()) && multimap);

    if (multiMap)
    {
        QImage offlineImage = originalOffImage;
        QImage onlineImage = originalOnImage;

        addMultiMapIconOnImage(offlineImage);
        addMultiMapIconOnImage(onlineImage);

        offImage = QPixmap::fromImage(offlineImage);
        onImage = QPixmap::fromImage(onlineImage);
    }
    else
    {
        offImage = QPixmap::fromImage(originalOffImage);
        onImage = QPixmap::fromImage(originalOnImage);
    }

    setDeviceOffline(); // заново пропингуется ежели оно было в онлайне
}
//////////////////////////////////////////////
void NetDeviceGraphicalItem::setImages(const QImage &offlineImage, const QImage &onlineImage)
{
    originalOffImage = offlineImage;
    originalOnImage = onlineImage;

    setMultiMap(multiMap);

    if (deviceIsOnline)
        setPixmap(onImage);
    else
        setPixmap(offImage);
}
//////////////////////////////////////////////
QImage NetDeviceGraphicalItem::getOriginalOffImage() const
{
    return originalOffImage;
}
///////////////////////////////////////////////
QImage NetDeviceGraphicalItem::getOriginalOnImage() const
{
    return originalOnImage;
}
///////////////////////////////////////////////
QPixmap NetDeviceGraphicalItem::getOffImagePixmap() const
{
    return offImage;
}
///////////////////////////////////////////////
QPixmap NetDeviceGraphicalItem::getOnImagePixmap() const
{
    return onImage;
}
//////////////////////////////////////////////
QGraphicsSimpleTextItem *NetDeviceGraphicalItem::getTextItem()
{
    return textItem;
}
//////////////////////////////////////////////
const QGraphicsSimpleTextItem *NetDeviceGraphicalItem::getTextItem() const
{
    return textItem;
}
//////////////////////////////////////////////
void NetDeviceGraphicalItem::setTextItemText(const QString &deviceName, const QString &deviceAddress)
{
    textItem->setText(deviceName + "\n" + deviceAddress);
}
//////////////////////////////////////////////
void NetDeviceGraphicalItem::addMultiMapIconOnImage(QImage &image)
{
    QPainter painter;
    QImage multiMapIcon("://Resources/graphicalItems/multiLevel.png");

    painter.begin(&image);
    painter.drawImage((image.width() - multiMapIcon.width()), (image.height() - multiMapIcon.height()), multiMapIcon);
    painter.end();
}
//////////////////////////////////////////////
QVariant NetDeviceGraphicalItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if ((change == QGraphicsItem::ItemSelectedChange) && (value == true))
    {
        // нас выбрали (выделили) на рисунке
        emit itemIsSelected();
    }
    if (change == QGraphicsItem::ItemPositionChange)
    {
        // нас передвигают
        QPointF pointToEmit = this->scenePos();
        textItem->setPos(pointToEmit.x(), (pointToEmit.y() + this->boundingRect().height()));

        // для удобства отправляем не верхний левый угол, а центр нашей иконки
        pointToEmit.setX(pointToEmit.x() + this->boundingRect().width() / 2);
        pointToEmit.setY(pointToEmit.y() + this->boundingRect().height() / 2);
        emit positionChanged(pointToEmit);
    }

    return QGraphicsPixmapItem::itemChange(change, value);
}
//////////////////////////////////////////////
void NetDeviceGraphicalItem::createActions()
{
    editAct = new QAction(tr("Изменить"), this);
    editAct->setStatusTip(tr("Изменение выбрранного устройства"));
    editAct->setWhatsThis(tr("Изменение характеристик выбранного устройства"));
    connect(editAct, &QAction::triggered, this, &NetDeviceGraphicalItem::editAct_slot);

    connectToOther = new QAction(tr("Связать с устройством"), this);
    connectToOther->setStatusTip(tr("Соединить связью с выбранным устройством"));
    connectToOther->setWhatsThis(tr("Добавление связи этого устройства и другого устройства"));
    connect(connectToOther, &QAction::triggered, this, &NetDeviceGraphicalItem::connectToOther_slot);

    deleteDeviceAct = new QAction(tr("Удалить"), this);
    deleteDeviceAct->setStatusTip(tr("Удаление выбранного устройства"));
    deleteDeviceAct->setWhatsThis(tr("Удаление выбранного устройства и его связей с карты"));
    connect(deleteDeviceAct, &QAction::triggered, this, &NetDeviceGraphicalItem::deleteDeviceAct_slot);

    // ------------------------------------------------------------------------
    openWeb = new QAction(tr("Открыть веб-интерфейс"), this);
    openWeb->setStatusTip(tr("Открыть в браузере веб-интерфейс утсройства"));
    openWeb->setWhatsThis(tr("Открыть в браузере веб-интерфейс данного утсройства"));
    connect(openWeb, &QAction::triggered, this, &NetDeviceGraphicalItem::openWeb_slot);
    // ------------------------------------------------------------------------
}
//////////////////////////////////////////////
void NetDeviceGraphicalItem::setupTextItem()
{
    textItem = new QGraphicsSimpleTextItem;
    textItem->setFlags(textItem->flags() | QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemSendsGeometryChanges);
    textItem->setBrush(QBrush(Qt::black));

    QFont textItemFont = textItem->font();
    textItemFont.setUnderline(true);
    textItem->setFont(textItemFont);

    setTextItemText(netDevice->getName(), netDevice->getNetworkAddressStringFormatted());
}
//////////////////////////////////////////////
bool NetDeviceGraphicalItem::operator ==(const NetDeviceGraphicalItem &other) const
{
    return ((originalOffImage == other.getOriginalOffImage()) && (originalOnImage == other.getOriginalOnImage()) &&
            (offImage.toImage() == other.getOffImagePixmap().toImage()) && (onImage.toImage() == other.getOnImagePixmap().toImage()));
}
/////////////////////////////////////////////
void NetDeviceGraphicalItem::setDeviceOnline()
{
    if (deviceIsOnline)
        return; // уже все стоит

    deviceIsOnline = true;
    setPixmap(onImage);
    emit deviceOnlineStatusChanged(true);
}
/////////////////////////////////////////////
void NetDeviceGraphicalItem::setDeviceOffline()
{
    if (!deviceIsOnline)
        return; // уже все стоит

    deviceIsOnline = false;
    setPixmap(offImage);
    emit deviceOnlineStatusChanged(false);
}
/////////////////////////////////////////////
void NetDeviceGraphicalItem::setDeviceStatus(const bool &alive)
{
    if (!netDevice->network_isBeingPinged())
        return; // игнорируем сигнал, т.к. мы не пингуемся

    if (alive)
        setDeviceOnline();
    else
        setDeviceOffline();
}
/////////////////////////////////////////////
void NetDeviceGraphicalItem::editAct_slot()
{
    emit userRequestToEditDevice();
}
/////////////////////////////////////////////
void NetDeviceGraphicalItem::connectToOther_slot()
{
    setSelected(true); // необходимо, иначе почему-то может самоселектить и коннектиться сам на себя, лол
    emit userRequestToConnectDevices();
}
/////////////////////////////////////////////
void NetDeviceGraphicalItem::deleteDeviceAct_slot()
{
    emit userRequestToDeleteDevice();
}


// ------------------------------------------------------------------------
void NetDeviceGraphicalItem::openWeb_slot()
{
    emit getWebInterfaceOfdevice();
}
// ------------------------------------------------------------------------


//////////////////////////////////////////////
void NetDeviceGraphicalItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;

    menu.addAction(editAct);
    menu.addAction(connectToOther);
    menu.addAction(openWeb);
    menu.addSeparator();
    menu.addAction(deleteDeviceAct);

    menu.exec(event->screenPos());

    event->accept();
}
//////////////////////////////////////////////
void NetDeviceGraphicalItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        emit userDoubleClickedItem();
    }

    QGraphicsPixmapItem::mouseDoubleClickEvent(event);
}
/////////////////////////////////////////////
NetDeviceGraphicalItem::~NetDeviceGraphicalItem()
{
    delete textItem;
}



/* -----------------------------------------------------------
 * -----------------------------------------------------------
 * ----------------------------------------------------------- */



NetDevicePointItem::NetDevicePointItem(NetDeviceLink *deviceLink) :
    QObject(), QGraphicsPixmapItem(), netDeviceLink(deviceLink)
{
    setPixmap(QPixmap("://Resources/graphicalItems/RectItem.png"));

    setFlags(flags() | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemSendsGeometryChanges);

    createActions();
}
///////////////////////////////////////////
void NetDevicePointItem::setPos(const QPointF &pos)
{
    QGraphicsPixmapItem::setPos(QPointF((pos.x() - (this->pixmap().width() / 2)), (pos.y() - (this->pixmap().height() / 2))));
}
///////////////////////////////////////////
void NetDevicePointItem::setPos(qreal x, qreal y)
{
    QGraphicsPixmapItem::setPos(x, y);
}
///////////////////////////////////////////
void NetDevicePointItem::createActions()
{
    deletePointAct = new QAction(tr("Удалить"), this);
    deletePointAct->setStatusTip(tr("Удаление выбранной связи"));
    deletePointAct->setWhatsThis(tr("Удаление выбранной связи с карты"));
    connect(deletePointAct, &QAction::triggered, this, &NetDevicePointItem::deletePointAct_slot);
}
///////////////////////////////////////////
QVariant NetDevicePointItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    static bool firstRun = true; // чтобы не эмитить сигнал при самом-самом первом перемещении (еще до отрисовки)

    if (change == QGraphicsItem::ItemPositionChange)
    {
        // нас передвигают
        if (!firstRun)
        {
            QPointF scenePosition = this->scenePos(); // эмитим позицию не левого верхнего угла, а центра
            scenePosition.setX(scenePosition.x() + this->pixmap().width() / 2);
            scenePosition.setY(scenePosition.y() + this->pixmap().height() / 2);
            emit positionChanged(scenePosition);
        }
        else
            firstRun = false;
    }

    return QGraphicsPixmapItem::itemChange(change, value);
}
///////////////////////////////////////////
void NetDevicePointItem::deletePointAct_slot()
{
    emit userRequestToDeleteLink();
}
///////////////////////////////////////////
void NetDevicePointItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;

    menu.addAction(deletePointAct);

    menu.exec(event->screenPos());

    event->accept();
}



/* -----------------------------------------------------------
 * -----------------------------------------------------------
 * ----------------------------------------------------------- */



NetDeviceLineItem::NetDeviceLineItem(QPointF &firstPoint, QPointF &secondPoint, NetDeviceLink *deviceLink) :
    QObject(), QGraphicsLineItem(),
    netDeviceLink(deviceLink), pointOne(firstPoint), pointTwo(secondPoint)
{
    setLine(QLineF(pointOne, pointTwo));

    setPen(QPen(Qt::blue, PenWidth));

    setFlags(flags() | QGraphicsItem::ItemSendsGeometryChanges);
}
/////////////////////////////////////////
void NetDeviceLineItem::firstPointChanged(const QPointF &newPos)
{
    pointOne = newPos;

    setLine(QLineF(pointOne, pointTwo));
}
////////////////////////////////////////
void NetDeviceLineItem::secondPointChanged(const QPointF &newPos)
{
    pointTwo = newPos;

    setLine(QLineF(pointOne, pointTwo));
}



























