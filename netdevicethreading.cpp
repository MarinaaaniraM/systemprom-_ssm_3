#include "netdevicethreading.h"



NetDeviceThreading::NetDeviceThreading(QObject *parent) :
    QObject(parent), QRunnable()
{
}
//////////////////////////////////////
void NetDeviceThreading::run()
{
    //boost::asio::io_service io_service;

    pinger = new Pinger(io_service, timeToRePing, timeToWaitForPingReplyModifyer, this);
    connect(this, &NetDeviceThreading::timeToRePingChanged, pinger, &Pinger::setTime_rePing);
    connect(this, &NetDeviceThreading::timeToWaitForPingReplyModifyerChanged, pinger, &Pinger::setTime_waitForReplyModifyer);
    connect(pinger, &Pinger::ping_successfully, this, &NetDeviceThreading::deviceIsAlive_slot);

    pinger->startWorking();

    io_service.run();
}
/////////////////////////////////////
void NetDeviceThreading::setTimeToRePing(const int &value)
{
    timeToRePing = value;

    emit timeToRePingChanged(timeToRePing);
}
/////////////////////////////////////
void NetDeviceThreading::setTimeToWaitForPingReplyModifyer(const int &value)
{
    timeToWaitForPingReplyModifyer = value;

    emit timeToWaitForPingReplyModifyerChanged(timeToWaitForPingReplyModifyer);
}
/////////////////////////////////////
void NetDeviceThreading::addPingableDevice_slot(const QString &uuid, const QString &ipAddr)
{
    pinger->addPingIP(uuid, ipAddr);
}
/////////////////////////////////////
void NetDeviceThreading::removePingableDevice_slot(const QString &uuid)
{
    pinger->removePingIP(uuid);
}
/////////////////////////////////////
void NetDeviceThreading::deviceIsAlive_slot(const bool &alive, const CustomQStrList &uuids)
{
    if (uuids.isEmpty())
        return;

    emit deviceIsAlive(alive, uuids);
}
/////////////////////////////////////
NetDeviceThreading::~NetDeviceThreading()
{
    io_service.stop();

    deleteLater();
}








































