
#include "CBaseMessageList.h"

CBaseMessageList::CBaseMessageList()
{
	m_MaxCount = MAX_LIST_COUNT;
}


CBaseMessageList::~CBaseMessageList()
{
}

void CBaseMessageList::SetMaxCount( int NewCount)
{
	if(NewCount > MAX_LIST_COUNT)
	    m_MaxCount = MAX_LIST_COUNT;
	else
		m_MaxCount = NewCount;

}

// poluchit' chislo elementov v spiske
int CBaseMessageList::GetCount(ULONG* count)
{
	if(count == NULL)
		return NULL_POINTER;

	*count = 0;

	return TYPE_OK;
}

// udalit' soobshenie iz spiska
int CBaseMessageList::FreeMessage( BYTE* Msg )
{
	if(Msg == NULL)
		return NULL_POINTER;

	delete[] Msg;

	return TYPE_OK;
}


// dobavit' soobshenie
int CBaseMessageList::AddMessage( BYTE*  msg, BOOL Flag)
{

	if(msg == NULL)
		return NULL_POINTER;
	else
		return TYPE_OK;
}


// zabrat' soobshenie
int CBaseMessageList::GetMessage( BYTE** msg)
{
	int          RetCode = TYPE_OK;
	ULONG        Size = 0;

	if(msg == NULL)
		return NULL_POINTER;
	else
	{
		*msg = NULL;
		return TYPE_OK;
	}
}


BYTE* CBaseMessageList::GetFreeBuffer( ULONG  Size )
{
	return  new BYTE[Size];
}
