#include "CAbonentList.h"


CAbonentList::CAbonentList()
{
	m_list = NULL;
	m_critical = NULL;
}

CAbonentList::~CAbonentList()
{
	DeleteList();
	
	if(m_list != NULL)
	{
		if(m_critical != NULL)
		    m_critical->EnterCritical();

		delete m_list;
		m_list = NULL;	

		if(m_critical != NULL)
		    m_critical->LeaveCritical();
	}

    if(m_critical != NULL)
	{
		delete m_critical;
		m_critical = NULL;
	}
}

// sozdat' spisok
int CAbonentList::Create()
{
	int  RetCode = TYPE_OK;

	if((m_list = new CList) == NULL)
		return EMPTY_MEMORY;

	if((m_critical = new CCriticalSection) == NULL)
	{
		delete m_list;
		m_list = NULL;
		return EMPTY_MEMORY;
	}

	if((RetCode = m_critical->Create()) != TYPE_OK)
	{
		delete m_list;
		m_list = NULL;
		delete m_critical;
		m_critical = NULL;
		return RetCode;
	}
	
	return TYPE_OK;
}

// poluchit' chislo elementov v spiske
int CAbonentList::GetCount(ULONG* count)
{
	if((m_critical == NULL) || (m_list == NULL) || (count == NULL))
		return NULL_POINTER;

	m_critical->EnterCritical();
	*count = m_list->GetCount();
	m_critical->LeaveCritical();

	return TYPE_OK;
}

// dobavit' abonenta
int CAbonentList::AddAbonent( Addr*  Abn )
{
	Addr*  abn = NULL;
	int    RetCode = TYPE_OK;

	if((m_critical == NULL) || (m_list == NULL) || (Abn == NULL))
		return NULL_POINTER;

    if((abn = new Addr)== NULL)
		return EMPTY_MEMORY;
  
	*abn = *Abn;
	m_critical->EnterCritical();
        RetCode = m_list->AddItem((void*) abn, sizeof(Addr*), TRUE);
	m_critical->LeaveCritical();

	return RetCode;

}

int CAbonentList::AddAbonent( ULONG  IP, ULONG  ID )
{
	Addr    Abn;

	Abn.Object  = IP;
	Abn.Abonent = ID;

	return AddAbonent( &Abn );
}


int CAbonentList::AddAbonent( char* IP, ULONG  ID )
{
	in_addr    adr;

	if(IP == NULL)
		 return NULL_POINTER;

	 if(strlen(IP) <= 15 )
		return ERROR_SIZE;
		 
	if((adr.s_addr = inet_addr(IP)) == -1)
		return ERROR_SIZE;

	return AddAbonent( adr.s_addr, ID );
}



// udalit' abonenta iz spiska
int CAbonentList::Delete( Addr*  Abn )
{
	int      RetCode = TYPE_OK;
	Addr*    abn = NULL;

	if(Abn == NULL)
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();
	if((abn = FindAbonent(Abn)) == NULL)
	{
	      m_critical->LeaveCritical();
		  return ERAB_ABN_NOFOUND;
	}
	RetCode = m_list->DeleteItem((void*)abn);
	m_critical->LeaveCritical();

	delete abn;
	abn = NULL;
  
	return RetCode;
}

// proverit' na nalichie v spiske
BOOL CAbonentList::IfAbonent( Addr*  Abn )
{
	int      RetCode = TYPE_OK;
	Addr*    abn = NULL;

	if(Abn == NULL)
		return FALSE;

	if((m_list == NULL) || ( m_critical == NULL ))
		return FALSE;

	m_critical->EnterCritical();
	    abn = FindAbonent(Abn);
	m_critical->LeaveCritical();

	if(abn == NULL)
		return FALSE;
	else
		return TRUE;
}

// udalit' spisok
void CAbonentList::DeleteList()
{
	ULONG		Count = 0;
	Addr*		abn = NULL;
	int			RetCode = TYPE_OK;
	ULONG       Size = 0;

	if((m_list == NULL) || ( m_critical == NULL ))
		return ;

		if((Count = m_list->GetCount()) == 0)
			return;

		if((RetCode = m_list->GetFirst((void**)&abn, &Size)) != TYPE_OK)
			return;

		if(abn != NULL)
		{
			delete abn;
			abn = NULL;
        }

		while((RetCode = m_list->GetNext((void**)&abn, &Size)) == TYPE_OK) 
		{
			if(abn != NULL)
			{
				delete abn;
				abn = NULL;
			}
		}
}

// naiti v spiske
Addr* CAbonentList::FindAbonent( Addr*  Abn )
{
	Addr*               abn = NULL;
	ULONG		    Size = 0;
	int                 RetCode = TYPE_OK;

	if(Abn == NULL)
		return NULL;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL;

	m_critical->EnterCritical();
	if((RetCode = m_list->GetFirst((void**)&abn, &Size)) != TYPE_OK)
	{
		m_critical->LeaveCritical();
		return NULL;
	}
	
	if(abn == NULL)
	{
		m_critical->LeaveCritical();
		return NULL;
	}
	else
	{
		if((abn->Abonent == Abn->Abonent) &&
			(abn->Object == Abn->Object ))
		{
			m_critical->LeaveCritical();
			return abn;
		}
	}

	while((RetCode = m_list->GetNext((void**)&abn, &Size)) == TYPE_OK) 
	{
		if(abn == NULL)
		{
			m_critical->LeaveCritical();
			return NULL;
		}
		else
		{
			if((abn->Abonent == Abn->Abonent) &&
				(abn->Object == Abn->Object ))
			{
				m_critical->LeaveCritical();
				return abn;
			}
		}
	}

	m_critical->LeaveCritical();
	return NULL;
}


// poluchit' perviy element
int CAbonentList::GetFirstAbonent(Addr*  Abn)
{
	Addr*               abn = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;

	if(Abn == NULL)
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();

	if((RetCode = m_list->GetFirst((void**)&abn, &Size)) != TYPE_OK)
	{
		m_critical->LeaveCritical();
		return ERAB_ABN_ZERO;
	}
	
	if(abn == NULL)
	{
		m_critical->LeaveCritical();
		return ERAB_ABN_ZERO;
	}
	else
	{
		m_critical->LeaveCritical();
		*Abn = *abn;
		return TYPE_OK;
	}
}

// poluchit' sleduyshii
int CAbonentList::GetNextAbonent( Addr*  Abn )
{
	Addr*               abn = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;


	if(Abn == NULL)
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();

	if((RetCode = m_list->GetNext((void**)&abn, &Size)) != TYPE_OK)
	{
		m_critical->LeaveCritical();
		return ERAB_ABN_ZERO;
	}
	
	if(abn == NULL)
	{
		m_critical->LeaveCritical();
		return ERAB_ABN_ZERO;
	}
	else
	{
		m_critical->LeaveCritical();
		*Abn = *abn;
		return TYPE_OK;
	}
}


// zamenit' abonenta. ishem DestAbn i perepisivaem na SrcAbn
int CAbonentList::Replace( Addr*  SrcAbn,  Addr*  DestAbn  )
{
	Addr*               abn = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;

	if((SrcAbn == NULL)||(DestAbn == NULL))
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();
	    abn = FindAbonent(DestAbn);

	    if(abn == NULL)
		{
	        m_critical->LeaveCritical();
		    return ERAB_ABN_NOFOUND;
		}

	    *abn = *SrcAbn;
	m_critical->LeaveCritical();

	return TYPE_OK;
}
