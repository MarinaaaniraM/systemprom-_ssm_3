#include "CNetworkTree.h"


CNetworkTree::CNetworkTree()
{
	m_List = NULL;
	m_Select = NULL;
	m_Flag = FALSE;
}

CNetworkTree::~CNetworkTree()
{
	Close();
}

// udalenie pered zakritiem
int CNetworkTree::Close()
{
	return TYPE_OK;
}


// sozdat' derevo
int CNetworkTree::Create()
{
	int RetCode = TYPE_OK;


    if((m_List = new CNetWorkList()) == NULL)
	   return EMPTY_MEMORY;

    if((RetCode = m_List->Create()) != TYPE_OK)
    {
	    return RetCode;
    }
  
	m_Select = NULL;

	return RetCode;
}


//-----------------------------------------------------------------------------------------
// NAVIGACIYA
//-----------------------------------------------------------------------------------------
// vibrat' uzel
int CNetworkTree::Select(NETWORK_INFO* Net)
{
	int RetCode = TYPE_OK;
	CNetwork*     Network = NULL;

	if(Net == NULL)
		return NULL_POINTER;

	if(m_List == NULL)
		return ERNET_ZERO;

    m_List->m_critical->EnterCritical();
    if((Network = m_List->FindNetwork( Net )) == NULL)
	{
		m_List->m_critical->LeaveCritical();
		return ERNET_NOFOUND;
	}

	m_List->m_critical->LeaveCritical();

	m_Select = Network;

	return TYPE_OK;
}




//zaprosit' svoye opisanie
int CNetworkTree::GetInfo(NETWORK_INFO* Net)
{
	int RetCode = TYPE_OK;
	CNetwork*     Network = NULL;

	if(Net == NULL)
		return NULL_POINTER;

	if(m_List == NULL)
		return ERNET_NOFOUND;

    m_List->m_critical->EnterCritical();
    if((Network = m_List->FindNetwork( Net )) == NULL)
	{
		m_List->m_critical->LeaveCritical();
		return ERNET_NOFOUND;
	}
	m_List->m_critical->LeaveCritical();

	return Network->GetInfo(Net);

}



//-----------------------------------------------------------------------------------------
// PROVERKI, POISK
//-----------------------------------------------------------------------------------------
// proverit' na nalichie v spiske
// CHILD_TREE - proverka tol'ko na nalichie v dochernem spiske
// ALL_TREE   - proverka na nalichie vo vsiom dereve
BOOL CNetworkTree::IfNetwork( NETWORK_INFO*  Net, BYTE  Flags)
{
	CNetwork*     Network = NULL;


	if(Net == NULL)
		return FALSE;

	if(m_List == NULL)
		return FALSE;


	switch(Flags)
	{
	case ALL_TREE:
		{
            m_List->m_critical->EnterCritical();
            if((Network = m_List->FindNetwork( Net )) != NULL)
			{
            	m_List->m_critical->LeaveCritical();
		        return TRUE;
			}
			else
			{
            	m_List->m_critical->LeaveCritical();
				return FALSE;
			}
		}
	case CHILD_TREE:
		{
			if(m_Select == NULL)
				return FALSE;
			else
				return m_Select->IfNetwork(Net);
		}
	default:
		{
			return FALSE;
		}

	}
}



// proverit' na nalichie v spiske
// CHILD_TREE - proverka tol'ko na nalichie v dochernem spiske
// ALL_TREE   - proverka na nalichie vo vsiom dereve
BOOL CNetworkTree::IfAbonent( Addr*  Abn, BYTE  Flags )
{
	CNetwork*     Network = NULL;

	if(Abn == NULL)
		return FALSE;

	if(m_List == NULL)
		return FALSE;

	switch(Flags)
	{
	case ALL_TREE:
		{
            if((Network = m_List->GetFirstNetwork()) != NULL)
			{
				if(Network->IfAbonent(Abn))
		            return TRUE;
			}
			while((Network = m_List->GetNextNetwork()) != NULL)
			{
				if(Network->IfAbonent(Abn))
		            return TRUE;
			}
    		return FALSE;
		}
	case CHILD_TREE:
		{
			if(m_Select == NULL)
				return FALSE;
			else
				return m_Select->IfAbonent(Abn);
		}
	default:
		{
			return FALSE;
		}

	}
}



// naiti uzel po abonentu
// pole Net->m_NetMasck - dolgno sodergat' masku seti
// TYPE_OK - v sluchae uspeha i rezul'tat v Net. V protivnom
// sluchae v Net budut '0'.
int CNetworkTree::FindNetByAbn( Addr*  Abn, NETWORK_INFO*  Net)
{
	CNetwork*     Network = NULL;
	int           RetCode = TYPE_OK;

	if((Abn == NULL) || (Net == NULL))
		return NULL_POINTER;

	if(m_List == NULL)
		return ERNET_NOFOUND;


    if((Network = m_List->GetFirstNetwork()) != NULL)
	{
		if(Network->IfAbonent(Abn))
            return Network->GetInfo(Net);
	}

	while((Network = m_List->GetNextNetwork()) != NULL)
	{
		if(Network->IfAbonent(Abn))
            return Network->GetInfo(Net);
	}

	return ERNET_NOFOUND;
}



//-----------------------------------------------------------------------------------------
// OPERACII S SETIAMI
//-----------------------------------------------------------------------------------------
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!! esli Parent = NULL to vse deistvia provodiatsia v tekushem uzlom !!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// zapros kolichestva podsetey
int CNetworkTree::GetNetCount(ULONG* Count, NETWORK_INFO*  Parent )
{
	int           RetCode = TYPE_OK;
	CNetwork*     Network = NULL;

	if(Count == NULL)
		return NULL_POINTER;

	if(m_List == NULL)
		return ERNET_NOFOUND;

    if(Parent == NULL)
	{
		if(m_Select == NULL)
		{
			Count = 0;
			return ERNET_NOFOUND;
		}
		return m_Select->GetNetCount(Count);
	}

    m_List->m_critical->EnterCritical();
    if((Network = m_List->FindNetwork( Parent )) != NULL)
	{
       	m_List->m_critical->LeaveCritical();
 		return Network->GetNetCount(Count);
	}
	else
	{
       	m_List->m_critical->LeaveCritical();
		Count = 0;
		return ERNET_NOFOUND;
	}

}



// dobavit' podset' v spisok
int CNetworkTree::AddNetwork( NETWORK_INFO*  Net, NETWORK_INFO*  Parent )
{
	int           RetCode = TYPE_OK;
	CNetwork*     Network = NULL;

	if(Net == NULL)
		return NULL_POINTER;

	if(m_List == NULL)
		return ERNET_NOFOUND;


    m_List->m_critical->EnterCritical();
	Network = m_List->FindNetwork( Net );
    m_List->m_critical->LeaveCritical();
	
	if(Network == NULL)
	{
	    if((RetCode = m_List->AddNetwork(Net)) != TYPE_OK)
		    return RetCode;
	}

	if(!m_Flag)
	{
		if((RetCode = Select(Net)) != TYPE_OK)
			return NULL_POINTER;
		m_Flag = TRUE;
	}

	if(Parent != NULL)
	{
        m_List->m_critical->EnterCritical();
		Network = m_List->FindNetwork( Parent );
       	m_List->m_critical->LeaveCritical();
		if(Network != NULL)
            return Network->AddNetwork(Net);
		else
			return ERNET_NOFOUND;
	}

	if(m_Select != NULL)
        return m_Select->AddNetwork(Net);

	return ERNET_NOFOUND;
}




int CNetworkTree::AddNetwork( ULONG  IP, ULONG  Mask, ULONG UserData , NETWORK_INFO*  Parent)
{
	NETWORK_INFO    Net;

	Net.m_IP = IP;
	Net.m_NetMasck = Mask;
	Net.m_UserData = UserData;

	return AddNetwork( &Net, Parent );
}



int CNetworkTree::AddNetwork( char* IP, char* Mask, ULONG UserData, NETWORK_INFO*  Parent )
{
	in_addr    net;
        in_addr    mask;

	if((IP == NULL)||(Mask == NULL))
		 return NULL_POINTER;

	if((strlen(IP) <= 15)||(strlen(Mask) <= 15))
		return ERROR_SIZE;
		 
	if((net.s_addr = inet_addr(IP)) == -1)
		return ERROR_SIZE;

	if((mask.s_addr = inet_addr(Mask)) == -1)
		return ERROR_SIZE;

	return AddNetwork( net.s_addr, mask.s_addr, UserData , Parent);
}



// udalit' podset'iz spiska
int CNetworkTree::DeleteNetwork( NETWORK_INFO*  Net, NETWORK_INFO*  Parent)
{
	int           RetCode = TYPE_OK;
	CNetwork*     Network = NULL;

	if(Net == NULL)
		return NULL_POINTER;

	if(m_List == NULL)
		return ERNET_NOFOUND;

	if(Parent != NULL)
	{
        m_List->m_critical->EnterCritical();
		Network = m_List->FindNetwork( Parent );
       	m_List->m_critical->LeaveCritical();
		if(Network != NULL)
            return Network->DeleteNetwork(Net);
		else
			return ERNET_NOFOUND;
	}

	if(m_Select != NULL)
        return m_Select->DeleteNetwork(Net);

	return ERNET_NOFOUND;
}



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!           v tekushem uzle                                        !!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// poluchit' perviy element
int CNetworkTree::GetFirstNetwork(NETWORK_INFO*  Net)
{
	if(m_Select == NULL)
		return ERNET_ZERO;

	return m_Select->GetFirstNetwork(Net);
}



// poluchit' sleduyshii
int CNetworkTree::GetNextNetwork(NETWORK_INFO*  Net)
{
	if(m_Select == NULL)
		return ERNET_ZERO;

	return m_Select->GetNextNetwork(Net);
}



// zamenit' abonenta
// ishem DestNet i perepisivaem na SrcNet
int CNetworkTree::Replace( NETWORK_INFO*  SrcNet,  NETWORK_INFO*  DestNet  )
{
	if(m_Select == NULL)
		return ERNET_ZERO;

	return m_Select->Replace(SrcNet, DestNet);
}



// zaprosit' pole UserData
int CNetworkTree::GetNetUserData(NETWORK_INFO*  Net)
{
	if(m_Select == NULL)
		return ERNET_ZERO;

	return m_Select->GetNetUserData(Net);
}



int CNetworkTree::SetNetUserData(NETWORK_INFO*  Net)
{
	if(m_Select == NULL)
		return ERNET_ZERO;

	return m_Select->SetNetUserData(Net);
}




//-----------------------------------------------------------------------------------------
// OPERACII S ABONENTAMI 
//-----------------------------------------------------------------------------------------
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!! esli Parent = NULL to vse deistvia provodiatsia v tekushem uzlom !!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// zapros kolichestva abonentov
int CNetworkTree::GetAbonentCount(ULONG* Count, NETWORK_INFO*  Parent)
{
	int           RetCode = TYPE_OK;
	CNetwork*     Network = NULL;

	if(Count == NULL)
		return NULL_POINTER;

	if(m_List == NULL)
		return ERNET_NOFOUND;

	if(Parent != NULL)
	{
        m_List->m_critical->EnterCritical();
		Network = m_List->FindNetwork( Parent );
       	m_List->m_critical->LeaveCritical();
		if(Network != NULL)
            return Network->GetAbonentCount(Count);
		else
			return ERNET_NOFOUND;
	}

	if(m_Select != NULL)
        return m_Select->GetAbonentCount(Count);

	return ERNET_NOFOUND;
}


// dobavit' abonenta
int CNetworkTree::AddAbonent( Addr*  Abn, NETWORK_INFO*  Parent)
{
	int           RetCode = TYPE_OK;
	CNetwork*     Network = NULL;

	if(Abn == NULL)
		return NULL_POINTER;

	if(m_List == NULL)
		return ERNET_NOFOUND;

	if(Parent != NULL)
	{
        m_List->m_critical->EnterCritical();
		Network = m_List->FindNetwork( Parent );
       	m_List->m_critical->LeaveCritical();
		if(Network != NULL)
            return Network->AddAbonent(Abn);
		else
			return ERNET_NOFOUND;
	}

	if(m_Select != NULL)
        return m_Select->AddAbonent(Abn);

	return ERNET_NOFOUND;
}


int CNetworkTree::AddAbonent( ULONG  IP, ULONG  ID, NETWORK_INFO*  Parent)
{
	int           RetCode = TYPE_OK;
	CNetwork*     Network = NULL;


	if(m_List == NULL)
		return ERNET_NOFOUND;

	if(Parent != NULL)
	{
        m_List->m_critical->EnterCritical();
		Network = m_List->FindNetwork( Parent );
       	m_List->m_critical->LeaveCritical();
		if(Network != NULL)
            return Network->AddAbonent(IP, ID);
		else
			return ERNET_NOFOUND;
	}

	if(m_Select != NULL)
        return m_Select->AddAbonent(IP, ID);

	return ERNET_NOFOUND;
}


int CNetworkTree::AddAbonent( char* IP, ULONG  ID, NETWORK_INFO*  Parent )
{
	int           RetCode = TYPE_OK;
	CNetwork*     Network = NULL;


	if(m_List == NULL)
		return ERNET_NOFOUND;

	if(Parent != NULL)
	{
        m_List->m_critical->EnterCritical();
		Network = m_List->FindNetwork( Parent );
       	m_List->m_critical->LeaveCritical();
		if(Network != NULL)
            return Network->AddAbonent(IP, ID);
		else
			return ERNET_NOFOUND;
	}

	if(m_Select != NULL)
        return m_Select->AddAbonent(IP, ID);

	return ERNET_NOFOUND;
}



// udalit' abonenta iz spiska
int CNetworkTree::DeleteAbonent( Addr*  Abn, NETWORK_INFO*  Parent )
{
	int           RetCode = TYPE_OK;
	CNetwork*     Network = NULL;

	if(Abn == NULL)
		return NULL_POINTER;

	if(m_List == NULL)
		return ERNET_NOFOUND;

	if(Parent != NULL)
	{
        m_List->m_critical->EnterCritical();
		Network = m_List->FindNetwork( Parent );
       	m_List->m_critical->LeaveCritical();
		if(Network != NULL)
            return Network->DeleteAbonent(Abn);
		else
			return ERNET_NOFOUND;
	}

	if(m_Select != NULL)
        return m_Select->DeleteAbonent(Abn);

	return ERNET_NOFOUND;
}



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!           v tekushem uzle                                        !!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// poluchit' perviy element
int CNetworkTree::GetFirstAbonent(Addr*  Abn)
{
	if(m_Select == NULL)
		return ERNET_ZERO;

	return m_Select->GetFirstAbonent(Abn);
}


// poluchit' sleduyshii
int CNetworkTree::GetNextAbonent( Addr*  Abn )
{
	if(m_Select == NULL)
		return ERNET_ZERO;

	return m_Select->GetNextAbonent(Abn);
}


// zamenit' abonenta
// ishem DestAbn i perepisivaem na SrcAbn
int CNetworkTree::Replace( Addr*  SrcAbn,  Addr*  DestAbn  )
{
	if(m_Select == NULL)
		return ERNET_ZERO;

	return m_Select->Replace(SrcAbn, DestAbn);
}


