
#include "CFkServer.h"



CFkServer::CFkServer()
{

}


CFkServer::~CFkServer()
{

}


int CFkServer::Create()
{
	return CServerQueue::Create( FK_SERVER );
}


int CFkServer::OnFkMessages(MsgHeader* Header)
{
	int RetCode = TYPE_OK;

	if(Header == NULL)
		return NULL_POINTER;

	switch(Header->Packet)
	{
      // FROM MODI
      //------------
	case MMODI_PARAM_FK:
		{// zapros parametrov MODI
			OnModiParam();
			break;
		}
	case MMODI_STOP_FK:
		{// zavershenie raboti MODI
			OnModiStop();
			break;
		}
	case MMODI_GET_REMOTE_IP_FK:
		{// 
			OnGetRemoteIP();
			break;
		}
	case MMODI_GET_LOCAL_ABN_FK:
		{// 
			OnGetLocalAbn();
			break;
		}
	case MMODI_START_FK:
		{// zavershenie raboti MODI
			OnModiStart();
			break;
		}
	case MMODI_REC_ADDR_FK:
		{// poluchena adresnaya tablica
			OnModiRecAddr();
			break;
		}
	case MMODI_CREATE_ADDR_FK:
		{// sozdanie adresnoy tablici
			ADDR_INFO  *AddrInfo = NULL;

			if((Header->Size - MSG_HEADER) < ADDR_INFO_SIZE)
				return ZERO_SIZE;

            AddrInfo = (ADDR_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnModiCreateAddr(AddrInfo);
			break;
		}
	case MMODI_LOCK_ADDR_FK:
		{// zapret raboti s adresnaya tablica
			OnModiLockAddr();
			break;
		}
	case MMODI_UNLOCK_ADDR_FK:
		{// razreshenie raboti s adresnaya tablica
			OnModiUnLockAddr();
			break;
		}
	case MMODI_ERROR_FK:
		{// avariynoe zavershenie raboti
			ERROR_INFO  *Error = NULL;

			if((Header->Size - MSG_HEADER) < ERROR_INFO_SIZE)
				return ZERO_SIZE;

                        Error = (ERROR_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnModiError(Error);
			break;
		}
	case MMODI_STATUS_LINE_FK:
		{
		        NETWORK_INFO    *NetInfo = NULL;
			
		        if((Header->Size - MSG_HEADER) < NETWORK_INFO_SIZE)
				return ZERO_SIZE;
				
                        NetInfo = (NETWORK_INFO*)(((BYTE*)Header) + MSG_HEADER);
			
			OnModiStatusLine(NetInfo);
	                break;	
		}
     // FROM TM
     //------------
	case MTM_PARAM_FK:
		{// zapros parametrov TM
			OnTmParam();
			break;
		}
	case MTM_START_FK:
		{// nachalo raboti TM
			OnTmStart();
			break;
		}
	case MTM_STOP_FK:
		{// zavershenie raboti TM
			OnTmStop();
			break;
		}
	case MTM_RESTART_FK:
		{// komanda restart TM OK
			OnTmRestartOk();
			break;
		}
	case MTM_STOP_LINE_FK:
		{// ostanov modulia napravleniia
			LINE_INFO  *LineInfo = NULL;

			if((Header->Size - MSG_HEADER) < LINE_INFO_SIZE)
				return ZERO_SIZE;

            LineInfo = (LINE_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnTmStopLine(LineInfo);
			break;
		}
	case MTM_START_LINE_FK:
		{// zapusk modulia napravleniia
			LINE_INFO  *LineInfo = NULL;

			if((Header->Size - MSG_HEADER) < LINE_INFO_SIZE)
				return ZERO_SIZE;

            LineInfo = (LINE_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnTmStartLine(LineInfo);
			break;
		}
	case MTM_ERROR_FK:
		{// avariynoe zavershenie raboti
			ERROR_INFO  *Error = NULL;

			if((Header->Size - MSG_HEADER) < ERROR_INFO_SIZE)
				return ZERO_SIZE;

            Error = (ERROR_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnTmError(Error);
			break;
		}
	case MTM_CONN_ON_FK:
		{// ustanovlenie soedineniya po napravleniu
			LINE_INFO  *LineInfo = NULL;

			if((Header->Size - MSG_HEADER) < LINE_INFO_SIZE)
				return ZERO_SIZE;

            LineInfo = (LINE_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnTmConnectLine(LineInfo);
			break;
		}
	case MTM_CONN_OFF_FK:
		{// razriv soedineniya po napravleniu
			LINE_INFO  *LineInfo = NULL;

			if((Header->Size - MSG_HEADER) < LINE_INFO_SIZE)
				return ZERO_SIZE;

            LineInfo = (LINE_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnTmDsConnectLine(LineInfo);
			break;
		}
	case MTM_GET_STAT_FK:
	       {
	          OnGetStatistics();
		  break;
	       }
   case MTM_ABN_CONN_FK:
    {
       ABONENT_INFO* AbnInfo = NULL;

			 if((Header->Size - MSG_HEADER) < ABONENT_INFO_SIZE)
				    return ZERO_SIZE;

       AbnInfo = (ABONENT_INFO*)(((BYTE*)Header) + MSG_HEADER);
       OnAbonentConnect(AbnInfo);
       break;
    }
   case MTM_ABNBUS_CONN_FK:
    {
       ABONENT_INFO* AbnInfo = NULL;

			 if((Header->Size - MSG_HEADER) < ABONENT_INFO_SIZE)
				    return ZERO_SIZE;

       AbnInfo = (ABONENT_INFO*)(((BYTE*)Header) + MSG_HEADER);
       OnAbonentBusConnect(AbnInfo);
       break;
    }
	}

	return RetCode;
}



// FROM MODI
//------------
void CFkServer::OnModiParam()
{

}


void CFkServer::OnModiStop()
{

}

void CFkServer::OnModiStart()
{

}

void CFkServer::OnModiRecAddr()
{

}


void CFkServer::OnGetStatistics()
{

}



void CFkServer::OnModiCreateAddr(ADDR_INFO *AddrInfo)
{

}


void CFkServer::OnModiLockAddr()
{

}


void CFkServer::OnModiUnLockAddr()
{

}


void CFkServer::OnModiError(ERROR_INFO  *Error)
{

}


void CFkServer::OnModiStatusLine(NETWORK_INFO  *NetInfo)
{

}


void CFkServer::OnGetRemoteIP()
{

}

void CFkServer::OnGetLocalAbn()
{

}


// FROM TM
//------------
void CFkServer::OnTmParam()
{

}


void CFkServer::OnTmStart()
{

}

void CFkServer::OnTmStop()
{

}


void CFkServer::OnTmRestartOk()
{

}


void CFkServer::OnTmStopLine(LINE_INFO *Line)
{

}


void CFkServer::OnTmStartLine(LINE_INFO *Line)
{

}


void CFkServer::OnTmError(ERROR_INFO  *Error)
{

}

void CFkServer::OnTmConnectLine(LINE_INFO *Line)
{

}


void CFkServer::OnTmDsConnectLine(LINE_INFO *Line)
{

}

// �����������/���������� �������
void CFkServer::OnAbonentConnect(ABONENT_INFO* AbnInfo)
{
}


void CFkServer::OnAbonentBusConnect(ABONENT_INFO* AbnInfo)
{
}



int CFkServer::SendMessageToModi(ULONG IdMessage, BYTE* Packet, ULONG Size)
{
	return CServerQueue::SendMessage(MODI_SERVER, IdMessage, Packet, Size);
}


int CFkServer::SendMessageToTm(ULONG IdMessage, BYTE* Packet, ULONG Size)
{
	return CServerQueue::SendMessage(TM_SERVER, IdMessage, Packet, Size);
}




// ���������� ���������
//========================
// TO MODI
//------------
// parametri MODI
int CFkServer::MSG_ParamModi(MODI_PARAMETERS *Param)
{
	if(Param == NULL)
		return NULL_POINTER;
   
	return SendMessageToModi(MFK_PARAM_MODI, (BYTE*)Param, MODI_PARAM_SIZE);
}


// ostanov MODI
int CFkServer::MSG_StopModi()
{   
	return SendMessageToModi(MFK_STOP_MODI, NULL, 0);
}

// restart MODI
int CFkServer::MSG_RestartModi()
{   
	return SendMessageToModi(MFK_RESTART_MODI, NULL, 0);
}


// TO TM
//------------
// parametri TM
int CFkServer::MSG_ParamTm(TM_PARAMETER *Param)
{
	if(Param == NULL)
		return NULL_POINTER;
   
	return SendMessageToTm(MFK_PARAM_TM, (BYTE*)Param, TM_PARAM_SIZE);
}

// ostanov TM
int CFkServer::MSG_StopTm()
{   
	return SendMessageToTm(MFK_STOP_TM, NULL, 0);
}

// restart TM
int CFkServer::MSG_RestartTm()
{   
	return SendMessageToTm(MFK_RESTART_TM, NULL, 0);
}

// ostanov modulia napravleniia
int CFkServer::MSG_StopLine(LINE_INFO *LineInfo)
{
	if(LineInfo == NULL)
		return NULL_POINTER;
   
	return SendMessageToTm(MFK_STOP_LINE_TM, (BYTE*)LineInfo, LINE_INFO_SIZE);
}

// zapusk modulia napravleniia
int CFkServer::MSG_StartLine(LINE_INFO *LineInfo)
{
	if(LineInfo == NULL)
		return NULL_POINTER;
   
	return SendMessageToTm(MFK_START_LINE_TM, (BYTE*)LineInfo, LINE_INFO_SIZE);
}

// ����� �� ��� ����������
//-------------------------
// zapret zapisi v strukturu statistiki
int CFkServer::MSG_LockStat()
{
	SendMessageToModi(MFK_LOCK_STAT_MODI, NULL, 0);

	return SendMessageToTm(MFK_LOCK_STAT_TM, NULL, 0);
}

// razreshenie zapisi v strukturu statistiki
int CFkServer::MSG_UnLockStat()
{
	SendMessageToModi(MFK_UNLOCK_STAT_MODI, NULL, 0);

	return SendMessageToTm(MFK_UNLOCK_STAT_TM, NULL, 0);
}

// sozdanie statistiki
int CFkServer::MSG_CreatStat(ADDR_INFO *StatInfo)
{
	if(StatInfo == NULL)
		return NULL_POINTER;

//	SendMessageToModi(MFK_CREATE_STAT_MODI, (BYTE*)StatInfo, ADDR_INFO_SIZE);

	return SendMessageToTm(MFK_CREATE_STAT_TM, (BYTE*)StatInfo, ADDR_INFO_SIZE);
}




int CFkServer::MSG_SetRemoteIP(BYTE  *Array, ULONG Size)
{
	if(Array == NULL)
		return NULL_POINTER;


	return SendMessageToModi(MFK_SET_REMOTE_IP_MODI, Array, Size);

}

int CFkServer::MSG_SetLocalAbn(char  *Path)
{
	int     Len = 0;

	if(Path == NULL)
		return NULL_POINTER;

	Len = strlen(Path)+1;
	if((Len + 1) >= (MQ_MESSAGE_SIZE - MSG_HEADER))
		return ERROR_SIZE;

	return SendMessageToModi(MFK_SET_LOCAL_ABN_MODI, (BYTE*)Path, Len);

}


// ���������� ������ ������� �������������
int CFkServer::MSG_ReLoadRoute()
{
	return SendMessageToTm(MFK_RELOAD_ROUTE, NULL, 0);
}
