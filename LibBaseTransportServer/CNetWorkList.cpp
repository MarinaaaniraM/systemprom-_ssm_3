#include "CNetWorkList.h"


//============================================================================
//      Class  CNetwork
//============================================================================

CNetwork::CNetwork()
{
  m_NetInfo.m_IP = 0;
  m_NetInfo.m_NetMasck = 0;
  m_NetInfo.m_UserData = 0;
  m_AbnList = NULL;
  m_NetList = NULL;
  m_Parent = NULL;
  m_count = 0;

}


CNetwork::~CNetwork()
{
  if(m_AbnList != NULL)
  {
	  delete m_AbnList;
	  m_AbnList = NULL;
  }

  if(m_NetList != NULL)
  {
	  delete m_NetList;
	  m_NetList = NULL;
  }

}



//zaprosit' svoye opisanie
int CNetwork::GetInfo(NETWORK_INFO* Net)
{
   int  RetCode = TYPE_OK;

   if(Net == NULL)
	   return NULL_POINTER;

   *Net = m_NetInfo;

   return RetCode;
}


int CNetwork::Create(NETWORK_INFO* Net, CNetWorkList* Parent )
{
   int  RetCode = TYPE_OK;

   if(Net == NULL)
	   return NULL_POINTER;

   m_NetInfo = *Net;
   m_Parent = Parent;
   
   if((m_AbnList = new CAbonentList()) == NULL)
	   return EMPTY_MEMORY;

   if((RetCode = m_AbnList->Create()) != TYPE_OK)
   {
	   delete m_AbnList;
	   return RetCode;
   }

   if((m_NetList = new CNetWorkList()) == NULL)
   {
	   delete m_AbnList;
	   return EMPTY_MEMORY;
   }

   if((RetCode = m_NetList->Create(this)) != TYPE_OK)
   {
	   delete m_AbnList;
	   delete m_NetList;
	   return RetCode;
   }
   
   m_count = 0;

   return TYPE_OK;
}


// zapros kolichestva podsetey
int CNetwork::GetNetCount(ULONG* Count)
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->GetCount(Count);
}

// dobavit' podset' v spisok
int CNetwork::AddNetwork( NETWORK_INFO*  Net )
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->AddNetwork(Net);
}

int CNetwork::AddNetwork( ULONG  IP, ULONG  Mask, ULONG UserData )
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->AddNetwork(IP, Mask, UserData);
}

int CNetwork::AddNetwork( char* IP, char* Mask, ULONG UserData )
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->AddNetwork(IP, Mask, UserData);
}

// udalit' podset'iz spiska
int CNetwork::DeleteNetwork( NETWORK_INFO*  Net )
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->Delete(Net);
}


// proverit' na nalichie v spiske
BOOL CNetwork::IfNetwork( NETWORK_INFO*  Net)
{
	if(m_NetList == NULL)
		return FALSE;

	return m_NetList->IfNetwork(Net);
}


// poluchit' perviy element
int CNetwork::GetFirstNetwork(NETWORK_INFO*  Net)
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->GetFirstNetwork(Net);
}


// poluchit' sleduyshii
int CNetwork::GetNextNetwork(NETWORK_INFO*  Net)
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->GetNextNetwork(Net);
}


// zamenit' abonenta
int CNetwork::Replace( NETWORK_INFO*  SrcNet,  NETWORK_INFO*  DestNet  )
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->Replace( SrcNet,  DestNet  );
}


// zaprosit' pole UserData
int CNetwork::GetNetUserData(NETWORK_INFO*  Net)
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->GetUserData( Net );
}


int CNetwork::SetNetUserData(NETWORK_INFO*  Net)
{
	if(m_NetList == NULL)
		return ERNET_ZERO;

	return m_NetList->SetUserData( Net );
}



// zapros kolichestva abonentov
int CNetwork::GetAbonentCount(ULONG* Count)
{
	if(m_AbnList == NULL)
		return ERAB_ABN_ZERO;

	return m_AbnList->GetCount( Count );
}


// dobavit' abonenta
int CNetwork::AddAbonent( Addr*  Abn )
{
	if(m_AbnList == NULL)
		return ERAB_ABN_ZERO;

	return m_AbnList->AddAbonent( Abn );
}


int CNetwork::AddAbonent( ULONG  IP, ULONG  ID )
{
	if(m_AbnList == NULL)
		return ERAB_ABN_ZERO;

	return m_AbnList->AddAbonent( IP, ID );
}


int CNetwork::AddAbonent( char* IP, ULONG  ID )
{
	if(m_AbnList == NULL)
		return ERAB_ABN_ZERO;

	return m_AbnList->AddAbonent( IP, ID );
}


// udalit' abonenta iz spiska
int CNetwork::DeleteAbonent( Addr*  Abn )
{
	if(m_AbnList == NULL)
		return ERAB_ABN_ZERO;

	return m_AbnList->Delete( Abn );
}


// proverit' na nalichie v spiske
BOOL CNetwork::IfAbonent( Addr*  Abn )
{
	if(m_AbnList == NULL)
		return FALSE;

	return m_AbnList->IfAbonent( Abn );
}


// poluchit' perviy element
int CNetwork::GetFirstAbonent(Addr*  Abn)
{
	if(m_AbnList == NULL)
		return ERAB_ABN_ZERO;

	return m_AbnList->GetFirstAbonent( Abn );
}


// poluchit' sleduyshii
int CNetwork::GetNextAbonent( Addr*  Abn )
{
	if(m_AbnList == NULL)
		return ERAB_ABN_ZERO;

	return m_AbnList->GetNextAbonent( Abn );
}


// zamenit' abonenta
int CNetwork::Replace( Addr*  SrcAbn,  Addr*  DestAbn  )
{
	if(m_AbnList == NULL)
		return ERAB_ABN_ZERO;

	return m_AbnList->Replace( SrcAbn, DestAbn );
}





//============================================================================
//      Class  CNetWorkList
//============================================================================
CNetWorkList::CNetWorkList()
{
	m_list = NULL;
	m_critical = NULL;
}


CNetWorkList::~CNetWorkList()
{
	if(m_list != NULL)
	{
		if(m_critical != NULL)
		    m_critical->EnterCritical();

		delete m_list;
		m_list = NULL;	

		if(m_critical != NULL)
		    m_critical->LeaveCritical();
	}

    if(m_critical != NULL)
	{
		delete m_critical;
		m_critical = NULL;
	}
}


// sozdat' spisok
int CNetWorkList::Create(CNetwork*  Parent)
{
	int  RetCode = TYPE_OK;

	m_Parent = Parent;

	if((m_list = new CList) == NULL)
		return EMPTY_MEMORY;

	if((m_critical = new CCriticalSection) == NULL)
	{
		delete m_list;
		m_list = NULL;
		return EMPTY_MEMORY;
	}

	if((RetCode = m_critical->Create()) != TYPE_OK)
	{
		delete m_list;
		m_list = NULL;
		delete m_critical;
		m_critical = NULL;
		return RetCode;
	}
	
	return TYPE_OK;
}


// poluchit' chislo elementov v spiske
int CNetWorkList::GetCount(ULONG* count)
{
	if((m_critical == NULL) || (m_list == NULL) || (count == NULL))
		return NULL_POINTER;

	m_critical->EnterCritical();
	*count = m_list->GetCount();
	m_critical->LeaveCritical();

	return TYPE_OK;
}


// dobavit' set'
int CNetWorkList::AddNetwork( NETWORK_INFO*  Net )
{
	CNetwork*       net = NULL;
	CNetWorkList*   Root = NULL;
	int             RetCode = TYPE_OK;

	if((m_critical == NULL) || (m_list == NULL) || (Net == NULL))
		return NULL_POINTER;

	if(m_Parent != NULL)
    { 
		if(m_Parent->m_Parent != NULL)
		{
			Root = m_Parent->m_Parent;

           	Root->m_critical->EnterCritical();
			net = Root->FindNetwork(Net);
            Root->m_critical->LeaveCritical();

			if(net != NULL)
			{
            	m_critical->EnterCritical();
                RetCode = m_list->AddItem((void*) net, sizeof(CNetwork*), TRUE);
	            m_critical->LeaveCritical();
				if(RetCode == TYPE_OK)
					net++;
	            return RetCode;
			}
		}
	}

    if((net = new CNetwork)== NULL)
		return EMPTY_MEMORY;

	if((RetCode = net->Create(Net,this)) != TYPE_OK)
	{
		delete net;
		return EMPTY_MEMORY;
	}

	net->m_NetInfo = *Net;
	m_critical->EnterCritical();
        RetCode = m_list->AddItem((void*) net, sizeof(CNetwork*), TRUE);
	m_critical->LeaveCritical();

	if(RetCode != TYPE_OK)
	{
		delete net;
		return EMPTY_MEMORY;
	}

	if(Root != NULL)
	{
	   Root->m_critical->EnterCritical();
       RetCode = Root->m_list->AddItem((void*) net, sizeof(CNetwork*), TRUE);
	   Root->m_critical->LeaveCritical();
	}

	if(RetCode == TYPE_OK)
	   net++;

	return RetCode;

}


int CNetWorkList::AddNetwork( ULONG  IP, ULONG  Mask, ULONG UserData )
{
	NETWORK_INFO    Net;

	Net.m_IP = IP;
	Net.m_NetMasck = Mask;
	Net.m_UserData = UserData;

	return AddNetwork( &Net );
}


int CNetWorkList::AddNetwork( char* IP, char* Mask, ULONG UserData )
{
	in_addr    net;
    in_addr    mask;

	if((IP == NULL)||(Mask == NULL))
		 return NULL_POINTER;

	if((strlen(IP) <= 7)||(strlen(Mask) <= 7))
		return ERROR_SIZE;
		 
	if((net.s_addr = inet_addr(IP)) == -1)
		return ERROR_SIZE;

	if((mask.s_addr = inet_addr(Mask)) == -1)
		return ERROR_SIZE;

	return AddNetwork( net.s_addr, mask.s_addr, UserData );
}



// udalit' set' iz spiska
int CNetWorkList::Delete( NETWORK_INFO*  Net )
{
	int              RetCode = TYPE_OK;
	CNetwork*        net = NULL;

	if(Net == NULL)
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();
	if((net = FindNetwork(Net)) == NULL)
	{
	      m_critical->LeaveCritical();
		  return ERNET_NOFOUND;
	}
	RetCode = m_list->DeleteItem((void*)net);
	m_critical->LeaveCritical();
   
	if(net->GetParentList() == this)
	{
	    delete net;
	    net = NULL;
	}
	else
		net--;

	return RetCode;
}


// proverit' na nalichie v spiske
BOOL CNetWorkList::IfNetwork( NETWORK_INFO*  Net)
{
	int              RetCode = TYPE_OK;
	CNetwork*        net = NULL;

	if(Net == NULL)
		return FALSE;

	if((m_list == NULL) || ( m_critical == NULL ))
		return FALSE;

	m_critical->EnterCritical();
	    net = FindNetwork(Net);
	m_critical->LeaveCritical();

	if(net == NULL)
		return FALSE;
	else
		return TRUE;
}


// udalit' spisok
void CNetWorkList::DeleteList()
{
	ULONG		     Count = 0;
	CNetwork*        net = NULL;
	int			     RetCode = TYPE_OK;
	ULONG            Size = 0;

	if((m_list == NULL) || ( m_critical == NULL ))
		return ;

		if((Count = m_list->GetCount()) == 0)
			return;

		if((RetCode = m_list->GetFirst((void**)&net, &Size)) != TYPE_OK)
			return;

		if(net != NULL)
		{
			delete net;
			net = NULL;
        }

		while((RetCode = m_list->GetNext((void**)&net, &Size)) == TYPE_OK) 
		{
			if(net != NULL)
			{
				delete net;
				net = NULL;
			}
		}
}


// naiti v spiske
CNetwork* CNetWorkList::FindNetwork( NETWORK_INFO*  Net )
{
	CNetwork*           net = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;

	if(Net == NULL)
		return NULL;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL;

	if((RetCode = m_list->GetFirst((void**)&net, &Size)) != TYPE_OK)
		return NULL;
	
	if(net == NULL)
		return NULL;
	else
	{
        if((net->m_NetInfo.m_IP & net->m_NetInfo.m_NetMasck) == (Net->m_IP & Net->m_NetMasck))
			return net;
	}

	while((RetCode = m_list->GetNext((void**)&net, &Size)) == TYPE_OK) 
	{
		if(net == NULL)
			return NULL;
		else
		{
            if((net->m_NetInfo.m_IP & net->m_NetInfo.m_NetMasck) == (Net->m_IP & Net->m_NetMasck))
				return net;
		}
	}

	return NULL;
}


// poluchit' perviy element
int CNetWorkList::GetFirstNetwork(NETWORK_INFO*  Net)
{
	CNetwork*           net = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;

	if(Net == NULL)
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();

	if((RetCode = m_list->GetFirst((void**)&net, &Size)) != TYPE_OK)
	{
		m_critical->LeaveCritical();
		return ERNET_ZERO;
	}
	
	if(net == NULL)
	{
		m_critical->LeaveCritical();
		return ERNET_ZERO;
	}
	else
	{
		m_critical->LeaveCritical();
		*Net = net->m_NetInfo;
		return TYPE_OK;
	}
}



// poluchit' perviy element
CNetwork* CNetWorkList::GetFirstNetwork()
{
	CNetwork*           net = NULL;
	ULONG		    Size = 0;
	int                 RetCode = TYPE_OK;


	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL;

	m_critical->EnterCritical();

	if((RetCode = m_list->GetFirst((void**)&net, &Size)) != TYPE_OK)
	{
		m_critical->LeaveCritical();
		return NULL;
	}
	else	
	{
		m_critical->LeaveCritical();
		return net;
	}
	
}





// poluchit' sleduyshii
int CNetWorkList::GetNextNetwork(NETWORK_INFO*  Net)
{
	CNetwork*           net = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;


	if(Net == NULL)
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();

	if((RetCode = m_list->GetNext((void**)&net, &Size)) != TYPE_OK)
	{
		m_critical->LeaveCritical();
		return ERNET_ZERO;
	}
	
	if(net == NULL)
	{
		m_critical->LeaveCritical();
		return ERNET_ZERO;
	}
	else
	{
		m_critical->LeaveCritical();
		*Net = net->m_NetInfo;
		return TYPE_OK;
	}
}



// poluchit' sleduyshii
CNetwork* CNetWorkList::GetNextNetwork()
{
	CNetwork*           net = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;


	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL;

	m_critical->EnterCritical();

	if((RetCode = m_list->GetNext((void**)&net, &Size)) != TYPE_OK)
	{
		m_critical->LeaveCritical();
		return NULL;
	}
	else
	{
		m_critical->LeaveCritical();
		return net;
	}
}





// zamenit' abonenta. ishem DestAbn i perepisivaem na SrcAbn
int CNetWorkList::Replace( NETWORK_INFO*  SrcNet,  NETWORK_INFO*  DestNet  )
{
	CNetwork*           net = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;

	if((SrcNet == NULL)||(DestNet == NULL))
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();
	    net = FindNetwork(DestNet);

	    if(net == NULL)
		{
	        m_critical->LeaveCritical();
		    return ERNET_NOFOUND;
		}

	    net->m_NetInfo = *SrcNet;
	m_critical->LeaveCritical();

	return TYPE_OK;
}



// zaprosit' pole UserData
int CNetWorkList::GetUserData(NETWORK_INFO*  Net)
{
	CNetwork*           net = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;

	if(Net == NULL)
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();
	    net = FindNetwork(Net);

	    if(net == NULL)
		{
	        m_critical->LeaveCritical();
		    return ERNET_NOFOUND;
		}

	    Net->m_UserData = net->m_NetInfo.m_UserData;
	m_critical->LeaveCritical();

	return TYPE_OK;
}


int CNetWorkList::SetUserData(NETWORK_INFO*  Net)
{
	CNetwork*           net = NULL;
	ULONG				Size = 0;
	int                 RetCode = TYPE_OK;

	if(Net == NULL)
		return NULL_POINTER;

	if((m_list == NULL) || ( m_critical == NULL ))
		return NULL_POINTER;

	m_critical->EnterCritical();
	    net = FindNetwork(Net);

	    if(net == NULL)
		{
	        m_critical->LeaveCritical();
		    return ERNET_NOFOUND;
		}

	    net->m_NetInfo.m_UserData = Net->m_UserData;
	m_critical->LeaveCritical();

	return TYPE_OK;
}
