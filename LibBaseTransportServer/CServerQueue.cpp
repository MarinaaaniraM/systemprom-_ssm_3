
#include "CServerQueue.h"
#include <unistd.h>
#include <stdlib.h>


CServerQueue::CServerQueue() 
{
	m_MsgList = NULL;
	m_ThreadIdRead = 0;
	m_ThreadIdHandler = 0;
	m_Type = 0;

	m_MessageTM = NULL;	
	m_MessageFK = NULL;	
	m_MessageMODI = NULL;	

	m_Message = NULL;

}

CServerQueue::~CServerQueue()
{
//      if(m_Type == MODI_SERVER)
//              printf("~CServerQueue()\n");
     Close();
}


int CServerQueue::Create(BYTE Type)
{
    int   RetCode = TYPE_OK;

//printf("#1\n");
	if(Type == 0)
		return ERSRV_TYPE;
	else
           m_Type = Type;

//printf("#2\n");

	// sozdaem priemnuy ochered'

    if((m_MsgList = new CMessageList()) == NULL)
		return EMPTY_MEMORY;
//printf("#3\n");
		
	if((RetCode = m_MsgList->Create()) != TYPE_OK)
	{
		Close();
		return RetCode;
	}

//printf("#4\n");

	if((m_MessageTM = new CMessageQueue()) == NULL)
	{
		Close();
		return EMPTY_MEMORY;
	}
//printf("#5\n");

	if((m_MessageFK = new CMessageQueue()) == NULL)
	{
		Close();
		return EMPTY_MEMORY;
	}
//printf("#6\n");

	if((m_MessageMODI = new CMessageQueue()) == NULL)
	{
		Close();
		return EMPTY_MEMORY;
	}
//printf("#7\n");

	// sozdaem kluchi
	
	// smotrim kto mi
	switch(m_Type)
	{
	case FK_SERVER:
		{
			if((RetCode = m_MessageFK->Create(ftok(SERVER_PATH_BIN, FK_SERVER))) != TYPE_OK)
			{
				Close();
				return RetCode;
			}
			if((RetCode = m_MessageTM->Create(ftok(SERVER_PATH_BIN, TM_SERVER))) != TYPE_OK)
			{
				Close();
				return RetCode;
			}
			if((RetCode = m_MessageMODI->Create(ftok(SERVER_PATH_BIN, MODI_SERVER))) != TYPE_OK)
			{
				Close();
				return RetCode;
			}
			m_Message = m_MessageFK;
			break;
		}
	case TM_SERVER:
		{
			if((RetCode = m_MessageFK->Create(ftok(SERVER_PATH_BIN, FK_SERVER), FALSE)) != TYPE_OK)
			{
				Close();
				return RetCode;
			}
			if((RetCode = m_MessageTM->Create(ftok(SERVER_PATH_BIN, TM_SERVER), FALSE)) != TYPE_OK)
			{
				Close();
				return RetCode;
			}
			if((RetCode = m_MessageMODI->Create(ftok(SERVER_PATH_BIN, MODI_SERVER), FALSE)) != TYPE_OK)
			{
				Close();
				return RetCode;
			}
			m_Message = m_MessageTM;
			break;
		}
	case MODI_SERVER:
		{
			if((RetCode = m_MessageMODI->Create(ftok(SERVER_PATH_BIN, MODI_SERVER), FALSE)) != TYPE_OK)
			{
				Close();
				return RetCode;
			}
			m_Message = m_MessageMODI;
			
			if((RetCode = m_MessageFK->Create(ftok(SERVER_PATH_BIN, FK_SERVER), FALSE)) != TYPE_OK)
			{
				Close();
				return RetCode;
			}
//printf("#8\n");

			if((RetCode = m_MessageTM->Create(ftok(SERVER_PATH_BIN, TM_SERVER), FALSE)) != TYPE_OK)
			{
				Close();
				return RetCode;
			}

			break;
		}
	}

    // sozdaem potok na chtenie iz ocheredi
//if(m_Type == MODI_SERVER)
//       printf("#12\n");
//	printf("!! %d !!\n",
	pthread_create( &m_ThreadIdHandler, NULL, &QueueHandler, (void*)this );
        sleep(1);
//if(m_Type == MODI_SERVER)
//       printf("#13\n");
	pthread_create( &m_ThreadIdRead, NULL, &ReadQueue, (void*)this );
//printf("#11\n");
//if(m_Type == MODI_SERVER)
//       printf("#13\n");
       
//      sleep(1);
    // sozdaem potok obrabotki soobsheniy
//if(m_Type == MODI_SERVER)
//       printf("#14\n");
       
//	pthread_create( &m_ThreadIdHandler, NULL, &QueueHandler, (void*)this );
	
//if(m_Type == MODI_SERVER)
//       printf("#15\n");
       
	return TYPE_OK;
}



// Potok na chtenie iz ocheredi   
void* ReadQueue(void* Ptr)
{
	CServerQueue*	Parent = (CServerQueue*)Ptr;
	ULONG           ReadBytes = 0;
	int             RetCode = TYPE_OK;
	BYTE            *Buf = NULL; //[MQ_MESSAGE_SIZE + 1];
	BYTE            Step = 0;

//        printf("job-1\n");
        sleep(1);
	if((Buf = new BYTE[MQ_MESSAGE_SIZE + 1]) == NULL)
	  return NULL;
	
//        printf("job-2\n");
	if(Parent == NULL)
	{
		Parent->m_ThreadIdRead = 0;
		delete[] Buf;
		return NULL;
	}
//        printf("job-3\n");
	if((Parent->m_Message == NULL) || (Parent->m_MsgList == NULL))
	{
		Parent->m_ThreadIdRead = 0;
		delete[] Buf;
		return NULL;
	}


//        printf("job-44\n");
	for(;;)
	{
		if((RetCode = Parent->m_Message->ReceiveMessage(Buf, &ReadBytes)) != TYPE_OK)
		{
    		 // poka nichego ne delaem
		    if(Step <= 250)
		    {
		      Step++;
//		 return NULL;
       		      continue;
		    }
		    else
		    {
		     delete[] Buf;
		     return NULL;
		    }
		}

		if(ReadBytes >= MSG_HEADER)
		{// dobavliaem v spisok
			
			if((RetCode = Parent->m_MsgList->AddMessage(Buf)) != TYPE_OK)
			{ // obrabativaem oshibku pri dobavlenii v spisok
				 // poka nichego ne delaem
        		continue;
			}
		}

	} // of for(;;)

        delete[] Buf;
	
	return NULL;
}



void* QueueHandler(void* Ptr)
{
	CServerQueue*	Parent = (CServerQueue*)Ptr;
//	ULONG           ReadBytes = 0;
	int             RetCode = TYPE_OK;
	BYTE*           Buf = NULL;


//        printf("job-1\n");
	if(Parent == NULL)
	{
		Parent->m_ThreadIdHandler = 0;
		return NULL;
	}
//        printf("job-2\n");
	if(Parent->m_MsgList == NULL)
	{
		Parent->m_ThreadIdHandler = 0;
		return NULL;
	}

//        printf("job-4\n");
	for(;;)
	{
//        printf("job-5\n");
		Parent->m_MsgList->WaitForEvent();
//        printf("job-6\n");
		while((RetCode = Parent->m_MsgList->GetMessage(&Buf)) == TYPE_OK)
		{
		    if(Buf != NULL)
			{// esli ne pustoe to razbiraem
			    Parent->MessageHandler(Buf);
			}
		}

	} // of for(;;)

	return NULL;
}



void CServerQueue::Close()
{
//printf("enter to CServerQueue destructor...\n");
	if(m_ThreadIdRead != 0)
	{
//printf("#13\n");

		pthread_cancel(m_ThreadIdRead);
//		pthread_join(m_ThreadIdRead, NULL);
//		printf("ServerQ - read deleted\n");
//printf("#14\n");

	}
	if(m_ThreadIdHandler != 0)
	{
//printf("#15\n");
	
		pthread_cancel(m_ThreadIdHandler);
//		pthread_join(m_ThreadIdHandler, NULL);
//		printf("ServerQ - handler deleted\n");
//printf("#16\n");
	
        }
//printf("#17\n");

	if(m_MsgList != NULL)
	{
		delete m_MsgList;
		m_MsgList = NULL;
	}
//printf("#18\n");

	if(m_MessageTM != NULL)
	{
		delete m_MessageTM;
		m_MessageTM = NULL;
	}
//printf("#19\n");

	if(m_MessageFK != NULL)
	{
		delete m_MessageFK;
		m_MessageFK = NULL;
	}
//printf("#20\n");

	if(m_MessageMODI != NULL)
	{
		delete m_MessageMODI;
		m_MessageMODI = NULL;
	}
//printf("#21\n");
//      if(m_Type == MODI_SERVER)
//           printf("~CServerQueue::Close()\n");
	
}



int CServerQueue::OnFkMessages(MsgHeader* Header)
{
    int  RetCode = TYPE_OK;

	return RetCode;
}
 
int CServerQueue::OnModiMessages(MsgHeader* Header)
{
    int  RetCode = TYPE_OK;

	return RetCode;
}

int CServerQueue::OnTmMessages(MsgHeader* Header)
{
    int  RetCode = TYPE_OK;

	return RetCode;
}

int CServerQueue::MessageHandler(BYTE* Packet)
{
    int  RetCode = TYPE_OK;
	MsgHeader*   Header = (MsgHeader*)Packet;

	if(Header == NULL)
		return NULL_POINTER;

	if(Header->Size < MSG_HEADER)
		return ZERO_SIZE;

	switch(Header->Src.Abonent)
	{
	case SERVICE_FK:
		{
			if((RetCode = OnFkMessages(Header)) != TYPE_OK)
			{// ������ ��� ������� ������. ���� �� ������������.
			}
			break;
		}
	case SERVICE_MODI:
		{
			if((RetCode = OnModiMessages(Header)) != TYPE_OK)
			{// ������ ��� ������� ������. ���� �� ������������.
			}
			break;
		}
	case SERVICE_TM:
		{
			if((RetCode = OnTmMessages(Header)) != TYPE_OK)
			{// ������ ��� ������� ������. ���� �� ������������.
			}
			break;
		}
	}
	
	delete[] Packet;

	return RetCode;
}


int CServerQueue::SendMessage(BYTE Abn, ULONG IdMessage, BYTE* Packet, ULONG Size)
{
    int                RetCode = TYPE_OK;
	MsgHeader*         Header = NULL;
	BYTE*              packet = NULL;
	BYTE*              data = NULL;
	CMessageQueue*	   Message = NULL;	

	if((Packet == NULL) && (Size > 0))
		return NULL_POINTER;

	if((packet = new BYTE[MSG_HEADER + Size]) == NULL)
		return EMPTY_MEMORY;

	Header = (MsgHeader*)packet;

	Header->UnUsed_1;                              // ���������������
	Header->UnUsed_2;                              // ���������������
        Header->ID = 0;	                               // ID ������	
        Header->Size = MSG_HEADER + Size;	           // ����� ������   
        Header->Protocol = MESSAGES_PROTOKOL_TYPE;     // ��� ��������� 	
        Header->Packet = IdMessage;	      	           // ��� ������		 			
        Header->Rezerv = 0;     	                   // ���������������	
        Header->Priority = 0;	                       // ���������
        Header->Count = 0;		                       // ����� ���������
	Header->Src.Object = m_Type;                   // �����������


	switch(Abn) // ����������
	{
	case FK_SERVER:
		{
			Header->Src.Abonent = SERVICE_FK;
			Message = m_MessageFK;
			break;
		}
	case TM_SERVER:
		{
			Header->Src.Abonent = SERVICE_TM;
			Message = m_MessageTM;
			break;
		}
	case MODI_SERVER:
		{
			Header->Src.Abonent = SERVICE_MODI;
			Message = m_MessageMODI;
			break;
		}
	}

//
	if(Size > 0)
	{
		data = packet + MSG_HEADER;
		memcpy(data, Packet, Size);
	}


        if(Message == NULL)
	     {
	      return NULL_POINTER;
	     }
	RetCode = Message->SendMessage( packet, Header->Size);

	delete[] packet;



	return RetCode;
}
