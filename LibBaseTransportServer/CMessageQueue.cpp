#include "CMessageQueue.h"




CMessageQueue::CMessageQueue()
{
	m_ID = -1;
	m_flags = IPC_CREAT | 0666;

	m_type = TRUE;

}






CMessageQueue::~CMessageQueue()
{
	if(m_ID != -1)
	    Close();
}


// zakritie / udalenie ocheredi
int CMessageQueue::Close()
{
       int RetCode = -1;
       
	if(m_type)
	{
//	printf("\n������� ������� �� �������\n");
		RetCode = msgctl(m_ID, IPC_RMID, NULL);
//	printf("\n������� ������� �� ������� = %d\n",RetCode);
		m_ID = -1;
	}

	return TYPE_OK;
}

// zapros parametrov ocheredi
int  CMessageQueue::GetAttrib(msqid_ds* atr)
{

	if(atr == NULL)
		return NULL_POINTER;

	*atr = m_attr;

	if(msgctl(m_ID, IPC_STAT, atr) == -1)
		return ERQ_GETATTR;
	else
	{
	    m_attr = *atr;
		return TYPE_OK;
	}

}




// chislo v ocheredi
int CMessageQueue::GetCount(ULONG *Count)
{
	int            RetCode = TYPE_OK;
	msqid_ds       atr;

	if(Count == NULL)
		return  NULL_POINTER;

	if(msgctl(m_ID, IPC_STAT, &atr) == -1)
		return ERQ_GETATTR;

	*Count = atr.msg_qnum;

	return RetCode;
}


// poslat' soobshenie
int CMessageQueue::SendMessage(BYTE* Buf, ULONG Size)
{
	if(Buf == NULL)
		return  NULL_POINTER;
	if(Size == 0)
		return  ZERO_SIZE;

        if(Size > MQ_MESSAGE_SIZE)
	{
	   printf("===> Send Size = %d \n ",Size);
	   return ERQ_SENDMSG;
	}

	memcpy(m_buf.m_msg, Buf, Size);
	m_buf.m_type = 1;
//	printf("===> Send Size = %d \n ",Size);
	if(msgsnd(m_ID, &m_buf, Size, 0) == -1)
		return ERQ_SENDMSG;

	return TYPE_OK;
}


// prochitat' is ocheredi
int CMessageQueue::ReceiveMessage(BYTE* Buf, ULONG* ReadBytes)
{
	int		Count = -1;

	if((Buf == NULL) || (ReadBytes == NULL))
		return  NULL_POINTER;


//    printf("===> ReceiveMessage... \n ");

    if((Count = msgrcv(m_ID, &m_buf, MQ_MESSAGE_SIZE, 0, MSG_NOERROR)) == -1)
		return ERQ_RECEIVE;

	memcpy(Buf, m_buf.m_msg, Count);

	*ReadBytes = (ULONG)Count;
	
	return TYPE_OK;

}


// sozdanie ocheredi
int CMessageQueue::Create(key_t Key, BOOL Server )
{

//	m_ID = Key; - �������, ID � key - ������ ����
	m_type = Server;
	
	if(m_type)
	    {
//	        printf("\n Server\n");
		m_flags = IPC_CREAT | IPC_EXCL | 0666;
	    }
	else
	{
//	        printf("\n Client\n")
		m_flags = IPC_CREAT | 0666 ;
	}


	if((m_ID = msgget(Key, m_flags)) == -1)
	{
	    if(!m_type) 
		return ERQ_OPEN;
// Roman 14-11-2008 ��������� ������ ��� ������� �� � "���������" ��������
// �� ���������� �����. ������ ������ ���������
//	    m_flags = IPC_CREAT | 0666 ;// �� ������������ ����
	
	    if((m_ID = msgget(Key, IPC_CREAT | 0666)) == -1)
		    return ERQ_OPEN;

	    msgctl(m_ID, IPC_RMID, NULL); // ������� �������

	    if((m_ID = msgget(Key, m_flags)) == -1) // ����� �������� ������� �������
		return ERQ_OPEN;			    
		
	    printf("===> ������� �������� �������.... \n ");

	    
	}
	// � ���������� m_ID != -1
	return TYPE_OK;
}
