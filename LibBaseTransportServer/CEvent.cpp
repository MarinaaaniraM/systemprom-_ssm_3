// []------------------------------------------------------------------------[]
//  |                                                                        |
//  |                                                                        |
// []------------------------------------------------------------------------[]

#include "CEvent.h"

//
CEvent::CEvent()
{
    m_event = NULL;
    m_critical = NULL;
    m_count = 1;
    m_last = 0;
}


CEvent::~CEvent()
{
    if(m_event != NULL)
    {
        pthread_cond_destroy(m_event);
        delete m_event;
        m_event = NULL;
   }
    if(m_critical != NULL)
	{
	    delete m_critical;
	    m_critical = NULL;
	}
}

int CEvent::Create()
{
	int    RetCode = TYPE_OK;


     for(int i = 0; i < MAX_EVENTS; i++)
      {
         m_global[i].ID = -1;
         m_global[i].Status = 0;
      }

      m_global[0].ID = 0;

    if((m_critical = new CCriticalSection) == NULL)
	    return EMPTY_MEMORY;

	  if((RetCode = m_critical->Create()) != TYPE_OK)
		{
		    delete m_critical;
		    m_critical = NULL;

		    return RetCode;
		}

	    if((m_event = new pthread_cond_t) == NULL)
		{
		    delete m_critical;
		    m_critical = NULL;

		    return EMPTY_MEMORY;
		}

	    if(pthread_cond_init(m_event, NULL) != 0)
		{
		        delete m_critical;
		        m_critical = NULL;

            delete m_event;
            m_event = NULL;

            return ERE_INIT;
		}

		return TYPE_OK;

}


int  CEvent::AddEvent(int ID)
{
     if(m_count == MAX_EVENTS)
            return  EMPTY_MEMORY;

      m_global[m_count].ID = ID;
      m_global[m_count].Status = 0;
      m_count++;



//     for(int i = 0; i < MAX_EVENTS; i++)
//    {
//         if(m_global[i].ID == -1)
//           {
//               m_global[i].ID = ID;
//               m_global[i].Status = 0;
//               m_count++;
//               return TYPE_OK;
//           }
//      }
      return  TYPE_OK;
}


int  CEvent::WaitForEvent(ULONG  sec)
{
	int RetCode = TYPE_OK;
	// Roman 24-11-11 Checking!
	if(!m_critical || !m_event)
	  return RetCode;

	if(m_count > 1)
	   return WaitForMultipleEvent(sec);

	if(sec == 0)
	{
		m_critical->EnterCritical();
		pthread_cond_wait(m_event, m_critical->m_lock);
		m_critical->LeaveCritical();
		return TYPE_OK;
	}
	else
	{
		timespec  Time;

		Time.tv_sec = time(0) + sec;
		Time.tv_nsec = 0;

		m_critical->EnterCritical();
		RetCode = pthread_cond_timedwait(m_event, m_critical->m_lock, &Time);
		m_critical->LeaveCritical();
		if(RetCode == 0)
		   return TYPE_OK;

		if(RetCode == ETIMEDOUT)
		   return ERE_BYTIME;
		else
		   return RetCode;

	}
}


void CEvent::SetEvent(int ID)
{
//    m_critical->EnterCritical();

     for(int i = 0; i < MAX_EVENTS; i++)
      {
         if( m_global[i].ID == ID)
           {
               m_global[i].Status = 1;
               m_critical->LeaveCritical();
	       pthread_cond_signal(m_event);
               return;
           }
      }

//    m_critical->LeaveCritical();
}


int  CEvent::WaitForMultipleEvent(ULONG  sec)
{
	int RetCode = TYPE_OK;

	m_critical->EnterCritical();
	if((RetCode = _GetSetEvent()) != -1)
	{
	    m_critical->LeaveCritical();
	    return RetCode;
	}

	if(sec == 0)
	{
		pthread_cond_wait(m_event, m_critical->m_lock);
	}
	else
	{
		timespec  Time;

		Time.tv_sec = time(0) + sec;
		Time.tv_nsec = 0;

		RetCode = pthread_cond_timedwait(m_event, m_critical->m_lock, &Time);

		if(RetCode == ETIMEDOUT)
        {
	       m_critical->LeaveCritical();
		   return ERE_BYTIME;
		}
	}

	RetCode = _GetSetEvent();
	m_critical->LeaveCritical();
	return RetCode;
}




int CEvent::GetSetEvent(BOOL last)
{

    if(last)
    {
       m_critical->EnterCritical();
       for(int i = m_last; i < m_count; i++)
       {
         if(m_global[i].Status == 1)
           {
               m_global[i].Status = 0;
    	       m_critical->LeaveCritical();
               return m_global[i].ID;
           }
       }
       for(int j = 0; j <= m_last; j++)
	   {
         if(m_global[j].Status == 1)
           {
               m_global[j].Status = 0;
	           m_last = j;
               return m_global[j].ID;
           }
	   }
       m_last = 0;
	   m_critical->LeaveCritical();
	   return -1;
	}


     m_critical->EnterCritical();
     for(int i = 0; i < m_count; i++)
     {
         if(m_global[i].Status == 1)
           {
               m_global[i].Status = 0;
    	       m_critical->LeaveCritical();
               return m_global[i].ID;
           }
     }


	m_critical->LeaveCritical();
    return -1;
}


int CEvent::_GetSetEvent()
{
     for(int i = m_last; i < m_count; i++)
      {
         if(m_global[i].Status == 1)
           {
               m_global[i].Status = 0;
	       m_last = i;
               return m_global[i].ID;
           }
      }
     for(int j = 0; j <= m_last; j++)
	 {
         if(m_global[j].Status == 1)
           {
               m_global[j].Status = 0;
	           m_last = j;
               return m_global[j].ID;
           }
	 }

     m_last = 0;
    return -1;
}


BOOL CEvent::GetStatusEvent(int ID)
{
    m_critical->EnterCritical();
	if((ID < 0) || (ID > MAX_EVENTS))
	{
		m_critical->LeaveCritical();
		return FALSE;
	}

    if(m_global[ID].Status == 1)
	{
		m_critical->LeaveCritical();
		return TRUE;
	}
	else
	{
		m_critical->LeaveCritical();
		return FALSE;
	}

}

void CEvent::ResetAllEvent(void)
{
     m_critical->EnterCritical();

     for(int i = 0; i < m_count; i++)
      {
          m_global[i].Status = 0;
      }
      m_last = 0;
	 m_critical->LeaveCritical();
}

