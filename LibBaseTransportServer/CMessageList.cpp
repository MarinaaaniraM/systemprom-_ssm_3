
#include "CMessageList.h"

CMessageList::CMessageList()
{
	m_list = NULL;
	m_critical = NULL;
	m_criticalSend = NULL;
	m_event = NULL;
	m_flag = FALSE;
}


CMessageList::~CMessageList()
{
	if(m_list != NULL)
	{
		if(m_critical != NULL)
		    m_critical->EnterCritical();
		m_list->DeleteList();
		delete m_list;
		m_list = NULL;
		if(m_critical != NULL)
		    m_critical->LeaveCritical();
	}

    if(m_critical != NULL)
	{
		delete m_critical;
		m_critical = NULL;
	}
    if(m_criticalSend != NULL)
	{
		delete m_criticalSend;
		m_criticalSend = NULL;
	}

	if(!m_flag)
	{
		if(m_event != NULL)
		{
			delete m_event;
			m_event = NULL;
		}
	}
}

int  CMessageList::WaitForEvent(int Priority,ULONG  sec)
{
  return m_event->WaitForEvent(sec);
}

// sozdat' spisok
int CMessageList::Create(CEvent* Event)
{
	int  RetCode = TYPE_OK;

	if(Event != NULL)
	{
		m_flag = TRUE;
		m_event = Event;
	}
	else
	{
		if((m_event = new CEvent) == NULL)
			return EMPTY_MEMORY;

		if((RetCode = m_event->Create()) != TYPE_OK)
		{
			delete m_event;
			m_event = NULL;
			return RetCode;
		}
	}

	if((m_list = new CList) == NULL)
	{
		delete m_event;
		m_event = NULL;
		return EMPTY_MEMORY;
	}

	if((m_critical = new CCriticalSection) == NULL)
	{
		delete m_event;
		m_event = NULL;
		delete m_list;
		m_list = NULL;
		return EMPTY_MEMORY;
	}
	if((m_criticalSend = new CCriticalSection) == NULL)
	{
		delete m_event;
		m_event = NULL;
		delete m_list;
		m_list = NULL;
		delete m_critical;
		m_critical = NULL;
		return EMPTY_MEMORY;
	}

	if((RetCode = m_critical->Create()) != TYPE_OK)
	{
		delete m_event;
		m_event = NULL;
		delete m_list;
		m_list = NULL;
		delete m_critical;
		m_critical = NULL;
		delete m_criticalSend;
		m_criticalSend = NULL;
		return RetCode;
	}

	if((RetCode = m_criticalSend->Create()) != TYPE_OK)
	{
		delete m_event;
		m_event = NULL;
		delete m_list;
		m_list = NULL;
		delete m_critical;
		m_critical = NULL;
		delete m_critical;
		m_critical = NULL;
		return RetCode;
	}


	return TYPE_OK;
}


// udalit' spisok
int CMessageList::DeleteList()
{
	int  RetCode = TYPE_OK;

	if((m_critical == NULL) || (m_list == NULL))
		return NULL_POINTER;

	m_critical->EnterCritical();
	m_list->DeleteList();
	m_critical->LeaveCritical();
//  m_event->SetEvent();// Roman 22-11-11
	return RetCode;
}


// poluchit' chislo elementov v spiske
int CMessageList::GetCount(ULONG* count)
{
	if((m_critical == NULL) || (m_list == NULL) || (count == NULL))
		return NULL_POINTER;

	m_critical->EnterCritical();
	*count = m_list->GetCount();
	m_critical->LeaveCritical();

	return TYPE_OK;
}

BOOL CMessageList::IfFull()
{
	ULONG        Count = 0;

	if((m_critical == NULL) || (m_list == NULL) )
	   return TRUE;

	if(m_MaxCount == -1)
	   return FALSE;

        GetCount(&Count);

	if(Count+1 > m_MaxCount )
           return TRUE;
	else
           return FALSE;
}


// dobavit' soobshenie
int CMessageList::AddMessage( BYTE*  msg, BOOL Flag)
{
	MsgHeader*   Header = (MsgHeader*)msg;
	int          RetCode = TYPE_OK;
	ULONG        Count = 0;


	if(m_criticalSend->IfCriticalLock())
	{
		return ERL_LOCKED;
	}

	if((m_critical == NULL) || (m_list == NULL) || (msg == NULL))
		return NULL_POINTER;

	if(Header->Size < MSG_HEADER)
		return ZERO_SIZE;
//	if((Header->Size - (sizeof(GHEADER)+Header->Count*sizeof(Addr))) > MAX_TRANSPORT_PACKET_SIZE)
//	        return ERROR_SIZE;

//	if(m_MaxCount != -1)
//	{
//	   GetCount(&Count);
//	   if(Count+1 > m_MaxCount )
//          return ERL_FULL;
//	}

	m_critical->EnterCritical();
	if(m_MaxCount != -1)
	{
	   if(m_list->GetCount()+1 > m_MaxCount )
	   {
	       m_critical->LeaveCritical();
               return ERL_FULL;
	   }
	}

        RetCode = m_list->AddItem((void*) msg, Header->Size, Flag, Header->Priority);
	m_critical->LeaveCritical();

	m_event->SetEvent();

	return RetCode;
}


// zabrat' soobshenie
int CMessageList::GetMessage( BYTE** msg)
{
	int          RetCode = TYPE_OK;
	ULONG        Size = 0;

	if((m_critical == NULL) || (m_list == NULL) || (msg == NULL))
		return NULL_POINTER;

	  m_critical->EnterCritical();
      RetCode = m_list->GetTop((void**) msg, &Size);
	  m_critical->LeaveCritical();

	return RetCode;
}


int CMessageList::LockMessageList()
{
	if(m_criticalSend == NULL)
		return NULL_POINTER;

	 m_criticalSend->EnterCritical();
	 m_critical->EnterCritical();

	return TYPE_OK;
}


int CMessageList::UnLockMessageList()
{
	if(m_criticalSend == NULL)
		return NULL_POINTER;

	if(m_criticalSend->IfCriticalLock())
			m_criticalSend->LeaveCritical();

	if(m_critical->IfCriticalLock())
			m_critical->LeaveCritical();

	return TYPE_OK;
}
