
#include "CModiServer.h"



CModiServer::CModiServer()
{

}


CModiServer::~CModiServer()
{
//   printf("~CModiServer\n");
}


int CModiServer::Create()
{
    return CServerQueue::Create( MODI_SERVER );
 //  printf("CModiServer\n");
 //  return TYPE_OK;
}


int CModiServer::OnModiMessages(MsgHeader* Header)
{
	int RetCode = TYPE_OK;

	if(Header == NULL)
		return NULL_POINTER;


	switch(Header->Packet)
	{
      // FROM FK
      //------------
	case MFK_PARAM_MODI:
		{// parametri MODI
			MODI_PARAMETERS *Param = NULL;

			if((Header->Size - MSG_HEADER) < MODI_PARAM_SIZE)
				return ZERO_SIZE;

            Param = (MODI_PARAMETERS*)(((BYTE*)Header) + MSG_HEADER);

			OnFkParameters(Param);
			break;
		}
	case MFK_STOP_MODI:
		{// ostanov MODI
			OnFkCommandStop();
			break;
		}
	case MFK_RESTART_MODI:
		{// restart MODI
			OnFkCommandRestart();
			break;
		}
	case MFK_LOCK_STAT_MODI:
		{// zapret zapisi v strukturu statistiki
			OnFkLockStat();
			break;
		}
	case MFK_UNLOCK_STAT_MODI:
		{// zapret zapisi v strukturu statistiki
			OnFkUnLockStat();
			break;
		}
	case MFK_CREATE_STAT_MODI:
		{// razreshenie zapisi v strukturu statistiki
			STAT_INFO *StatInfo = NULL;

			if((Header->Size - MSG_HEADER) < STAT_INFO_SIZE)
				return ZERO_SIZE;

            StatInfo = (STAT_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnFkCreateStat(StatInfo);
			break;
		}
	case MFK_SET_REMOTE_IP_MODI:
		{// 
		        BYTE  *Array = (BYTE*)(((BYTE*)Header) + MSG_HEADER);
			OnSetRemoteIP(Array, Header->Size - MSG_HEADER);
			break;
		}
	case MFK_SET_LOCAL_ABN_MODI:
		{// 
		     char  *Array = (char*)(((BYTE*)Header) + MSG_HEADER);
//			if(((BYTE)(Array + (Header->Size - MSG_HEADER))) == 0)
			        OnSetAccessMatrix(Array);
			break;
		}
     // FROM TM
     //------------
	case MTM_START_MODI:
		{// nachalo raboti TM
			OnTmStart();
			break;
		}
	case MTM_STOP_MODI:
		{// zavershenie raboti TM
			OnTmStop();
			break;
		}
	case MTM_CONN_ON_MODI:
		{// ustanovlenie soedineniya po napravleniu
			LINE_INFO  *LineInfo = NULL;

			if((Header->Size - MSG_HEADER) < LINE_INFO_SIZE)
				return ZERO_SIZE;

            LineInfo = (LINE_INFO*)(((BYTE*)Header) + MSG_HEADER);

            OnTmConnectLine(LineInfo);
			break;
		}
	case MTM_CONN_OFF_MODI:
		{// razriv soedineniya po napravleniu
			LINE_INFO  *LineInfo = NULL;

			if((Header->Size - MSG_HEADER) < LINE_INFO_SIZE)
				return ZERO_SIZE;

            LineInfo = (LINE_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnTmDsConnectLine(LineInfo);
			break;
		}
  case MTM_GET_ADDR_MODI:
    {
      OnGetAccessMatrix();
      break;
    }
   case MTM_ABN_CONN_MODI:
    {
       ABONENT_INFO* AbnInfo = NULL;

			 if((Header->Size - MSG_HEADER) < ABONENT_INFO_SIZE)
				    return ZERO_SIZE;

       AbnInfo = (ABONENT_INFO*)(((BYTE*)Header) + MSG_HEADER);
       OnAbonentConnect(AbnInfo);
       break;
    }
   case MTM_ABNBUS_CONN_MODI:
    {
       ABONENT_INFO* AbnInfo = NULL;

			 if((Header->Size - MSG_HEADER) < ABONENT_INFO_SIZE)
				    return ZERO_SIZE;

       AbnInfo = (ABONENT_INFO*)(((BYTE*)Header) + MSG_HEADER);
       OnAbonentBusConnect(AbnInfo);
       break;
    }
	}

	return RetCode;
}




// FROM FK
//------------
// parametri MODI
void CModiServer::OnFkParameters(MODI_PARAMETERS *Param)
{

}

// ostanov MODI
void CModiServer::OnFkCommandStop()
{

}

// restart MODI
void CModiServer::OnFkCommandRestart()
{

}

// zapret zapisi v strukturu statistiki
void CModiServer::OnFkLockStat()
{

}

// zapret zapisi v strukturu statistiki
void CModiServer::OnFkUnLockStat()
{

}

// razreshenie zapisi v strukturu statistiki
void CModiServer::OnFkCreateStat(STAT_INFO *StatInfo)
{

}


void CModiServer::OnSetAccessMatrix(char *Path)
{

}


void CModiServer::OnSetRemoteIP(BYTE *Array, ULONG Size)
{

}



// FROM TM
//------------
// nachalo raboti TM
void CModiServer::OnTmStart()
{

}

// zavershenie raboti TM
void CModiServer::OnTmStop()
{

}

// ustanovlenie soedineniya po napravleniu
void CModiServer::OnTmConnectLine(LINE_INFO *LineInfo)
{

}

// razriv soedineniya po napravleniu
void CModiServer::OnTmDsConnectLine(LINE_INFO *LineInfo)
{

}


// ������ �������� �������
void CModiServer::OnGetAccessMatrix()
{

}


// �����������/���������� �������
void CModiServer::OnAbonentConnect(ABONENT_INFO* AbnInfo)
{

}


void CModiServer::OnAbonentBusConnect(ABONENT_INFO* AbnInfo)
{

}


int CModiServer::SendMessageToFk(ULONG IdMessage, BYTE* Packet, ULONG Size)
{
	return CServerQueue::SendMessage(FK_SERVER, IdMessage, Packet, Size);
}


int CModiServer::SendMessageToTm(ULONG IdMessage, BYTE* Packet, ULONG Size)
{
	return CServerQueue::SendMessage(TM_SERVER, IdMessage, Packet, Size);
}







// ���������� ���������
//========================
// TO FK
//------------
// zapros parametrov MODI
int CModiServer::MSG_GetParam()
{   
	return SendMessageToFk(MMODI_PARAM_FK, NULL, 0);
}


// zavershenie raboti MODI
int CModiServer::MSG_Stop()
{   
	return SendMessageToFk(MMODI_STOP_FK, NULL, 0);
}

// nachalo raboti MODI
int CModiServer::MSG_Start()
{   
	return SendMessageToFk(MMODI_START_FK, NULL, 0);
}


// poluchena adresnaya tablica
int CModiServer::MSG_RecAddr()
{   
	return SendMessageToFk(MMODI_REC_ADDR_FK, NULL, 0);
}

// avariynoe zavershenie raboti
int CModiServer::MSG_Error(ERROR_INFO *Error)
{   
	if(Error == NULL)
		return NULL_POINTER;

	return SendMessageToFk(MMODI_ERROR_FK, (BYTE*)Error, ERROR_INFO_SIZE);
}


int CModiServer::MSG_StatusLine( NETWORK_INFO *NetInfo )
{
	if(NetInfo == NULL)
		return NULL_POINTER;

	return SendMessageToFk(MMODI_STATUS_LINE_FK, (BYTE*)NetInfo, NETWORK_INFO_SIZE);
}


// ����� �� ��� ����������
//-------------------------
// poluchena adresnaya tablica
int CModiServer::MSG_CreateAddr(ADDR_INFO *AddrInfo, ULONG  AbnType)
{   
	if(AddrInfo == NULL)
		return NULL_POINTER;

   switch(AbnType)
   {
     case ABONENT_TM:
	        {
             return SendMessageToTm(MMODI_CREATE_ADDR_TM, (BYTE*)AddrInfo, ADDR_INFO_SIZE);
          }

     case ABONENT_FK:
	        {
             return SendMessageToFk(MMODI_CREATE_ADDR_FK, (BYTE*)AddrInfo, ADDR_INFO_SIZE);
          }
   }
//	SendMessageToFk(MMODI_CREATE_ADDR_FK, (BYTE*)AddrInfo, ADDR_INFO_SIZE);
//	return SendMessageToTm(MMODI_CREATE_ADDR_TM, (BYTE*)AddrInfo, ADDR_INFO_SIZE);
   return TYPE_OK;
}

// zapret raboti s adresnaya tablica
int CModiServer::MSG_LockAddr()
{
//	SendMessageToFk(MMODI_LOCK_ADDR_FK, NULL, 0);

	return SendMessageToTm(MMODI_LOCK_ADDR_TM, NULL, 0);
}


// razreshenie raboti s adresnaya tablica
int CModiServer::MSG_UnLockAddr()
{
//	SendMessageToFk(MMODI_UNLOCK_ADDR_FK, NULL, 0);

	return SendMessageToTm(MMODI_UNLOCK_ADDR_TM, NULL, 0);
}


int CModiServer::MSG_GetAccessMatrix()
{
	return SendMessageToFk(MMODI_GET_LOCAL_ABN_FK, NULL, 0);

}


int CModiServer::MSG_GetRemoteIP()
{
	return SendMessageToFk(MMODI_GET_REMOTE_IP_FK, NULL, 0);


}


