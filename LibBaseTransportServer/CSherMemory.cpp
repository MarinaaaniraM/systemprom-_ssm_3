//=======================================================================================
//			Obshaya pamiat' 
//
//=======================================================================================

#include "CSherMemory.h"



CSherMemory::CSherMemory( )
{
	 m_key = 0;
	 m_flags =  DIR_MODE;
	 m_mode = IPC_CREAT|IPC_EXCL;
	 m_ptr = NULL;
	 m_size = sizeof(MemInfo);
	 m_Ptr = NULL;
	 m_srv = FALSE;
	 m_info = NULL;
	 m_ID = -1;
	 m_semId = -1;
}


CSherMemory::~CSherMemory()
{


    if( m_srv )
	{
	   if(m_semId != -1)
	    semctl(m_semId, 0, IPC_RMID);
	   if(m_ptr != NULL)
     	     shmdt((void*)m_ptr);
	   shmctl(m_ID, IPC_RMID, 0);
	}
	else
	{
	   if(m_ptr != NULL)
	     shmdt((void*)m_ptr);
	}

}


// sozdanie i otkritie oblasty
int CSherMemory::Create( key_t  key, ULONG  Size )
{
	int    Fd;


	if(Size == 0)
		return ZERO_SIZE;
	else
		m_size += Size;

	m_key = key;

	if((m_ID = shmget(m_key, m_size, 0)) == -1)
	{ 
		  if((m_ID = shmget(m_key, m_size, m_mode|m_flags)) == -1)
			   return  ERM_OPEN;
		m_srv = TRUE;
	}
  else
  {
		m_srv = FALSE;
  }
	if((m_ptr = (BYTE*)shmat(m_ID,NULL,0)) == (void*)-1)
		return ERM_MAP;



        m_info = (MemInfo*)m_ptr;
	m_Ptr = m_ptr + sizeof(MemInfo);

  if(m_srv)
	{
	  memset(m_Ptr, 0, Size);
        }  

    if( m_srv )
	{
	    if((m_semId = semget(m_key,1,m_flags|m_mode)) == -1)
		return ERM_OPEN;
	    m_semarg.val = 1;
	    semctl(m_semId,0,SETVAL,m_semarg);
	    
	}
	else
	{
	    if((m_semId = semget(m_key,1,m_flags)) == -1)
		return ERM_OPEN;
	
	}
	
        m_semwait.sem_num = 0;
	m_semwait.sem_op = -1;
	m_semwait.sem_flg = SEM_UNDO;
	    
	m_sempost.sem_num = 0;
	m_sempost.sem_op = 1;
	m_sempost.sem_flg = SEM_UNDO;

	return TYPE_OK;
}






// sozdanie i otkritie oblasty
int CSherMemory::_Create( key_t  key, ULONG  Size, BOOL Server )
{
	int    Fd;


	if(Size == 0)
		return ZERO_SIZE;
	else
		m_size += Size;

	m_key = key;
	
	m_srv = Server;
	
	if(m_srv)
	{// server
	   if((m_ID = shmget(m_key, m_size, m_mode|m_flags)) == -1)
	        return  ERM_OPEN;
	   if((m_ptr = (BYTE*)shmat(m_ID,NULL,0)) == (void*)-1)
		return ERM_MAP;

           m_info = (MemInfo*)m_ptr;
	   m_Ptr = m_ptr + sizeof(MemInfo);
	   memset(m_Ptr, 0, Size);
	   
	   if((m_semId = semget(m_key,1,m_flags|m_mode)) == -1)
		return ERM_OPEN;
	   m_semarg.val = 1;
	   semctl(m_semId,0,SETVAL,m_semarg);
	}
	else
	{//client
	   if((m_ID = shmget(m_key, m_size, m_flags)) == -1)
	        return  ERM_OPEN;
	   if((m_ptr = (BYTE*)shmat(m_ID,NULL,0)) == (void*)-1)
		return ERM_MAP;

           m_info = (MemInfo*)m_ptr;
	   m_Ptr = m_ptr + sizeof(MemInfo);
	   
	    if((m_semId = semget(m_key,1,m_flags)) == -1)
		return ERM_OPEN;
	}

	
        m_semwait.sem_num = 0;
	m_semwait.sem_op = -1;
	m_semwait.sem_flg = SEM_UNDO;
	    
	m_sempost.sem_num = 0;
	m_sempost.sem_op = 1;
	m_sempost.sem_flg = SEM_UNDO;

	return TYPE_OK;
}



// blokirovka oblasti
void CSherMemory::MemLock()
{
	semop(m_semId, &m_semwait,1);
}


void CSherMemory::MemUnLock()
{
	semop(m_semId, &m_sempost,1);
}


