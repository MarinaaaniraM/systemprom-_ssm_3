
#include <stdio.h>
#include "CTmServer.h"



CTmServer::CTmServer()
{

}


CTmServer::~CTmServer()
{
// printf("enter to CTmServer destructor...\n");
}


int CTmServer::Create()
{
	return CServerQueue::Create( TM_SERVER );
}


int CTmServer::OnTmMessages(MsgHeader* Header)
{
	int RetCode = TYPE_OK;

	if(Header == NULL)
		return NULL_POINTER;


	switch(Header->Packet)
	{
      // FROM FK
      //------------
	case MFK_PARAM_TM:
		{// parametri TM
			TM_PARAMETER *Param = NULL;

			if((Header->Size - MSG_HEADER) < TM_PARAM_SIZE)
				return ZERO_SIZE;

            Param = (TM_PARAMETER*)(((BYTE*)Header) + MSG_HEADER);

			OnFkParameters(Param);
			break;
		}
	case MFK_STOP_TM:
		{// ostanov TM
			OnFkCommandStop();
			break;
		}
	case MFK_RESTART_TM:
		{// restart TM
			OnFkCommandRestart();
			break;
		}
	case MFK_LOCK_STAT_TM:
		{// zapret zapisi v strukturu statistiki
			OnFkLockStat();
			break;
		}
	case MFK_RELOAD_ROUTE:
		{
		         OnFkReLoadRoute();
		         break;
		}
	case MFK_UNLOCK_STAT_TM:
		{// razreshenie zapisi v strukturu statistiki
			OnFkUnLockStat();
			break;
		}
	case MFK_CREATE_STAT_TM:
		{// sozdanie strukturi statistiki
			ADDR_INFO *StatInfo = NULL;

			if((Header->Size - MSG_HEADER) < ADDR_INFO_SIZE)
				return ZERO_SIZE;

            StatInfo = (ADDR_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnFkCreateStat(StatInfo);
			break;
		}
	case MFK_STOP_LINE_TM:
		{// ostanov modulia napravleniia
			LINE_INFO  *LineInfo = NULL;

			if((Header->Size - MSG_HEADER) < LINE_INFO_SIZE)
				return ZERO_SIZE;

            LineInfo = (LINE_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnFkCommandStopLine(LineInfo);
			break;
		}
	case MFK_START_LINE_TM:
		{// zapusk modulia napravleniia
			LINE_INFO  *LineInfo = NULL;

			if((Header->Size - MSG_HEADER) < LINE_INFO_SIZE)
				return ZERO_SIZE;

                        LineInfo = (LINE_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnFkCommandStartLine(LineInfo);
			break;
		}
     // FROM TM
     //------------
	case MMODI_CREATE_ADDR_TM:
		{// sozdanie adresnoy tablici
			ADDR_INFO  *AddrInfo = NULL;

			if((Header->Size - MSG_HEADER) < ADDR_INFO_SIZE)
				return ZERO_SIZE;

                        AddrInfo = (ADDR_INFO*)(((BYTE*)Header) + MSG_HEADER);

			OnModiCreateAddr(AddrInfo);
			break;
		}
	case MMODI_LOCK_ADDR_TM:
		{// zapret raboti s adresnaya tablica
			OnModiLockAddr();
			break;
		}
	case MMODI_UNLOCK_ADDR_TM:
		{// razreshenie raboti s adresnaya tablica
			OnModiUnLockAddr();
			break;
		}
	}

	return RetCode;
}



// FROM FK
//------------
// parametri TM
void CTmServer::OnFkParameters(TM_PARAMETER *Param)
{

}

// ostanov TM
void CTmServer::OnFkCommandStop()
{

}

// restart TM
void CTmServer::OnFkCommandRestart()
{

}

// zapret zapisi v strukturu statistiki
void CTmServer::OnFkLockStat()
{

}

// razreshenie zapisi v strukturu statistiki
void CTmServer::OnFkUnLockStat()
{

}

// sozdanie strukturi statistiki
void CTmServer::OnFkCreateStat(ADDR_INFO *StatInfo)
{

}

// ostanov modulia napravleniia
void CTmServer::OnFkCommandStopLine(LINE_INFO *LineInfo)
{

}

// zapusk modulia napravleniia
void CTmServer::OnFkCommandStartLine(LINE_INFO *LineInfo)
{

}


// FROM MODI
//------------
// sozdanie adresnoy tablici
void CTmServer::OnModiCreateAddr(ADDR_INFO *AddrInfo)
{

}

// zapret raboti s adresnaya tablica
void CTmServer::OnModiLockAddr()
{

}

// razreshenie raboti s adresnaya tablica
void CTmServer::OnModiUnLockAddr()
{

}


int CTmServer::SendMessageToFk(ULONG IdMessage, BYTE* Packet, ULONG Size)
{
	return CServerQueue::SendMessage(FK_SERVER, IdMessage, Packet, Size);
}


int CTmServer::SendMessageToModi(ULONG IdMessage, BYTE* Packet, ULONG Size)
{
	return CServerQueue::SendMessage(MODI_SERVER, IdMessage, Packet, Size);
}







// ���������� ���������
//========================
// TO FK
//------------
// zapros parametrov TM
int CTmServer::MSG_GetParam()
{
	return SendMessageToFk(MTM_PARAM_FK, NULL, 0);
}

int CTmServer::MSG_GetStatistics()
{
	return SendMessageToFk(MTM_GET_STAT_FK, NULL, 0);
}

// komanda restart TM OK
int CTmServer::MSG_RestartOk()
{
	return SendMessageToFk(MTM_RESTART_FK, NULL, 0);
}

// ostanov modulia napravleniia
int CTmServer::MSG_StopLine(LINE_INFO *LineInfo)
{
	if(LineInfo == NULL)
		return NULL_POINTER;

	return SendMessageToFk(MTM_STOP_LINE_FK, (BYTE*)LineInfo, LINE_INFO_SIZE);
}

// zapusk modulia napravleniia
int CTmServer::MSG_StartLine(LINE_INFO *LineInfo)
{
	if(LineInfo == NULL)
		return NULL_POINTER;

	return SendMessageToFk(MTM_START_LINE_FK, (BYTE*)LineInfo, LINE_INFO_SIZE);
}

// avariynoe zavershenie raboti
int CTmServer::MSG_Error(ERROR_INFO *Error)
{
	if(Error == NULL)
		return NULL_POINTER;

	return SendMessageToFk(MTM_ERROR_FK, (BYTE*)Error, ERROR_INFO_SIZE);
}


// ����� �� ��� ����������
//-------------------------
// nachalo raboti TM
int CTmServer::MSG_Start()
{
	SendMessageToFk(MTM_START_FK, NULL, 0);

	return SendMessageToModi(MTM_START_MODI, NULL, 0);
}

// zavershenie raboti TM
int CTmServer::MSG_Stop()
{
	SendMessageToFk(MTM_STOP_FK, NULL, 0);

	return SendMessageToModi(MTM_STOP_MODI, NULL, 0);
}

int CTmServer::MSG_GetAddr()
{
	return SendMessageToModi(MTM_GET_ADDR_MODI, NULL, 0);
}


// ustanovlenie soedineniya po napravleniu
int CTmServer::MSG_ConnectLine(LINE_INFO *LineInfo)
{
	if(LineInfo == NULL)
		return NULL_POINTER;

	SendMessageToFk(MTM_CONN_ON_FK, (BYTE*)LineInfo, LINE_INFO_SIZE);

	return SendMessageToModi(MTM_CONN_ON_MODI, (BYTE*)LineInfo, LINE_INFO_SIZE);
}

// razriv soedineniya po napravleniu
int CTmServer::MSG_DsConnectLine(LINE_INFO *LineInfo)
{
	if(LineInfo == NULL)
		return NULL_POINTER;

	SendMessageToFk(MTM_CONN_OFF_FK, (BYTE*)LineInfo, LINE_INFO_SIZE);

	return SendMessageToModi(MTM_CONN_OFF_MODI, (BYTE*)LineInfo, LINE_INFO_SIZE);
}


// �����������/���������� ��������
int CTmServer::MSG_AbonentConnect(Addr *adr, BOOL IfConnect /*= TRUE*/, BOOL IfRemote/* = FALSE*/)
{
  ABONENT_INFO		AbnInfo;

	if(adr == NULL)
		return NULL_POINTER;

   AbnInfo.m_adr = *adr;
   AbnInfo.m_IfConnect = IfConnect;
   AbnInfo.m_IfRemote  =  IfRemote;

  SendMessageToFk(MTM_ABN_CONN_FK, (BYTE*)&AbnInfo, ABONENT_INFO_SIZE);
	return SendMessageToModi(MTM_ABN_CONN_MODI, (BYTE*)&AbnInfo, ABONENT_INFO_SIZE);
}


// ���������� ������ ������� �������������
void CTmServer::OnFkReLoadRoute()
{

}
