// ObjList.cpp: implementation of the CObjList class.
//
//////////////////////////////////////////////////////////////////////

#include "CList.h"


CList::CList()
{
	m_bot = m_top = NULL;
	m_count = 0;
	m_next = NULL;
}

CList::~CList()
{
	if(m_count != 0)
		DeleteList();
//	printf("List deleted\n");	
}

// dobavit' element v spisok
int CList::AddItem(void* item, ULONG size, BOOL flag ,BYTE Prior)
{
	CItem*       newItem = NULL;
	int          RetCode = TYPE_OK;
//	LONG         Id;

	if(item == NULL) 
		return NULL_POINTER;
	if(size == 0) 
		return ZERO_SIZE;

	if((newItem = new CItem()) == NULL)
		return EMPTY_MEMORY;

	if((RetCode = newItem->Create((BYTE*)item, size, flag,Prior)) != TYPE_OK)
		return RetCode;

	if(m_count == 0)
	{
		m_bot = m_top = newItem;
		m_count = 1;
		m_next = NULL;
	}
	else
	{
	   if(newItem->m_Prior == 3)
	   {
		m_bot->SetBottom(newItem);
		newItem->SetTop(m_bot);
		m_bot = newItem;
		m_count++;
		m_next = NULL;
		RetCode = TYPE_OK;
	   }
	   else
	     RetCode = Insert(newItem);
        }
	return RetCode;
}

int CList::Insert(CItem* Item)
{
  int      RetCode = TYPE_OK;
  CItem*   item = m_top;
  CItem*   after = NULL;
  CItem*   next = NULL;
  
  if(Item == NULL)
     return NULL_POINTER;
     
  if(item == NULL)
  {//spisok pust
     m_bot = m_top = Item;
     m_count = 1;
     m_next = NULL;
     return RetCode;
  }     
     
  while(item != NULL)
  {
//  printf("P1 = %d  P2 = %d\n",item->m_Prior, Item->m_Prior);
	if(item->m_Prior <= Item->m_Prior) 
	{// prioritet vishe nashego
//	printf("#1\n");
	   after = item;
	   item = item->GetBot();
	   continue;
	}
	else 
	{//visokie prioritet' zakonchilis'
//	printf("#2\n");
	   break;
	}
//	  item = item->GetBot();
   }
// 	printf("#3\n");
  
   if((after == NULL) && (item == NULL))
   { //dobavliaem v conec
// 	printf("#4\n");
	m_bot->SetBottom(Item);
	Item->SetTop(m_bot);
	m_bot = Item;
	m_count++;
	m_next = NULL;
	return RetCode;
   }
  
   if((after == NULL) && (item != NULL))
   {
        if((next = item->GetTop()) == NULL)
	{// dobavliaem pervim
// 	printf("#5\n");
	  m_top->SetTop(Item);
	  Item->SetBottom(m_top);
//	  Item->SetTop(NULL);
	  m_top = Item;
	  m_count++;
	  m_next = NULL;
	  return RetCode;
	}
	else
	{
// 	printf("#6\n");
	  next->SetBottom(Item);
	  item->SetTop(Item);
	  Item->SetTop(next);
	  Item->SetBottom(item);
	  m_count++;
	  m_next = NULL;
	  return RetCode;
	}
   }
   if((next = after->GetBot()) == NULL)
   {// posledniy v soiske
// 	printf("#7\n");

	m_bot->SetBottom(Item);
	Item->SetTop(m_bot);
	m_bot = Item;
	m_count++;
	m_next = NULL;
	return RetCode;
   }
// 	printf("#8\n");
   
   Item->SetTop(after);
   Item->SetBottom(next);
   after->SetBottom(Item);
   next->SetTop(Item);
   m_count++;
   m_next = NULL;
   return RetCode;
}


UALONG CList::GetNumItem(void * Item)
{
	if(Item == NULL) return 0;

	 CItem  *item=m_top;
	 while(item!=NULL)
	 {
		 if(item->m_Item == Item) return item->m_ID;
		 item=item->m_bot;
	 }
	 return 0;
}

int CList::DeleteItem(void* Item)
{
	
	if(Item == NULL)
		return NULL_POINTER;

	return DeleteItem(GetNumItem(Item));
}

// udalit' element spiska
int CList::DeleteItem(UALONG ID)
{
	CItem* item = NULL;
	CItem* top = NULL;
	CItem* bot = NULL;

	if((item = FindItem(ID)) == NULL)
		return NULL_POINTER;

	top = item->GetTop();
	bot = item->GetBot();

	if(top == NULL && bot != NULL)
	{// perviy element
		bot->SetTop(NULL);
		m_top = bot;
	}
	if(top != NULL && bot == NULL)
	{// v konce spiska
		top->SetBottom(NULL);
		m_bot = top;
	}
	if(top == NULL && bot == NULL)
	{// edinstvenniy element spiska
		m_top = m_bot = NULL;
	}
	if(top != NULL && bot != NULL)
	{// v seredine spiska
		bot->SetTop(top);
		top->SetBottom(bot);
	}

	if(m_next == item)
		m_next = item->GetBot();

	delete item;
	m_count--;

	return TYPE_OK;
}

// naiti element spiska
CItem* CList::FindItem(UALONG ID)
{
	CItem* item = m_top;
	while(item != NULL)
	{
		if(item->m_ID == ID) 
			return item;
		else 
			item = item->GetBot();
	}
	return NULL;
}

void CItem::SetBottom(CItem *item)
{
	m_bot = item;
}

void  CItem::SetTop(CItem *item)
{
	m_top = item;
}


// udalit' perviy
int CList::DelTop()
{
	if(m_count == 0)
		return ERL_EMPTY;

	if(m_top == NULL)
		return NULL_POINTER;

	if(m_count == 1)
	{
		if(m_top != NULL)
		{
			delete m_top;
			m_top = m_bot = NULL;
			m_count = 0;
			m_next = NULL;
		}
	}
	else
	{// > 1
		CItem* topItem = m_top;
		m_top = m_top->GetBot();

		if(m_top == NULL)
			return NULL_POINTER;

		m_top->SetTop(NULL);

		if(m_next == topItem)
			m_next = m_top;

		m_count--;
		delete topItem;
	}

	return TYPE_OK;
}

// udalit' posledniy
int CList::DelBot()
{

	if(m_count == 0)
		return ERL_EMPTY;

	if(m_bot == NULL)
		return NULL_POINTER;


	if(m_count == 1)
	{
		if(m_bot != NULL)
		{
			delete m_bot;
			m_top = m_bot = NULL;
			m_count = 0;
			m_next = NULL;
		}
	}
	else
	{
		CItem* botItem = m_bot;
		m_bot = m_bot->GetTop();

		if(m_bot == NULL)
			return NULL_POINTER;

		if(m_next == botItem)
			m_next = NULL;

		m_bot->SetBottom(NULL);
		m_count--;
		delete botItem;
	}

	return TYPE_OK;
}

// zabrat' element iz spiska
int CList::GetItem(UALONG ID, void** item, ULONG* size, BOOL  Flag)
{
	CItem* Item = NULL;

	if((item == NULL) || (size == NULL))
		return NULL_POINTER;

	if((Item = FindItem(ID)) == NULL)	
		return NULL_POINTER;

	*size = Item->GetSize();
        if(!Item->m_flag)
	{// eto blok pamiati
	    if((*item =(void*) new BYTE[*size]) == NULL) 
		return  EMPTY_MEMORY;

	    memcpy(*item,Item->GetItemPtr(),*size);
//	   *item = Item->GetItemPtr();
	}
	else// eto ukazatel	
	   *item = Item->GetItemPtr();
	   
//	*size = Item->GetSize();
	if(Flag)	
		return TYPE_OK;
	else
	    return DeleteItem(ID);
}

// sdelat' kopiy elementa
int CList::CopyItem(UALONG ID, void** item, ULONG* size)
{
	CItem* Item = NULL;

	if((item == NULL) || (size == NULL))
		return NULL_POINTER;

	if((Item = FindItem(ID)) == NULL)
		return NULL_POINTER;

	*size = Item->GetSize();

	if((*item =(void*) new BYTE[*size]) == NULL) 
		return  EMPTY_MEMORY;

	memcpy(*item,Item->GetItemPtr(),*size);

	return TYPE_OK;
}



int CList::GetTop(void** item, ULONG* size)
{
	int         RetCode = TYPE_OK;
	
	if(m_count == 0)
	{
		*item = NULL;
		return ERL_EMPTY;
	}
	if(m_top == 0) 	
	{
		*item = NULL;
	    return NULL_POINTER;
	}

	if((RetCode = GetItem(m_top->m_ID, item, size, FALSE)) != TYPE_OK)
		return RetCode;

	return TYPE_OK;
}

// poluchit' ukazatel' na perviy element ne zabiraya ego
int CList::GetFirst(void** item, ULONG* size)
{
	int      RetCode = TYPE_OK;

	m_next = m_top;
	if(m_count == 0) 
	{
		*item = NULL;
		*size = 0;
		return ERL_EMPTY;
	}
	if(m_next == 0) 	
	{
		*item = NULL;
		*size = 0;
		return NULL_POINTER;
	}

	if((RetCode = GetItem(m_next->m_ID, item, size, TRUE)) != TYPE_OK)
		return RetCode;

	m_next = m_top->m_bot;

	return TYPE_OK;
}


// poluchit' ukazatel' na sleduyshiy element ne zabiraya ego
int CList::GetNext(void** item, ULONG* size)
{
	int         RetCode = TYPE_OK;

	if(m_next == NULL)
	{
		*item = NULL;
		*size = 0;
		return ERL_END;
	}

	if(m_count == 0)
	{
		m_next = NULL;
		*item = NULL;
		*size = 0;
		return ERL_EMPTY;
	}

	if((RetCode = GetItem(m_next->m_ID, item, size, TRUE)) != TYPE_OK)
		return RetCode;

	m_next = m_next->m_bot;

	return TYPE_OK;
}



//udalit' spisok
void CList::DeleteList()
{
	CItem* item = m_top;
	CItem* Item = item;

	while(item != NULL)
	{
//	     printf("Delete Item\n");
		Item = item;
		item = item->GetBot();
		DeleteItem(Item->m_ID);
	}
	m_top = m_bot = m_next = NULL;
	m_count = 0;
//	printf("Delete List ok\n");
}
