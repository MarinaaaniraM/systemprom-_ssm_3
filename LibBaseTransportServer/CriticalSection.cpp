// []------------------------------------------------------------------------[]
//  |      ������� ����������� ������ (������ ������ ��������)               |
//  |          1/02/2005                                                     |
// []------------------------------------------------------------------------[]

#include "CriticalSection.h"

// �����������
CCriticalSection::CCriticalSection()
{
    m_lock = NULL;
//	m_server = TRUE;
//	m_global = FALSE;
}

// ����������
CCriticalSection::~CCriticalSection()
{

//	if(!m_global)
//	{
	    if(m_lock != NULL)
		{
            pthread_mutex_destroy(m_lock);
            delete m_lock;
            m_lock = NULL;
		}
//	}
//	else
//	{
//	  if(m_server)
//	  {
//           pthread_mutexattr_destroy(&m_attr);
//           pthread_mutex_destroy(m_lock);
//	  }
//	}
}

// ��������
//int CCriticalSection::Create(pthread_mutex_t* Lock, BOOL  Server)
int CCriticalSection::Create()
{
//	if(Lock == NULL )
//	{
//		m_server = TRUE;
//		m_lock = NULL;
//		m_global = FALSE;
//	}
//    else
//	{
//	    m_server = Server;
//	    m_lock = Lock;
//		m_global = TRUE;
//	}

//	if(m_lock == NULL) 
//	{// ������� ��������� ������
	    if((m_lock = new pthread_mutex_t) == NULL)
		    	return EMPTY_MEMORY;
	    if(pthread_mutex_init(m_lock, NULL) != 0)
		{
             delete m_lock;
             m_lock = NULL;
             return ERC_INIT;
		}
		return TYPE_OK;
//	}


//	if(m_server)
//	{// �������������� ���������� ������
//		if(pthread_mutexattr_init(&m_attr) != 0)
//             return ERC_INIT;
//		if(pthread_mutexattr_setpshared(&m_attr, PTHREAD_PROCESS_SHARED) != 0)
//             return ERC_INIT;
//	    if(pthread_mutex_init(m_lock, &m_attr) != 0)
//             return ERC_INIT;
//
//         return TYPE_OK;
//	}


// return TYPE_OK;
	
}

// ���� � ������
void CCriticalSection::EnterCritical()
{ // ���� ��� ��������� ������
		if(m_lock == NULL) return;
	  pthread_mutex_lock(m_lock);
}

// ����� �� ������
void CCriticalSection::LeaveCritical()
{ // ���� ��� ��������� ������
		if(m_lock == NULL) return;
	  pthread_mutex_unlock(m_lock);
}

// �������� ��������� ������
BOOL CCriticalSection::IfCriticalLock()
{// ���� ��� ��������� ������
	 int   RetCode = 0;

//	  if(m_lock == NULL) 
//		  return TRUE;
     if((RetCode = pthread_mutex_trylock(m_lock)) == 0)
     {
	 pthread_mutex_unlock(m_lock);

 	 return FALSE; // ��������
     }

     if(RetCode == EBUSY)
	return TRUE;
    else
    {
        return RetCode; // ������ �� ������������
    }
}
