
#include "CPriorityMessageList.h"

CPriorityMessageList::CPriorityMessageList()
{
	m_PriorityList = NULL;
	m_DataList = NULL;
	m_Priority = -1;
	m_event = NULL;
}


CPriorityMessageList::~CPriorityMessageList()
{
    if(m_PriorityList != NULL)
	{
		delete m_PriorityList;
		m_PriorityList = NULL;
	}

    if(m_DataList != NULL)
	{
		delete m_DataList;
		m_DataList = NULL;
	}
    if(m_event != NULL)
    {
       delete m_event;
       m_event = NULL;
    }	
}

int  CPriorityMessageList::WaitForEvent(int Priority, ULONG  sec)
{
        int RetCode = TYPE_OK;
	
	if((m_PriorityList == NULL) && (m_DataList == NULL))
		return NULL_POINTER ;

	if(m_PriorityList == NULL)
			return m_DataList->WaitForEvent(Priority,sec);

        switch(Priority)
	{
	   case PARENT_MESSAGE_LIST:
	   {
	      RetCode = m_event->WaitForEvent(sec);
	      break;
	   }
	   case PRIORITY_MESSAGE_LIST:
	   { 
	      RetCode = m_PriorityList->WaitForEvent(Priority,sec);
	      break;
	   }
	   case DATA_MESSAGE_LIST:
	   {
	      RetCode = m_DataList->WaitForEvent(Priority,sec);
	      break;
	   }
	   default:
	   {
	      RetCode = m_event->WaitForEvent(sec);
	      break;
	   }	 	 
	}
	
        return RetCode;	
}

// sozdat' spisok
int CPriorityMessageList::Create(int Priority , int DataMaxCount, int PrMaxCount)
{
	int  RetCode = TYPE_OK;


    if((m_event = new CEvent()) == NULL)
		return EMPTY_MEMORY;
		
    if((RetCode = m_event->Create()) != TYPE_OK)
    {
       delete m_event;
       m_event = NULL;
    }

    if((m_DataList = new CMessageList()) == NULL)
    {
          delete m_event;
          m_event = NULL;
	  return EMPTY_MEMORY;
    }

	if((RetCode = m_DataList->Create()) != TYPE_OK)
	{
		delete m_DataList;
		m_DataList = NULL;

                delete m_event;
                m_event = NULL;

		return RetCode;
	}

	m_Priority = Priority;

	if(m_Priority == -1)
		return TYPE_OK;


    if((m_PriorityList = new CMessageList()) == NULL)
    {
		delete m_DataList;
		m_DataList = NULL;

                delete m_event;
                m_event = NULL;
		
		return EMPTY_MEMORY;
    }

	if((RetCode = m_PriorityList->Create()) != TYPE_OK)
	{
		delete m_PriorityList;
		m_PriorityList = NULL;
		delete m_DataList;
		m_DataList = NULL;

                delete m_event;
                m_event = NULL;

		return RetCode;
	}
    
	m_DataList->SetMaxCount(DataMaxCount);
	m_PriorityList->SetMaxCount(PrMaxCount);
    

	return TYPE_OK;
}


// udalit' spisok
int CPriorityMessageList::DeleteList()
{
	int  RetCode = TYPE_OK;

	if((m_PriorityList == NULL) && (m_DataList == NULL))
		return NULL_POINTER;

	if(m_PriorityList == NULL)
	      return m_DataList->DeleteList();

    if((RetCode = m_PriorityList->DeleteList()) != TYPE_OK)
		return RetCode;

	return m_DataList->DeleteList();
}


// poluchit' chislo elementov v spiske
int CPriorityMessageList::GetCount(ULONG* count)
{
        int RetCode = TYPE_OK;
	
	if(count == NULL) 
		return NULL_POINTER;

	if((m_PriorityList == NULL) && (m_DataList == NULL))
		return NULL_POINTER;
	
	
	RetCode = m_PriorityList->GetCount(count);
	if(*count == 0)
	  return m_DataList->GetCount(count);
	else
	  return RetCode;	
}



// poluchit' chislo elementov v spiske
int CPriorityMessageList::_GetCount(ULONG* count)
{
	if(count == NULL) 
		return NULL_POINTER;

	if((m_PriorityList == NULL) && (m_DataList == NULL))
		return NULL_POINTER;
	
	switch(*count)
	{
	case PRIORITY_MESSAGE_LIST:
		{
			if(m_PriorityList == NULL)
			{
				*count = 0;
				return TYPE_OK;
			}
			return m_PriorityList->GetCount(count);
		}
	case DATA_MESSAGE_LIST:
		{
			return m_DataList->GetCount(count);
		}
	default :
		{
			return m_DataList->GetCount(count);
		}
	}
}



// dobavit' soobshenie
int CPriorityMessageList::AddMessage( BYTE*  msg, BOOL Flag)
{
	MsgHeader*   Header = (MsgHeader*)msg;
	int          RetCode = TYPE_OK;

	if(msg == NULL)
		return NULL_POINTER;

	if((m_PriorityList == NULL) && (m_DataList == NULL))
		return NULL_POINTER;

	if(Header->Size < MSG_HEADER)
		return ZERO_SIZE;
		
	if((Header->Size - (sizeof(GHEADER)+Header->Count*sizeof(Addr))) > MAX_TRANSPORT_PACKET_SIZE)	
	        return ERROR_SIZE;
		

	if(Header->Protocol == (WORD)m_Priority)
	{
			if(m_PriorityList == NULL)
			{
			     if((RetCode = m_DataList->AddMessage(msg,Flag)) == TYPE_OK)
			        m_event->SetEvent();
			     return RetCode;
			}
			else
			{
			     if((RetCode = m_PriorityList->AddMessage(msg,Flag)) == TYPE_OK)
			        m_event->SetEvent();
			     return RetCode;
			}
	}
	else
	{
	    if((RetCode = m_DataList->AddMessage(msg,Flag)) == TYPE_OK)
            m_event->SetEvent();
         return RetCode;
	}

}


int CPriorityMessageList::GetPriorityMessage( BYTE** msg)
{
	if(msg == NULL)
		return NULL_POINTER;

	if(m_PriorityList == NULL) 
		return NULL_POINTER;

	return m_PriorityList->GetMessage(msg);
}

int CPriorityMessageList::GetDataMessage( BYTE** msg)
{
	if(msg == NULL)
		return NULL_POINTER;

	if(m_DataList == NULL)
		return NULL_POINTER;

	return m_DataList->GetMessage(msg);
}


// zabrat' soobshenie
int CPriorityMessageList::GetMessage( BYTE** msg)
{
	int          RetCode = TYPE_OK;
	ULONG        Size = 0;


	if(msg == NULL)
		return NULL_POINTER;

	if((m_PriorityList == NULL) && (m_DataList == NULL))
		return NULL_POINTER;

	if(m_PriorityList == NULL)
			return m_DataList->GetMessage(msg);

	RetCode = m_PriorityList->GetMessage(msg);

	switch(RetCode)
	{
	case ERL_EMPTY:
		{
			return m_DataList->GetMessage(msg);
		}

	case TYPE_OK:
		{
			return RetCode;
		}
	default:
		{
			return RetCode;
		}
	}
}
