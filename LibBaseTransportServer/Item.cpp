#include "CList.h"

CItem::CItem()
{
	m_top = m_bot = NULL;
	m_Item = NULL;
	m_size = m_ID = 0;
	m_flag = TRUE;
	m_Prior = 3;
}

CItem::~CItem()
{

	if((m_Item != NULL) && ( !m_flag ))
		delete[] m_Item;
}

int CItem::Create(BYTE* item, ULONG size , BOOL  flag ,BYTE Prior)
{
	if(item == NULL) 
		return NULL_POINTER; 

	if(size == 0) 
		return ZERO_SIZE;
	m_flag = flag;
	m_Prior = Prior;
	if(flag)
	{// zapominaem ukazatel'
		m_Item = item;
	}
	else
	{
		if((m_Item = new BYTE[size]) == NULL) return EMPTY_MEMORY;
		memcpy((void*)m_Item,(void*)item,size);
	}
	m_ID = (UALONG) this;
	m_size = size;

	return TYPE_OK;
}
