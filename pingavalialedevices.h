#ifndef PINGAVALIALEDEVICES_H
#define PINGAVALIALEDEVICES_H

#include <QObject>
#include <QVector>
#include <QMap>
#include <QDebug>
#include "ICMP_boost/ping.h"

class PingAvalialeDevices : public QObject
{
    Q_OBJECT
public:
    explicit PingAvalialeDevices(QObject *parent = 0);
    ~PingAvalialeDevices();

    QVector<QString>* getEnableAddresses()
    {
        return addresses;
    }
    void setAddress(QString addr)   // Диапазон адресов, в которых будем искать
    {
        strAddr.clear();
        strAddr.append(addr);
    }
    void startingPing();

public slots:
    void pinged(bool success, boost::asio::ip::icmp::endpoint endpoint);

private:
    boost::asio::io_service io_service;
    Pinger* pinger;

    QVector <QUuid>* addrUuid;
    QVector <QString>* addresses;   // Запсываем адреса, ответившие на пинг
    QString strAddr;

    void addNewHosts();     // Добавление порции адресов для пинга
    void deleteHosts();     // Удаление из массива пингов ненужные адреса    
};

#endif // PINGAVALIALEDEVICES_H

