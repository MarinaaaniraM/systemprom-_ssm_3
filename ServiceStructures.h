#ifndef SERVICESTRUCTURES_H
#define SERVICESTRUCTURES_H


#include <QString>
#include <QImage>


namespace ServiceStructures
{


///////////////////////////////////////////////////
// описание типа устройства
struct DeviceTypeInterface
{
    QString name;
    mutable QImage onlineImage;   // by the way, для простоты пусть готовая структура содержит иконки строго размером 32х32
    mutable QImage offlineImage;
    QString onlineImagePath;
    QString offlineImagePath;



    void reloadImages() const
    {
        onlineImage = QImage(onlineImagePath);
        offlineImage = QImage(offlineImagePath);
    }
};


///////////////////////////////////////////////////
// общие настройки программы
struct ProgramSettings
{
    bool scrollLogOutput;                    // прокручивать ли вывод лога?
    bool saveLogFileBeforeExit;              // сохранять ли файл лога перед завершением работы?
    bool askBeforeDeletion;                  // спрашивать ли "можно удалить?" перед удалением устройства/связи
    bool usingIdealThreadCount;              // использовать ли идеальное число тредов, или же анлим
    int timeToWaitForPingReplyModifyer;      // ждем ответа на опрос устройства в течение timeToRePing * timeToWaitForPingReplyModifyer
    int timeToRePing;                        // время, которое ждем перед тем, как снова начать пинговать устройство
};


///////////////////////////////////////////////////
// представление номера версии программы
struct VersionInfo
{
    int bigVerion; // первая цифра
    int mainVersion; // вторая цифра
    int subVersion; // третья цифра
    unsigned long buildNum;  // номер билда

    /////
    bool isValid()
    {
        return (bigVerion || mainVersion || subVersion);
    }
    /////
    bool operator == (const VersionInfo &other) const
    {
        return (!((other.bigVerion - bigVerion) || (other.mainVersion - mainVersion) || (other.subVersion - subVersion)));
    }
    /////
    bool operator != (const VersionInfo &other) const
    {
        return !(this->operator ==(other));
    }
    /////
    static QString toString(const VersionInfo &source)
    {
        return QString(QString::number(source.bigVerion, 10) + "." +
                       QString::number(source.mainVersion, 10) + "." +
                       QString::number(source.subVersion, 10));
    }
    /////
    static QString getBuildNum(const VersionInfo &source)
    {
        return QString::number(source.buildNum);
    }
};

//////////////////////////////////////////////////
// виды возможных сообщений лога
enum class LogMessageType
{
    normal = 0,
    success,
    warning,
    error
};

}


/* --------------------------------------------------------------
 *
 *                  темплейты и прочий сервис-скам
 *
 * -------------------------------------------------------------- */

// выбор сигнала для перегруженных сигналов
template <typename... Args> struct SELECT
{
    template<typename C, typename R>
    static constexpr auto OVERLOAD_OF( R (C::*pmf)(Args...) ) -> decltype(pmf)
    {
        return pmf;
    }
};



// оператор сравнения для DTI
bool operator == (const ServiceStructures::DeviceTypeInterface &dti1, const ServiceStructures::DeviceTypeInterface &dti2);



typedef QList<QString> CustomQStrList; // для передачи сигнала с таким параметром между тредами. Регистрируется в конструкторе главного интерфейса



namespace ServiceStructures
{

DeviceTypeInterface findInList(const QList<DeviceTypeInterface> *targetList, const QString &name,
                               const QString &onlineImagePath, const QString &offlineImagePath); // не найдем - вернем пустую

DeviceTypeInterface findInList(const QList<DeviceTypeInterface> *targetList, const QString &name); // same shit

}









#endif // SERVICESTRUCTURES_H
