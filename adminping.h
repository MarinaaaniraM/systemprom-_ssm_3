// Этот файл является частью проекта ПС "Конструктор".
// Время создания: Птн Сен 16 13:37:52 2011
// Реализация класса взаимодействия с сервером безопасности

#ifndef ADMINPING_H
#define ADMINPING_H

#include <QObject>
#include <QString>
#include <QHostAddress>
#include <QApplication>

#include <namedobject.h>
#include "include/CMsgSocket.h"
#include "include/CMessageList.h"

#define SECURITY_CONNECT        11000
#define SECURITY_REQUEST_ALL 	11001
#define SECURITY_REQUEST_SRC 	11002
#define SECURITY_REPLY          11003

class AdminPing : public QObject, public CMsgSocket
{
    Q_OBJECT
public:
    explicit AdminPing(QObject *parent = 0);

    int Create( QHostAddress ip, unsigned short port);
    int StateRequest( QHostAddress ip = QHostAddress(INADDR_ANY));

signals:
    void devicePingedSuccesessful(QHostAddress addr);

private:
    virtual BOOL OnReceive(BYTE* msg);
    virtual int OnConnect();
    virtual int OnDisconnect( int ErrCode);
    void ProcReply(GHEADER* hdr);
    CMessageList inL, outL;
};

#endif // ADMINPING_H
