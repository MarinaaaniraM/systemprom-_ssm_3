#include "pingavalialedevices.h"

#include <QDebug>
#include <QUuid>
#include <QHostAddress>

#include "ICMP_boost/ping.h"

#define STEP_ADDRESS 254

PingAvalialeDevices::PingAvalialeDevices(QObject *parent) :
    QObject(parent)
{
    addrUuid = new QVector <QUuid>();
    pinger = new Pinger(io_service, 1, 1, this);
    addresses = new QVector<QString>();
    if (strAddr.isEmpty())
    {
        strAddr.append("127.0.0");
    }
    connect (pinger, &Pinger::pingedIP, this, &PingAvalialeDevices::pinged);
}


// ----------------------------------------------------------------------------
PingAvalialeDevices::~PingAvalialeDevices()
{
    delete addrUuid;
    delete addresses;
}

// ----------------------------------------------------------------------------
void PingAvalialeDevices::startingPing()
{
    pinger->startWorking();
    addNewHosts();

    io_service.run();
}


// ----------------------------------------------------------------------------
void PingAvalialeDevices::addNewHosts()
{
    strAddr.append(".0");

    QHostAddress ipAddress;
    ipAddress.setAddress(strAddr);

    for (quint32 i = 0; i <= STEP_ADDRESS; ++i)
    {
        QHostAddress addr;
        addr.setAddress(ipAddress.toIPv4Address() + i);

        QUuid uuid = QUuid::createUuid();
        pinger->addPingIP(uuid.toString(), addr.toString());
        addrUuid->append(uuid);
    }
}


// ----------------------------------------------------------------------------
void PingAvalialeDevices::deleteHosts()
{
    for (int j = 0; j < addrUuid->size(); ++j)
    {
        pinger->removePingIP(addrUuid->at(j).toString());
    }
    addrUuid->clear();
}


// ----------------------------------------------------------------------------
void PingAvalialeDevices::pinged(bool success, boost::asio::ip::icmp::endpoint endpoint)
{
    QString ip = QString::fromUtf8(endpoint.address().to_string().c_str());

    if (success)
    {
//        qDebug() << "Found address" << ip;
        addresses->append(ip);
    }
    if (!success)
    {
//        qDebug() << "Fault" << ip;
        deleteHosts();
        io_service.stop();
        return;
    }
}








