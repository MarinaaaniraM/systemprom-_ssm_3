#include <QApplication>
#include "maininterface.h"
#include "VersionInfo.h"






int main(int argc, char *argv[])
{
    QApplication zeApp(argc, argv);

    zeApp.setApplicationName(QString(QObject::tr("Сетевой монитор  v") +
                                       ServiceStructures::VersionInfo::toString(currentVersion)));
    zeApp.setApplicationVersion(ServiceStructures::VersionInfo::toString(currentVersion));

    MainInterface mainWin;
    QIcon icon("://Resources/SystempromLogo.png");
    mainWin.setWindowIcon(icon);
    zeApp.setWindowIcon(icon);

    mainWin.load_preDefinedDeviceTypeInterfacesImagesReload();

    mainWin.load_customDeviceTypeInterfaces();

    mainWin.show();

    return zeApp.exec();
}
