#ifndef GLOBALCONSTANTS_H
#define GLOBALCONSTANTS_H


#include "ServiceStructures.h"
#include <QList>


namespace GlobalConstants
{

const QString logFileName = "log.txt";
const QString customDeviceTypeInterfacesFileName = "cDTI.ssmss";
const QString settingsDirName = "Settings";

}


////////////////////////////////////////////////


namespace PreDefinedDeviceTypeInterface
{

// заданные дефолтные типы интерфейсов
extern const ServiceStructures::DeviceTypeInterface phoneModem;

extern const ServiceStructures::DeviceTypeInterface claster;

extern const ServiceStructures::DeviceTypeInterface bluetoothDevice;

extern const ServiceStructures::DeviceTypeInterface standaloneNet;

extern const ServiceStructures::DeviceTypeInterface wifiDevice;

extern const ServiceStructures::DeviceTypeInterface firewallDevice;

extern const ServiceStructures::DeviceTypeInterface antenn;

extern const ServiceStructures::DeviceTypeInterface ipTelephone;

extern const ServiceStructures::DeviceTypeInterface printer;

extern const ServiceStructures::DeviceTypeInterface internet;

extern const ServiceStructures::DeviceTypeInterface satellite;

extern const ServiceStructures::DeviceTypeInterface computer;

extern const ServiceStructures::DeviceTypeInterface localNetwork;

extern const ServiceStructures::DeviceTypeInterface unknownDevice;


// QList со всеми этими дефолтными типами - для простоты просмотра
extern const QList<ServiceStructures::DeviceTypeInterface> preDefinedDeviceTypeInterfaceList;

}


#endif // GLOBALCONSTANTS_H
