#include <QMessageBox>
#include <QDialog>
#include <QInputDialog>
#include <QFileDialog>
#include "dialogmessager.h"
#include "ServiceStructures.h"
#include "VersionInfo.h"
#include "GlobalConstants.h"
#include "devicetypeinterfaceaddwidget.h"
#include "xmloperator.h"





DialogMessager::DialogMessager(QWidget *parent) :
    QWidget(parent)
{
    //
}
/////////////////////////////////////////////////
void DialogMessager::loadCustomDeviceTypeInterfaces()
{
    if (!(XMLOperator::getCustomDeviceTypeInterfaces(customDeviceTypeInterfaces)))
    {
        // не смогли загрузить нормально
        emit warning(tr("Файл настроек пользовательских типов устройств не может быть загружен"));
    }
}
/////////////////////////////////////////////////
void DialogMessager::saveCustomDeviceTypeInterfaces()
{
    if (!(XMLOperator::saveCustomDeviceTypeInterfaces(customDeviceTypeInterfaces)))
    {
        // не смогли сохранить нормально
        emit warning(tr("Файл настроек пользовательских типов устройств не может быть сохранен"));
        showErrorDialog(tr("Файл настроек пользовательских типов устройств не может быть сохранен"));
    }
}
/////////////////////////////////////////////////
const QList<ServiceStructures::DeviceTypeInterface> *DialogMessager::getCustomDTIList()
{
    return &customDeviceTypeInterfaces;
}
/////////////////////////////////////////////////
bool DialogMessager::closeChangedTabWidget()
{
    return raiseBoolDialog(tr("<center><h3>Имеются несохраненные изменения в карте сети</h3></center>"),
                           tr("<center><i>Все равно закрыть карту?</i></center>"),
                           tr("Закрыть"), tr("Отмена"));
}
/////////////////////////////////////////////////
bool DialogMessager::closeProgramWithUnsavedChanges()
{
    return raiseBoolDialog(tr("<center><h3>Имеются несохраненные карты сети</h3></center>"),
                           tr("<center><i>Все равно закрыть программу?</i></center>"),
                           tr("Закрыть"), tr("Отмена"));
}
/////////////////////////////////////////////////
QString DialogMessager::getFileNameToSaveTabWidget()
{
    return QFileDialog::getSaveFileName(this, tr("Сохранить файл"), ".",
                                        tr("Карта сети (*.ssmm)"));
}
/////////////////////////////////////////////////
QString DialogMessager::getFileNameToOpenMap()
{
    QFileDialog *fileDialog = new QFileDialog(this);
    fileDialog->setFileMode(QFileDialog::ExistingFile);
    fileDialog->setNameFilter(tr("Карты SSM (*.ssmm)"));

    if (fileDialog->exec())
    {
        QStringList strList = fileDialog->selectedFiles();
        if (!strList.isEmpty())
            return strList.last(); // т.к. мы в режиме QFileDialog::ExistingFile, то там может быть лишь один элемент
    }

    return "";
}
/////////////////////////////////////////////////
bool DialogMessager::selectedGraphicsItemCanBeDeleted()
{
    return raiseBoolDialog(tr("<center><h3>Подтверждение удаления элемента</h3></center>"),
                           tr("<center><i>Действительно удалить элемент?</i></center>"),
                           tr("Удалить"), tr("Отмена"));
}
/////////////////////////////////////////////////
void DialogMessager::cannotOpenFile()
{
    QMessageBox::warning(this, tr("Ошибка загрузки"),
                         tr("Невозможно загрузить карту"), QMessageBox::Ok);
}
/////////////////////////////////////////////////
int DialogMessager::getNumber(const QString &title, const QString &text,
                              const int &defaultValue, const int &min, const int &max, const int &step)
{
    bool ok;

    int returner = QInputDialog::getInt(this, title, text, defaultValue, min, max, step, &ok);

    if (ok)
        return returner;

    return -1;
}
/////////////////////////////////////////////////
void DialogMessager::showAboutProgramDialog()
{
    QMessageBox::about(this, tr("О программе"),
                       QString(tr("<center><h2>SSM</h2></center>"
                                  "<center><h3><i>Сетевой монитор</i></h3></center>"
                                  "<li>Сетевой мониторинг</li>"
                                  "<li>Составление карт вычислительных сетей</li>"
                                  "<li>Опрос и контроль узлов сетей</li>"
                                  "<li> </li>"
                                  "<i>Сделано в ОАО \"Концерн \"Системпром\"\".</i> Версия ") + ServiceStructures::VersionInfo::toString(currentVersion) +
                               tr(", билд ") + ServiceStructures::VersionInfo::getBuildNum(currentVersion)));
}
/////////////////////////////////////////////////
void DialogMessager::showEditDeviceTypeInterfacesDialog()
{
    DeviceTypeInterfaceAddWidget *dtiAddWidget = new DeviceTypeInterfaceAddWidget(&customDeviceTypeInterfaces, this);
    connect(dtiAddWidget, &DeviceTypeInterfaceAddWidget::warning, this, &DialogMessager::showWarningDialog);

    dtiAddWidget->show();
}
/////////////////////////////////////////////////
void DialogMessager::showWarningDialog(const QString &warnText)
{
    QMessageBox::warning(this, tr("Предупреждение"), warnText);
}
/////////////////////////////////////////////////
void DialogMessager::showErrorDialog(const QString &errText)
{
    QMessageBox::critical(this, tr("Ошибка"), errText);
}
/////////////////////////////////////////////////
bool DialogMessager::raiseBoolDialog(const QString &text, const QString &informativeText,
                                     const QString &okButtonText, const QString &cancelButtonText)
{
    QMessageBox msgBox;
    msgBox.setText(text);
    msgBox.setInformativeText(informativeText);

    QPushButton *okButton = msgBox.addButton(okButtonText, QMessageBox::AcceptRole);
    QPushButton *cancelButton = msgBox.addButton(cancelButtonText, QMessageBox::RejectRole);

    msgBox.exec();

    if (msgBox.clickedButton() == okButton)
        return true;
    if (msgBox.clickedButton() == cancelButton)
        return false;

    return false; // нажали Esc?
}






















