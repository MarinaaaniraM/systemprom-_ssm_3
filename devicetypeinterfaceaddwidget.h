#ifndef DEVICETYPEINTERFACEADDWIDGET_H
#define DEVICETYPEINTERFACEADDWIDGET_H




#include <QDialog>
#include <QListWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include "ServiceStructures.h"




class DeviceTypeInterfaceAddWidget : public QDialog
{
    Q_OBJECT


public:
    explicit DeviceTypeInterfaceAddWidget(QList<ServiceStructures::DeviceTypeInterface> *userDeviceTypeInterfaces, QWidget *parent = 0);


protected:
    QList<ServiceStructures::DeviceTypeInterface> *customDeviceTypeInterfaces; //указатель на лист пользовательских DeviceTypeInterface
    QListWidget *listWidget;
    QPushButton *okButton;
    QPushButton *addNewDTIButton;
    QPushButton *deleteDTIButton;
    QLineEdit *addNewDTIName;
    QLineEdit *addNewDTIOnlineIconPath;
    QLineEdit *addNewDTIOfflineIconPath;
    QPushButton *addNewDTIOnlineIconButton;
    QPushButton *addNewDTIOfflineIconButton;
    QLabel *pictureLabel;

    void initListWidgetContent();
    void createElements();
    void setupLayout();
    void connectComponents();

    void bad_addNewDTIOnlineIconPath();
    void bad_addNewDTIOfflineIconPath();


protected slots:
    void listRowChanged(const int &currentRow);
    void showInfoAboutDTI(const ServiceStructures::DeviceTypeInterface &dti, const bool &preDefined);
    void addNewDTIOnlineIconButton_slot();
    void addNewDTIOfflineIconButton_slot();
    void addNewDTIButton_slot();
    void deleteDTIButton_slot();
    void addNewDTIOfflineIconPath_textChanged_slot();
    void addNewDTIOnlineIconPath_textChanged_slot();
    void addNewDTIName_textChanged_slot();


signals:
    void warning(QString warnText);


protected:
    enum LocalDefines
    {
        GridLayoutMinimalSizeSide = 10,         // условный дефайн для минимальной (квадратной) стороны ячейки самого QGridLayout.
                                                // всё равно реальный размер будут задавать сами виджеты
        GridLayoutWidgetMinimalWidth = 50,      // а вот это уже реальный размер, устанавливаемый в самих виджетах
        DeviceTypeInterfaceIconSideSize = 32    // дефолтный размер стороны иконки, используемой для изображения
    };


};

#endif // DEVICETYPEINTERFACEADDWIDGET_H
