#include "GlobalConstants.h"






namespace PreDefinedDeviceTypeInterface
{


// заданные дефолтные типы интерфейсов
const ServiceStructures::DeviceTypeInterface phoneModem = {QObject::tr("Модем"), QImage("://Resources/defaultDeviceTypeInterfaceImages/wireOn.png"),
                                                          QImage("://Resources/defaultDeviceTypeInterfaceImages/wireOff.png"),
                                                           "://Resources/defaultDeviceTypeInterfaceImages/wireOn.png",
                                                          "://Resources/defaultDeviceTypeInterfaceImages/wireOff.png"};

const ServiceStructures::DeviceTypeInterface claster = {QObject::tr("Кластер"), QImage("://Resources/defaultDeviceTypeInterfaceImages/clusterOn.png"),
                                                        QImage("://Resources/defaultDeviceTypeInterfaceImages/clusterOff.png"),
                                                        "://Resources/defaultDeviceTypeInterfaceImages/clusterOn.png",
                                                       "://Resources/defaultDeviceTypeInterfaceImages/clusterOff.png"};

const ServiceStructures::DeviceTypeInterface bluetoothDevice = {QObject::tr("Bluetooth-устройство"), QImage("://Resources/defaultDeviceTypeInterfaceImages/bluetoothOn.png"),
                                                                QImage("://Resources/defaultDeviceTypeInterfaceImages/bluetoothOff.png"),
                                                                "://Resources/defaultDeviceTypeInterfaceImages/bluetoothOn.png",
                                                               "://Resources/defaultDeviceTypeInterfaceImages/bluetoothOff.png"};

const ServiceStructures::DeviceTypeInterface standaloneNet = {QObject::tr("Отдельная сеть"), QImage("://Resources/defaultDeviceTypeInterfaceImages/objectOn.png"),
                                                              QImage("://Resources/defaultDeviceTypeInterfaceImages/objectOff.png"),
                                                              "://Resources/defaultDeviceTypeInterfaceImages/objectOn.png",
                                                             "://Resources/defaultDeviceTypeInterfaceImages/objectOff.png"};

const ServiceStructures::DeviceTypeInterface wifiDevice = {QObject::tr("WiFi-устройство"), QImage("://Resources/defaultDeviceTypeInterfaceImages/modemOn.png"),
                                                           QImage("://Resources/defaultDeviceTypeInterfaceImages/modemOff.png"),
                                                           "://Resources/defaultDeviceTypeInterfaceImages/modemOn.png",
                                                          "://Resources/defaultDeviceTypeInterfaceImages/modemOff.png"};

const ServiceStructures::DeviceTypeInterface firewallDevice = {QObject::tr("Сетевой экран"), QImage("://Resources/defaultDeviceTypeInterfaceImages/firewallOn.png"),
                                                         QImage("://Resources/defaultDeviceTypeInterfaceImages/firewallOff.png"),
                                                               "://Resources/defaultDeviceTypeInterfaceImages/firewallOn.png",
                                                              "://Resources/defaultDeviceTypeInterfaceImages/firewallOff.png"};

const ServiceStructures::DeviceTypeInterface antenn = {QObject::tr("Антенна"), QImage("://Resources/defaultDeviceTypeInterfaceImages/antennOn.png"),
                                                       QImage("://Resources/defaultDeviceTypeInterfaceImages/antennOff.png"),
                                                       "://Resources/defaultDeviceTypeInterfaceImages/antennOn.png",
                                                      "://Resources/defaultDeviceTypeInterfaceImages/antennOff.png"};

const ServiceStructures::DeviceTypeInterface ipTelephone = {QObject::tr("IP-телефон"), QImage("://Resources/defaultDeviceTypeInterfaceImages/phoneOn.png"),
                                                            QImage("://Resources/defaultDeviceTypeInterfaceImages/phoneOff.png"),
                                                            "://Resources/defaultDeviceTypeInterfaceImages/phoneOn.png",
                                                           "://Resources/defaultDeviceTypeInterfaceImages/phoneOff.png"};

const ServiceStructures::DeviceTypeInterface printer = {QObject::tr("Сетевой принтер"), QImage("://Resources/defaultDeviceTypeInterfaceImages/printerOn.png"),
                                                        QImage("://Resources/defaultDeviceTypeInterfaceImages/printerOff.png"),
                                                        "://Resources/defaultDeviceTypeInterfaceImages/printerOn.png",
                                                       "://Resources/defaultDeviceTypeInterfaceImages/printerOff.png"};

const ServiceStructures::DeviceTypeInterface internet = {QObject::tr("Внешняя сеть"), QImage("://Resources/defaultDeviceTypeInterfaceImages/internetOn.png"),
                                                         QImage("://Resources/defaultDeviceTypeInterfaceImages/internetOff.png"),
                                                         "://Resources/defaultDeviceTypeInterfaceImages/internetOn.png",
                                                        "://Resources/defaultDeviceTypeInterfaceImages/internetOff.png"};

const ServiceStructures::DeviceTypeInterface satellite = {QObject::tr("Шлюз"), QImage("://Resources/defaultDeviceTypeInterfaceImages/satelliteOn.png"),
                                                          QImage("://Resources/defaultDeviceTypeInterfaceImages/satelliteOff.png"),
                                                          "://Resources/defaultDeviceTypeInterfaceImages/satelliteOn.png",
                                                         "://Resources/defaultDeviceTypeInterfaceImages/satelliteOff.png"};

const ServiceStructures::DeviceTypeInterface computer = {QObject::tr("АРМ"), QImage("://Resources/defaultDeviceTypeInterfaceImages/pcOn.png"),
                                                         QImage("://Resources/defaultDeviceTypeInterfaceImages/pcOff.png"),
                                                         "://Resources/defaultDeviceTypeInterfaceImages/pcOn.png",
                                                        "://Resources/defaultDeviceTypeInterfaceImages/pcOff.png"};

const ServiceStructures::DeviceTypeInterface localNetwork = {QObject::tr("Локальная сеть"), QImage("://Resources/defaultDeviceTypeInterfaceImages/netOn.png"),
                                                             QImage("://Resources/defaultDeviceTypeInterfaceImages/netOff.png"),
                                                             "://Resources/defaultDeviceTypeInterfaceImages/netOn.png",
                                                            "://Resources/defaultDeviceTypeInterfaceImages/netOff.png"};

const ServiceStructures::DeviceTypeInterface unknownDevice = {QObject::tr("Неизвестное устройство"), QImage("://Resources/defaultDeviceTypeInterfaceImages/unknownOn.png"),
                                                              QImage("://Resources/defaultDeviceTypeInterfaceImages/unknownOff.png"),
                                                              "://Resources/defaultDeviceTypeInterfaceImages/unknownOn.png",
                                                             "://Resources/defaultDeviceTypeInterfaceImages/unknownOff.png"};


// QList со всеми этими дефолтными типами - для простоты просмотра
const QList<ServiceStructures::DeviceTypeInterface> preDefinedDeviceTypeInterfaceList = {
    phoneModem, claster, bluetoothDevice, standaloneNet, wifiDevice, firewallDevice, antenn, ipTelephone, printer, internet, satellite, computer, localNetwork, unknownDevice};



}



