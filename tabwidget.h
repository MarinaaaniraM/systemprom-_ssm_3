#ifndef TABWIDGET_H
#define TABWIDGET_H


#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QList>
#include <QString>
#include <QAction>
#include <QPoint>
#include "NetLogicalDevices.h"
#include "addnewnetdevicewidget.h"
#include <ServiceStructures.h>
#include <GlobalConstants.h>

class TabWidget : public QWidget
{
    Q_OBJECT


public:
    explicit TabWidget(const QList<ServiceStructures::DeviceTypeInterface> *usersDTIs,
                       QRectF sceneBaseRect = QRectF(0, 0, 0, 0), QWidget *parent = 0);
    QString getFileName() const;
    QString getShortenFileName() const; // возвращает fileName с отрезанным путем - только само имя карты
    void setFileName(const QString &value);
    bool checkIfDocumentWasChanged() const;
    void setDocumentToBeChanged(const bool &value);
    void addNewDeviceAct_fromMenu(); // добавление девайса в центр экрана - для вызова из меню главного GUI

    bool saveToXML(); // true если сохранен успешно
    void obtainNewDevice(NetDevice *newDevice);
    void obtainNewDeviceLink(NetDeviceLink *newLink);
    void setDevicePos(NetDevice *device, const qreal &x, const qreal &y);
    NetDevice * getDeviceByUUID(const QString &uuid, bool &ok);
    void suspendThreads(const bool &suspend); // для оптимизации - чтобы работали треды только на текущей вкладке

    // ============================================================================
    void addSearchDevices_fromMenu(QVector<QString>* dev, int type);
    void addSearchlDevices(QVector<QString>* dev, int type);
    bool getAdminType()
    {
        return adminType;
    }
    void setAdminType(bool b);
    // ============================================================================

    virtual ~TabWidget();


protected:
    QString fileName;
    bool documentWasChanged = false;
    bool graphicsSceneWasChanged = false; // true если была изменена сцена
    bool askBeforeDeviceDeletion = true;

    QGraphicsScene *graphicsScene;
    QGraphicsView *graphicsView;

    QList<NetDevice *> netDevicesList;
    QList<NetDeviceLink *> netDeviceLinksList;

    const QList<ServiceStructures::DeviceTypeInterface> *customDTIs;

    QPoint posWhereMenuWasCalledFrom; // для запоминания позиции, из которой вызвали меню

    bool firstDeviceToBeConnectedIsSelected = false; // используется при связывании устройств. true если пришел сигнал "хочу коннектиться"
                                                     // затем ловится сигнал о том, что девайс выбрали
    NetDevice *firstDeviceToBeConnected;             // а это, собственно, сам девайс для коннекта


signals:
    void warning(QString warnText);
    void warningFromDevice(QString warnText, ServiceStructures::LogMessageType type);
    void documentChanged(QWidget *thisWidget); // должен вызываться с параметром this!
    void newDeviceWasAdded(QString text,
                           ServiceStructures::LogMessageType messageType = ServiceStructures::LogMessageType::normal);
    void newLinkWasAdded(QString text,
                         ServiceStructures::LogMessageType messageType = ServiceStructures::LogMessageType::normal);
    void deviceWasDeleted(QString text,
                          ServiceStructures::LogMessageType messageType = ServiceStructures::LogMessageType::normal);
    void linkWasDeleted(QString text,
                        ServiceStructures::LogMessageType messageType = ServiceStructures::LogMessageType::normal);
    void openAnotherTabWidget(QString fileName);
    void startPingingDevice(QString uuid, QString ipAddr);
    void stopPingingDevice(QString uuid);
    void addTextToLog(QString text, ServiceStructures::LogMessageType messageType);

    // ========================================================================
    void pingTypeWasChanged(bool adminType);
    // ========================================================================


protected:
    QAction *addNewDeviceAct;

    // ========================================================================
    QAction* setAdminTypeAct;
    bool adminType;
    // ========================================================================

    void createActions();
    void connectComponents();

    bool canBeResized(const QSize &newSize, const QList<QGraphicsItem *> &graphicsItems) const;
    AddNewNetDeviceWidget * createAddNewNetDeviceWidget();

    void contextMenuEvent(QContextMenuEvent *event);
    void resizeEvent(QResizeEvent *event);


protected slots:
    void addNewDeviceAct_slot();    
    // ========================================================================
    void setAdminTypeAct_slot();
    // ========================================================================

    void obtainNewDevice_slot(NetDevice *newDevice, int x, int y);
    void sceneChanged(const QList<QRectF> &region);
    void userRequestedToEditDevice(NetDevice *editedDevice);
    void userRequestedToConnectDevices(NetDevice *firstDevice);
    void userSelectedDevice(NetDevice *selectedDevice);
    void userRequestedToDeleteDevice(NetDevice *selectedDevice);
    void userRequestedToDeleteLink(NetDeviceLink *selectedLink);
    void userRequestedToOpenMultiMap(NetDevice *selectedDevice);


public slots:
    void setAskBeforeDeviceDeletion(const bool &value);
    void setDevicesOnline(const bool &online, const CustomQStrList &uuids);
};

#endif // TABWIDGET_H
