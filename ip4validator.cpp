#include <QStringList>
#include "ip4validator.h"






IP4Validator::IP4Validator(QObject *parent) :
    QValidator(parent)
{
}
//////////////////////////////////////////
QValidator::State IP4Validator::validate(QString &input, int &pos) const
{
    QStringList octets = input.split(".");

    QValidator::State state = QValidator::Acceptable;

    foreach (QString octet, octets)
    {
        bool ok;

        if (octet.toInt(&ok) > 255)
            return QValidator::Invalid;

        if (!ok)
            state = QValidator::Intermediate; // assuming it's empty
    }

    return state;
}
