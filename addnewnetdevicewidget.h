#ifndef ADDNEWNETDEVICEWIDGET_H
#define ADDNEWNETDEVICEWIDGET_H




#include <QDialog>
#include <QPushButton>
#include <QComboBox>
#include <QList>
#include <QLabel>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QCheckBox>
#include "ServiceStructures.h"
#include "NetLogicalDevices.h"


#include <QDebug>



class AddNewNetDeviceWidget : public QDialog
{
    Q_OBJECT


public:
    explicit AddNewNetDeviceWidget(const QList<ServiceStructures::DeviceTypeInterface> *customInterfaces, QWidget *parent = 0);
    void setDeviceToEdit(NetDevice *deviceToEdit); // используется при редактировании текущего устройства - указатель на редактируемое устройство

    // ============================================================================
    void searchDevices(QString dev, int type, int x, int y);
    // ============================================================================

protected:
    QPushButton *okButton;
    QPushButton *cancelButton;
    QPushButton *openFileToMultiMapButton;
    QComboBox *possibleDTIs;
    QLabel *pictureLabel; // иконка устройства
    QLineEdit *deviceName;
    QLineEdit *multiMapName; // если устройство может открыть еще одну карту
    QLineEdit *deviceAddress;
    QPlainTextEdit *aboutDevice; // тут вводится описание устройства
    QCheckBox *shouldBePinged; // чекбокс "пинговать ли устройство"

    const QList<ServiceStructures::DeviceTypeInterface> *customDTIs;

    NetDevice *device = 0; // тот самый девайс, который мы создаем/редактируем
    bool usingDeviceThatAlreadyExist = false; // true если device установлен


signals:
    void createNewNetDevice(NetDevice *newDevice, int x, int y); // эмитится, если мы создавали новое устройство. Тут оно, собственно, и сидит.
    void warning(QString warnText);


protected:
    void createElements();
    void initComboBoxContent();
    void setupLayout();
    void connectComponents();
    void bad_deviceName();
    void bad_deviceAddress();



protected slots:
    void openFileToMultiMapButton_slot();
    void possibleDTIs_currentIndexChanged_slot(const int &index);
    void okButton_slot();
    void deviceName_textChanged_slot();
    void deviceAddress_textChanged_slot();


protected:
    enum LocalDefines
    {
        GridLayoutMinimalSizeSide = 10,         // условный дефайн для минимальной (квадратной) стороны ячейки самого QGridLayout.
                                                // всё равно реальный размер будут задавать сами виджеты
        GridLayoutWidgetMinimalWidth = 50,      // а вот это уже реальный размер, устанавливаемый в самих виджетах
        DeviceTypeInterfaceIconSideSize = 32    // дефолтный размер стороны иконки, используемой для изображения
    };


};

#endif // ADDNEWNETDEVICEWIDGET_H
