#include <QSettings>
#include <QCloseEvent>
#include <QMenuBar>
#include <QWhatsThis>
#include <QApplication>
#include <QDir>
#include <QThread>
#include <QThreadPool>
#include "maininterface.h"
#include "xmloperator.h"
#include "GlobalConstants.h"

#include <QDebug>


MainInterface::MainInterface(QWidget *parent)
    : QMainWindow(parent)
{
    qRegisterMetaType<CustomQStrList>("CustomQStrList");

    settingsRead();

    QDir::setCurrent(QCoreApplication::applicationDirPath()); // чтобы в дальнейшем считать текущей директорией
                                                              // директорию с экзешником
                                                              // и не ипать мозги с хитрыми вызовами экзекьютабла

    dialogMessager = new DialogMessager(this);
    logDialog = new LogDialog(this);

    pingThread = new NetDeviceThreading(this);
    pingThread->setAutoDelete(false); // сами удалим

    tabWidget = new QTabWidget(this);
    setCentralWidget(tabWidget);


    // ========================================================================
    search = NULL;
    // ========================================================================

    createActions();
    createMenus();
    createToolbars();

    setupComponents();
    connectComponents();

    QThreadPool::globalInstance()->start(pingThread);

    newFileAct_slot()->setDocumentToBeChanged(true); // создаем пустую карту в качестве первой вкладки

    if (windowMaximizesOnStart)
        showMaximized();

    // ========================================================================
//    connect(tabWidget, &TabWidget::pingTypeWasChanged, this, &MainInterface::changePingType);
    // ========================================================================
}


//////////////////////////////////////////////
void MainInterface::load_customDeviceTypeInterfaces()
{
    dialogMessager->loadCustomDeviceTypeInterfaces();
}
//////////////////////////////////////////////
void MainInterface::load_preDefinedDeviceTypeInterfacesImagesReload()
{
    for (int i = 0; i < PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size(); i++)
    {
        PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.at(i).reloadImages();
    }
}
//////////////////////////////////////////////
void MainInterface::settingsRead()
{
    bool ok;

    QSettings userSettings("Systemprom", "SSM", this);

    userSettings.beginGroup("appGeometry"); // размер, место, вид окна
    if (!(userSettings.value("fullscreen", true).toBool()))
    {
        // если сеттингсов нет, то по умолчанию фулскрин
        if ((userSettings.contains("size")) && (userSettings.contains("pos")))
        {
            resize(userSettings.value("size").toSize());
            move(userSettings.value("pos").toPoint());
            show();
            windowMaximizesOnStart = false;
        }
    }
    else
    {
        windowMaximizesOnStart = true;
    }
    userSettings.endGroup();

    userSettings.beginGroup("programSettings");
    programSettings.scrollLogOutput = userSettings.value("scrollLogOutput", true).toBool();
    programSettings.saveLogFileBeforeExit = userSettings.value("saveLogFileBeforeExit", true).toBool();
    programSettings.askBeforeDeletion = userSettings.value("askBeforeDeletion", true).toBool();
    programSettings.usingIdealThreadCount = userSettings.value("usingIdealThreadCount", false).toBool();

    programSettings.timeToRePing = userSettings.value("timeToRePing", DEFAULT_TIME_TO_RE_PING).toInt(&ok);
    if (!ok)
        programSettings.timeToRePing = DEFAULT_TIME_TO_RE_PING;

    programSettings.timeToWaitForPingReplyModifyer = userSettings.value("timeToWaitForPingReply",
                                                                DEFAULT_TIME_TO_WAIT_FOR_PING_REPLY).toInt(&ok);
    if (!ok)
        programSettings.timeToWaitForPingReplyModifyer = DEFAULT_TIME_TO_WAIT_FOR_PING_REPLY;

    userSettings.endGroup();
}
/////////////////////////////////////////////
void MainInterface::settingsWrite()
{
    QSettings userSettings("Systemprom", "SSM", this);

    userSettings.beginGroup("appGeometry"); // размер, место, вид окна
    userSettings.setValue("fullscreen", (isFullScreen() || isMaximized()));
    userSettings.setValue("size", size());
    userSettings.setValue("pos", pos());
    userSettings.endGroup();

    userSettings.beginGroup("programSettings");
    userSettings.setValue("scrollLogOutput", programSettings.scrollLogOutput);
    userSettings.setValue("saveLogFileBeforeExit", programSettings.saveLogFileBeforeExit);
    userSettings.setValue("askBeforeDeletion", programSettings.askBeforeDeletion);
    userSettings.setValue("usingIdealThreadCount", programSettings.usingIdealThreadCount);
    userSettings.setValue("timeToRePing", programSettings.timeToRePing);
    userSettings.setValue("timeToWaitForPingReply", programSettings.timeToWaitForPingReplyModifyer);
    userSettings.endGroup();
}
//////////////////////////////////////////////
void MainInterface::connectComponents()
{
    connect(logDialog, &LogDialog::closeEventHappened, this, &MainInterface::logDialogWasClosed);
    connect(logDialog, &LogDialog::warning, dialogMessager, &DialogMessager::showWarningDialog);

    connect(dialogMessager, &DialogMessager::warning, logDialog, &LogDialog::appendText);

    connect(tabWidget, &QTabWidget::currentChanged, this, &MainInterface::tabWidgetIndexChanged);

    connect(this, &MainInterface::setTimeToRePing, pingThread, &NetDeviceThreading::setTimeToRePing);
    connect(this, &MainInterface::setTimeToWaitForPingReplyModifyer, pingThread, &NetDeviceThreading::setTimeToWaitForPingReplyModifyer);
}
//////////////////////////////////////////////
void MainInterface::setupComponents()
{
    logDialog->setOutputScroll(programSettings.scrollLogOutput);
    logDialog->setSaveLogFileOnClose(programSettings.saveLogFileBeforeExit);

    pingThread->setTimeToRePing(programSettings.timeToRePing);
    pingThread->setTimeToWaitForPingReplyModifyer(programSettings.timeToWaitForPingReplyModifyer);
}
//////////////////////////////////////////////
void MainInterface::createActions()
{
    closeProgramAct = new QAction(tr("Выход"), this);
    closeProgramAct->setStatusTip(tr("Выход из программы"));
    closeProgramAct->setWhatsThis(tr("Выйти из программы SSM"));
    closeProgramAct->setIcon(QIcon("://Resources/actionIcons/exit.png"));
    connect(closeProgramAct, &QAction::triggered, this, &MainInterface::close);

    whatsThisAct = new QAction(tr("Что это?"), this);
    whatsThisAct->setStatusTip(tr("Режим \"Что это?\""));
    whatsThisAct->setWhatsThis(tr("Переход в режим \"Что это?\". Наведите курсор на интересующее действие, щелкните мышью и увидите подсказку"));
    whatsThisAct->setIcon(QIcon("://Resources/actionIcons/what_is_this.png"));
    connect(whatsThisAct, &QAction::triggered, this, &MainInterface::whatsThisAct_slot);

    aboutQtAct = new QAction(tr("О Qt"), this);
    aboutQtAct->setStatusTip(tr("Просмотр информации о Qt"));
    aboutQtAct->setWhatsThis(tr("Просмотр информационного окна о Qt"));
    aboutQtAct->setIcon(QIcon("://Resources/actionIcons/Qt_logo.png"));
    connect(aboutQtAct, &QAction::triggered, qApp, &QApplication::aboutQt);

    aboutProgramAct = new QAction(tr("О программе"), this);
    aboutProgramAct->setStatusTip(tr("Просмотр информации о программе"));
    aboutProgramAct->setWhatsThis(tr("Просмотр информационного окна о программе"));
    aboutProgramAct->setIcon(QIcon("://Resources/actionIcons/about.png"));
    connect(aboutProgramAct, &QAction::triggered, dialogMessager, &DialogMessager::showAboutProgramDialog);

    showLogAct = new QAction(tr("Показывать протокол работы"), this);
    showLogAct->setStatusTip(tr("Показывать протокол работы?"));
    showLogAct->setWhatsThis(tr("Отображать ли протокол работы в отдельном окне?"));
    showLogAct->setCheckable(true);
    showLogAct->setChecked(true);
    connect(showLogAct, &QAction::triggered, this, &MainInterface::showLogAct_slot);

    clearLogAct = new QAction(tr("Очистить протокол работы"), this);
    clearLogAct->setStatusTip(tr("Убрать старые сообщения в протоколе"));
    clearLogAct->setWhatsThis(tr("Удалить старые сообщения в протоколе работы"));
    connect(clearLogAct, &QAction::triggered, logDialog, &LogDialog::clearLog);

    scrollLogOutputAct = new QAction(tr("Прокрутка сообщений протокола работы"), this);
    scrollLogOutputAct->setStatusTip(tr("Прокрутка сообщений протокола к самым последним"));
    scrollLogOutputAct->setWhatsThis(tr("Прокручивать протокол работы к последним сообщениям при их получении"));
    scrollLogOutputAct->setCheckable(true);
    scrollLogOutputAct->setChecked(programSettings.scrollLogOutput);
    connect(scrollLogOutputAct, &QAction::triggered, this, &MainInterface::scrollLogOutputAct_slot);

    saveLogFileBeforeCloseAct = new QAction(tr("Сохранять протокол перед выходом"), this);
    saveLogFileBeforeCloseAct->setStatusTip(tr("Сохранение сообщения протокола при выходе из программы"));
    saveLogFileBeforeCloseAct->setWhatsThis(tr("Сохранять файл протокола работы при выходе из программы, с пометкой о дате?"));
    saveLogFileBeforeCloseAct->setCheckable(true);
    saveLogFileBeforeCloseAct->setChecked(programSettings.saveLogFileBeforeExit);
    connect(saveLogFileBeforeCloseAct, &QAction::triggered, this, &MainInterface::saveLogFileBeforeCloseAct_slot);

    newFileAct = new QAction(tr("Новая карта"), this);
    newFileAct->setStatusTip(tr("Создать новую карту"));
    newFileAct->setWhatsThis(tr("Создать новую пустую карту сети"));
    newFileAct->setIcon(QIcon("://Resources/actionIcons/add_new_map.png"));
    newFileAct->setShortcut(QKeySequence(QKeySequence::New));
    connect(newFileAct, &QAction::triggered, this, &MainInterface::newFileAct_slot);

    closeFileAct = new QAction(tr("Закрыть карту"), this);
    closeFileAct->setStatusTip(tr("Закрыть текущую карту"));
    closeFileAct->setWhatsThis(tr("Закрыть открытую в данный момент карту сети"));
    closeFileAct->setIcon(QIcon("://Resources/actionIcons/close_map.png"));
    closeFileAct->setShortcut(QKeySequence(QKeySequence::Close));
    connect(closeFileAct, &QAction::triggered, this, &MainInterface::closeFileAct_slot);

    editNetDeviceInterfacesAct = new QAction(tr("Типы устройств..."), this);
    editNetDeviceInterfacesAct->setStatusTip(tr("Редактирование типов устройств"));
    editNetDeviceInterfacesAct->setWhatsThis(tr("Просмотр и редактирование возможных типов устройств"));
    connect(editNetDeviceInterfacesAct, &QAction::triggered, dialogMessager, &DialogMessager::showEditDeviceTypeInterfacesDialog);

    // ============================================================================
    addNewNetDevice = new QAction(tr("Добавить устройство"), this);
    addNewNetDevice->setStatusTip(tr("Добавление нового устройства"));
    addNewNetDevice->setWhatsThis(tr("Добавление нового устройства с заданными параметрами в центр текущей карты"));
    addNewNetDevice->setIcon(QIcon("://Resources/actionIcons/add_device.png"));
    connect(addNewNetDevice, &QAction::triggered, this, &MainInterface::addNewNetDevice_slot);

    searchNewDevices = new QAction(tr("Поиск устройств"), this);
    searchNewDevices->setStatusTip(tr("Поиск доступных устройств"));
    searchNewDevices->setWhatsThis(tr("Поиск доступных устройств по их IP адресам"));
    searchNewDevices->setIcon(QIcon("://Resources/actionIcons/search_devices.png"));
    connect(searchNewDevices, &QAction::triggered, this, &MainInterface::searchNewDevice_slot);
     // ============================================================================

    saveFileAct = new QAction(tr("Сохранить карту"), this);
    saveFileAct->setStatusTip(tr("Сохранить текущую карту"));
    saveFileAct->setWhatsThis(tr("Сохранить открытую в данный момент карту сети"));
    saveFileAct->setIcon(QIcon("://Resources/actionIcons/save_map.png"));
    saveFileAct->setShortcut(QKeySequence(QKeySequence::Save));
    connect(saveFileAct, &QAction::triggered, this, &MainInterface::saveFileAct_slot);

    saveFileAsAct = new QAction(tr("Сохранить карту как..."), this);
    saveFileAsAct->setStatusTip(tr("Сохранить текущую карту как..."));
    saveFileAsAct->setWhatsThis(tr("Сохранить открытую в данный момент карту сети, задав путь к файлу"));
    saveFileAsAct->setIcon(QIcon("://Resources/actionIcons/save_map.png"));
    saveFileAsAct->setShortcut(QKeySequence(QKeySequence::SaveAs));
    connect(saveFileAsAct, &QAction::triggered, this, &MainInterface::saveFileAsAct_slot);

    openFileAct = new QAction(tr("Открыть карту..."), this);
    openFileAct->setStatusTip(tr("Открытие карты сети"));
    openFileAct->setWhatsThis(tr("открыть заданную пользователем карту сети"));
    openFileAct->setIcon(QIcon("://Resources/actionIcons/open_map.png"));
    openFileAct->setShortcut(QKeySequence(QKeySequence::Open));
    connect(openFileAct, &QAction::triggered, this, &MainInterface::openFileAct_slot);

    askBeforeDeviceDeletion = new QAction(tr("Спрашивать перед удалением"), this);
    askBeforeDeviceDeletion->setStatusTip(tr("Спрашивать подтверждение перед удалением элементов карты"));
    askBeforeDeviceDeletion->setWhatsThis(tr("Спрашивать, действительно ли удалить отмеченный для удаления элемент карты"));
    askBeforeDeviceDeletion->setCheckable(true);
    askBeforeDeviceDeletion->setChecked(programSettings.askBeforeDeletion);
    connect(askBeforeDeviceDeletion, &QAction::triggered, this, &MainInterface::askBeforeDeviceDeletion_slot);

    limitingNumberOfConnections = new QAction(tr("Ограничить количество подключений"), this);
    limitingNumberOfConnections->setStatusTip(tr("Ограничение количества подключений по сети"));
    limitingNumberOfConnections->setWhatsThis(QString(
                 tr("Для ускорения работы можно ограничить количество подключений, используемых устройствами. ") +
                 tr("Каждое устройство использует одно подключение. Оптимальное количество подключений для вашей машины составляет ") +
                 ((QThread::idealThreadCount() == -1) ? QString::number(QThread::idealThreadCount()) : tr("<не определено>")) +
                 tr(" подключений")));
    limitingNumberOfConnections->setCheckable(true);
    limitingNumberOfConnections->setChecked(programSettings.usingIdealThreadCount);
    connect(limitingNumberOfConnections, &QAction::triggered, this, &MainInterface::limitingNumberOfConnections_slot);

    timeToRePingAct = new QAction(tr("Время повторного опроса"), this);
    timeToRePingAct->setStatusTip(tr("Установка времени повторного опроса"));
    timeToRePingAct->setWhatsThis(tr("Установка времени (сек), по истечении которого производится повторный опрос устройства"));
    connect(timeToRePingAct, &QAction::triggered, this, &MainInterface::timeToRePing_slot);

    timeToWaitForPingReplyModifyerAct = new QAction(tr("Время ожидания ответа"), this);
    timeToWaitForPingReplyModifyerAct->setStatusTip(tr("Установка модификатора времени ожидания ответа"));
    timeToWaitForPingReplyModifyerAct->setWhatsThis(tr("Установка модификатора времени, по истечении которого отсутствие ответа от устройства означает его недоступность. Умножается на время повторного опроса."));
    connect(timeToWaitForPingReplyModifyerAct, &QAction::triggered, this, &MainInterface::timeToWaitForPingReplyModifyer_slot);
}

//////////////////////////////////////////////
void MainInterface::createMenus()
{
    fileMenu = new QMenu(tr("Файл"), this);
    fileMenu->addAction(newFileAct);
    fileMenu->addAction(openFileAct);
    fileMenu->addAction(saveFileAct);
    fileMenu->addAction(saveFileAsAct);
    fileMenu->addAction(closeFileAct);
    fileMenu->addSeparator();
    fileMenu->addAction(closeProgramAct);

    editMenu = new QMenu(tr("Правка"), this);
    editMenu->addAction(addNewNetDevice);
    editMenu->addAction(searchNewDevices);
    editMenu->addSeparator();
    editMenu->addAction(editNetDeviceInterfacesAct);

    settingsMenu = new QMenu(tr("Настройки"), this);
    settingsMenu->addAction(clearLogAct);
    settingsMenu->addSeparator();
    settingsMenu->addAction(scrollLogOutputAct);
    settingsMenu->addAction(saveLogFileBeforeCloseAct);
    settingsMenu->addAction(showLogAct);
    settingsMenu->addSeparator();
    settingsMenu->addAction(askBeforeDeviceDeletion);
    settingsMenu->addAction(limitingNumberOfConnections);
    settingsMenu->addSeparator();
    settingsMenu->addAction(timeToRePingAct);
    settingsMenu->addAction(timeToWaitForPingReplyModifyerAct);

    helpMenu = new QMenu(tr("Помощь"), this);
    helpMenu->addAction(whatsThisAct);
    helpMenu->addSeparator();
    helpMenu->addAction(aboutQtAct);
    helpMenu->addAction(aboutProgramAct);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(editMenu);
    menuBar()->addMenu(settingsMenu);
    menuBar()->addSeparator();
    menuBar()->addMenu(helpMenu);
}
//////////////////////////////////////////////
void MainInterface::createToolbars()
{
    fileToolBar = addToolBar(tr("Файл"));
    fileToolBar->addAction(newFileAct);
    fileToolBar->addSeparator();
    fileToolBar->addAction(openFileAct);
    fileToolBar->addAction(saveFileAct);
    fileToolBar->addSeparator();
    fileToolBar->addAction(closeFileAct);    
    fileToolBar->addAction(addNewNetDevice);
    fileToolBar->addSeparator();
    fileToolBar->addAction(searchNewDevices);
}
//////////////////////////////////////////////
bool MainInterface::tabWidgetsHaveUnsavedChanges()
{
    for (int i = 0; i < tabWidget->count(); i++)
    {
        TabWidget *currentTab = static_cast<TabWidget *>(tabWidget->widget(i));
        if (currentTab->checkIfDocumentWasChanged())
            return true;
    }

    return false;
}
//////////////////////////////////////////////
void MainInterface::enableTabWidgetActions(const bool &enable)
{
    closeFileAct->setEnabled(enable);
    addNewNetDevice->setEnabled(enable);
    saveFileAct->setEnabled(enable);
    saveFileAsAct->setEnabled(enable);
}
//////////////////////////////////////////////
void MainInterface::saveCurrentTab(TabWidget *currentTab)
{
    if (currentTab->saveToXML())
    {
        // сохранили удачно
        logDialog->appendText(QString(tr("Карта ") + currentTab->getFileName() + tr(" успешно сохранена")),
                              ServiceStructures::LogMessageType::success);

        int changedPageIndex = tabWidget->indexOf(currentTab);
        if (changedPageIndex == -1)
        {
            // 404: not found. WTF?!
            dialogMessager->showErrorDialog(tr("Внутренняя ошибка: сигнал от ненайденной вкладки TabWidget"));
            logDialog->appendText(tr("Внутренняя ошибка: сигнал от ненайденной вкладки TabWidget"),
                                  ServiceStructures::LogMessageType::error);

            return;
        }

        tabWidget->tabBar()->setTabTextColor(changedPageIndex, Qt::black);
        tabWidget->tabBar()->setTabText(changedPageIndex, currentTab->getShortenFileName());
    }
    else
    {
        // сохранение провалилось
        logDialog->appendText(QString(tr("Карта ") + currentTab->getFileName() + tr(" не может быть сохранена!")),
                              ServiceStructures::LogMessageType::error);
        dialogMessager->showErrorDialog(QString(tr("Карта ") + currentTab->getFileName() + tr(" не может быть сохранена!")));
    }
}
//////////////////////////////////////////////
void MainInterface::connectTabWidget(TabWidget *connectingTab)
{
    connect(connectingTab, &TabWidget::documentChanged, this, &MainInterface::tabWidgetContentChanged);
    connect(connectingTab, &TabWidget::newDeviceWasAdded, logDialog, &LogDialog::appendText);
    connect(connectingTab, &TabWidget::newLinkWasAdded, logDialog, &LogDialog::appendText);
    connect(connectingTab, &TabWidget::deviceWasDeleted, logDialog, &LogDialog::appendText);
    connect(connectingTab, &TabWidget::linkWasDeleted, logDialog, &LogDialog::appendText);
    connect(connectingTab, &TabWidget::openAnotherTabWidget, this, &MainInterface::openFile);
    connect(connectingTab, &TabWidget::warningFromDevice, this, &MainInterface::warnFromDevice_slot);
    connect(connectingTab, &TabWidget::addTextToLog, logDialog, &LogDialog::appendText);

    connect(this, &MainInterface::setAskBeforeDeviceDeletion, connectingTab, &TabWidget::setAskBeforeDeviceDeletion);
}
//////////////////////////////////////////////
void MainInterface::whatsThisAct_slot()
{
    if (QWhatsThis::inWhatsThisMode())
    {
        QWhatsThis::leaveWhatsThisMode();
    }
    else
    {
        QWhatsThis::enterWhatsThisMode();
    }
}
//////////////////////////////////////////////
void MainInterface::showLogAct_slot()
{
    if (showLogAct->isChecked())
    {
        logDialog->setVisible(true);
        logDialog->appendText(tr("Окно лога загружено"), ServiceStructures::LogMessageType::success);
    }
    else
    {
        logDialog->setVisible(false);
        logDialog->appendText(tr("Окно лога закрыто"), ServiceStructures::LogMessageType::warning);
    }
}
//////////////////////////////////////////////
void MainInterface::logDialogWasClosed()
{
    showLogAct->setChecked(false);
}
//////////////////////////////////////////////
void MainInterface::scrollLogOutputAct_slot()
{
    programSettings.scrollLogOutput = scrollLogOutputAct->isChecked();
    logDialog->setOutputScroll(programSettings.scrollLogOutput);
}
//////////////////////////////////////////////
void MainInterface::saveLogFileBeforeCloseAct_slot()
{
    programSettings.saveLogFileBeforeExit = saveLogFileBeforeCloseAct->isChecked();
    logDialog->setSaveLogFileOnClose(programSettings.saveLogFileBeforeExit);
}
//////////////////////////////////////////////
TabWidget * MainInterface::newFileAct_slot()
{
    TabWidget *newTabWidget = new TabWidget(dialogMessager->getCustomDTIList());
    tabWidget->addTab(newTabWidget, tr("Новая карта"));

    logDialog->appendText(tr("Создана новая карта"), ServiceStructures::LogMessageType::normal);

    connectTabWidget(newTabWidget);

    enableTabWidgetActions(true);

    return newTabWidget;
}
//////////////////////////////////////////////
void MainInterface::closeFileAct_slot()
{
    if (tabWidget->currentIndex() == -1)
        return; // нет открытых вкладок

    TabWidget *currentTab = static_cast<TabWidget *>(tabWidget->currentWidget());

    if (currentTab->checkIfDocumentWasChanged())
    {
        // документ был изменен!
        if (!dialogMessager->closeChangedTabWidget())
            return; // юзер отказался от закрытия
    }

    tabWidget->removeTab(tabWidget->currentIndex());
    logDialog->appendText(QString(tr("Карта ") + currentTab->getFileName() + tr(" была закрыта")),
                          ServiceStructures::LogMessageType::warning);
    delete currentTab;

    if (!tabWidget->count())
    {
        // больше нет открытых вкладок!
        enableTabWidgetActions(false);
    }
}
//////////////////////////////////////////////
void MainInterface::tabWidgetContentChanged(QWidget *sigSender)
{
    if (ignoreTabWidgetContentChanged)
    {
        ignoreTabWidgetContentChanged = false;

        TabWidget *currentTab = static_cast<TabWidget *>(sigSender);
        currentTab->setDocumentToBeChanged(false);

        return;
    }

    int changedPageIndex = tabWidget->indexOf(sigSender);
    if (changedPageIndex == -1)
    {
        // 404: not found. WTF?!
        dialogMessager->showErrorDialog(tr("Внутренняя ошибка: сигнал от ненайденной вкладки TabWidget"));
        logDialog->appendText(tr("Внутренняя ошибка: сигнал от ненайденной вкладки TabWidget"),
                              ServiceStructures::LogMessageType::error);

        return;
    }

    tabWidget->tabBar()->setTabTextColor(changedPageIndex, Qt::red);
}
//////////////////////////////////////////////
void MainInterface::addNewNetDevice_slot()
{
    if (!tabWidget->count())
        return;

    TabWidget *currentTab = static_cast<TabWidget *>(tabWidget->currentWidget());
    currentTab->addNewDeviceAct_fromMenu();
}


// ============================================================================
void MainInterface::searchNewDevice_slot()
{
    search = new SearchDevicesWidget();
    connect(search, &SearchDevicesWidget::addSearchDevices, this, &MainInterface::addSearchDevices_slot);
    search->show();
}
// ============================================================================



//////////////////////////////////////////////
void MainInterface::saveFileAct_slot()
{
    if (!tabWidget->count())
        return;

    TabWidget *currentTab = static_cast<TabWidget *>(tabWidget->currentWidget());

    if (currentTab->getFileName().isEmpty())
    {
        // ни разу не сохраняли, не присвоено имя файла.
        QString fileNameAssigned = dialogMessager->getFileNameToSaveTabWidget();

        if (fileNameAssigned.isEmpty())
            return; // юзер решил не сохраняться

        currentTab->setFileName(fileNameAssigned);
    }

    saveCurrentTab(currentTab);
}
//////////////////////////////////////////////
void MainInterface::saveFileAsAct_slot()
{
    if (!tabWidget->count())
        return;

    TabWidget *currentTab = static_cast<TabWidget *>(tabWidget->currentWidget());

    QString fileNameAssigned = dialogMessager->getFileNameToSaveTabWidget();

    if (fileNameAssigned.isEmpty())
        return; // юзер решил не сохраняться

    currentTab->setFileName(fileNameAssigned);

    saveCurrentTab(currentTab);
}
//////////////////////////////////////////////
void MainInterface::askBeforeDeviceDeletion_slot()
{
    programSettings.askBeforeDeletion = askBeforeDeviceDeletion->isChecked();
    emit setAskBeforeDeviceDeletion(programSettings.askBeforeDeletion);
}
//////////////////////////////////////////////
void MainInterface::openFileAct_slot()
{
    QString fileName = dialogMessager->getFileNameToOpenMap();

    if (fileName.isEmpty())
        return;

    openFile(fileName);
}
//////////////////////////////////////////////
void MainInterface::openFile(const QString &fileName)
{
    QString type;
    TabWidget *newTabWidget = XMLOperator::getTabWidget(&type, fileName, dialogMessager->getCustomDTIList());

   if (type == "admin")
   {
        newTabWidget->setAdminType(true);
   }

    if (!newTabWidget)
    {
        dialogMessager->cannotOpenFile();
        return;
    }

    tabWidget->addTab(newTabWidget, newTabWidget->getShortenFileName());
    newTabWidget->setDocumentToBeChanged(false);

    connectTabWidget(newTabWidget);

    enableTabWidgetActions(true);

    logDialog->appendText(QString(tr("Карта ") + newTabWidget->getFileName() + tr(" успешно загружена")),
                          ServiceStructures::LogMessageType::success);

    ignoreTabWidgetContentChanged = true; // а то она считает, что изменилась - а мы лишь заполнили карту загруженными данными да ресайзнули
}
//////////////////////////////////////////////
void MainInterface::warnFromDevice_slot(const QString &warnText, const ServiceStructures::LogMessageType &messageType)
{
    logDialog->appendText(warnText, messageType);
    dialogMessager->showWarningDialog(warnText);
}
//////////////////////////////////////////////
void MainInterface::limitingNumberOfConnections_slot()
{
    programSettings.usingIdealThreadCount = limitingNumberOfConnections->isChecked();
    if (!programSettings.usingIdealThreadCount)
    {
        QThreadPool::globalInstance()->setMaxThreadCount(MAX_THREAD_COUNT);
    }
    else
    {
        if (QThread::idealThreadCount() != -1)
            QThreadPool::globalInstance()->setMaxThreadCount(QThread::idealThreadCount());
    }
}
//////////////////////////////////////////////
void MainInterface::tabWidgetIndexChanged(const int &newIndex)
{
    for (int i = 0; i < tabWidget->count(); i++)
    {
        if (i == newIndex)
            continue;

        TabWidget *currentTab = static_cast<TabWidget *>(tabWidget->widget(i));
        currentTab->suspendThreads(true); // пробежим по всем, что уж там
                                          // вреда не будет, а перепроверить полезно
        disconnect(pingThread, &NetDeviceThreading::deviceIsAlive, currentTab, &TabWidget::setDevicesOnline);
        disconnect(currentTab, &TabWidget::startPingingDevice, pingThread, &NetDeviceThreading::addPingableDevice_slot);
        disconnect(currentTab, &TabWidget::stopPingingDevice, pingThread, &NetDeviceThreading::removePingableDevice_slot);
    }

    if (newIndex != -1)
    {
        TabWidget *currentTab = static_cast<TabWidget *>(tabWidget->widget(newIndex));
        connect(pingThread, &NetDeviceThreading::deviceIsAlive, currentTab, &TabWidget::setDevicesOnline);
        connect(currentTab, &TabWidget::startPingingDevice, pingThread, &NetDeviceThreading::addPingableDevice_slot);
        connect(currentTab, &TabWidget::stopPingingDevice, pingThread, &NetDeviceThreading::removePingableDevice_slot);
        currentTab->suspendThreads(false);
    }
}
//////////////////////////////////////////////
void MainInterface::timeToRePing_slot()
{
    int newValue = dialogMessager->getNumber(tr("Установка времени повторного опроса"),
                                             tr("Период повторного опроса (сек):"),
                                             programSettings.timeToRePing, 1, MAX_TIME_TO_RE_PING, 1);
    if (newValue != -1)
    {
        programSettings.timeToRePing = newValue;
        emit setTimeToRePing(programSettings.timeToRePing);
    }
}
//////////////////////////////////////////////
void MainInterface::timeToWaitForPingReplyModifyer_slot()
{
    int newValue = dialogMessager->getNumber(tr("Установка модификатора времени ожидания ответа"),
                                             tr("Модификатор (умножается на время повторного опроса):"),
                                             programSettings.timeToWaitForPingReplyModifyer, 1,
                                             MAX_TIME_TO_WAIT_FOR_PING_REPLY, 1);
    if (newValue != -1)
    {
        programSettings.timeToWaitForPingReplyModifyer = newValue;
        emit setTimeToWaitForPingReplyModifyer(programSettings.timeToWaitForPingReplyModifyer);
    }
}
//////////////////////////////////////////////
void MainInterface::closeEvent(QCloseEvent *event)
{
    if (tabWidgetsHaveUnsavedChanges())
    {
        // есть несохраненные вкладки
        if (!dialogMessager->closeProgramWithUnsavedChanges())
        {
            event->ignore();
            return;
        }
    }

    if (tabWidget->count())
    {
        // есть текущая вкладка
        TabWidget *currentTab = static_cast<TabWidget *>(tabWidget->currentWidget());
        currentTab->suspendThreads(true);
    }

    settingsWrite();

    dialogMessager->saveCustomDeviceTypeInterfaces();

    event->accept();

    // ============================================================================
    if (search)
    {
        search->close();
        delete search;
    }
    // ============================================================================

}
//////////////////////////////////////////////
MainInterface::~MainInterface()
{
    if (!logDialog->close())
    {
        logDialog->renameLogFileBeforeClose();
        delete logDialog;
    }
    delete search;
}


void MainInterface::addSearchDevices_slot(QVector<QString>* dev, int type)
{
    if (!tabWidget->count())
        return;

    TabWidget *currentTab = static_cast<TabWidget *>(tabWidget->currentWidget());
    currentTab->addSearchDevices_fromMenu(dev, type);
}
















