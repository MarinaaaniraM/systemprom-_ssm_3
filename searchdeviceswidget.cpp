#include "searchdeviceswidget.h"
#include "ui_searchdeviceswidget.h"

SearchDevicesWidget::SearchDevicesWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SearchDevicesWidget)
{
    ui->setupUi(this);
    checkArray = new QVector<QCheckBox*>();
    readyToAddDevives = new QVector<QString>();

    // Окошки
    ui->typeBox->setHidden(true);
    ui->typeLabel->setHidden(true);
    ui->addButton->setHidden(true);
    ui->allCheckBox->setHidden(true);

    ui->notFoundLabel->setHidden(true);
    ui->gridLayout_3->setSpacing(0);
    ui->gridLayout_3->setMargin(1);

}

SearchDevicesWidget::~SearchDevicesWidget()
{
    delete readyToAddDevives;
    delete checkArray;
    delete addresses;
    delete ui;
}


// ----------------------------------------------------------------------------
void SearchDevicesWidget::on_findButton_clicked()
{
    if (ui->broadcastBox->isChecked())
    {
        broadcastPing = new BroadcastPing_Linux(this);
        broadcastPing->setBroadcastAddress(ui->addressLine->text());
        broadcastPing->startingPing();
        addresses = broadcastPing->getEnableAddresses();
    }
    else
    {
        ping = new PingAvalialeDevices(this);
        ping->setAddress(ui->addressLine->text());  // Какой диапазон пинговать
        ping->startingPing();                       // Начинаем пинговать
        addresses = ping->getEnableAddresses();     // Складываем, чего напинговали
    }


    // Стираем инфу по старому поиску
    if (!checkArray->isEmpty())
    {
        for (int i = 0; i < checkArray->size(); ++i)
        {
            ui->gridLayout->removeWidget(checkArray->at(i));
            delete checkArray->at(i);
        }
        checkArray->clear();
    }

    // Доступные адреса имеются
    if (!addresses->isEmpty())
    {
        // Окошки
        ui->typeBox->setHidden(false);
        ui->typeLabel->setHidden(false);
        ui->addButton->setHidden(false);
        ui->allCheckBox->setHidden(false);

        ui->typeBox->setDisabled(false);
        ui->typeLabel->setDisabled(false);
        ui->addButton->setDisabled(false);
        ui->allCheckBox->setDisabled(false);

        ui->notFoundLabel->setHidden(true);

        // Генерируем сколько надо checkbox
        for (int i = 0; i < addresses->size(); ++i)
        {
            QCheckBox* box;
            box = new QCheckBox(addresses->at(i), this);
            checkArray->append(box);
            ui->gridLayout_3->addWidget(box);
        }
    }
    else    // Нет доступных адресов
    {
        // Окошки
        ui->typeBox->setHidden(false);
        ui->typeLabel->setHidden(false);
        ui->addButton->setHidden(false);
        ui->allCheckBox->setHidden(false);

        ui->typeBox->setDisabled(true);
        ui->typeLabel->setDisabled(true);
        ui->addButton->setDisabled(true);
        ui->allCheckBox->setDisabled(true);

        ui->notFoundLabel->setHidden(false);
    }
}


// ----------------------------------------------------------------------------
void SearchDevicesWidget::on_allCheckBox_clicked()
{
    bool isCheckedAll = false;
    if (ui->allCheckBox->isChecked())
        isCheckedAll = true;
    for (int i = 0; i < checkArray->size(); ++i)
    {
        checkArray->at(i)->setChecked(isCheckedAll);
    }
}


// ----------------------------------------------------------------------------
void SearchDevicesWidget::on_addButton_clicked()
{
    readyToAddDevives->clear();
    for (int i = 0; i < checkArray->size(); ++i)
    {
        if (checkArray->at(i)->isChecked())
            readyToAddDevives->append(addresses->at(i));
    }
    emit addSearchDevices(readyToAddDevives, ui->typeBox->currentIndex());
}


// ----------------------------------------------------------------------------
void SearchDevicesWidget::on_broadcastBox_stateChanged(int arg1)
{
    if (arg1)
    {
        ui->xxxLabel->setHidden(true);
        ui->addressLine->setFixedWidth(150);
        ui->addressLine->setText("153.15.255.255");
    }
    else
    {
        ui->xxxLabel->setHidden(false);
        ui->addressLine->setFixedWidth(110);
        ui->addressLine->setText("153.15.252");
    }
}


// ----------------------------------------------------------------------------
void SearchDevicesWidget::closeEvent(QCloseEvent *event)
{
}





















