//	���� ���� �������� ������ ������� �� "�����������".
//	����� ��������: ��� ��� 16 14:25:58 2011

#ifndef __NAMED_OBJECT
#define __NAMED_OBJECT

#include <QEvent>
#include <QString>

#define NODE_EVENT_TYPE  1001

// ����� ������� ������� ������ �� ����
class NodeEvent : public QEvent
{
public:
// �������� ����� ��������������� ����, ����� ������� �������, ��������� ����
    NodeEvent (ulong adr,  struct timeval t, ushort st)
        : QEvent( (QEvent::Type) NODE_EVENT_TYPE), addr( adr), tv( t), state( st) {}

    ulong getNodeState( struct timeval* t, ushort& st)
    {
        *t = tv;
        st = state;
        return addr;
    }
private:
      ulong addr;
      struct timeval tv;
      ushort state;
};

// ����� ������������� �������
class NamedObject
{
  public:
    inline NamedObject( char* name) : objName( name) {}
    virtual ~NamedObject() {}
    inline QString name() { return objName;}
  private:
    QString objName;
};

#endif
