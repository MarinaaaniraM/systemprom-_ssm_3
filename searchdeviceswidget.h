#ifndef SEARCHDEVICESWIDGET_H
#define SEARCHDEVICESWIDGET_H

#include <QWidget>
#include <pingavalialedevices.h>
#include <broadcastping_linux.h>

#include <addnewnetdevicewidget.h>
#include <QVector>
#include <QCheckBox>
#include <QLabel>

namespace Ui {
class SearchDevicesWidget;
}

class SearchDevicesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SearchDevicesWidget(QWidget *parent = 0);
    ~SearchDevicesWidget();

private slots:
    void on_findButton_clicked();
    void on_allCheckBox_clicked();
    void on_addButton_clicked();
    void on_broadcastBox_stateChanged(int arg1);

signals:
    void addSearchDevices(QVector<QString>* dev, int type);     // Заполнили массив адресов readyToAddDevives,
                                                                // теперь наносим на карту

private:
    Ui::SearchDevicesWidget *ui;

    PingAvalialeDevices* ping;
    BroadcastPing_Linux* broadcastPing;
    QVector<QString>* addresses;            // Список адресов, которые напинговал ping
    QVector<QCheckBox*>* checkArray;        // Массив графических checkbox
    QVector<QString>* readyToAddDevives;    // Список адресов, которые отмечены checkbox ля добавления на карту

    void closeEvent(QCloseEvent *event);
};

#endif // SEARCHDEVICESWIDGET_H
