/* тут описываются сетевые устройства с _логической_ точки зрения. */


#ifndef NETLOGICALDEVICES_H
#define NETLOGICALDEVICES_H



#include <QObject>
#include <QString>
#include <QHostAddress>
#include <QDesktopServices>
#include <QUrl>
#include "GraphicalItems.h"
#include "ServiceStructures.h"



// класс для описания сетевого устройства
class NetDevice : public QObject
{
    Q_OBJECT


public:
    explicit NetDevice(QString deviceName, QObject *parent = 0);
    explicit NetDevice(QObject *parent = 0);
    bool isMultiLevel() const;
    bool network_isBeingPinged() const;
    void network_setPinging(const bool &value);
    QString getMultiMapName() const;
    NetDeviceGraphicalItem * getGraphicalItem();
    const NetDeviceGraphicalItem * getGraphicalItem() const;
    const ServiceStructures::DeviceTypeInterface *getDTI() const;
    QString getName() const;
    QString getAboutDeviceText() const;
    QString getNetworkAddressString() const;
    QString getNetworkAddressStringFormatted() const;
    void setName(const QString &text);
    void setAboutDeviceText(const QString &text);
    void setMultiMapName(const QString &mapName);
    void network_setNetworkAddress(const QHostAddress &address);
    bool network_setNetworkAddress(const QString &address); // true if ok, false otherwise
    void setDeviceTypeInterface(const ServiceStructures::DeviceTypeInterface *dti);
    QHostAddress network_getNetworkAddress() const;
    QString getUUID() const;
    void setUUID(const QString &value);
    void resetThread();
    void suspendThread(const bool &suspend);

    virtual ~NetDevice();


protected:
    bool multiLevel = false; // является ли девайс представлением "вложенной" карты сети, т.е. еще одного рисунка?
    bool network_shouldUsePing = false; // пинговать ли устройство?

    NetDeviceGraphicalItem *graphicalItem = 0;

    QString name;
    QString aboutDevice;   // описание устройства
    QString deviceUUID;
    const ServiceStructures::DeviceTypeInterface *deviceTypeInterface;
    QString multiMapName;  // если через устройство можно перейти на другую карту, то тут - путь к ней; если пустая строка - перейти нельзя
    QHostAddress network_address;


protected slots:
    void userRequestToEditDevice();
    void userSelectedDevice();
    void userRequestToConnectDevices();
    void userRequestToDeleteDevice();
    void userDoubleClickedItem();
    void deviceOnlineStatusChanged(const bool &isOnline);

    // ========================================================================
    void openWebInterface();
    // ========================================================================


protected:
    void createThread();
    void destroyThread();


public:
    bool operator ==(const NetDevice &other) const;


signals:
    void requestToEditDevice(NetDevice *thisDevice); // должен вызываться с параметром this!
    void graphicalItemIsSelected(NetDevice *thisDevice); // должен вызываться с параметром this!
    void requestToConnectDevices(NetDevice *thisDevice); // должен вызываться с параметром this!
    void requestToDeleteDevice(NetDevice *thisDevice); // должен вызываться с параметром this!
    void requestToOpenMultiMap(NetDevice *thisDevice); // должен вызываться с параметром this!
    void warning(QString text, ServiceStructures::LogMessageType messageType);
    void startPingingMe(QString uuid, QString ipAddr);
    void stopPingingMe(QString uuid);
    void deviceOnlineStatusChanged_writeToLog(QString text, ServiceStructures::LogMessageType messageType);


};


////////////////////////////////////////////////////////

class NetDeviceLink : public QObject            // класс для описания связи сетевых устройств
{
    Q_OBJECT


public:
    explicit NetDeviceLink(NetDevice *device1, NetDevice *device2, QObject *parent = 0);
    void paintElements(QGraphicsScene *sceneForPainting);
    const NetDevice * getFirstDevice() const;
    const NetDevice * getSecondDevice() const;
    QPointF getFirstPointPos() const;
    QPointF getSecondPointPos() const;
    void setFirstPointPos(const qreal &x, const qreal &y);
    void setSecondPointPos(const qreal &x, const qreal &y);
    void setFirstPointPosBeforePaint(const qreal &x, const qreal &y);
    void setSecondPointPosBeforePaint(const qreal &x, const qreal &y);

    virtual ~NetDeviceLink();


protected:
    NetDevice *firstDevice = 0;     // c логической точки зрения, связь - не более чем некое сочленение
    NetDevice *secondDevice = 0;    // между двумя NedDevice'ами

    NetDevicePointItem *pointItem_1 = 0;
    NetDevicePointItem *pointItem_2 = 0;
    NetDeviceLineItem *lineItem_1 = 0;
    NetDeviceLineItem *lineItem_2 = 0;
    NetDeviceLineItem *lineItem_3 = 0;

    QPointF pointItem_1_pointToDraw; // используется в paintElements() для выставления координат точек
    QPointF pointItem_2_pointToDraw; // используется в paintElements() для выставления координат точек
    QPointF firstDevicePos; // используется в paintElements() для выставления координат точек
    QPointF secondDevicePos; // используется в paintElements() для выставления координат точек


public:
    bool operator ==(const NetDeviceLink &other) const;


protected:
    void connectComponents();
    void createElements();


protected slots:
    void userRequestedToDeleteLink();


signals:
    void requestToDeleteDevice(NetDeviceLink *thisLink); // должен вызываться с параметром this!


};



#endif // NETLOGICALDEVICES_H
