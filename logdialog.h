#ifndef LOGDIALOG_H
#define LOGDIALOG_H



#include <QDialog>
#include <QTextEdit>
#include <QString>
#include "ServiceStructures.h"



class LogDialog : public QDialog
{
    Q_OBJECT


public:
    explicit LogDialog(QWidget *parent = 0);
    void renameLogFileBeforeClose(); // переименовывает файл лога, добавляя ему дату
                                     // вызывать только перед закрытием программы, после сохранения файла лога!
    void setSaveLogFileOnClose(const bool &shoudBeSaved);


public slots:
    void appendText(const QString &textToAppend,
                    const ServiceStructures::LogMessageType &messageType = ServiceStructures::LogMessageType::normal);
    void setOutputScroll(const bool &scroll);
    void clearLog();

protected:
    QTextEdit *editor;
    bool outputScroll = true;
    bool saveLogFileOnClose = true;

    void scrollTextToEnd();
    void saveTextToLog();
    void restoreTextFromLog();

    void closeEvent(QCloseEvent *event);

signals:
    void warning(QString warnText);
    void closeEventHappened();

protected:
    enum LocalDefines
    {
        LogMessageIconSquareSize = 12,
        LogDialogMinimalWidth = 300
    };

};

#endif // LOGDIALOG_H
