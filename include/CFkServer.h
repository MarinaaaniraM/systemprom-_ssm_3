#ifndef __FK_SERVER_MESSAGE_QUEUE__
#define __FK_SERVER_MESSAGE_QUEUE__

//#include "BaseTypes.h"
#include "CServerQueue.h"


class CFkServer : public CServerQueue
{

public:
	 CFkServer();
    virtual ~CFkServer();

     virtual int Create();

	 // ����������� ���������
	 //========================
     // FROM MODI
     //------------
     // zapros parametrov MODI
     virtual void OnModiParam();

     // zavershenie raboti MODI
     virtual void OnModiStop();
     
     virtual void OnModiStart();

     // poluchena adresnaya tablica
     virtual void OnModiRecAddr();

     // sozdanie adresnoy tablici
     virtual void OnModiCreateAddr(ADDR_INFO *AddrInfo);

     // zapret raboti s adresnaya tablica
     virtual void OnModiLockAddr();

     // razreshenie raboti s adresnaya tablica
     virtual void OnModiUnLockAddr();

	 // avariynoe zavershenie raboti
     virtual void OnModiError(ERROR_INFO  *Error);
     
     virtual void OnModiStatusLine(NETWORK_INFO  *NetInfo);
     
     virtual void OnGetRemoteIP();
     
     virtual void OnGetLocalAbn();

     // FROM TM
     //------------
     // zapros parametrov TM
     virtual void OnTmParam();
     // nachalo raboti TM
     virtual void OnTmStart();

	 // zavershenie raboti TM
     virtual void OnTmStop();

    // komanda restart TM OK
     virtual void OnTmRestartOk();

	 // ostanov modulia napravleniia
     virtual void OnTmStopLine(LINE_INFO *Line);

     // zapusk modulia napravleniia
     virtual void OnTmStartLine(LINE_INFO *Line);

     // avariynoe zavershenie raboti
     virtual void OnTmError(ERROR_INFO  *Error);

	 // ustanovlenie soedineniya po napravleniu
     virtual void OnTmConnectLine(LINE_INFO *Line);

	 // razriv soedineniya po napravleniu
     virtual void OnTmDsConnectLine(LINE_INFO *Line);

     // ����������� � �����������/���������� ��������
     virtual void OnAbonentConnect(ABONENT_INFO* AbnInfo);
     virtual void OnAbonentBusConnect(ABONENT_INFO* AbnInfo);
     
     // zapros tablici statistiki
     virtual void OnGetStatistics();

	 // ���������� ���������
	 //========================
     // TO MODI
     //------------
	 // parametri MODI
	 int MSG_ParamModi(MODI_PARAMETERS *Param);

	 // ostanov MODI
	 int MSG_StopModi();

	 // restart MODI
	 int MSG_RestartModi();
	 
	 int MSG_SetRemoteIP(BYTE *Array, ULONG Size);
	 
	 int MSG_SetLocalAbn(char  *Path);

     // TO TM
     //------------
     // parametri TM
	 int MSG_ParamTm(TM_PARAMETER *Param);

	 // ostanov TM
	 int MSG_StopTm();

	 // restart TM
	 int MSG_RestartTm();

	 // ostanov modulia napravleniia
	 int MSG_StopLine(LINE_INFO *LineInfo);

	 // zapusk modulia napravleniia
	 int MSG_StartLine(LINE_INFO *LineInfo);

	 // ����� �� ��� ����������
	 //-------------------------
	 // zapret zapisi v strukturu statistiki
	 int MSG_LockStat();

	 // razreshenie zapisi v strukturu statistiki
         int MSG_UnLockStat();
	 
	 // ���������� ������ ������� �������������
	 int MSG_ReLoadRoute();

	 // sozdanie statistiki
	 int MSG_CreatStat(ADDR_INFO *StatInfo);


protected:
     virtual int OnFkMessages(MsgHeader* Header);
     int SendMessageToTm(ULONG IdMessage, BYTE* Packet, ULONG Size);
     int SendMessageToModi(ULONG IdMessage, BYTE* Packet, ULONG Size);

};


#endif
