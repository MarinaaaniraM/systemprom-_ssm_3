///////////////////////////////////////////////////////////////////////
/// ClientCommServer.h: interface for the CClientCommServer class.
/// CClientCommServer - ��������� �������� �������������� � �������� �����, author:Sergey Nikulin
///////////////////////////////////////////////////////////////////////
#ifndef _CLIENTCOMMSERVER_H
#define _CLIENTCOMMSERVER_H


#include "CMsgSocket.h"
#include "CEvent.h"
#include "CPriorityMessageList.h"


#define HANDLER_COMMAND    0x01   // 00000001
#define HANDLER_DATA       0x02   // 00000010
#define CONNECT_SERVER     0x04   // 00000100 - srazu ustanovit' soedinenie
#define RECONNECT_SERVER   0x08   // 00001000 - pole Reconect = TRUE

/// PACKET - ��������� ��������� ������
typedef struct
{
  GHEADER  m_Header;
  BYTE    *m_Buf;
  Addr    *m_Abonent;

} PACKET;

// Potok na chtenie iz ocheredi
void* Handler(void* Ptr);
// Potok na chtenie iz ocheredi
void* HandlerData(void* Ptr);


/// CClientCommServer - ��������� �������� �������������� � �������� �����, author:Sergey Nikulin
class CClientCommServer : public CMsgSocket
{
public:
	CClientCommServer();
	virtual ~CClientCommServer();
public:
/// ������ ������ � �������� �����.
/// \param IP_Server ����� ������� ����� (� ���� ������)
/// \param ServerPort ���������� ���� ������� ����� (� ���� ������)
/// \param ID_Abonent ������������� �������� ��� �������������� �� ������� �����
/// \param Flag ������� ����� ��������� ��������� ����������
  virtual int Initialization(char* IP_Server, WORD ServerPort,  ULONG ID_Abonent, BYTE Flag);

/// ������ ������ � �������� �����.
/// \param IP_Server ����� ������� ����� (����� � ������� ������� ����)
/// \param ServerPort ���������� ���� ������� ����� (����� � ������� ������� ����)
/// \param ID_Abonent ������������� �������� ��� �������������� �� ������� �����
/// \param Flag ������� ����� ��������� ��������� ����������
	virtual int Initialization(ULONG IP_Server, WORD ServerPort, ULONG ID_Abonent,
		BYTE Flag = (HANDLER_COMMAND | HANDLER_DATA | CONNECT_SERVER ));

	/// �������� ������.
	/// \param Msg ��������� �� ������ ������
	/// \param Flag ���� =1, ������ �� ����������, ������������ ������� ������ Msg
	int SendMessage(BYTE* Msg, BOOL Flag);

/// �������� ������.
	/// \param Packet ��������� PACKET, ��������������� ������������ �����
	int SendMessageEx(PACKET *Packet);

	/// ������ �����
	/// \param Msg ��������� �� ������ ��������� ������
	int GetMessage(BYTE** Msg);

	/// ��������� ������� � �������
	ULONG GetCount();

	/// �������� ������
	void WaitForEvent();

        int GetParameters();
        int DisConnect();
		int SetLogFile(WORD TypeProtocol = 0);
        int Start(BOOL Recon = TRUE);
	void Clear();

/// ��������� � ���������� � ��������
	virtual void OnConnectServer();

/// ��������� �� ��������� tcp-����������
	virtual int  OnConnect();

/// ��������� � ������� tcp-����������
	virtual int  OnDisconnect(int ErrCode);

	/// ��������� � ������� ���������� � ��������
        virtual void OnServerDisConnect();
        virtual void OnServerStop();
        virtual void OnQueueSize();
        virtual void OnSendLock(ULONG Code , in_addr IpNet);
        virtual void OnSendUnLock(ULONG Code, in_addr IpNet );
        virtual void OnSendError(ULONG IdPacket, int ErrCode, in_addr addr);
        virtual void OnParameters(ULONG Count, LINE_INFO* ChannelInfo);
        virtual void OnReceivePacket(BYTE* msg);

	friend void* Handler(void* Ptr);
	friend void* HandlerData(void* Ptr);



private:

	/// ���������� ���������� � ��������
	virtual BOOL OnReceive(BYTE* msg);

  void MessageHandlers(BYTE* Packet);

//public:

        int MSG_GetConnect();
        int MSG_DisConnect();
        int MSG_GetParameters();
		int MSG_SetLogFile(WORD TypeProtocol);

protected:
	Addr m_Address;


	/// ������� ���������  �������
	CMessageList         *m_OutputList;

	/// ������� �������� ���������� �������
	CPriorityMessageList *m_InputList;

	/// ��������� �����
	pthread_t             m_ThreadIdHandler;
	pthread_t             m_ThreadIdHandlerData;

	BOOL                  m_Connect;
        BOOL                  m_FlagCommand;
        BOOL                  m_FlagData;

};

#endif
