#ifndef __ALL_ERROR_TYPE__
#define __ALL_ERROR_TYPE__


// vsio normal'no
#define  TYPE_OK                            0

// pazmer bloka
#define  ERROR_BLOCK_SIZE                   1000

#define  BASE_ERROR                         TYPE_OK + ERROR_BLOCK_SIZE
  //1000

// obshie tipi oshibok
#define NULL_POINTER                        BASE_ERROR + 1   // nulevoi ukazatel
#define ZERO_SIZE                           BASE_ERROR + 2   // nulevaya dlinna 
#define EMPTY_MEMORY                        BASE_ERROR + 3   // pri vizove new 
#define ERROR_SIZE                          BASE_ERROR + 4   // nulevaya dlinna 
#define EXIT_THREAD                         BASE_ERROR + 5   // ���������� ������
#define ERROR_PACKET                        BASE_ERROR + 6   // ����� ��� : ������ � ���������
#define ERROR_STRING                        BASE_ERROR + 7   // ����� ��� : ������ � ������



// oshibki kriticheskoy sekcii
#define ER_CRITICAL			                BASE_ERROR + ERROR_BLOCK_SIZE	
// 2000
#define ERC_INIT                            ER_CRITICAL + 1              // pri vizove pthread_mutex_init


// oshibki ocheredi
#define ER_QUEUE			                ER_CRITICAL + ERROR_BLOCK_SIZE	
// 3000
#define ERQ_EMPTYNAME                       ER_QUEUE + 1                  // net imeni
#define ERQ_CLOSE                           ER_QUEUE + 2                  // pri vizove close
#define ERQ_UNLINK                          ER_QUEUE + 3                  // pri vizove unlink
#define ERQ_GETATTR                         ER_QUEUE + 4                  // pri vizove getattr
#define ERQ_OPEN                            ER_QUEUE + 5                  // pri vizove open
#define ERQ_SETATTR                         ER_QUEUE + 6                  // pri vizove setattr
#define ERQ_SENDMSG                         ER_QUEUE + 7                  // pri vizove send
#define ERQ_LENBUF                          ER_QUEUE + 8                  // razmer bufera malen'kiy
#define ERQ_RECEIVE                         ER_QUEUE + 9                  // pri vizove receive

// 4000
// oshibki spiska
#define ER_LIST			                    ER_QUEUE + ERROR_BLOCK_SIZE	
#define ERL_EMPTY                           ER_LIST + 1   // pustoi spisok 
#define ERL_END                             ER_LIST + 2   // konec spiska 
#define ERL_LOCKED                          ER_LIST + 3   // spisok zablokirovan
#define ERL_FULL                            ER_LIST + 4   // perepolnenie spiska 


// 5000
// oshibki socketa
#define ER_SOCKET			                ER_LIST + ERROR_BLOCK_SIZE	
#define ERS_NOHOST		                    ER_SOCKET + 1  // nepravilnij IP-adres
#define ERS_NOPORT		                    ER_SOCKET + 2  // nepravilnij IP-port
#define ERS_NOPROT		                    ER_SOCKET + 3  // nepravilnij protocol
#define ERS_NOROUTE		                    ER_SOCKET + 4  // net marshruta dlja soedinenija
#define ERS_ACCESS		                    ER_SOCKET + 5  // net prav
#define ERS_INUSE		                    ER_SOCKET + 6  // popitka povtornogo sozdania soedinenija
#define ERS_REFUSED		                    ER_SOCKET + 7  // otkaz v ustanovke soedinenija
#define ERS_TIMEOUT		                    ER_SOCKET + 8  // popitka povtornogo sozdania soedinenija
#define ERS_NOCON		                    ER_SOCKET + 9  // soedinenija net ili razorvano
#define ERS_AGAIN		                    ER_SOCKET + 10  // trebuetsja povtornoe 4tenie dannih


// 6000
// oshibki sobitiya
#define ER_EVENT			                ER_SOCKET + ERROR_BLOCK_SIZE	
#define ERE_INIT                            ER_EVENT + 1              // pri vizove pthread_
#define ERE_BYTIME                          ER_EVENT + 2              // vihod iz ogidaniya po vremeni


//7000
// oshibki obshey pamiati
#define ER_MEMORY			                ER_EVENT + ERROR_BLOCK_SIZE	
#define ERM_OPEN                            ER_MEMORY + 1   // pri otkritii oblasti     
#define ERM_MAP                             ER_MEMORY + 2   // pri vizove mmap     


// 8000
// oshibki servera soobsheniy
#define ER_SERVER			                ER_MEMORY + ERROR_BLOCK_SIZE	
#define ERSRV_TYPE                          ER_SERVER + 1   // tip servera     


// 9000
// oshibki spiska v obshey pamyti
#define ER_MEMORY_LIST		                ER_SERVER + ERROR_BLOCK_SIZE	
#define ERML_GETMESSAGE                     ER_MEMORY_LIST + 1   // popitka povtornogo vizova GetMessage     
#define ERML_EMPTY                          ER_MEMORY_LIST + 2   // spisok pust     
#define ERML_CALCBUFFER                     ER_MEMORY_LIST + 3   // oshibka pri vichislenii adresa buffera     
#define ERML_GETBUFFER                      ER_MEMORY_LIST + 4   // popitka povtornogo vizova GetFreeBuffer     
#define ERML_FREEMESSAGE                    ER_MEMORY_LIST + 5   // 
#define ERML_FREEBUFFER                     ER_MEMORY_LIST + 6   // 

// 10000
// Oshibki spiska clientov
#define ER_CLIENT_LIST		                ER_MEMORY_LIST + ERROR_BLOCK_SIZE	
#define ERCL_ABN_COUNT		                ER_CLIENT_LIST + 1   //	Header->Count == 0
#define ERCL_ABN_NOFOUND                    ER_CLIENT_LIST + 2   // klient poluchatel' ne nayden v spiske
#define ERCL_NEW_CLIENT                     ER_CLIENT_LIST + 3   // oshibka pri sozdanii novogo klienta
#define ERCL_ADD_CLIENT                     ER_CLIENT_LIST + 4   // oshibka pri dobavlenii novogo klienta v spisok
#define ERCL_LINE_NOFOUND                   ER_CLIENT_LIST + 5   // zaproshennoe napravlenie ne naideno
#define ERCL_LINE_ISBLOCK                   ER_CLIENT_LIST + 6   // napravlenie blokirovano


//11000
// Oshibki spiska abonentov
#define ER_ABONENT_LIST		                ER_CLIENT_LIST + ERROR_BLOCK_SIZE	
#define ERAB_ADD_ABONENT                    ER_ABONENT_LIST + 1   // oshibka pri dobavlenii novogo abonenta v spisok
#define ERAB_ABN_NOFOUND                    ER_ABONENT_LIST + 2   // abonent v spiske ne nayden
#define ERAB_ABN_ZERO                       ER_ABONENT_LIST + 3   // spisok pust


//12000
// Oshibki spiska setey
#define ER_NETWORK_LIST		                ER_ABONENT_LIST + ERROR_BLOCK_SIZE	
#define ERNET_ADD                           ER_NETWORK_LIST + 1   // oshibka pri dobavlenii novoy seti v spisok
#define ERNET_NOFOUND                       ER_NETWORK_LIST + 2   // set' v spiske ne nayden
#define ERNET_ZERO                          ER_NETWORK_LIST + 3   // spisok pust
#define ERNET_NOPARENT                      ER_NETWORK_LIST + 4   // eto kornevoy element

//13000
// Oshibki spiska setey
#define ER_POST_LIST		                ER_NETWORK_LIST + ERROR_BLOCK_SIZE	
#define ERPOST_ID_CLIENT                    ER_POST_LIST + 1      // oshibka v ID klienta
#define ERPOST_DONT_WORK                    ER_POST_LIST + 2      // server peregrugen
#define ERPOST_FORMAT                       ER_POST_LIST + 3      // oshibka v formate soobsheniya


//14000
//Oshibki zagolovka paketa
#define ER_PACKET		                    ER_POST_LIST + ERROR_BLOCK_SIZE	
#define ERPACKET_SIZE                       ER_PACKET + 1          // oshibochnaya dlinna paketa
#define ERPACKET_SRC                        ER_PACKET + 2          // oshibka v pole otpravitelya
#define ERPACKET_ADDR                       ER_PACKET + 3          // oshibka v pole poluchatelia
#define ERPACKET_TYPE                       ER_PACKET + 4          // tip protokola ne sootvetctvuet tipu modulia 
#define ERPACKET_CODE                       ER_PACKET + 5          // oshibka v pole identifikatora paketa 

//15000
// Oshibki klassa CIPRoute
#define ERROR_C_IP_ROUTE		            ER_PACKET + ERROR_BLOCK_SIZE
#define ERROR_C_IP_ROUTE_SOCKET	            ERROR_C_IP_ROUTE + 1	//Socket() error
#define ERROR_C_IP_ROUTE_BIND	            ERROR_C_IP_ROUTE + 2	//Bind() error
#define ERROR_C_IP_ROUTE_CLOSE	            ERROR_C_IP_ROUTE + 3	//Close() error
#define ERROR_C_IP_ROUTE_SEND	            ERROR_C_IP_ROUTE + 4	//Send() error
#define ERROR_C_IP_ROUTE_RECV	            ERROR_C_IP_ROUTE + 5	//Recv() error
#define ERROR_C_IP_ROUTE_NO_GW	            ERROR_C_IP_ROUTE + 6	//Ne poluchen gateway

//16000
//Oshibki failovoy sistemi
#define ER_FILE		                        ERROR_C_IP_ROUTE + ERROR_BLOCK_SIZE	
#define EROPEN_DIR                          ER_FILE + 1          // oshibka pri otkritii papki
#define ERCREATE_DIR                        ER_FILE + 2          // oshibka pri sozdanii
#define ER_END_DIR                          ER_FILE + 3          // doshli do konca papki
#define ER_DELETE                           ER_FILE + 4          // oshibka pri udalenii papki
#define ER_OPEN_FILE                        ER_FILE + 5          // oshibka pri otcritii faila
#define ER_CLOSE_FILE                       ER_FILE + 6          // oshibka pri zakritii faila
#define ER_SEEK                             ER_FILE + 7          // oshibka pri smeshenii v faile
#define ER_RENAME                           ER_FILE + 8          // oshibka pri vizove rename
#define ER_END_FILE                         ER_FILE + 9          // doshli do konca faila

//17000
#define  ER_MUTEX						   (ER_FILE + ERROR_BLOCK_SIZE)
#define  ER_MUTEX_INIT					   (ER_MUTEX + 1)
#define  ER_MUTEX_DESTROY				   (ER_MUTEX + 2) 
#define  ER_MUTEX_LOCK					   (ER_MUTEX + 3)
#define  ER_MUTEX_UNLOCK				   (ER_MUTEX + 4)

//18000
#define  ER_SHMEM 						   (ER_MUTEX + ERROR_BLOCK_SIZE)
#define  ER_SHMEM_FTOK					   (ER_SHMEM + 1)
#define  ER_SHMEM_SHMGET				   (ER_SHMEM + 2)
#define  ER_SHMEM_SHMAT					   (ER_SHMEM + 3)
#define  ER_SHMEM_SHMCTL				   (ER_SHMEM + 4)
#define  ER_SHMEM_SHMDT					   (ER_SHMEM + 5)
#define  ER_SHMEM_SEMGET				   (ER_SHMEM + 6)
#define  ER_SHMEM_SEMCTL				   (ER_SHMEM + 7)
#define  ER_SHMEM_SEMOP					   (ER_SHMEM + 8)

//19000
#define  ER_ACCESS_MATRIX 				   (ER_SHMEM + ERROR_BLOCK_SIZE)
#define  ER_AM_LOCAL_CLIENT_ARRAY_FULL	  		   (ER_ACCESS_MATRIX + 1) //Spisok lokal'nih abonentov perepolnen
#define  ER_AM_REMOTE_CLIENT_ARRAY_FULL	 		   (ER_ACCESS_MATRIX + 2) //Spisok udalen'nih abonentov perepolnen
#define  ER_AM_LINES_ARRAY_FULL				   (ER_ACCESS_MATRIX + 3) //Spisok napravleniy perepolnen
#define  ER_AM_LOCK                       		   (ER_ACCESS_MATRIX + 4)

//20000
#define  ER_TLK 					   (ER_ACCESS_MATRIX + ERROR_BLOCK_SIZE)
#define  ER_TLK_LOGIN					   (ER_TLK + 1)
#define  ER_TLK_CREATE_MSG		          	   (ER_TLK + 2)
#define  ER_TLK_SEND_MSG		          	   (ER_TLK + 3)

//21000
#define  ER_UDP_PROTOCOL 				   (ER_TLK + ERROR_BLOCK_SIZE)
#define  ER_UDP_UNDEFINE				   (ER_UDP_PROTOCOL + 1)  // popali v neopredelennoe sostoyanie
#define  ER_UDP_CONFLICT				   (ER_UDP_PROTOCOL + 2)  // konflict interesov


//22000
#define  ER_BUS				  		   (ER_UDP_PROTOCOL + ERROR_BLOCK_SIZE)
#define  ER_BUS_OPENFILE				   (ER_BUS + 1)  // oshibka otkritiya faila
#define  ER_BUS_READ					   (ER_BUS + 2)  // oshibka chteniya elementa
#define  ER_BUS_PACKET					   (ER_BUS + 3)  // oshibka pri obrabotke paketa
#define  ER_BUS_NOFOUND					   (ER_BUS + 4)  // zaproshennaya shina ne naidena
#define  ER_BUS_ABN_NOFOUND				   (ER_BUS + 5)  // abonent shini ne naiden


#endif
