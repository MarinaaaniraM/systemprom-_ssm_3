/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Basic Socket class, author: Roman St.Zhavnis
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _CUdpSocket_H
#define _CUdpSocket_H

#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include "BaseTypes.h"
#include "ErrorType.h"

#ifndef SOCKET
typedef int SOCKET;
#endif

#ifndef PORT
typedef unsigned short PORT;
#endif

extern int set_address(char *hname, char *sname, struct sockaddr_in *sap, char *protocol );

class CUdpSocket{
public:
	CUdpSocket();
	~CUdpSocket();
public: /// �������� ����������. ��-��������� - ����������
	/// ����� � server=1 ������ ������������ �����
	/// hname ����� ����: "127.0.0.1" ��� "localhost", ���� NULL ������������� INADDR_ANY
	/// sname: "25" ��� "smtp"
	int Create(char *hname, char *sname, BYTE prior,  BOOL server = 0 );
	/// ip � port - ����� � network order
	int Create(UINT ip, PORT port, BYTE prior,  BOOL server = 0 );
	/// ip � port - ����� � network order
	int Connect(UINT ip = INADDR_ANY, PORT port = 0);

	/// �������� ������� (�� ������ select)
	/// �������� ����� - ����� ������� ���� (������/������/������) � ��������� �� ��������� ������� �� ��������� ��������
	/// ��������� ����� - ����� ������� ���������. to=NULL - ��� ��������, ����� �������
	int WaitForEvent(BYTE &rd, BYTE &wr, BYTE &ex, struct timeval *to);
	/// ��������� ����������
	int Disconnect();
	/// ������
	/// ��������� � ����� msg �� ����� ��� len ���� (�������� ����� ��������� ���� ��������������� � len)
	int Read(BYTE *msg, ULONG &len);
	/// ������
	/// �������� len ���� �� ������ msg
	int Write(BYTE *msg, ULONG len);
	/// �������� len ���� �� ������ msg
	/// ip � port - ����� � network order
	int WriteTo(BYTE *msg, ULONG len,UINT ip = INADDR_ANY, PORT port = 0);
	/// ������ ���������� - ��� UDP ����� �������� � �� �������� ������ ����
	int isConnected();		/// ������ ���������� 1=����
	/// ������ IP ���������� ����� ���������� ()
	/// ���� ���������� ���, �� ��� ������� - ��������� ��� ������� ����� ��� INADDR_ANY
	/// ���� ���������� ���, �� ��� ������� ������ ����� ����������
	ULONG GetRemoteAdr();
	/// ���������� ���������� ���� ���������� �����
	PORT GetRemotePrt();
	// ������ IP ����������� ������ ����� ���������� (��� ������� ������ ����� ���������)
	inline ULONG GetLocalAdr () { return ladr.sin_addr.s_addr;};

	inline ULONG GetRxBytes() {return rxBytes;};
	inline ULONG GetTxBytes() {return txBytes;};
	inline bool isServer() {return((srv)?(TRUE):(FALSE)); };

	struct sockaddr_in radr; /// � ��� ���������
	struct sockaddr_in ladr; /// ������ ��������� (��� ������� ���� �� �����������)
private:
	SOCKET s; ///�����
	int con;  ///������ ����������
	BYTE srv; /// ���� �������
	BYTE ip_tos; /// ��������� ip
	ULONG rxBytes, txBytes; /// ���������� ������/�������� �������
	/// ����� ���������� ��� ������. ���� ip!=INADDR_ANY, �� ������ � ������ ip
	/// ip - ����o � network order
	//int WaitForConnect(UINT ip = INADDR_ANY);

};


#endif
