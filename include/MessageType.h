#ifndef __MESSAFES_TYPE__
#define __MESSAFES_TYPE__

#include "BaseTypes.h"

// tipi soobsheniy
//------------------------------
#define MSG_BASE            100
#define MSG_PAUSE	    100
#define MSG_BLOCK	    1000

// ot FK
//=========
#define MFK_BASE			    MSG_BASE + MSG_PAUSE

#define MFK_PARAM_MODI          MSG_BASE + 1  // parametri MODI
#define MFK_STOP_MODI           MSG_BASE + 2  // ostanov MODI
#define MFK_START_MODI          MSG_BASE + 3  // zapusk  MODI
#define MFK_RESTART_MODI        MSG_BASE + 4  // restart MODI
#define MFK_LOCK_STAT_MODI      MSG_BASE + 5  // zapret zapisi v strukturu statistiki
#define MFK_UNLOCK_STAT_MODI    MSG_BASE + 6  // zapret zapisi v strukturu statistiki
#define MFK_CREATE_STAT_MODI    MSG_BASE + 7  // razreshenie zapisi v strukturu statistiki
#define MFK_SET_REMOTE_IP_MODI  MSG_BASE + 8  // massiva IP udalennih serverov
#define MFK_SET_LOCAL_ABN_MODI  MSG_BASE + 9  // zapros massiva abonentov

#define MFK_PARAM_TM            MSG_BASE + 10  // parametri TM
#define MFK_STOP_TM             MSG_BASE + 11  // ostanov TM
#define MFK_RESTART_TM          MSG_BASE + 12  // restart TM
#define MFK_LOCK_STAT_TM        MSG_BASE + 13  // zapret zapisi v strukturu statistiki
#define MFK_UNLOCK_STAT_TM      MSG_BASE + 14  // razreshenie zapisi v strukturu statistiki
#define MFK_CREATE_STAT_TM      MSG_BASE + 15  // sozdanie strukturi statistiki
#define MFK_STOP_LINE_TM        MSG_BASE + 16  // ostanov modulia napravleniia
#define MFK_START_LINE_TM       MSG_BASE + 17  // zapusk modulia napravleniia
#define MFK_RELOAD_ROUTE        MSG_BASE + 18  // ���������� ������ ���� route.cfg

// ot MODI
//==========
#define MMODI_BASE			    MFK_BASE + MSG_BLOCK + MSG_PAUSE

#define MMODI_PARAM_FK          MMODI_BASE + 1  // zapros parametrov MODI
#define MMODI_STOP_FK           MMODI_BASE + 2  // zavershenie raboti MODI
#define MMODI_REC_ADDR_FK       MMODI_BASE + 3  // poluchena adresnaya tablica
#define MMODI_CREATE_ADDR_FK    MMODI_BASE + 4  // sozdanie adresnoy tablici
#define MMODI_LOCK_ADDR_FK      MMODI_BASE + 5  // zapret raboti s adresnaya tablica
#define MMODI_UNLOCK_ADDR_FK    MMODI_BASE + 6  // razreshenie raboti s adresnaya tablica
#define MMODI_ERROR_FK          MMODI_BASE + 7  // avariynoe zavershenie raboti
#define MMODI_START_FK          MMODI_BASE + 8  // nachalo raboti MODI
#define MMODI_GET_REMOTE_IP_FK  MMODI_BASE + 9  // zapros massiva IP udalennih serverov
#define MMODI_GET_LOCAL_ABN_FK  MMODI_BASE + 10 // zapros massiva abonentov
#define MMODI_STATUS_LINE_FK    MMODI_BASE + 11 // sostoyanie napravleniya

#define MMODI_CREATE_ADDR_TM    MMODI_BASE + 12 // sozdanie adresnoy tablici
#define MMODI_LOCK_ADDR_TM      MMODI_BASE + 13 // zapret raboti s adresnaya tablica
#define MMODI_UNLOCK_ADDR_TM    MMODI_BASE + 14 // razreshenie raboti s adresnaya tablica


// ot TM
//==========
#define MTM_BASE			    MMODI_BASE + MSG_BLOCK + MSG_PAUSE

#define MTM_PARAM_FK            MTM_BASE + 1  // zapros parametrov TM
#define MTM_START_FK            MTM_BASE + 2  // nachalo raboti TM
#define MTM_STOP_FK             MTM_BASE + 3  // zavershenie raboti TM
#define MTM_RESTART_FK          MTM_BASE + 4  // komanda restart TM OK
#define MTM_STOP_LINE_FK        MTM_BASE + 5  // ostanov modulia napravleniia
#define MTM_START_LINE_FK       MTM_BASE + 6  // zapusk modulia napravleniia
#define MTM_ERROR_FK            MTM_BASE + 7  // avariynoe zavershenie raboti
#define MTM_CONN_ON_FK          MTM_BASE + 8  // ustanovlenie soedineniya po napravleniu
#define MTM_CONN_OFF_FK         MTM_BASE + 9  // razriv soedineniya po napravleniu
#define MTM_ABN_CONN_FK         MTM_BASE + 10 // �����������/���������� �������
#define MTM_GET_STAT_FK         MTM_BASE + 11 // zapros tablici statistiki
#define MTM_ABNBUS_CONN_FK      MTM_BASE + 12 // �����������/���������� ������� ����� ����


#define MTM_START_MODI          MTM_BASE + 30 // nachalo raboti TM
#define MTM_STOP_MODI           MTM_BASE + 31 // zavershenie raboti TM
#define MTM_CONN_ON_MODI        MTM_BASE + 32 // ustanovlenie soedineniya po napravleniu
#define MTM_CONN_OFF_MODI       MTM_BASE + 33 // razriv soedineniya po napravleniu
#define MTM_GET_ADDR_MODI       MTM_BASE + 34 // ������ �������� �������
#define MTM_ABN_CONN_MODI       MTM_BASE + 35 // �����������/���������� �������
#define MTM_ABNBUS_CONN_MODI    MTM_BASE + 36 // �����������/���������� �������



// ot ������ ���������
//=====================
#define MLINE_BASE			    MTM_BASE + MSG_BLOCK + MSG_PAUSE

#define MLINE_CONN_ON_TM       MLINE_BASE + 1  // ustanovlenie soedineniya po napravleniu
#define MLINE_CONN_OFF_TM      MLINE_BASE + 2  // razriv soedineniya po napravleniu


// identifikator otpravitelia
//------------------------------
#define BASE_SERVISE_ID     255

// ��� ������������� ������ ��������� Addr ������������ ���
// ������������� ����������� ( ���� Object) � ���������� (���� Abonent).
#define SERVICE_FK          BASE_SERVISE_ID + 1
#define SERVICE_MODI        BASE_SERVISE_ID + 2
#define SERVICE_TM          BASE_SERVISE_ID + 3

#define SERVICE_LINE_BASE   SERVICE_TM + 100      // dlya nignih transportnih processov


#define  FK_SERVER            1   // 00000001
#define  TM_SERVER            2   // 00000010
#define  MODI_SERVER          4   // 00000100
#define  MONITOR_KUF          8   // 00001000
#define  AGENT_KUF           16   // 00010000
#define  APD_236             32   // 00100000


// ���� ��� �� �� � ���
#define  SERVER_PATH_BIN		  "/systprom/OSPO/bin/TransportServer/"
#define  SERVER_PATH_LOG		  "/systprom/OSPO/log/TransportServer/"
#define  SERVER_PATH_LIB		  "/systprom/OSPO/lib/TransportServer/"
#define  SERVER_PATH_CFG		  "/systprom/OSPO/etc/TransportServer/"
#define  SERVER_ROUTE_CFG		  "/systprom/OSPO/etc/TransportServer/route.cfg"
#define  SERVER_PATH_DB		          "/systprom/OSPO/bin/TransportServer/protocols.s3db"
#define  SERVER_PATH_BUS		  "/systprom/OSPO/etc/TransportServer/"

// ���� ��� ��� ���
#define  KPKUF_PATH_BIN		  "/systprom/OSPO/bin/KP_KUF/"
#define  KPKUF_PATH_LOG		  "/systprom/OSPO/log/KP_KUF/"
#define  KPKUF_PATH_LIB		  "/systprom/OSPO/lib/KP_LIB/"
#define  KPKUF_PATH_CFG		  "/systprom/OSPO/etc/KP_KUF/Network/"
#define  KPKUF_PATH_SDB		  "/systprom/OSPO/var/KP_KUF/sdb/"

//------------------------------
// struktura soobsheniya
#define MsgHeader   GHEADER

#define MSG_HEADER    sizeof(MsgHeader)

#endif
