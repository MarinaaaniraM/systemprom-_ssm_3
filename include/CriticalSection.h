// []------------------------------------------------------------------------[]
//  |      ������� ����������� ������ (������ ������ ��������)               |
//  |          1/02/2005                                                     |
// []------------------------------------------------------------------------[]

#ifndef __SIMPLE_CRITICAL_SECTION__
#define __SIMPLE_CRITICAL_SECTION__

#include <sys/errno.h>
#include <pthread.h>
#include "ErrorType.h"
#include "BaseTypes.h"

class CEvent;

class CCriticalSection {
// �����������/����������
public:
    CCriticalSection();
   ~CCriticalSection();
   
public:
    // ��������
//    int Create(pthread_mutex_t* Lock = NULL, BOOL  Server = TRUE);
    int Create();
    // ����� � �������
    void EnterCritical();   
    // �������� �������
    void LeaveCritical();
    // ��������� ��������� ������� (TRUE - ������)
    BOOL IfCriticalLock();

private:
     pthread_mutex_t       *m_lock;
     pthread_mutexattr_t    m_attr;
//	 BOOL                   m_server;
//	 BOOL                   m_global;

friend class CEvent;     
};



#endif
