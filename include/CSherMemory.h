//=======================================================================================
//			Obshaya pamiat' 
//
//=======================================================================================

#ifndef __SHERED_MEMORY__
#define __SHERED_MEMORY__

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
//#include <semaphore.h>
#include "ErrorType.h"
#include "BaseTypes.h"


// maksimal'naya dlina imeni 
#define MAX_NAME_LEN       100

#define FILE_MODE    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH
#define DIR_MODE     FILE_MODE | S_IXUSR | S_IXGRP | S_IXOTH

typedef struct tagMemInfo
{
	key_t    m_key;

} MemInfo;

union semun{
  int    val;
  struct semid_ds  *buf;
  unsigned short   *array;
};


class CSherMemory
{

public:
	 CSherMemory();
	~CSherMemory();

public:
    // sozdanie i otkritie oblasty
	int   Create(key_t  key, ULONG  Size );
        int  _Create( key_t  key, ULONG  Size, BOOL Server = FALSE);
	// blokirovka oblasti
	void MemLock();
	void MemUnLock();
	void ClearMem()
	{
	   if((m_Ptr != NULL) && (m_size != 0))
	      memset(m_Ptr, 0, m_size);
	};
//	int  MemTryLock();
//	int  SemGetValue();


	BYTE*    m_Ptr;            // ukazatel' na nachalo oblasty dlia usera
        BOOL     m_srv;

protected:

	 key_t    m_key;          // imia obshey oblasty
	 int      m_flags;         // flagi otkritiya
	 int      m_mode;          // biti dostupa
	 ULONG    m_size;          // razmer oblasti
	 MemInfo* m_info;          // semephore
	 BYTE*    m_ptr;           // ukazatel' na nachalo oblasty
	 int      m_ID;
	 
	 int                m_semId;
	 struct   sembuf    m_semwait;
	 struct   sembuf    m_sempost;
	 union    semun     m_semarg;

};


#endif
