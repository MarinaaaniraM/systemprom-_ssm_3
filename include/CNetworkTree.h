//============================================================================
//
//    
//============================================================================

#ifndef __TREE__NETWORK_H__
#define __TREE__NETWORK_H__

#include "CNetWorkList.h"

#define  CHILD_TREE    1 
#define  ALL_TREE      2 



class CNetworkTree
{
public:
   CNetworkTree();
   ~CNetworkTree();
public:
	// sozdat' derevo
	int Create();

	//-----------------------------------------------------------------------------------------
	// NAVIGACIYA
	//-----------------------------------------------------------------------------------------
	// vibrat' uzel
	int Select(NETWORK_INFO* Net);
	//zaprosit' svoye opisanie
    int GetInfo(NETWORK_INFO* Net);

	//-----------------------------------------------------------------------------------------
	// PROVERKI, POISK
	//-----------------------------------------------------------------------------------------
	// proverit' na nalichie v spiske
	// CHILD_TREE - proverka tol'ko na nalichie v dochernem spiske
	// ALL_TREE   - proverka na nalichie vo vsiom dereve
    BOOL IfNetwork( NETWORK_INFO*  Net, BYTE  Flags = ALL_TREE);
	// proverit' na nalichie v spiske
	// CHILD_TREE - proverka tol'ko na nalichie v dochernem spiske
	// ALL_TREE   - proverka na nalichie vo vsiom dereve
    BOOL IfAbonent( Addr*  Abn, BYTE  Flags = ALL_TREE);
    // naiti uzel po abonentu
	// pole Net->m_NetMasck - dolgno sodergat' masku seti
	// TYPE_OK - v sluchae uspeha i rezul'tat v Net. V protivnom
	// sluchae v Net budut '0'.
	int FindNetByAbn( Addr*  Abn, NETWORK_INFO*  Net);

	//-----------------------------------------------------------------------------------------
	// OPERACII S SETIAMI
	//-----------------------------------------------------------------------------------------
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!! esli Parent = NULL to vse deistvia provodiatsia v tekushem uzlom !!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// zapros kolichestva podsetey
	int GetNetCount(ULONG* Count, NETWORK_INFO*  Parent = NULL);
	// dobavit' podset' v spisok
	int AddNetwork( NETWORK_INFO*  Net, NETWORK_INFO*  Parent = NULL );
	int AddNetwork( ULONG  IP, ULONG  Mask, ULONG UserData = 0, NETWORK_INFO*  Parent = NULL );
	int AddNetwork( char* IP, char* Mask, ULONG UserData = 0, NETWORK_INFO*  Parent = NULL );
	// udalit' podset'iz spiska
	int DeleteNetwork( NETWORK_INFO*  Net, NETWORK_INFO*  Parent = NULL );
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!!           v tekushem uzle                                        !!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// poluchit' perviy element
	int GetFirstNetwork(NETWORK_INFO*  Net);
	// poluchit' sleduyshii
	int GetNextNetwork(NETWORK_INFO*  Net);
	// zamenit' abonenta
	// ishem DestNet i perepisivaem na SrcNet
	int Replace( NETWORK_INFO*  SrcNet,  NETWORK_INFO*  DestNet  ); 
	// zaprosit' pole UserData
	int GetNetUserData(NETWORK_INFO*  Net);
	int SetNetUserData(NETWORK_INFO*  Net);

	//-----------------------------------------------------------------------------------------
	// OPERACII S ABONENTAMI 
	//-----------------------------------------------------------------------------------------
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!! esli Parent = NULL to vse deistvia provodiatsia v tekushem uzlom !!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// zapros kolichestva abonentov
	int GetAbonentCount(ULONG* Count, NETWORK_INFO*  Parent = NULL);
	// dobavit' abonenta
	int AddAbonent( Addr*  Abn, NETWORK_INFO*  Parent = NULL );
	int AddAbonent( ULONG  IP, ULONG  ID, NETWORK_INFO*  Parent = NULL );
	int AddAbonent( char* IP, ULONG  ID, NETWORK_INFO*  Parent = NULL );
	// udalit' abonenta iz spiska
	int DeleteAbonent( Addr*  Abn, NETWORK_INFO*  Parent = NULL );

	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!!           v tekushem uzle                                        !!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// poluchit' perviy element
	int GetFirstAbonent(Addr*  Abn);
	// poluchit' sleduyshii
	int GetNextAbonent( Addr*  Abn );
	// zamenit' abonenta
	// ishem DestAbn i perepisivaem na SrcAbn
	int Replace( Addr*  SrcAbn,  Addr*  DestAbn  ); // ishem DestAbn i perepisivaem na SrcAbn


protected:
	// udalenie pered zakritiem
	int Close();

protected:
//	CNetwork*       m_Root;
	CNetwork*       m_Select;
	CNetWorkList*   m_List;

	BOOL            m_Flag;

};

#endif
