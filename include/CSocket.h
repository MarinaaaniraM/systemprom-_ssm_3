/////////////////////////////////////////////////////////////////////////////////
// Basic Socket class, author: Roman St.Zhavnis
/////////////////////////////////////////////////////////////////////////////////
#ifndef _CSOCKET_H
#define _CSOCKET_H

#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include "BaseTypes.h"
#include "ErrorType.h"

#define isvalidsock(s) ((s)>=0)
#define set_errno(e) erno=(e)

typedef int SOCKET;
typedef unsigned short PORT;

int set_address(char *hname, char *sname, struct sockaddr_in *sap, char *protocol );

class CSocket{
public:
	CSocket();
	~CSocket();
public: // �������� ����������. ��-��������� - ����������
	// ����� � server=1 ������ ������������ �����
	// hname ����� ����: "127.0.0.1" ��� "localhost", ���� NULL ������������� INADDR_ANY
	// sname: "25" ��� "smtp"
	int Create(char *hname, char *sname, BYTE prior,  BOOL server = 0 );
	// ip � port - ����� � network order
	int Create(UINT ip, PORT port, BYTE prior,  BOOL server = 0 );
	//// ip - ����o � network order
	int Connect(UINT ip = INADDR_ANY);

	// �������� ������� (�� ������ select)
	// �������� ����� - ����� ������� ���� (������/������/������) � ��������� �� ��������� ������� �� ��������� ��������
	// ��������� ����� - ����� ������� ���������. to=NULL - ��� ��������, ����� �������
	int WaitForEvent(BYTE &rd, BYTE &wr, BYTE &ex, struct timeval *to);
	// ��������� ����������
	int Disconnect();
	// ������
	int Read(BYTE *msg, ULONG &len);
	// ������
	int Write(BYTE *msg, ULONG len);
	// ������ ����������
	int isConnected();		// ������ ���������� 1=����
	// ������ IP ���������� ����� ����������
	// ���� ���������� ��� ���, �� ��� ������� ������ INADDR_ANY
	// ���� ���������� ��� ���, �� ��� ������� ������ ����� � �������� ���� ������� �����������
	inline ULONG GetRemoteAdr() { return radr.sin_addr.s_addr;};
	// ������ IP ����������� ������ ����� ���������� (��� ������� ������ ����� ���������)
	inline ULONG GetLocalAdr () { return ladr.sin_addr.s_addr;};
	// ������ ���� ����������� ������ ����� ���������� (��� ������� ������ ����� ���������)
	inline PORT  GetLocalPort() { return ladr.sin_port;};
	inline ULONG GetRxBytes() {return rxBytes;};
	inline ULONG GetTxBytes() {return txBytes;};
	// ������ ������� ���������� ����������
	inline bool isServer() {return((srv)?(TRUE):(FALSE)); };
  // ������ ������
	struct sockaddr_in radr; // � ��� ���������
	struct sockaddr_in ladr; // ������ ��������� (��� ������� ���� �� �����������)
private:
	SOCKET s;//�����
	int con; //������ ����������
	BYTE srv;// ���� �������
	BYTE ip_tos; // ��������� ip
	ULONG rxBytes, txBytes; // ���������� ������/�������� �������
	// ����� ���������� ��� ������. ���� ip!=INADDR_ANY, �� ������ � ������ ip
	// ip - ����o � network order
	int WaitForConnect(UINT ip = INADDR_ANY);

};


#endif
