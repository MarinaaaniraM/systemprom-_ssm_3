//============================================================================
//      
//    
//============================================================================

#ifndef __NETWORK__LIST_H__
#define __NETWORK__LIST_H__

#include "BaseTypes.h"
#include "MessageType.h"
#include "ErrorType.h"
#include "CList.h"
#include "CAbonentList.h"



class CNetworkTree;
class CNetWorkList;
class CNetwork;


class CNetwork
{
friend class CNetWorkList;
friend class CNetworkTree;
 
public:
	CNetwork();
	~CNetwork();
	//sozdanie ob'ekta
	int Create(NETWORK_INFO* Net, CNetWorkList* Parent = NULL);
	//zaprosit' svoye opisanie
        int GetInfo(NETWORK_INFO* Net);
	// zapros kolichestva podsetey
	int GetNetCount(ULONG* Count);
	// dobavit' podset' v spisok
	int AddNetwork( NETWORK_INFO*  Net );
	int AddNetwork( ULONG  IP, ULONG  Mask, ULONG UserData = 0 );
	int AddNetwork( char* IP, char* Mask, ULONG UserData = 0 );
	// udalit' podset'iz spiska
	int DeleteNetwork( NETWORK_INFO*  Net );
	// proverit' na nalichie v spiske
    BOOL IfNetwork( NETWORK_INFO*  Net);
	// poluchit' perviy element
	int GetFirstNetwork(NETWORK_INFO*  Net);
	// poluchit' sleduyshii
	int GetNextNetwork(NETWORK_INFO*  Net);
	// zamenit' abonenta
	int Replace( NETWORK_INFO*  SrcNet,  NETWORK_INFO*  DestNet  ); // ishem DestNet i perepisivaem na SrcNet
	// zaprosit' pole UserData
	int GetNetUserData(NETWORK_INFO*  Net);
	int SetNetUserData(NETWORK_INFO*  Net);

	// zapros kolichestva abonentov
	int GetAbonentCount(ULONG* Count);
	// dobavit' abonenta
	int AddAbonent( Addr*  Abn );
	int AddAbonent( ULONG  IP, ULONG  ID );
	int AddAbonent( char* IP, ULONG  ID );
	// udalit' abonenta iz spiska
	int DeleteAbonent( Addr*  Abn );
	// proverit' na nalichie v spiske
    BOOL IfAbonent( Addr*  Abn );

	// poluchit' perviy element
	int GetFirstAbonent(Addr*  Abn);
	// poluchit' sleduyshii
	int GetNextAbonent( Addr*  Abn );
	// zamenit' abonenta
	int Replace( Addr*  SrcAbn,  Addr*  DestAbn  ); // ishem DestAbn i perepisivaem na SrcAbn




protected:
	// poluchit' ukazatel' na spisok podsetey
	CNetWorkList* GetNetworkList() { return m_NetList; };
	// poluchit' ukazatel' na spisok abonentov
	CAbonentList* GetAbonentList() { return m_AbnList; };
	// poluchit' ukazatel' na roditelia
	CNetWorkList* GetParentList() { return m_Parent;};
    

    CNetwork operator++ (int )	{  m_count ++; };

    CNetwork operator-- (int )	
	{  
		if(m_count <= 1) 
	       m_count = 0;
		else
		   m_count --; 
	};

protected:
  NETWORK_INFO      m_NetInfo;
  CAbonentList     *m_AbnList;
  CNetWorkList     *m_NetList;
  CNetWorkList     *m_Parent;

  ULONG             m_count;

};


class CNetWorkList 
{
friend class CNetwork;
friend class CNetworkTree;
public:
	 CNetWorkList();
    ~CNetWorkList();

public:
	// sozdat' spisok
       int Create(CNetwork*  Parent = NULL);
	// poluchit' chislo elementov v spiske
	int GetCount(ULONG* count);
	// dobavit' set' v spisok
	int AddNetwork( NETWORK_INFO*  Net );
	int AddNetwork( ULONG  IP, ULONG  Mask, ULONG UserData = 0 );
	int AddNetwork( char* IP, char* Mask, ULONG UserData = 0 );

	// udalit' set'iz spiska
	int Delete( NETWORK_INFO*  Net );
	// proverit' na nalichie v spiske
        BOOL IfNetwork( NETWORK_INFO*  Net);

	// poluchit' perviy element
	int GetFirstNetwork(NETWORK_INFO*  Net);
	// poluchit' sleduyshii
	int GetNextNetwork(NETWORK_INFO*  Net);
	// zamenit' abonenta
	int Replace( NETWORK_INFO*  SrcNet,  NETWORK_INFO*  DestNet  ); // ishem DestNet i perepisivaem na SrcNet
	// zaprosit' pole UserData
	int GetUserData(NETWORK_INFO*  Net);
	int SetUserData(NETWORK_INFO*  Net);


private:
	// udalit' spisok
	void DeleteList();
	// naiti v spiske
	CNetwork* FindNetwork( NETWORK_INFO*  Net );
	// poluchit' perviy element
        CNetwork* GetFirstNetwork();
	// poluchit' sleduyshii
	CNetwork* GetNextNetwork();

protected:
	CList               *m_list;
	CCriticalSection    *m_critical;
	CNetwork            *m_Parent;

};




#endif 
