//=======================================================================================
//			Simple message 
//
//=======================================================================================

#ifndef __MESSAGE_QUEUE__
#define __MESSAGE_QUEUE__

#include <sys/ipc.h>
#include <sys/msg.h>
#include "ErrorType.h"
#include "BaseTypes.h"

// regimi zacritia
#define  MQ_CLOSE      FALSE  // bez udalenia
#define  MQ_DELETE     TRUE   // zacrit' i udalit' imia

// atributi po umolchaniu
#define  MQ_MAX_MESSAGE     256   // chislo soobchenii v jcheredi
#define  MQ_MESSAGE_SIZE    512   // dlinna coobchenia


typedef struct
{
	long		m_type;
	char        m_msg[MQ_MESSAGE_SIZE];

} Message;



class CMessageQueue
{
public:
	CMessageQueue();
	~CMessageQueue();
public:
	// zacritie
	int Close();
	// zapros parametrov spiska
	int  GetAttrib(msqid_ds* atr);
	// chislo v ocheredi
	int GetCount(ULONG *Count);
	// poslat' soobshenie
	int SendMessage(BYTE* Buf, ULONG Size);
	// prochitat' is ocheredi
	int ReceiveMessage(BYTE* Buf, ULONG* ReadBytes);
	// sozdenie ob'ekta
	int Create(key_t Key, BOOL Server = TRUE );

protected:
	int       m_flags;  // flagi sozdania
	msqid_ds  m_attr;   // atributi
	int       m_ID;     // descriptor
	BOOL      m_type;   // TRUE - server

	Message   m_buf;
};









#endif
