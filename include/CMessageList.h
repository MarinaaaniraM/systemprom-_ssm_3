//============================================================================
//
//    
//============================================================================

#ifndef __MESSAGE_LIST_H__
#define __MESSAGE_LIST_H__


#include "CEvent.h"
#include "CList.h"
#include "CBaseMessageList.h"



class CMessageList : public CBaseMessageList
{
public:
	 CMessageList();
    virtual ~CMessageList();

public:
	// udalit' spisok
	virtual int DeleteList();
	// sozdat' spisok
    int Create( CEvent* Event = NULL );
	// poluchit' chislo elementov v spiske
	virtual int GetCount(ULONG* count );
	virtual BOOL IfFull();
	// dobavit' soobshenie
	virtual int AddMessage( BYTE*  msg, BOOL Flag = FALSE);
	// zabrat' soobshenie
	virtual int GetMessage( BYTE** msg);
        virtual int WaitForEvent( int Priority = DATA_MESSAGE_LIST, ULONG  sec = 0);   

public:
	int LockMessageList();
	int UnLockMessageList();

protected:
	CList               *m_list;
	CCriticalSection    *m_critical;
	CCriticalSection    *m_criticalSend;
	CEvent		    *m_event;
	BOOL                 m_flag;

};




#endif
