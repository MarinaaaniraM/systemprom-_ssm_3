//============================================================================
//
//    
//============================================================================

#ifndef __PRIORITY_MESSAGE_LIST_H__
#define __PRIORITY_MESSAGE_LIST_H__


#include "CMessageList.h"



class CPriorityMessageList : public CBaseMessageList
{
public:
	 CPriorityMessageList();
    virtual ~CPriorityMessageList();

public:
	// udalit' spisok
	virtual int DeleteList();
	// sozdat' spisok
	int Create( int Priority = -1, int DataMaxCount = -1, int PrMaxCount = -1 );
	// poluchit' chislo elementov v spiske
	virtual int GetCount(ULONG* count );
	virtual int _GetCount(ULONG* count );
	// dobavit' soobshenie
	virtual int AddMessage( BYTE*  msg, BOOL Flag = FALSE);
	// zabrat' soobshenie
	virtual int GetMessage( BYTE** msg);
	int GetPriorityMessage( BYTE** msg);
	int GetDataMessage( BYTE** msg);
        virtual int  WaitForEvent(int Priority = PARENT_MESSAGE_LIST,ULONG  sec = 0);   

protected:
	CMessageList        *m_PriorityList;
	CMessageList        *m_DataList;
	int		     m_Priority;
	CEvent              *m_event;

};




#endif
