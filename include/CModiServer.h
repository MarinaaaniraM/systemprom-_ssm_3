#ifndef __MODI_SERVER_MESSAGE_QUEUE__
#define __MODI_SERVER_MESSAGE_QUEUE__

#include "CServerQueue.h"

class CModiServer : public CServerQueue
{

public:
	 CModiServer();
    virtual ~CModiServer();

     virtual int Create();

	 // ����������� ���������
	 //========================

     // FROM FK
     //------------
	 // parametri MODI
     virtual void OnFkParameters(MODI_PARAMETERS *Param);

     // ostanov MODI
     virtual void OnFkCommandStop();

	 // restart MODI
     virtual void OnFkCommandRestart();

     // zapret zapisi v strukturu statistiki
     virtual void OnFkLockStat();

     // zapret zapisi v strukturu statistiki
     virtual void OnFkUnLockStat();

	 // razreshenie zapisi v strukturu statistiki
     virtual void OnFkCreateStat(STAT_INFO *StatInfo);
     
     virtual void OnSetRemoteIP(BYTE *Array, ULONG Size);
     
     virtual void OnSetAccessMatrix(char *Path);


     // FROM TM
     //------------
	 // nachalo raboti TM
     virtual void OnTmStart();

     // zavershenie raboti TM
     virtual void OnTmStop();

     // ustanovlenie soedineniya po napravleniu
     virtual void OnTmConnectLine(LINE_INFO *LineInfo);

     // razriv soedineniya po napravleniu
     virtual void OnTmDsConnectLine(LINE_INFO *LineInfo);

     // ������ �������� �������
     virtual void OnGetAccessMatrix();

     // ����������� � �����������/���������� ��������
     virtual void OnAbonentConnect(ABONENT_INFO* AbnInfo);
     virtual void OnAbonentBusConnect(ABONENT_INFO* AbnInfo);

	 // ���������� ���������
	 //========================
     // TO FK
     //------------
     // zapros parametrov MODI
     int MSG_GetParam();

     // zavershenie raboti MODI
     int MSG_Stop();
     
     int MSG_Start();

     // poluchena adresnaya tablica
     int MSG_RecAddr();

     // avariynoe zavershenie raboti
     int MSG_Error(ERROR_INFO *Error);
     
     int MSG_StatusLine( NETWORK_INFO *NetInfo );

     // ����� �� ��� ����������
     //-------------------------
     // poluchena adresnaya tablica
     int MSG_CreateAddr(ADDR_INFO *AddrInfo, ULONG  AbnType = ABONENT_TM);

     // zapret raboti s adresnaya tablica
     int MSG_LockAddr();

     // razreshenie raboti s adresnaya tablica
     int MSG_UnLockAddr();

     int MSG_GetRemoteIP();
     
     int MSG_GetAccessMatrix();

protected:
     virtual int OnModiMessages(MsgHeader* Header);
     int SendMessageToFk(ULONG IdMessage, BYTE* Packet, ULONG Size);
     int SendMessageToTm(ULONG IdMessage, BYTE* Packet, ULONG Size);

};



#endif
