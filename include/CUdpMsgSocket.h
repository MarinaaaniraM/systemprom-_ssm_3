//////////////////////////////////////////////////////////////////////////////////
/// Socket class for send/receive udp-messages (with GHEADER), author:Roman St.Zhavnis
//////////////////////////////////////////////////////////////////////////////////
#ifndef _CUdpMsgSocket_H
#define _CUdpMsgSocket_H

#include <pthread.h>
#include <signal.h>
#include "CUdpSocket.h"
#include "CMessageList.h"

#define MAX_UDP_LENGTH 65535

class CUdpMsgSocket{
public:
	CUdpMsgSocket();
	virtual ~CUdpMsgSocket();
public:
// ������� ������
	/// ���������� ���������� ����������. �������� ����� Create!
	/// \param ip - ����� � network order.
	void SetLocalAdr(UINT ip);

	/// �������� ����������. ��-��������� - ������� ���������� ����������.
	/// \param hname ����� ����: "127.0.0.1" ��� "localhost", ���� NULL ������������� INADDR_ANY.
	/// \param sname ���� ��� ������: "25" ��� "smtp".
	/// \param inL � outL - ������� �� ����� � �������� ���������
	/// \param prior ��������� ip ������� (�� 0 �� 3).
	/// \param server ���� =1 sname ������������ ��� ���. ���� ��� ������ �������, � hname ��� ��������� ���������.
	/// \param server ���� =0 sname, hname ������������ ��� ����� ����������
	int Create(char *hname, char *sname, BYTE prior, CBaseMessageList *inL, CBaseMessageList *outL, BOOL server = 0 );

	/// �������� ����������.
	/// ������ �������� Create(char*, char*, BYTE, CBaseMessageList*, CBaseMessageList*, BOOL)
	/// \param ip � port - ����� � network order.
	int Create(UINT ip, PORT port, BYTE prior, CBaseMessageList *inL, CBaseMessageList *outL, BOOL server = 0 );

	/// ������ ����� �������, ��� ������������� ���������� ����������.
	/// \param reCon - ���� ������������� ��������� ����������
	/// \param ip - ��������� ����� ��� ��������� ����������
	/// \param port - ��������� ���� ��� ��������� ����������
	/// ������ ������ ���� ���������� ��� ����, �� ������ ip � port ��� ������� ��� �� ������������ ����
	/// �� �������� �� OnDisconnect() � OnReceive()
	int Connect(BYTE reCon, UINT ip = INADDR_ANY, PORT port = 0);

	/// �������� ���������.
	/// ���� ���� outList, �������� AddMessage().
	/// ����� ��������� ��������� � ����� ��������� �������� � ����������.
	/// \param msg ��������� �� ������ ���������
	/// \param ip ����� ���������� (���� ��� ����������)
	/// \param port ���� ���������� (���� ��� ����������)
	/// ��� ip � port �� �������������� ������ ������ ������!
	int MsgWrite(BYTE* msg, UINT ip = INADDR_ANY, PORT port = 0);

	/// ��������� ����������.
	/// \param ErrCode ���� =TYPE_OK, �� ��������� ���������� ����� ��������
	int Disconnect(int ErrCode = TYPE_OK);

  // �������������� ������
	/// ������ ����������.
	/// ������ ���������� 1=���� (������ ����� ��������)
	int isConnected();

	/// ����� ���������� ����.
	/// ������ IP ���������� ����� ���������� ()
	/// ���� ���������� ���, �� ��� ������� - ��������� ��� ������� ����� ��� INADDR_ANY
	/// ���� ���������� ���, �� ��� ������� ������ ����� ����������
	inline ULONG GetRemoteAdr() {return sock.GetRemoteAdr();};
  /// ���������� ���������� ���� ���������� �����
	inline PORT GetRemotePrt() {return sock.GetRemotePrt();};
	// ������ IP ����������� ������ ����� ���������� (��� ������� ������ ����� ���������)
	inline ULONG GetLocalAdr () {return sock.GetLocalAdr();};
  /// ����, ���������� � Create (��������� ��� �������, ��������� ��� �������)
  inline PORT GetPort() { return a_prt; };
	/// ���� �������.
	inline ULONG GetRxBytes() {return sock.GetRxBytes();};

	/// ���� ��������.
	inline ULONG GetTxBytes() {return sock.GetTxBytes();};

// ����������� ������
	/// ������ �����.
	/// ��������� ��� ������ ������� ���������. ���� ������ FALSE, ��������� �� ����� ��������� � ������ �������� ���������
	/// \param msg ��������� �� ������ ��������� ���������
	virtual BOOL OnReceive(BYTE* msg);

	/// ���������� ���������.
	/// ���������� ��� ������, ��������� � ������� ����������
	/// \param ErrCode ������� ������� ����������
	virtual int OnDisconnect(int ErrCode);

  // ����������� ������ (�� ��� ������!)
	/// ���� ��������� ������� ������. ���������� � ��� ������.
	int MainRCycle();

	/// ���� ��������� ������� ������. ���������� � ��� ������.
	int MainWCycle();
private:
  void Close(); // � ������ ������������� ����� �������� ��������� ����������. ������������ � �����������
	CUdpSocket sock;	// ������� ����� ���������� �����������
	CBaseMessageList *inList, *outList;	//������ ��������/��������� ���������
	pthread_t rptid, wptid;	// ����������� ������ ������������ ����������
	UINT a_ip;		// ip �� �������� �������� ����� ��� ������
	PORT a_prt;   // ����, ���������� � Create
	int needReConnect;
	BYTE hdr[MAX_UDP_LENGTH];
//  BYTE* buf;
//	BYTE buf[MAX_TRANSPORT_PACKET_SIZE+MSG_HEADER];		// ��� ������ ������ (���� �� NULL - �������� �����)
};

/// ���������� /////////////////////////////////////////////////
/// 1. UDP-������
/// 1.1 ������� CUdpMsgSocket � ������� Create( INADDR_ANY, PORT, PRIOR, INLIST, 0, 1)
/// PORT - udp-���� ��� "�������������"
/// PRIOR - ���� TOS IP-�������
/// INLIST - �������� ������
/// 0 - ������ �� �������� �����������, �.�. ���������� ���� ����� ������ ������
/// 1 - ���� �������
/// 1.2 ������� Connect(0), 0 - ���� ��������� "udp-����������" (��� 0 - ��������� ������ �� ���� �������)
/// 1.3 ������ OnReceive �/��� ��������� ��������� � ������ INLIST
/// 1.4 ����� (� ����� � OnReceive) �������� ����� ����������� � ������� GetRemoteAdr/GetRemotePort
/// 1.5 ������ ���������� � ������� MsgWrite( MSG, IP, PORT)
/// MSG - ��������� �� ����� ��������� � GHEADER
/// IP - ����� ����������� (���� ���������� �����, �� ����� ������� � ������� GetRemoteAdr)
/// PORT - ���� ���������� (���� ���������� �����, �� ���� ������� � ������� GetRemotePort)
///
/// 2. UDP-������
/// 1.1 ������� CUdpMsgSocket � ������� Create( IP, PORT, PRIOR, INLIST, OUTLIST, 0)
/// IP - �����, � ��� ����� ��������, ��� INADDR_ANY, ���� ����� ����� ����������
/// PORT - udp-����, � ��� ����� ��������, ��� 0, ���� ����� ����� ����������
/// PRIOR - ���� TOS IP-�������
/// INLIST - �������� ������
/// OUTLIST - ��������� ������ (����� �����, ���� �������� � ����� ������)
/// 0 - ���� �������
/// 1.2 ������� Connect(X), X - ���� ��������� "udp-����������" (��� 1 - ����� �� ��������� ���� ����������, ���� ������� ��� � Create())
/// 1.3 ������ ���������� � ������� MsgWrite( MSG[, IP, PORT]) ��� ����� OUTLIST
/// IP,PORT �� ���������, ���� ������ Connect(1)
/// 1.4 ������ ������ � OnReceive �/��� ��������� ��������� � ������ INLIST

#endif