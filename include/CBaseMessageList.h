//============================================================================
//
//    
//============================================================================

#ifndef __BASE_MESSAGE_LIST_H__
#define __BASE_MESSAGE_LIST_H__

#include "BaseTypes.h"
#include "MessageType.h"
#include "ErrorType.h"

#define PARENT_MESSAGE_LIST       0
#define PRIORITY_MESSAGE_LIST     1
#define DATA_MESSAGE_LIST         2

#define MAX_LIST_COUNT            0xFFFF

class CBaseMessageList 
{
public:
	 CBaseMessageList();
    virtual ~CBaseMessageList();

public:
	// udalit' spisok
	virtual int DeleteList() { return TYPE_OK; };
	// sozdat' spisok
        int Create() { return TYPE_OK; };
	// zadat' maksimal'noe chislo elementov spiska
        void SetMaxCount( int NewCount);
	// poluchit' chislo elementov v spiske
	virtual int GetCount(ULONG* count );
	virtual BOOL IfFull() { return FALSE; };
	// dobavit' soobshenie
	virtual int AddMessage( BYTE*  msg, BOOL Flag = FALSE);
	// udalit' soobshenie iz spiska
	virtual int FreeMessage( BYTE* Msg );
	// zabrat' soobshenie
	virtual int GetMessage( BYTE** msg);
        virtual int  WaitForEvent( int Priority = PARENT_MESSAGE_LIST,ULONG  sec = 0) { return TYPE_OK; };   
	virtual BYTE* GetFreeBuffer( ULONG  Size = MAX_TRANSPORT_PACKET_SIZE );

protected:

	int  m_MaxCount;  // maksimal'noe chislo elementov v spiske

};




#endif
