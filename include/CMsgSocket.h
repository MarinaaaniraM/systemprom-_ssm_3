//////////////////////////////////////////////////////////////////////////////////
/// Socket class for send/receive messages (with headers), author:Roman St.Zhavnis
//////////////////////////////////////////////////////////////////////////////////
#ifndef _CMSGSOCKET_H
#define _CMSGSOCKET_H

#include <pthread.h>
#include <signal.h>
#include "CSocket.h"
#include "CMessageList.h"

class CMsgSocket{
public:
	CMsgSocket();
	virtual ~CMsgSocket();
public:
// ������� ������
	/// ���������� ���������� ����������. �������� ����� Create!
	/// \param ip - ����� � network order.
	void SetLocalAdr(UINT ip);

	/// �������� ����������. ��-��������� - ������� ���������� ����������.
	/// \param hname ����� ����: "127.0.0.1" ��� "localhost", ���� NULL ������������� INADDR_ANY.
	/// \param sname ���� ��� ������: "25" ��� "smtp".
	/// \param inL � outL - ������� �� ����� � �������� ���������
	/// \param prior ��������� ip ������� (�� 0 �� 3).
	/// \param server ���� =1 ������ ������������ ����� � ������ ��� �� �������������.
	int Create(char *hname, char *sname, BYTE prior, CBaseMessageList *inL, CBaseMessageList *outL, BOOL server = 0 );

	/// �������� ����������.
	/// ������ �������� Create(char*, char*, BYTE, CBaseMessageList*, CBaseMessageList*, BOOL)
	/// \param ip � port - ����� � network order.
	int Create(UINT ip, PORT port, BYTE prior, CBaseMessageList *inL, CBaseMessageList *outL, BOOL server = 0 );

	/// ��������� �������� ����������.
	/// ���������� �����(���) ����� ��������� ���������� ���������� ������-�������.
	/// �������� ����������� ����� ��������� ������� ����������.
	/// ��-���������) ��������������� DEF_CLIENT_TIMEOUT.
	/// ���������� ������������� ��������.
	/// \param sec �������� �������� � ��������. ��� 0 �������� ���������� �� ������������
	UINT SetClientTimeout(UINT sec);

	/// ���������� ����������.
	/// \param reCon ���� = 1 �������������������������� �������������� ���������� ��� ������ (������ �������� ����� ������� ����, ������ - �������� ����������� ����� �������� ��������)
	/// ������ ������ TYPE_OK, ����� ������� ���������� �� �������������� ������ (ERS_INUSE)
	/// �� �������� �� OnDisconnect() � OnReceive()
	int Connect(BYTE reCon, UINT ip = INADDR_ANY);

	/// �������� ���������.
	/// ���� ���� outList, �������� AddMessage().
	/// ����� ��������� ��������� � ����� ��������� �������� � ����������.
	/// ����� ����������� � onReady()
	/// \param msg ��������� �� ������ ���������
	int MsgWrite(BYTE* msg);

	/// ��������� ����������.
	/// \param ErrCode ���� =TYPE_OK, �� �� ����� ������������� ������� ���������� ����������
	int Disconnect(int ErrCode = TYPE_OK);

  // ����������� ������ (�� ��� ������!)
	/// ���� ��������� ������� ������. ���������� � ��� ������.
	int MainRCycle();

	/// ���� ��������� ������� ������. ���������� � ��� ������.
	int MainWCycle();

  // �������������� ������
	/// ������ ����������.
	/// ������ ���������� 1=����
	int isConnected();

	/// ����� ���������� ����.
	/// ������ IP ���������� ����� ����������
	/// ���� ���������� ��� ���, �� ��� ������� ������ INADDR_ANY
	/// ���� ���������� ��� ���, �� ��� ������� ������ ����� � �������� ���� ������� �����������
	inline ULONG GetRemoteAdr() {return sock.GetRemoteAdr();};
	// ������ IP ����������� ������ ����� ���������� (��� ������� ������ ����� ���������)
	inline ULONG GetLocalAdr () {return sock.GetLocalAdr();};
	// ������ ���� ����������� ������ ����� ���������� (��� ������� ������ ����� ���������)
	inline PORT  GetLocalPort() {return sock.GetLocalPort();};

	/// ���� �������.
	inline ULONG GetRxBytes() {return sock.GetRxBytes();};

	/// ���� ��������.
	inline ULONG GetTxBytes() {return sock.GetTxBytes();};
// ����������� ������
	/// ���������� � ��������.
	/// ��������� ��� ���������� �� �������� (���� outList �� ���������)
	virtual int OnReady();

	/// ������ �����.
	/// ��������� ��� ������ ������� ���������. ���� ������ FALSE, ��������� �� ����� ��������� � ������ �������� ���������
	/// \param msg ��������� �� ������ ��������� ���������
	virtual BOOL OnReceive(BYTE* msg);

	/// ���������� �����������.
	/// ���������� ����� ������ Connect() ��� ����������� ��������� ����������
	virtual int OnConnect();

	/// ���������� ���������.
	/// ���������� ��� ������, ��������� � ������� ����������
	/// \param ErrCode ������� ������� ����������
	virtual int OnDisconnect(int ErrCode);
        void Close(); // � ������ ������������� ����� �������� ��������� ����������. ������������ � �����������

private:
//  void Close(); // � ������ ������������� ����� �������� ��������� ����������. ������������ � �����������
  int ActiveMsg( WORD type); // �������� ����������
  int IsActiveMsg(GHEADER* msg); // �������� ��������� �� ���������� ������ ����������
	CSocket sock;	// ������� ����� ���������� �����������
	CBaseMessageList *inList, *outList;	//������ ��������/��������� ���������
	pthread_t rptid, wptid;	// ����������� ������ ������������ ����������
	UINT allowed;		// ip � ������� ��������� ����������� ��� ������
	GHEADER hdr;		// �������� ��������� ������������ ������
	BYTE* buf;		// ��� ������ ������ (���� �� NULL - �������� �����)
	UINT bufPos;		// ������� � ������ ��� ������
	BYTE needReConnect;	// ���� ������������� ���������� ����������
	BYTE is_active;  // ������� ��������� ������� ���������� �� ����������
	UINT tout;		// ������� ��� ���������� ������� (� ��������)
};

#define DEF_CLIENT_TIMEOUT 5

#endif
