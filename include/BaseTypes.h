//==============================================================================
//   ������� ����  01.02.2005
//==============================================================================

#ifndef __BASE_TYPES_H__
#define __BASE_TYPES_H__

//using namespace std;

#include "unistd.h"
#include <stdio.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <string.h>

#pragma pack (1)

#define BYTE  unsigned char
#define WORD  unsigned short
#define INT   int
#define UINT  unsigned int
#define LONG  int
#define ULONG unsigned int
#define ALONG  long
#define UALONG unsigned long

//typedef unsigned char  BYTE;
//typedef unsigned short WORD;
//typedef int            INT;
//typedef unsigned int   UINT;
//typedef long           LONG;
//typedef unsigned long  ULONG;
//typedef int            BOOL;

#define BOOL  INT

#define TRUE  1
#define FALSE 0

//
#define MAX_TRANSPORT_PACKET_SIZE    0x1fffffff
#define MAX_COUNT_ITEM               10

typedef struct
{
      ULONG  Object;    // IP ������� (��� ����)
      ULONG  Abonent;   // ID ��������

}  Addr;


typedef struct
{
    ULONG    UnUsed_1;       // �� ������������
    ULONG    UnUsed_2;       // �� ������������
    ULONG    ID;             // ������������� ������
    ULONG    Size;           // ������ ������ � ����������
    Addr     Src;            // ����� ���������
    WORD     Protocol;       // ������������� ���� ���������
    WORD     Packet;         // ������������� ���� ������
    BYTE     Rezerv;         // ���� ������
    BYTE     Priority;       // ���������
    WORD     Count;          // ����� ���������

} GHEADER;


//-------------------------------------------------
// �������� ���� Rezerv
//-------------------------------------------------
//
#define  FL_ROUTE_PACKET         1   // 00000001  - ����� ���������������
#define  FL_NEED_TICKET          2   // 00000010  - ���������� ��������� �� �������� ��������
#define  FL_NEED_DELIVERY        4   // 00000100  - ���������� �������� ������
#define  FL_SEND_TO_DEFAULT      8   // 00001000  - ��������� �������� <�� ���������>
//#define  FL_SEND_TO_BUS         16   // 00010000  - paket shini dannih
#define  FL_ROUTE_STEP         240   // 11110000  - maksimal'noe chislo perepriemov



#define  MAX_PROTOCOL_IDENT		32767	//maksimal'niy id protokola
//  priznak paketa shiny dannih
//  nahoditsya v pole <Protocol>
#define  FL_BUS_TYPE_PACKET		32768	
// ����������������� ���� ����������
#define  MODI_PROTOKOL_TYPE             1000  // ���� �� - ���� ��
#define  MODI_SR_PROTOKOL_TYPE          1001  // ���� �� - ��
#define  TM_CLIENT_PROTOKOL_TYPE        1002  // �� �� - ������
#define  MESSAGES_PROTOKOL_TYPE         1003  // ������������� �����
#define  TM_AGPD_PROTOKOL_TYPE          1004  // TM - AgPD
#define  UPRAVLENIE_PROTOKOL_TYPE       1005  // Protocol upravleniya
#define  TM_SERVER_PROTOKOL_TYPE        1006  // Protocol vzaimodeistviya megdu moduliami TM
#define  POST_PROTOKOL_TYPE             1007  // paket dlya abonenta POST
#define  MANAGER_MKP_PROTOKOL_TYPE      1008  // protokol upravleniya MKP i Shluzom-A
#define  MKUF_AKUF_PROTOKOL_TYPE        1009  // MKUF - AKUF
//01.06.2009
#define  APD236_COMMAND_PROTOKOL_TYPE   1010  // APD_236 commsnd
#define  MAMEVR_PROTOKOL_TYPE           1011  // Protokol Manevr
//16.09.2010
#define  ESUTZ_CONVERTER_PROTOKOL_TYPE  1012  // Protocol konvertera ESU tzu
//11/07/2011
#define CENTR_SVIAZI_PROTOKOL_TYPE      1013  // Protocol centra sviazi
#define SHLUZ_RIC_PROTOCOL_TYPE         1014  // Protocol shluza RIC
#define SERVER_BUS_PROTOCOL_TYPE        1015  // Protocol servera obshey shini dannih
#define  PROTOKOL_TEST                  2000  // Protokol dlya Testa Alekseya


// Polzovatel'skie tipi protokolov
#define  PROTOKOL_UKVO               100   // kodogrammi UKVO
#define  PROTOKOL_KST                200
#define  PROTOKOL_POCHTA             300


// �������� ������������ �������� ��� ��������������� ���������:
#define ABONENT_COUNT                0xFF
#define ABONENT_FIRST                0xFFFFFFFF
#define ABONENT_LAST                 ABONENT_FIRST - ABONENT_COUNT


#define ABONENT_MODI                 ABONENT_FIRST
#define ABONENT_SR                   ABONENT_FIRST - 1
#define ABONENT_TM                   ABONENT_FIRST - 2
#define ABONENT_FK                   ABONENT_FIRST - 3
#define ABONENT_AGPD                 ABONENT_FIRST - 4
#define ABONENT_MKP                  ABONENT_FIRST - 5
#define ABONENT_SHLUZ_A              ABONENT_FIRST - 6
#define ABONENT_MKP_MANAGER          ABONENT_FIRST - 7
#define ABONENT_MONITOR_KUF          ABONENT_FIRST - 8
#define ABONENT_MANAGER_KUF          ABONENT_FIRST - 9
//01.06.2009
#define ABONENT_MANAGER_236          ABONENT_FIRST - 10
//27.02.2013 abonent servera obshey shini dannih
#define ABONENT_SOSHD                ABONENT_FIRST - 11


#define ABONENT_FIRST_FREE           ABONENT_AGPD - 1


typedef struct
{
    pthread_cond_t      m_event;
    pthread_mutex_t     m_lock;    // ������� ��� �������

} EVENT;


// ������������ �������������
//=================================================


//-------------------------------------------------
// MODI
//-------------------------------------------------
typedef struct
{
   ULONG   IP_Address_SR;
   ULONG   IP_Address_SS;
   ULONG   Port_MODI;
   ULONG   Port_SS;
   ULONG   Port_SR;
   ULONG   LanTimeout;
   ULONG   WanTimeout;
   BYTE    ColPovt;
   ULONG   MaxPacketSize;
   BYTE    priority;
   WORD    MaxCountClient;
   ULONG   NetMask;
   WORD    CountNetwork;
   BYTE    m_Flags;

} MODI_PARAMETERS;

#define MODI_PARAM_SIZE    sizeof(MODI_PARAMETERS)
//-------------------------------------------------



//-------------------------------------------------
// TM
//-------------------------------------------------
typedef struct
{
    ULONG     m_QueueSize;   // Max razmer klientskoy ocheredi
    ULONG     m_Port;        // klientskiy port
    ULONG     m_BusPort;     // klientskiy bus port 
    ULONG     m_NetMask;     // maska seti
    ULONG     m_IP_SS;       // adres Servera sviazi
    // Novie parametri
    //====================
    BYTE      m_Flags;       // dopolnitel'nie opcii
    ULONG     m_TimeOut;     // taimout molchaniya abonenta v sek

} TM_PARAMETER;

#define TM_PARAM_SIZE    sizeof(TM_PARAMETER)

// Znachenya polya <m_Flags>
#define  TM_LOCAL_CLIENT      1   // 00000001 - razreshit' obmen megdi lokal'nimi klientami
#define  TM_PRINT_PACKET      2   // 00000010 - pechat' v LOG vseh prohodyashih paketov
#define  TM_PRINT            64   // 01000000 - Vivod v konsol' vspomogatel'noy inf v consol'
#define  TM_DAEMON          128   // 10000000 - zapusk v regime demona. Flag TM_PRINT dolgen bit' snyat.




//-------------------------------------------------


//-------------------------------------------------
// �������� ������� (��� �������� ��������)
//-------------------------------------------------
typedef struct
{
   key_t  Key;
   ULONG  Size;

} ADDR_INFO;

#define ADDR_INFO_SIZE    sizeof(ADDR_INFO)
//-------------------------------------------------


//-------------------------------------------------
// ���������� (��� �������� ��������)
//-------------------------------------------------
typedef struct
{
} STAT_INFO;

#define STAT_INFO_SIZE    sizeof(STAT_INFO)
//-------------------------------------------------



//-------------------------------------------------
// �������� ������
//-------------------------------------------------
typedef struct
{
 int      m_RetCode;       // ��� ������
 char     m_Name[200];     // ��������

} ERROR_INFO;

#define ERROR_INFO_SIZE    sizeof(ERROR_INFO)
//-------------------------------------------------




//-------------------------------------------------
// ��������� ���������
//-------------------------------------------------

typedef struct
{
  Addr      m_adr;         //  ����� ��������
  BOOL      m_IfConnect;   //  TRUE - ������� ���������,
  BOOL      m_IfRemote;    //  FALSE - ������� ���������

} ABONENT_INFO;

#define ABONENT_INFO_SIZE    sizeof(ABONENT_INFO)


//-------------------------------------------------
// Opisanie seti
//-------------------------------------------------
typedef struct
{
 ULONG     m_IP;        // adres seti;
 ULONG     m_NetMasck;  // maska seti
 ULONG     m_UserData;  // eto mogut bit' ili dannie ili ukazatel'

} NETWORK_INFO;

#define NETWORK_INFO_SIZE    sizeof(NETWORK_INFO)
//-------------------------------------------------




//-------------------------------------------------
// �������� �����������
//-------------------------------------------------
// ��������� ���������� �� ����������� (������� ���� - ���� ����������)
#define  STATUS_PRIOR_A      1   // 00000001 - ( ���� m_Status, m_StatusLine ) - nizshiy
#define  STATUS_PRIOR_B      2   // 00000010 - ( ���� m_Status, m_StatusLine )
#define  STATUS_PRIOR_C      4   // 00000100 - ( ���� m_Status, m_StatusLine )
#define  STATUS_PRIOR_D      8   // 00001000 - ( ���� m_Status, m_StatusLine ) - visshiy
#define  STATUS_PRIOR_E     16   // 00010000 - ( ���� m_StatusLine )
#define  STATUS_PRIOR_F     32   // 00100000 - ( ���� m_StatusLine )
#define  STATUS_PRIOR_G     64   // 01000000 - ( ���� m_StatusLine )
#define  STATUS_PRIOR_I    128   // 10000000 - ( ���� m_StatusLine )


#define  STATUS_MODI         16  // 00010000 - prisutstvie servera sviazi (MODI)( ���� m_Status )
// ������� ������
#define  COMMAND_RESTART     32  // 00100000 - �������( ���� m_Status )
#define  COMMAND_DOWN        64  // 01000000 - ���������� ������( ���� m_Status )

#define  STATUS_SERVER      128  // 10000000 - ������� ������� ( ���� m_Status )

#define PRIORITY_COUNT        4  // ����� �����������
#define MAX_PORTCOUNT         8  // maksimal'noe chislo portov

// Tipi moduley napravleniy ( pole m_Type )
#define  MODULES_SIMPLE_TCP        2000  // prostoy TCP (bez prioriteta)
#define  MODULES_PRIORITY_TCP      2001  // TCP c 4 prioritetami (0 - 3)
#define  MODULES_UKVO              2002  // modul' UKVO
#define  MODULES_POST              2003  // ESU TZ
#define  MODULES_236               2004  // rabotaem cherez POST sami s soboy
#define  MODULES_APD_236_MANEVR    2005  // Modul' dlya raboti po protokolu manevr
#define  MODULES_APD_236_V         2006  // Modul' dlya raboti po protokolu apd T-236-v

//07.09.2011
#define  MODULES_CS                2007
#define  MODULES_REZERV            2008  // zarezervirovano

#define  MODULES_HIDDEN            3000 
#define  MODULES_UDP               3001  // rabotaem cherez UDP socket tol'ko slugebnimi paketami
#define  MODULES_APD_236_COMMAND   3002  // Modul' dlya vidachi komand apd-236


#define  MODULES_DONTUSE          4000           
#define  MODULES_TLG              4001  // modul' telegrapha
#define  MODULES_SHIROTA          4002  // modul' Shirota


// Tipi sostoyaniy ( pole ChannelType-> m_Status)
#define  STATUS_LOCK             128  // 10000000 Zapret peredachi
#define  STATUS_UP               1    // 00000001 Est' soedinenie po napravleniy

typedef struct
{
 ULONG       m_IPLan;                   // adres udalennogo LAN
 ULONG       m_NetMask;                 // maska seti
 ULONG       m_IdPort;                  // nomer porta

} PORT_INFO;

typedef struct
{
 WORD        m_Type;                      // tip modulia napravleniya
 BYTE        m_Status;                    // sostoyanie modulia
 BYTE        m_StatusLine;                // sostoyanie napravleniya
 ULONG        m_QueueMaxCount;             // razmer ocheredi
 ULONG       m_MaxPackSize;               // maksimal'niy razmer paketa
 ULONG       m_IP;                        // adres udalennogo WAN
// ULONG     m_NetMask;  - do izmeneniya
 ULONG       m_TypeDepend;                // Dlya raznih tipov moget tractovat'cya po raznomy
 PORT_INFO   m_PortInfo[MAX_PORTCOUNT];   // Info o kanalah soedinenia
 Addr        m_DefAb;                     // Default abonent to send packets to
 ULONG       m_TimeOut;                   // Timeout dlya proverki soedinenia

} LINE_INFO;

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// dlia moduley MODULES_POST i MODULES_236:
//---------------------------
//    m_StatusLine = 0;
//
//    m_TypeDepend = ID - identifikator servera sviazi dlia POST
//
//    PORT_INFO[0].m_IPLan   = IP;   - adres udalennoy LAN
//    PORT_INFO[0].m_NetMask = Mask; - maska udalennoy LAN
//    PORT_INFO[0].m_IdPort  = 0(MODULES_POST)  = ID(MODULES_236) - identifikator servera;
//
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define LINE_INFO_SIZE    sizeof(LINE_INFO)
//-------------------------------------------------


typedef struct
{
  ULONG   m_Claster;
  int     m_Boud;
  ULONG   m_PacketSize;
  ULONG   m_BufSize;
  BYTE    m_Status;

} ChannelType;

#define CHANNEL_TYPE_SIZE    sizeof(ChannelType)


#define DEFINE_UDP_PORT 5100



//=============================================================
//   Struct for Manevr line addr
//=============================================================
typedef struct
{
	char   m_UN[6];
	char   m_BA[2];

} M_Addr;

//=============================================================
//#define  TM_CLIENT_PROTOKOL_TYPE     1002  // �� �� - ������
//=============================================================

#define  CLIENT_GET_CONNECTION             1 // ������ ����������
#define  CLIENT_CLOSE_CONNECTION           2 // �������� ����������
#define  CLIENT_GET_PARAMETERS             3 // ������ ���������� �������
#define  CLIENT_SET_LOGFILE                4 // ������ ���������������� �������
#define  CLIENT_GET_STATUS_AB              5 // ������ ��������� ���������� ��������
#define  CLIENT_PING                       6 // ping - ���� �����

#define  TM_CONNECTION_OK                100 // ������������� ��������� ����������
#define  TM_CLOSE_CONNECTION_OK          101 // ������������� �������� ����������
#define  TM_QUEUE_SIZE                   102 // ���������� ���������� ����� � �������
#define  TM_SEND_LOCK                    103 // ������ ��������
#define  TM_SEND_UNLOCK                  104 // ���������� ��������
#define  TM_ERROR_CODE                   105 // ��� ��������
#define  TM_PARAMETERS                   106 // ��������� �������
#define  TM_CLOSE                        107 // ���������� ������
#define  TM_STATUS_AB                    108 // ��������� ���������� ��������
#define  TM_TICKET                       109 // ���������

// ���� ��������
#define  ERROR_ALL_OK                      0 // ������ ���
#define  ERROR_CLIENT_ID                   1 // ������������� ������� �� ������ � ������ ��������������
#define  ERROR_REMOTE_ADDR                 2 // ������� ������� ������ �� ������, �� ���������� � ������ ������-��������
#define  ERROR_REMOTE_NET                  3 // �������������� ����� ����
#define  ERROR_PACKET_SIZE                 4 // ��������� ����� ������
#define  ERROR_BLOCK_LINE                  5 // Zaproshennoe napravlenie blokirovano
#define  ERROR_SEND_PACKET                 6 // Oshibka pri peredache packeta
#define  ERROR_CLNT_LST_BLOCK              7 // perepolnenie klientskogo spiska
#define  ERROR_TIMEOUT                     8 // razriv soedineniya po taimoutu molchaniya
#define  ERROR_LOGFILE                     9 // oshibka pri sozdanii logfile



// Kodi soobsheniy TM_SEND_UNLOCK i TM_SEND_LOCK
#define  LOCK_STOP_SEND                    0 // V sluchae polnoy ostanovki peredachi dannih
#define  LOCK_BREAK_LINE                   1 // Razriv transportnogo soedineniya
#define  LOCK_PRIOR_A_LINE                 2 // V sluchae modulia <PRIORITY_TCP> klientu budut posilat'sia
#define  LOCK_PRIOR_B_LINE                 3 // soobsheniya pri razrive soedineniy po prioritetam
#define  LOCK_PRIOR_C_LINE                 4
#define  LOCK_PRIOR_D_LINE                 5
#define  LOCK_FULL_LIST                    6 // Perepolnenie ocheredi klienta
#define  LOCK_FULL_LINE                    7 // perepolnenie ocheredi na napravlenii

#define  UNLOCK_SEND_ALL                   10 // Razreshenie peredachi po vsem napravleniyam
#define  UNLOCK_SEND_LINE                  11 // Razreshenie peredachi po napravleniu
#define  UNLOCK_PRIOR_A_LINE               12 // V sluchae modulia <PRIORITY_TCP> klientu budut posilat'sia
#define  UNLOCK_PRIOR_B_LINE               13 // soobsheniya pri razrive soedineniy po prioritetam
#define  UNLOCK_PRIOR_C_LINE               14
#define  UNLOCK_PRIOR_D_LINE               15
#define  UNLOCK_QUEUE_LINE                 16 // Osvobogdenie mesta v ocheredi na napravlenie
#define  UNLOCK_QUEUE_CLIENT               17 // Osvobogdenie mesta v clientskoy ocheredi




#define  MAX_COUNT_SEND_LOCK_ERROR         100   // ecli posle otpravki klientu zapreta peredachi, on
                                                 // prodolgaet posilku paketov, to posle poluchenia
                                                 // zadannogo chisla paketov server razrivaet soedinenie


//=============================================================
// #define  TM_AGPD_PROTOKOL_TYPE       1004  // TM - AgPD
//=============================================================

#define  TM_GET_CONNECTION             1 // ������ ����������
#define  TM_CLOSE_CONNECTION           2 // �������� ����������

#define  TM_GET_PARAMETERS             3 // ������ ���������� �������

#define  AGPD_CONNECTION_OK            100 // ������������� ��������� ����������
#define  AGPD_CLOSE_CONNECTION_OK      101 // ������������� �������� ����������
#define  AGPD_QUEUE_SIZE               102 // ���������� ���������� ����� � �������
#define  AGPD_SEND_LOCK                103 // ������ ��������
#define  AGPD_SEND_UNLOCK              104 // ���������� ��������
#define  AGPD_ERROR_CODE               105 // ��� ��������
#define  AGPD_PARAMETERS               106 // ��������� �������
#define  AGPD_CLOSE                    107 // ���������� ������


// ���� ��������
#define ERROR_PROTOCOL_NO_FIND         1 // Protokol ne nayden
#define ERROR_LOAD_CONFIG              2 // Oshibka zagruzki konfiguracii po zaproshennomu protokolu
#define ERROR_ID_LINE_NO_FIND          3 // Identifikator napravleniya ne nayden
#define ERROR_PROTOCOL_TYPE            4 // Tip protokola ne sootvetstvuet soedineniuy
#define ERROR_PACKET_LOSE              5 // Paket otbroshen iz-za blokirovki napravleniya (po razlichnim prichinam)
#define ERROR_PACKET_FORMAT            6 // Nepravil'niy format paketa protokola bolee visokogo urovnia
#define ERROR_LINE_LOCK                7 // Napravlenie zablokirovano po priznaku (prioritetu, tipu peredachi), ukazannomu v pakete protokola bolee visokogo urovnia




//==============================================================================================
//     #define  TM_SERVER_PROTOKOL_TYPE     1006   Protocol vzaimodeistviya megdu moduliami TM
//==============================================================================================

#define  TM_SERVER_TICKET                     1 // Kvitanciya
#define  TM_SERVER_PACKET_PACKET              2 // Upakovanniy packet
#define  TM_SERVER_ERROR_CODE                 3 // error code

//17.04.2009
// 100 - 120 -  diapazon slugebnih UDP paketov
#define  TM_SERVER_UDP_QUERY                   100 // zapros TCP soedineniya
#define  TM_SERVER_UDP_ANSWER                  101 // otvet na zapros soedineniya
#define  TM_SERVER_UDP_ATTESTATION             102 // podtvergdenie
#define  TM_SERVER_UDP_INFORMATION             103 // infa - posle sozdaniya socketa
#define  TM_SERVER_UDP_ERROR_CODE              104 // error code
//17.04.2009


// ���� ��������
#define ERRORS_CLIENT_NOT_FOUND         1 // Ne nayden client
#define ERRORS_IP_LINE_NO_FOUND         2 // IP napravleniya ne nayden
#define ERRORS_LINE_LOCK                3 // Paket otbroshen iz-za blokirovki napravleniya (po razlichnim prichinam)
#define ERRORS_LINE_IN_USED             4 // Napravlenie uge ispol'zuetsya
#define ERRORS_TM_NOT_STARTED           5 // Neopredelennoe sosstoyanie
#define ERRORS_TM_CONFLICT              6 // konflict interesov


//==============================================================================================
//     SERVER_BUS_PROTOCOL_TYPE        1015  // Protocol servera obshey shini dannih
//==============================================================================================
#define  TM_SERVER_BUS_GET_INFO                 1 //Zapros inf po abonentam
#define  TM_SERVER_BUS_INFO			2 //Paket s inf o abonentam
#define  TM_SERVER_BUS_ABN_STATUS		3 //Izmenenie sostoyaniya abonenta
#define  TM_SERVER_BUS_DATA			4 //Paket s dannimi




//==============================================================================================
//    MODI-MODI
//==============================================================================================
#define MODI_GET_ABONENT_STAT     1106 // Day sostoyanie abonentov
#define MODI_ABONENT_STAT         1103 // Sostoyanie vseh abonentov
#define MODI_CHANGED_ABONENT_STAT 1104 // Izmenennoe sostoyanie abonentov
#define MODI_ACK_ABONENT_STAT     1107 // Kvitantciya
#define MODI_TIME_LIFE            1108

//==============================================================================================
//    CENTR_SVIAZI_PROTOKOL_TYPE 
//==============================================================================================
#define MODI_CS_ABONENT_LIST  	     2	       		
#define MODI_CS_ABONENT_LIST_REPLY   3
#define MODI_CS_ABONENT_OFF 	     4
#define MODI_CS_ABONENT_ON	     5

//==============================================================================================
//#define  UPRAVLENIE_PROTOKOL_TYPE    1005  // Protocol upravleniya
//==============================================================================================
#define DONE_CMD  0 //������� ���������� ������
#define ABINF_CMD 2 //������ ���������� �� ��������
#define CHINF_CMD 3 //������ ���������� � �����������
#define ALL_ABINF_CMD 8 //������ ���������� � ���� ���������
#define ALL_CHINF_CMD 9 //������ ���������� � ���� ������������
#define MCFG_CMD  4 //������ ������������ ������
#define CFGCH_CMD 5 //��������� ������������ ������
#define CHWORK_CMD 6 //��������� ������� ������ ������ �����������
#define ABCHST_CMD 7 //������ ���������� ������ ��������� | �����������
#define ABST_CMD  1 //������ ���������� �� ��������
#define CHST_CMD  10 //������ ���������� �� �����������
#define REMAB_INF_CMD 11 // ������ ���������� �� �������� ���������
#define VERSION_CMD 12 // ������ ������ ��
#define ERR_CMD   99   //��������� � ������� ���������� �������
#define UPRAVLENIE_REP_MASK 0x8000 // ���� ��� � ���� ������ ���������� ����� �� ��������������� �������

//=============================================================
//#define  MKUF_AKUF_PROTOKOL_TYPE     1009  // MKUF - AKUF
//=============================================================
//�� ���� � ����  (���������)
#define MKUF_GET_CONNECT           1     //������ ����������
#define MKUF_CLOSE_CONNECT         2     //�������� ����������
//������� ����������
#define MKUF_SET_TIMEOUT           100   //���������� ������ ������ ���������� �� ����������� ����
#define MKUF_GET_CONFIG_MOOD       101   //������ ������� ������������ ����
#define MKUF_SET_CONFIG_MOOD       102   //���������� ����� ������������ ����
#define MKUF_GET_CONFIG_PCKP       103   //������ ������� ������������ ����
#define MKUF_SET_CONFIG_PCKP       104   //���������� ����� ������������ ����
#define MKUF_GET_ALLCONFIG_PCKP    105   //������ ������� ������������ ���� � ����������� ����
#define MKUF_SET_ALLCONFIG_PCKP    106   //���������� ����� ������������ ���� � ����������� ����
#define MKUF_GET_CONFIG_SS         107   //������ ������� ������������ ��
#define MKUF_SET_CONFIG_SS         108   //���������� ����� ������������ ��
#define MKUF_GET_CONFIG_MKP        109   //������ ������� ������������ ���
#define MKUF_SET_CONFIG_MKP        110   //���������� ����� ������������ ���
#define MKUF_GET_STAT_SS           111   //������ ���������� ��
#define MKUF_GET_STAT_MKP          112   //������ ���������� ���
#define MKUF_GET_STAT_PCKP         113   //������ ���������� ����
#define MKUF_GET_STAT_MOOD         114   //������ ���������� ����
#define MKUF_GET_ALLSTAT_PCKP      115   //������ ���������� ���� � ����������� ����
#define MKUF_SET_STAT_MKP          116
#define MKUF_SET_LIST_MKP          117
//��������� ��������� ���������� ����
#define MKUF_TEXT_MESSAGE          500
// �� ���� � ���� (���������)
#define AKUF_CONNECT               1000   //������������� ��������� ����������
#define AKUF_CLOSE_CONNECT         1001   //������������� �������� ����������
#define AKUF_LOCK_SEND             1002   //������ ��������
#define AKUF_ULOCK_SEND            1003   //���������� ��������
#define AKUF_ERR_CODE              1004   //��� ��������
#define AKUF_END_JOB               1005   //���������� ������
#define AKUF_COMMAND_STATUS        1006   //��������� ���������� �������

//=============================================================
//16.09.2010
//#define  ESUTZ_CONVERTER_PROTOKOL_TYPE  1012  
//Protocol konvertera ESU tzu
//=============================================================
#define ESUTZ_CST_PACKET           100
#define ESUTZ_MAIL_PACKET          200
/*
//=============================================================
//11.07.2017
//#define CENTR_SVIAZI_PROTOKOL_TYPE      1013  // Protocol centra sviazi
//Protocol konvertera ESU tzu
//=============================================================
#define CS_DATA                   1  // pacet s dannimi
#define GET_ABN_STATUS            2  // zapros podcluchennih abonentov
#define ABN_STATUS                3  // sostoyanie abonentov
#define STOP_ABN_TRANSMIT         4  // zapret na peredachu abonentu
#define START_ABN_TRANSMIT        5  // razreshenie peredachi abonentu

*/
#define TEST_PACKET_SERVERA_SVYAZI    1 //tip paketa dlya testovogo protokola


#pragma pack()

#endif
