#ifndef __TM_SERVER_MESSAGE_QUEUE__
#define __TM_SERVER_MESSAGE_QUEUE__

#include "CServerQueue.h"

class CTmServer : public CServerQueue
{

public:
	 CTmServer();
     virtual ~CTmServer();

     virtual int Create();

	 // ����������� ���������
	 //========================

     // FROM FK
     //------------
	 // parametri TM
	 virtual void OnFkParameters(TM_PARAMETER *Param);

	 // ostanov TM
	 virtual void OnFkCommandStop();

	 // restart TM
	 virtual void OnFkCommandRestart();

	 //  zapret zapisi v strukturu statistiki
	 virtual void OnFkLockStat();

	 //  razreshenie zapisi v strukturu statistiki
	 virtual void OnFkUnLockStat();

	 //  sozdanie strukturi statistiki
	 virtual void OnFkCreateStat(ADDR_INFO *StatInfo);

	 //  ostanov modulia napravleniia
	 virtual void OnFkCommandStopLine(LINE_INFO *LineInfo);

	 //  zapusk modulia napravleniia
	 virtual void OnFkCommandStartLine(LINE_INFO *LineInfo);
	 
	 // ���������� ������ ������� �������������
	 virtual void OnFkReLoadRoute();


     // FROM MODI
     //------------
	 // sozdanie adresnoy tablici
	 virtual void OnModiCreateAddr(ADDR_INFO *AddrInfo);

	 // zapret raboti s adresnaya tablica
	 virtual void OnModiLockAddr();

	 // razreshenie raboti s adresnaya tablica
	 virtual void OnModiUnLockAddr();



	 // ���������� ���������
	 //========================
     // TO FK
     //------------
     // zapros parametrov TM
     int MSG_GetParam();
     
     int MSG_GetStatistics();

     // komanda restart TM OK
     int MSG_RestartOk();

     // ostanov modulia napravleniia
     int MSG_StopLine(LINE_INFO *LineInfo);

     // zapusk modulia napravleniia
     int MSG_StartLine(LINE_INFO *LineInfo);

     // avariynoe zavershenie raboti
     int MSG_Error(ERROR_INFO *Error);


     // ����� �� ��� ����������
     //-------------------------
     // nachalo raboti TM
     int MSG_Start();
     
     int MSG_GetAddr();

     // zavershenie raboti TM
     int MSG_Stop();

     // ustanovlenie soedineniya po napravleniu
     int MSG_ConnectLine(LINE_INFO *LineInfo);

     // razriv soedineniya po napravleniu
     int MSG_DsConnectLine(LINE_INFO *LineInfo);

     // �����������/���������� ��������
     int MSG_AbonentConnect(Addr *adr, BOOL IfConnect = TRUE, BOOL IfRemote = FALSE);


protected:
     virtual int OnTmMessages(MsgHeader* Header);
     int SendMessageToFk(ULONG IdMessage, BYTE* Packet, ULONG Size);
     int SendMessageToModi(ULONG IdMessage, BYTE* Packet, ULONG Size);

};


#endif
