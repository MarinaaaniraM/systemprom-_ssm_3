// []------------------------------------------------------------------------[]
//  |                     |
//  |                                                             |
// []------------------------------------------------------------------------[]

#ifndef __SIMPLE_EVENT__
#define __SIMPLE_EVENT__

//#include <sys/errno.h>
//#include <pthread.h>
//#include "BaseTypes.h"
#include <time.h>
#include "CriticalSection.h"

//class CCriticalSection

#define MAX_EVENTS    255

typedef struct
{
   int       ID;
   BYTE      Status;

} MEvents;

class CEvent
 {

public:
    CEvent();
   ~CEvent();
   
public:
	// Sozdanie ob'ekta
    int  Create();
	// Vizivaem pri odnom sobitii
	// Esli bilo dobavleno bol'she, to avtomatom vizovetsia <WaitForMultipleEvent>
    int  WaitForEvent(ULONG  sec = 0);
	// Vizivaem esli sobitii bol'she 1
    int  WaitForMultipleEvent(ULONG  sec = 0);
	// vistavlyaem sobitiye
    void SetEvent(int ID = 0);
	// dobavit' ID sobitiya v spisok
    int  AddEvent(int ID);
	// Poluchit' vistavlennie sobitiya
    int  GetSetEvent(BOOL last = FALSE);
	// Proverit' sostoyanie sobitiya bez ego sbrosa
    BOOL GetStatusEvent(int ID);
	// sbrosit' vse sobitiya bez proverki
    void ResetAllEvent(void);
    
protected:
	// Vizivaetsya tol'ko iz <WaitForMultipleEvent>
    int  _GetSetEvent(); 

private:
    pthread_cond_t      *m_event;
    pthread_condattr_t   m_attr;
    ULONG                m_count;
    MEvents              m_global[MAX_EVENTS];
    CCriticalSection    *m_critical;
    ULONG                m_last;
     
};



#endif
