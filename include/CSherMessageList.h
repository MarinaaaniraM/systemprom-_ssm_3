//============================================================================
//
//    
//============================================================================

#ifndef __SHER_MEMORY_MESSAGE_LIST_H__
#define __SHER_MEMORY_MESSAGE_LIST_H__


#include "CEvent.h"
#include "CList.h"
#include "CSherMemory.h"
#include "CBaseMessageList.h"
#include "CSherMemoryList.h"



class CSherMessageList : public CBaseMessageList 
{
public:
	 CSherMessageList();
    ~CSherMessageList();

public:
	// udalit' spisok
	virtual int DeleteList();
	// sozdat' spisok
    int Create( key_t  key, 
		        EVENT* Event, 
				ULONG  MaxCount = MAX_COUNT_ITEM,
	            ULONG  BufSize = MAX_TRANSPORT_PACKET_SIZE,
				BOOL Server = TRUE);
	// poluchit' chislo elementov v spiske
	virtual int GetCount(ULONG* count );
	// dobavit' soobshenie
	virtual int AddMessage( BYTE*  msg, BOOL Flag = FALSE);
	// udalit' soobshenie iz spiska
	virtual int FreeMessage( BYTE* Msg );
	// zabrat' soobshenie
	virtual int GetMessage( BYTE** msg);
    virtual void WaitForEvent( int Priority = DATA_MESSAGE_LIST) {   m_event->WaitForEvent(); };
	virtual BYTE* GetFreeBuffer( ULONG  Size = MAX_TRANSPORT_PACKET_SIZE );

protected:
	CSherMemoryList     *m_list;
	CEvent				*m_event;
	CSherMemory         *m_Memory;

	BOOL                 m_Server;

};




#endif