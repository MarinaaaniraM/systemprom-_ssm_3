//============================================================================
//
//    
//============================================================================

#ifndef __ABONENT__LIST_H__
#define __ABONENT__LIST_H__

#include "BaseTypes.h"
#include "MessageType.h"
#include "ErrorType.h"
#include "CList.h"
#include "CriticalSection.h"
#include "CSocket.h"


class CAbonentList 
{
public:
	 CAbonentList();
    ~CAbonentList();

public:
	// sozdat' spisok
    int Create();
	// poluchit' chislo elementov v spiske
	int GetCount(ULONG* count);
	// dobavit' abonenta
	int AddAbonent( Addr*  Abn );
	int AddAbonent( ULONG  IP, ULONG  ID );
	int AddAbonent( char* IP, ULONG  ID );

	// udalit' abonenta iz spiska
	int Delete( Addr*  Abn );
	// proverit' na nalichie v spiske
    BOOL IfAbonent( Addr*  Abn );

	// poluchit' perviy element
	int GetFirstAbonent(Addr*  Abn);
	// poluchit' sleduyshii
	int GetNextAbonent( Addr*  Abn );
	// zamenit' abonenta
	int Replace( Addr*  SrcAbn,  Addr*  DestAbn  ); // ishem DestAbn i perepisivaem na SrcAbn


private:
	// udalit' spisok
	void DeleteList();
	// naiti v spiske
	Addr* FindAbonent( Addr*  Abn );

protected:
	CList               *m_list;
	CCriticalSection    *m_critical;


};




#endif 
