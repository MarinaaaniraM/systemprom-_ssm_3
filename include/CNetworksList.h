//----------------------------------------------------------------------------------------
//      ����:		
//					CNetworksList.h        
//      ����������:
//					������ �����
//		�����������: 
//					��� "������ � ��"
//		�����:
//					�� � ��
//		�����:
//					������ ����� ����������, ���. (095)265-89-57, aligmi@yandex.ru
//----------------------------------------------------------------------------------------

#ifndef __NETWORKS__LIST_H__
#define __NETWORKS__LIST_H__

#include "BaseTypes.h"
#include "MessageType.h"
#include "ErrorType.h"
#include "CList.h"
#include "CAccessMatrix.h"



class CNetworksList 
{
public:
	 CNetworksList();
    ~CNetworksList();

public:
	// sozdat' spisok
    int Create();
	// poluchit' chislo elementov v spiske
	ULONG GetCount();
	// dobavit' Network
	int AddNetwork(Network* Abn);

	// udalit' Network iz spiska
	int Delete(ULONG Net);
	// proverit' na nalichie v spiske
        BOOL IfNetwork(ULONG Net);
	WORD GetIdNetwork(ULONG Net);

	// naiti v spiske
	Network* FindNetwork(ULONG Net);

	// poluchit' perviy element
	int GetFirstNetwork(Network*  Abn);
	// poluchit' sleduyshii
	int GetNextNetwork(Network*  Abn);
	// zamenit' Networka

private:
	// udalit' spisok
	void DeleteList();

protected:
	CList               *m_list;

};




#endif 
