//============================================================================
//
//    
//============================================================================

#ifndef __CLIST_H__
#define __CLIST_H__

#include "ErrorType.h"
#include "BaseTypes.h"





class  CItem  
{
public:
	// zapomnit' sleduyshego
	void SetBottom(CItem* item);
	// zapomnit' predidushego
	void SetTop(CItem* item);
	// sozdat' (flag = TRUE - zapominaem tol'ko ukazatel', bez kopirovania)
	int Create(BYTE* item, ULONG size , BOOL  flag = TRUE, BYTE Prior = 3); 
	// poluchit' predidushego
	CItem* GetTop() { if (m_top == NULL) return NULL;
						else	return m_top;};
    // poluchit' sleduyshego
	CItem* GetBot() { if(m_bot == NULL) return NULL;
						else	return m_bot;};
    // poluchit' razmep bloka
	ULONG   GetSize() { return m_size; };
	// poluchit' ukazatel'
	BYTE*  GetItemPtr() { return m_Item; };
	CItem();
	~CItem();

public:
	UALONG   m_ID;    // ID elementa


protected:
	BYTE*	m_Item;  // ukazatel' na block pamiati
	UALONG	m_size;  // razmer
	CItem*  m_top;   // ukazateli na sosedey
	CItem*	m_bot;
	BOOL    m_flag;
	BYTE    m_Prior;

friend class CList;
	
};



class  CList  
{
public:
	// udalit' spisok
	void DeleteList();
	// zabrat' perviy element 
	int GetTop(void** item, ULONG* size );
	// poluchit' perviy element
	int GetFirst(void** item, ULONG* size );
	// poluchit' sleduyshii
	int GetNext( void** item, ULONG* size )
;
    // sdelat' kopiy elementa
	int CopyItem(UALONG ID, void** item, ULONG* size);
	// zabrat' element iz spiska
	int GetItem(UALONG ID, void** item, ULONG* size, BOOL  Flag = TRUE);
	// dobavit' element v spisok
	int AddItem(void* item, ULONG size, BOOL flag = TRUE, BYTE Prior = 3);
	// udalit' element spiska
	int DeleteItem(void* Item);
	// poluchit' chislo elementov v spiske
	ULONG GetCount() { return m_count;};



	CList();
	virtual ~CList();

protected:
	// naiti element spiska
	CItem* FindItem(UALONG ID);
	UALONG GetNumItem(void * Item);
	// udalit' element spiska
	int DeleteItem(UALONG ID);
	
        int Insert(CItem* Item);

	// udalit' posledniy
	int DelBot();
	// udalit' perviy
	int DelTop();

protected:
	CItem* m_next;
	CItem* m_top;
	CItem* m_bot;
	ULONG  m_count;    // chislo elementov v spiske
};





#endif 
