//=======================================================================================
//			Server message 
//
//=======================================================================================

#ifndef __SERVER_MESSAGE_QUEUE__
#define __SERVER_MESSAGE_QUEUE__

#include <sys/ipc.h>
#include <pthread.h>
#include <sched.h>
#include "CMessageQueue.h"
#include "CMessageList.h"

// Potok na chtenie iz ocheredi   
void* ReadQueue(void* Ptr);
void* QueueHandler(void* Ptr);


class CServerQueue 
{
public:
	CServerQueue();
	virtual ~CServerQueue();


     virtual int Create(BYTE Type = 0);
	 void Close();
	 int SendMessage(BYTE Abn, ULONG IdMessage, BYTE* Packet, ULONG Size);

	 friend void* ReadQueue(void* Ptr);
	 friend void* QueueHandler(void* Ptr);

     virtual int OnFkMessages(MsgHeader* Header);
     virtual int OnModiMessages(MsgHeader* Header);
     virtual int OnTmMessages(MsgHeader* Header);

protected:
      int MessageHandler(BYTE* Packet);


protected:
	CMessageList*      m_MsgList;
	pthread_t          m_ThreadIdRead;
	pthread_t          m_ThreadIdHandler;
	BYTE               m_Type;

	CMessageQueue*	   m_MessageTM;	
	CMessageQueue*	   m_MessageFK;	
	CMessageQueue*	   m_MessageMODI;	

	CMessageQueue*	   m_Message;	
};


#endif
