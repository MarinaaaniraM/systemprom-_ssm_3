#ifndef _SHERMEMORY_LIST_
#define _SHERMEMORY_LIST_

#include "ErrorType.h"
#include "BaseTypes.h"
#include "MessageType.h"

// ���������� � BaseTypes.h
//#define MAX_TRANSPORT_PACKET_SIZE    2048
//#define MAX_COUNT_ITEM               10


typedef struct
{
	ULONG              m_MaxCount; // ������������ ����� ��������� ������
	ULONG              m_BufLen;   // ������������ ������ �������������� ������
	ULONG              m_Count;    // ������� ����� ��������� ������
	LONG               m_First;    // ����� ������� � ������
	LONG               m_Last;     // ����� ���������� � ������
	LONG               m_Free;     // ����� ��������� ������

} MemoryListInfo;


#define CREATE_MEMORY_LIST    1
#define OPEN_MEMORY_LIST      2

#define BUFF_PAUSE            10

class CSherMemoryList 
{
public:
	 CSherMemoryList();
    ~CSherMemoryList();

public:
	// udalit' spisok
	int FreeList();

	// sozdat' spisok
    int Create( MemoryListInfo* List, BYTE Flag = OPEN_MEMORY_LIST);

	// poluchit' chislo elementov v spiske
	ULONG GetCount();

	// zabrat' soobshenie
	int GetMessage( BYTE **Msg );
	// udalit' soobshenie iz spiska
	int FreeMessage( BYTE* Msg );

	// zaprosit' svobodniy bufer
	int GetFreeBuffer( BYTE **Buf );
	// dobavit' soobshenie
	int AddMessage( BYTE*  msg );

protected:
	BYTE*  GetBuffer(ULONG  Num );
	void   IncFirst(); 
	void   IncLast(); 


protected:
	MemoryListInfo*    m_MemoryListInfo;
	BOOL               m_GetMessage;
	BOOL               m_GetBuffer;
	BYTE              *m_Ptr;

}



#endif