#include "ServiceStructures.h"






bool operator == (const ServiceStructures::DeviceTypeInterface &dti1, const ServiceStructures::DeviceTypeInterface &dti2)
{
    return ((dti1.name == dti2.name) && (dti1.offlineImagePath == dti2.offlineImagePath) &&
            (dti1.onlineImagePath == dti2.onlineImagePath) && (dti1.onlineImage == dti2.onlineImage) &&
            (dti1.offlineImage == dti2.offlineImage));
}
/////////////////////////////////////////
ServiceStructures::DeviceTypeInterface ServiceStructures::findInList(const QList<ServiceStructures::DeviceTypeInterface> *targetList,
                                                                     const QString &name, const QString &onlineImagePath, const QString &offlineImagePath)
{
    for (int i = 0; i < targetList->size(); i++)
    {
        if ((targetList->at(i).name == name) && (targetList->at(i).offlineImagePath == offlineImagePath) &&
                (targetList->at(i).onlineImagePath == onlineImagePath))
            return targetList->at(i);
    }

    return ServiceStructures::DeviceTypeInterface{"", QImage(), QImage(), "", ""};
}
/////////////////////////////////////////
ServiceStructures::DeviceTypeInterface ServiceStructures::findInList(const QList<DeviceTypeInterface> *targetList, const QString &name)
{
    for (int i = 0; i < targetList->size(); i++)
    {
        if (targetList->at(i).name == name)
            return targetList->at(i);
    }

    return ServiceStructures::DeviceTypeInterface{"", QImage(), QImage(), "", ""};
}
























