#ifndef GRAPHICALITEMS_H
#define GRAPHICALITEMS_H




#include <QGraphicsPixmapItem>
#include <QGraphicsSimpleTextItem>
#include <QPixmap>
#include <QAction>
#include "ServiceStructures.h"



class NetDevice;
class NetDeviceLink;



class NetDeviceGraphicalItem : public QObject, public QGraphicsPixmapItem      // иконка для отображения NetDevice
                                                                               // наследуем QObject для получения сигналов/слотов
{
    Q_OBJECT


public:
    explicit NetDeviceGraphicalItem(NetDevice *device, QImage offlineImage, QImage onlineImage, QGraphicsItem *parent = 0);
    void setMultiMap(const bool &multimap = true); // добавление (true) или удаление (false) мини-иконки "много уровней"
    void setImages(const QImage &offlineImage, const QImage &onlineImage);
    QImage getOriginalOffImage() const;
    QImage getOriginalOnImage() const;
    QPixmap getOffImagePixmap() const;
    QPixmap getOnImagePixmap() const;
    QGraphicsSimpleTextItem * getTextItem();
    const QGraphicsSimpleTextItem * getTextItem() const;
    void setTextItemText(const QString &deviceName, const QString &deviceAddress);

    virtual ~NetDeviceGraphicalItem();


protected:
    NetDevice *netDevice;
    QImage originalOffImage;
    QImage originalOnImage;
    QPixmap offImage;
    QPixmap onImage;
    QGraphicsSimpleTextItem *textItem;
    bool multiMap = false; // идентично multiLevel хозяйского NetDevice'a
    bool deviceIsOnline = false;


    void addMultiMapIconOnImage(QImage &image); // для мультилевельной карты - добавить мини-иконку "много уровней" на иконки
    QVariant itemChange(GraphicsItemChange change, const QVariant &value); // переопределена для эмита сигналов
                                                                           // "элемент выбран" и "позиция изменилась"
    void createActions();
    void setupTextItem();

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);


public:
    bool operator ==(const NetDeviceGraphicalItem &other) const;


protected:
    QAction *editAct;
    QAction *connectToOther;
    QAction *deleteDeviceAct;

    // ------------------------------------------------------------------------
    QAction* openWeb;
    // ------------------------------------------------------------------------


public slots:
    void setDeviceOnline();
    void setDeviceOffline();
    void setDeviceStatus(const bool &alive); // online or offline


protected slots:
    void editAct_slot();
    void connectToOther_slot();
    void deleteDeviceAct_slot();

    // ========================================================================
    void openWeb_slot();
    // ========================================================================


signals:
    void itemIsSelected();
    void positionChanged(QPointF newPos);
    void userRequestToEditDevice();
    void userRequestToConnectDevices();
    void userRequestToDeleteDevice();
    void userDoubleClickedItem();
    void deviceOnlineStatusChanged(bool isOnline);

    // ========================================================================
    void getWebInterfaceOfdevice();
    // ========================================================================

};


///////////////////////////////////////////////


// пришлось наследовать от PixmapItem, т.к. чехарду с координатами RectItem я так и не поборол
// да, я знаю - это жутко через ЖОПУ, а что делать -_-
class NetDevicePointItem : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT


public:
    explicit NetDevicePointItem(NetDeviceLink *deviceLink);
    void setPos(const QPointF &pos);
    void setPos(qreal x, qreal y);


protected:
    NetDeviceLink *netDeviceLink;

    void createActions();

    QVariant itemChange(GraphicsItemChange change, const QVariant &value); // переопределена для эмита сигнала
                                                                           // "позиция изменилась"

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);


protected slots:
    void deletePointAct_slot();


protected:
    QAction *deletePointAct;


signals:
    void positionChanged(QPointF newPos);
    void userRequestToDeleteLink();


};


///////////////////////////////////////////////


class NetDeviceLineItem : public QObject, public QGraphicsLineItem
{
    Q_OBJECT


public:
    explicit NetDeviceLineItem(QPointF &firstPoint, QPointF &secondPoint, NetDeviceLink *deviceLink);


public slots:
    void firstPointChanged(const QPointF &newPos);
    void secondPointChanged(const QPointF &newPos);


protected:
    NetDeviceLink *netDeviceLink;
    QPointF pointOne;
    QPointF pointTwo;


protected:
    enum LocalDefines
    {
        PenWidth = 2
    };

};











#endif // GRAPHICALITEMS_H
