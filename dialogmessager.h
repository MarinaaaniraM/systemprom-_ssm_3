#ifndef DIALOGMESSAGER_H
#define DIALOGMESSAGER_H




#include <QWidget>
#include <QString>
#include <QList>
#include "ServiceStructures.h"


// различные диалоги - для сборки в одном виджете
// по-хорошему еще можно было влепить singleton, но лень. На будущее в планах.



class DialogMessager : public QWidget
{
    Q_OBJECT


public:
    explicit DialogMessager(QWidget *parent = 0);
    void loadCustomDeviceTypeInterfaces(); // загрузка из XML. Отдельный паблик метод на будущее - для сплэшскрина
    void saveCustomDeviceTypeInterfaces();
    const QList<ServiceStructures::DeviceTypeInterface> * getCustomDTIList();
    static bool closeChangedTabWidget(); // имеются несохраненные данные tabwidget'а!!11
    static bool closeProgramWithUnsavedChanges(); // а это коли совсем все закрываем
    QString getFileNameToSaveTabWidget();
    QString getFileNameToOpenMap();
    static bool selectedGraphicsItemCanBeDeleted(); // действительно удалить элемент с карты?
    void cannotOpenFile();
    int getNumber(const QString &title, const QString &text, const int &defaultValue,
                  const int &min, const int &max, const int &step); // returns -1 if error


public slots:
    void showAboutProgramDialog();
    void showEditDeviceTypeInterfacesDialog();
    void showWarningDialog(const QString &warnText);
    void showErrorDialog(const QString &errText);


protected:
    QList<ServiceStructures::DeviceTypeInterface> customDeviceTypeInterfaces; // пользовательские DeviceTypeInterface
    static bool raiseBoolDialog(const QString &text, const QString &informativeText,
                                const QString &okButtonText, const QString &cancelButtonText);


signals:
    void warning(QString warnText, ServiceStructures::LogMessageType messageType = ServiceStructures::LogMessageType::warning);


};

#endif // DIALOGMESSAGER_H
