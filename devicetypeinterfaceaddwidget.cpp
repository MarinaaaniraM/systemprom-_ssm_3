#include <QListWidgetItem>
#include <QGridLayout>
#include <QFileDialog>
#include <QStringList>
#include "devicetypeinterfaceaddwidget.h"
#include "GlobalConstants.h"





DeviceTypeInterfaceAddWidget::DeviceTypeInterfaceAddWidget(QList<ServiceStructures::DeviceTypeInterface> *userDeviceTypeInterfaces,
                                                           QWidget *parent) :
     QDialog(parent), customDeviceTypeInterfaces(userDeviceTypeInterfaces)
{
    createElements();

    initListWidgetContent();

    setupLayout();

    connectComponents();

    listWidget->setCurrentRow(0);

    setWindowTitle(tr("Управление типами устройств"));
    setAttribute(Qt::WA_DeleteOnClose);
}
/////////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::initListWidgetContent()
{
    for (int i = 0; i < PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size(); i++)
    {
        // почему не foreach? чтобы однозначно иметь соответствие между позицией в списке и listWidget'овым row
        QListWidgetItem *newItem = new QListWidgetItem(
                    QIcon(QPixmap::fromImage(PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.at(i).onlineImage)),
                    PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.at(i).name);  // всегда используем иконкой online image
        listWidget->insertItem(i, newItem);
    }

    // а теперь вставляем кастомные типы
    for (int i = 0; i < customDeviceTypeInterfaces->size(); i++)
    {
        QListWidgetItem *newItem = new QListWidgetItem(
                    QIcon(QPixmap::fromImage(customDeviceTypeInterfaces->at(i).onlineImage)),
                    customDeviceTypeInterfaces->at(i).name);
        listWidget->insertItem((i + PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size()), newItem);
    }
}
/////////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::createElements()
{
    listWidget = new QListWidget(this);
    listWidget->setStatusTip(tr("Список типов устройств"));
    listWidget->setWhatsThis(tr("Список используемых типов устройств - как предустановленных, так и пользовательских"));
    listWidget->setSelectionMode(QAbstractItemView::SingleSelection);

    okButton = new QPushButton(tr("ОК"), this);
    okButton->setStatusTip(tr("Ок"));
    okButton->setWhatsThis(tr("Закончить работу с диалогом"));
    okButton->setAutoDefault(false);

    addNewDTIButton = new QPushButton(tr("Добавить"), this);
    addNewDTIButton->setStatusTip(tr("Добавить новый тип устройств"));
    addNewDTIButton->setWhatsThis(tr("Создание нового типа устройств с заданными параметрами"));
    addNewDTIButton->setAutoDefault(false);

    deleteDTIButton = new QPushButton(tr("Удалить"), this);
    deleteDTIButton->setStatusTip(tr("Удалить тип устройств"));
    deleteDTIButton->setWhatsThis(tr("<qt>Удаление выбранного типа устройств. <i>Предустановленные типы удалить нельзя</i></qt>"));
    deleteDTIButton->setAutoDefault(false);

    addNewDTIName = new QLineEdit(this);
    addNewDTIName->setStatusTip(tr("Введите имя нового типа устройств"));
    addNewDTIName->setWhatsThis(tr("Ввод нового имени для пользовательского типа устройств"));

    addNewDTIOfflineIconPath = new QLineEdit(this);
    addNewDTIOfflineIconPath->setStatusTip(tr("Введите путь к иконке выключенного устройства"));
    addNewDTIOfflineIconPath->setWhatsThis(tr("Ввод пути к иконке выключенного пользовательского типа устройств"));

    addNewDTIOnlineIconPath = new QLineEdit(this);
    addNewDTIOnlineIconPath->setStatusTip(tr("Введите путь к иконке включенного устройства"));
    addNewDTIOnlineIconPath->setWhatsThis(tr("Ввод пути к иконке включенного пользовательского типа устройств"));

    addNewDTIOfflineIconButton = new QPushButton(QIcon("://Resources/actionIcons/OpenFile.png"), "", this);
    addNewDTIOfflineIconButton->setStatusTip(tr("Выбрать файл..."));
    addNewDTIOfflineIconButton->setWhatsThis(tr("Выбрать файл в отдельном диалоговом окне"));
    addNewDTIOfflineIconButton->setAutoDefault(false);

    addNewDTIOnlineIconButton = new QPushButton(QIcon("://Resources/actionIcons/OpenFile.png"), "", this);
    addNewDTIOnlineIconButton->setStatusTip(tr("Выбрать файл..."));
    addNewDTIOnlineIconButton->setWhatsThis(tr("Выбрать файл в отдельном диалоговом окне"));
    addNewDTIOnlineIconButton->setAutoDefault(false);

    pictureLabel = new QLabel(this);
    pictureLabel->setStatusTip(tr("Изображение включенного устройства"));
    pictureLabel->setWhatsThis(tr("Изображение, которое показывается, если устройство данного типа включено"));
}
///////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::setupLayout()
{
    // т.к. это только текстовые QLabel, то можно их создать локально только здесь, и не таскать в полях класса
    // все равно мы их parent, удалятся когда надо
    QLabel *typeNameLabel = new QLabel(tr("Название типа:"), this);
    QLabel *imageDTIOnLabel = new QLabel(tr("Пиктограмма вкл"), this);
    QLabel *imageDTIOffLabel = new QLabel(tr("Пиктограмма выкл"), this);


    QGridLayout *mainLayout = new QGridLayout(this);

    mainLayout->addWidget(listWidget, 0, 0, 17, 3);
    mainLayout->addWidget(addNewDTIButton, 0, 4, 1, 2);
    mainLayout->addWidget(deleteDTIButton, 2, 4, 1, 2);
    mainLayout->addWidget(typeNameLabel, 5, 3, 1, 3);
    mainLayout->addWidget(addNewDTIName, 6, 3, 1, 3);
    mainLayout->addWidget(imageDTIOnLabel, 8, 3, 1, 3);
    mainLayout->addWidget(addNewDTIOnlineIconPath, 9, 3, 1, 2);
    mainLayout->addWidget(addNewDTIOnlineIconButton, 9, 5, 1, 1);
    mainLayout->addWidget(imageDTIOffLabel, 11, 3, 1, 3);
    mainLayout->addWidget(addNewDTIOfflineIconPath, 12, 3, 1, 2);
    mainLayout->addWidget(addNewDTIOfflineIconButton, 12, 5, 1, 1);
    mainLayout->addWidget(pictureLabel, 14, 4, 3, 2); // размер куска под иконку задается тут - подбирать ручками
    mainLayout->addWidget(okButton, 19, 2, 1, 2);

    listWidget->setMinimumWidth(GridLayoutWidgetMinimalWidth * 3);
    addNewDTIButton->setMinimumWidth(GridLayoutWidgetMinimalWidth * 2);
    deleteDTIButton->setMinimumWidth(GridLayoutWidgetMinimalWidth * 2);
    typeNameLabel->setMinimumWidth(GridLayoutWidgetMinimalWidth * 3);
    addNewDTIName->setMinimumWidth(GridLayoutWidgetMinimalWidth * 3);
    imageDTIOnLabel->setMinimumWidth(GridLayoutWidgetMinimalWidth * 3);
    addNewDTIOnlineIconPath->setMinimumWidth(GridLayoutWidgetMinimalWidth * 2);
    addNewDTIOnlineIconButton->setMinimumWidth(GridLayoutWidgetMinimalWidth);
    imageDTIOffLabel->setMinimumWidth(GridLayoutWidgetMinimalWidth * 3);
    addNewDTIOfflineIconPath->setMinimumWidth(GridLayoutWidgetMinimalWidth * 2);
    addNewDTIOfflineIconButton->setMinimumWidth(GridLayoutWidgetMinimalWidth);
    pictureLabel->setMinimumWidth(GridLayoutWidgetMinimalWidth * 3); // и тут выставить ручками аналогичное значение
    okButton->setMinimumWidth(GridLayoutWidgetMinimalWidth * 2);

    for(int i = 0; i < mainLayout->rowCount(); i++)
    {
        mainLayout->setRowStretch(i, 1); // чтобы все были равны во stretch.
        mainLayout->setRowMinimumHeight(i, GridLayoutMinimalSizeSide);
    }
    for (int i = 0; i < mainLayout->columnCount(); i++)
    {
        mainLayout->setColumnStretch(i, 1); // аналогично для колонок
        mainLayout->setColumnMinimumWidth(i, GridLayoutMinimalSizeSide);
    }

    setLayout(mainLayout);
}
///////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::connectComponents()
{
    connect(listWidget, &QListWidget::currentRowChanged, this, &DeviceTypeInterfaceAddWidget::listRowChanged);
    connect(okButton, &QPushButton::clicked, this, &DeviceTypeInterfaceAddWidget::close);
    connect(addNewDTIOfflineIconButton, &QPushButton::clicked, this, &DeviceTypeInterfaceAddWidget::addNewDTIOfflineIconButton_slot);
    connect(addNewDTIOnlineIconButton, &QPushButton::clicked, this, &DeviceTypeInterfaceAddWidget::addNewDTIOnlineIconButton_slot);
    connect(addNewDTIButton, &QPushButton::clicked, this, &DeviceTypeInterfaceAddWidget::addNewDTIButton_slot);
    connect(deleteDTIButton, &QPushButton::clicked, this, &DeviceTypeInterfaceAddWidget::deleteDTIButton_slot);
}
///////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::bad_addNewDTIOnlineIconPath()
{
    // Застенчиво покраснеем полем ввода
    QPalette badPalette;
    badPalette.setColor(QPalette::Base, Qt::red);
    addNewDTIOnlineIconPath->setPalette(badPalette);

    connect(addNewDTIOnlineIconPath, &QLineEdit::textChanged, this, &DeviceTypeInterfaceAddWidget::addNewDTIOnlineIconPath_textChanged_slot);
    // фактически одноразовый коннект - при первом изменении перекрасим обратно в белый
}
///////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::bad_addNewDTIOfflineIconPath()
{
    // Застенчиво покраснеем полем ввода
    QPalette badPalette;
    badPalette.setColor(QPalette::Base, Qt::red);
    addNewDTIOfflineIconPath->setPalette(badPalette);

    connect(addNewDTIOfflineIconPath, &QLineEdit::textChanged, this, &DeviceTypeInterfaceAddWidget::addNewDTIOfflineIconPath_textChanged_slot);
    // фактически одноразовый коннект - при первом изменении перекрасим обратно в белый
}
///////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::listRowChanged(const int &currentRow)
{
    if (currentRow < PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size())
    {
        // выбран предустановленный тип
        showInfoAboutDTI(PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.at(currentRow), true);
    }
    else
    {
        // выбран пользовательский тип
        showInfoAboutDTI(customDeviceTypeInterfaces->at(currentRow - PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size()), false);
    }
}
//////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::showInfoAboutDTI(const ServiceStructures::DeviceTypeInterface &dti, const bool &preDefined)
{
    addNewDTIName->setText(dti.name);
    pictureLabel->setPixmap(QPixmap::fromImage(dti.onlineImage));

    if (preDefined)
    {
        deleteDTIButton->setEnabled(false);
    }
    else
    {
        deleteDTIButton->setEnabled(true);
    }
}
/////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::addNewDTIOnlineIconButton_slot()
{
    // создадим свой вместо дефолтного для большей кастомизации
    QFileDialog *fileDialog = new QFileDialog(this);
    fileDialog->setFileMode(QFileDialog::ExistingFile);
    fileDialog->setNameFilter(tr("Изображения (*.png)"));

    if (fileDialog->exec())
    {
        QStringList strList = fileDialog->selectedFiles();
        if (!strList.isEmpty())
            addNewDTIOnlineIconPath->setText(strList.last()); // т.к. мы в режиме QFileDialog::ExistingFile, то там может быть лишь один элемент
    }
}
/////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::addNewDTIOfflineIconButton_slot()
{
    // создадим свой вместо дефолтного для большей кастомизации
    QFileDialog *fileDialog = new QFileDialog(this);
    fileDialog->setFileMode(QFileDialog::ExistingFile);
    fileDialog->setNameFilter(tr("Изображения (*.png)"));

    if (fileDialog->exec())
    {
        QStringList strList = fileDialog->selectedFiles();
        if (!strList.isEmpty())
            addNewDTIOfflineIconPath->setText(strList.last()); // т.к. мы в режиме QFileDialog::ExistingFile, то там может быть лишь один элемент
    }
}
/////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::addNewDTIButton_slot()
{
    if ((!addNewDTIOfflineIconPath->text().isEmpty()) && (!addNewDTIOnlineIconPath->text().isEmpty()) && (!addNewDTIName->text().isEmpty()))
    {
        // поля не пусты, но необходимо проверить на валидность картинки
        QImage onImage;
        QImage offImage;

        if (onImage.load(addNewDTIOnlineIconPath->text(), "PNG"))
        {
            onImage = onImage.scaled(DeviceTypeInterfaceIconSideSize, DeviceTypeInterfaceIconSideSize);
        }
        else
        {
            // картинка не валидна!
            emit warning(tr("Изображение включенного устройства для указанного пути не может быть загружено"));
            bad_addNewDTIOnlineIconPath();

            return;
        }

        if (offImage.load(addNewDTIOfflineIconPath->text(), "PNG"))
        {
            offImage = offImage.scaled(DeviceTypeInterfaceIconSideSize, DeviceTypeInterfaceIconSideSize);
        }
        else
        {
            // картинка не валидна!
            emit warning(tr("Изображение выключенного устройства для указанного пути не может быть загружено"));
            bad_addNewDTIOfflineIconPath();

            return;
        }

        // раз мы здесь, то картинки мы сумели загрузить
        ServiceStructures::DeviceTypeInterface newDTI;
        newDTI.name = addNewDTIName->text();
        newDTI.offlineImage = offImage;
        newDTI.offlineImagePath = addNewDTIOfflineIconPath->text();
        newDTI.onlineImage = onImage;
        newDTI.onlineImagePath = addNewDTIOnlineIconPath->text();

        QListWidgetItem *newItem = new QListWidgetItem(QIcon(QPixmap::fromImage(newDTI.onlineImage)), newDTI.name);
        int rowNum = PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size() + customDeviceTypeInterfaces->size();

        customDeviceTypeInterfaces->append(newDTI);

        listWidget->insertItem(rowNum, newItem);
        listWidget->setCurrentRow(rowNum);
    }
    else
    {
        // кто-то неверно введен!
        if (addNewDTIName->text().isEmpty())
        {
            emit warning(tr("Необходимо ввести имя нового типа устройств"));

            // Застенчиво покраснеем полем ввода
            QPalette badPalette;
            badPalette.setColor(QPalette::Base, Qt::red);
            addNewDTIName->setPalette(badPalette);

            connect(addNewDTIName, &QLineEdit::textChanged, this, &DeviceTypeInterfaceAddWidget::addNewDTIName_textChanged_slot);
            // фактически одноразовый коннект - при первом изменении перекрасим обратно в белый

            return;
        }
        if (addNewDTIOnlineIconPath->text().isEmpty())
        {
            emit warning(tr("Необходимо ввести путь до изображения включенного устройства"));
            bad_addNewDTIOnlineIconPath();

            return;
        }
        if (addNewDTIOfflineIconPath->text().isEmpty())
        {
            emit warning(tr("Необходимо ввести путь до изображения выключенного устройства"));
            bad_addNewDTIOfflineIconPath();

            return;
        }
    }
}
/////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::deleteDTIButton_slot()
{
    int numToDelete = listWidget->currentRow();

    if (numToDelete < PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size())
    {
        return; // залезли в предустановленные типы. Неясно, как умудрились, но всё равно нельзя это делать.
    }

    QList<QListWidgetItem *> itemsToMove; // сложим сюда удаляемый элемент и все старшие по отношению к нему по row
    for (int i = numToDelete;
         i < (customDeviceTypeInterfaces->size() + PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size());
         i++)
    {
        itemsToMove.append(listWidget->takeItem(i));
    }

    QListWidgetItem *itemToDelete = itemsToMove.at(0); // тот самый, кого надо удалить
    itemsToMove.removeFirst(); // удалили из списка
    delete itemToDelete; // и снесли самого

    for (int i = 0; i < itemsToMove.size(); i++)
    {
        listWidget->insertItem((i + numToDelete), itemsToMove.at(i));
    }

    customDeviceTypeInterfaces->removeAt(numToDelete - PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size());
}
/////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::addNewDTIOfflineIconPath_textChanged_slot()
{
    QPalette goodPalette;
    goodPalette.setColor(QPalette::Base, Qt::white);
    addNewDTIOfflineIconPath->setPalette(goodPalette);

    // мавр сделал свое дело, мавра можно дисконнектнуть
    disconnect(addNewDTIOfflineIconPath, &QLineEdit::textChanged, this, &DeviceTypeInterfaceAddWidget::addNewDTIOfflineIconPath_textChanged_slot);
}
/////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::addNewDTIOnlineIconPath_textChanged_slot()
{
    QPalette goodPalette;
    goodPalette.setColor(QPalette::Base, Qt::white);
    addNewDTIOnlineIconPath->setPalette(goodPalette);

    // мавр сделал свое дело, мавра можно дисконнектнуть
    disconnect(addNewDTIOnlineIconPath, &QLineEdit::textChanged, this, &DeviceTypeInterfaceAddWidget::addNewDTIOnlineIconPath_textChanged_slot);
}
/////////////////////////////////////////////
void DeviceTypeInterfaceAddWidget::addNewDTIName_textChanged_slot()
{
    QPalette goodPalette;
    goodPalette.setColor(QPalette::Base, Qt::white);
    addNewDTIName->setPalette(goodPalette);

    // мавр сделал свое дело, мавра можно дисконнектнуть
    disconnect(addNewDTIName, &QLineEdit::textChanged, this, &DeviceTypeInterfaceAddWidget::addNewDTIName_textChanged_slot);
}






























