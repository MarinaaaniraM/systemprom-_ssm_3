#ifndef NETDEVICETHREADING_H
#define NETDEVICETHREADING_H



#include <QRunnable>
#include <QObject>
#include <QHostAddress>
#include <QString>
#include <QList>
#include "ICMP_boost/ping.h"




class NetDeviceThreading : public QObject, public QRunnable
{
    Q_OBJECT


public:
    explicit NetDeviceThreading(QObject *parent = 0);
    void run();

    virtual ~NetDeviceThreading();


public slots:
    void setTimeToRePing(const int &value);
    void setTimeToWaitForPingReplyModifyer(const int &value);
    void addPingableDevice_slot(const QString &uuid, const QString &ipAddr);
    void removePingableDevice_slot(const QString &uuid);
    void deviceIsAlive_slot(const bool &alive, const CustomQStrList &uuids);


signals:
    void deviceIsAlive(bool alive, CustomQStrList uuids);
    void timeToRePingChanged(int value);
    void timeToWaitForPingReplyModifyerChanged(int value);


public:
    Pinger *pinger;


protected:
    QHostAddress pingAddress;
    QString parentID;

    boost::asio::io_service io_service;

    int timeToRePing; // должны быть установлены до run()
    int timeToWaitForPingReplyModifyer;


};






#endif // NETDEVICETHREADING_H
