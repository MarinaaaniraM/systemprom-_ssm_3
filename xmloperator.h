#ifndef XMLOPERATOR_H
#define XMLOPERATOR_H



#include <QObject>
#include <QList>
#include <QString>
#include "ServiceStructures.h"
#include "NetLogicalDevices.h"
#include "tabwidget.h"



class QXmlStreamWriter;
class QXmlStreamReader;
class QStringRef;



class XMLOperator : public QObject
{
    Q_OBJECT


public:
    explicit XMLOperator(QObject *parent = 0);
    static bool getCustomDeviceTypeInterfaces(QList<ServiceStructures::DeviceTypeInterface> &obtained); // true if ok, false - failed
                                                                                                        // Получает пустой список и заполняет его
    static bool saveCustomDeviceTypeInterfaces(QList<ServiceStructures::DeviceTypeInterface> &customDTIs); // true if ok, false - failed

    static bool saveTabWidget(QString type, const QString &fileName, const QRectF &sceneRect,
                              QList<NetDevice *> &deviceList, QList<NetDeviceLink *> &deviceLinkList);

    static TabWidget * getTabWidget(QString* type, const QString &fileName, const QList<ServiceStructures::DeviceTypeInterface> *usersDTI); // возвращает созданный
                                                                                                 // TabWidget с заполненной из XML-ки инфой.
                                                                                                 // Если что-то пошло не так - вернет 0.
                                                                                                 // параметры - имя XML-файла и пользовательские типы DTI


protected:
    static ServiceStructures::DeviceTypeInterface constructDTIFromReadedInfo(const QString &name, const QString &onImagePath, const QString &offImagePath);
    static void writeNetDeviceData(QXmlStreamWriter *streamWriter, NetDevice *deviceToWrite);
    static void writeNetDeviceLinkData(QXmlStreamWriter *streamWriter, NetDeviceLink *linkToWrite);
    static bool getNetDeviceData(QXmlStreamReader *streamReader,
                                 const QList<ServiceStructures::DeviceTypeInterface> *usersDTI, TabWidget *tabWidget);
    static bool readedAttributeToBool(const QStringRef &value, bool &ok); // парсит атрибут с двумя возможными значениями _строк_ -
                                                                // "true" или "false" и переводит в bool
    static bool readNetDeviceData_DTI(QXmlStreamReader *streamReader, NetDevice *device,
                                      const QList<ServiceStructures::DeviceTypeInterface> *usersDTI);
    static bool readNetDeviceData_Pos(QXmlStreamReader *streamReader, NetDevice *device, TabWidget *tabWidget);
    static bool getNetDeviceLinkData(QXmlStreamReader *streamReader, TabWidget *tabWidget);
    static bool readXMLEndElements(QXmlStreamReader *streamReader, const unsigned int &repeatCount = 1,
                                   const bool &skipWhitescpaces = true); // читает repeatCount конечных элементов
                                                                         // использовать для скиппинга элементов
                                                                         // true if все ок, false если прочли не то


};

#endif // XMLOPERATOR_H
