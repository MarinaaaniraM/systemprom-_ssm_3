// Этот файл является частью проекта ПС "Конструктор".
// Время создания: Срд Янв 12 17:04:09 2011
// Реализация класса слежения за входящими ICMP-пакетами

#include "adminping.h"

AdminPing::AdminPing(QObject *parent) :
    QObject(parent)
{}


// Создание и установка соединения
int AdminPing::Create( QHostAddress ip, unsigned short port)
{
  SetClientTimeout( 0);
  inL.Create();
  outL.Create();
  if( CMsgSocket::Create( htonl( ip.toIPv4Address()), htons( port), 0, &inL, &outL) != TYPE_OK)
    return 0;
  if( CMsgSocket::Connect( TRUE) != TYPE_OK)
    return 0;
  return -1;
}


// Установка соединения
int AdminPing::OnConnect()
{
  GHEADER hdr;

  hdr.UnUsed_1 = SECURITY_CONNECT;
  hdr.Size = sizeof(GHEADER);
  hdr.Count = 0;

  MsgWrite( (BYTE*)&hdr);
  return 0;
}


// Прием пакета, обработка
BOOL AdminPing::OnReceive(BYTE* msg)
{
  GHEADER* hdr = (GHEADER*)msg;
  BOOL rval = FALSE;
  if( !hdr)
      return rval;
  switch( hdr->UnUsed_1)
  {
    case SECURITY_CONNECT: rval = TRUE;
               break;   // Подключились, можно изменить свой статус или вывести сообщение
    case SECURITY_REPLY: ProcReply( hdr);
                     rval = TRUE;
    break;
  }
  return rval;
}


//Обработка ответа сервера безопасности
void AdminPing::ProcReply(GHEADER* hdr)
{
  if( !hdr) return;

  if( hdr->Count == 0 || hdr->Size <= MSG_HEADER) return;

  struct timeval tv;
  gettimeofday( &tv, 0); // time of codogramm receive

  for( int i=0; i < hdr->Count; ++i)
  {
    Addr* adr = (Addr*)(hdr+1) + i;

    emit devicePingedSuccesessful(QHostAddress(adr->Object));
//    NodeEvent* ev = new NodeEvent(adr->Object, tv, adr->Abonent);
//    QApplication::postEvent(appWin, ev);
   // DO NOT DELETE NodeEvent - it will be cleared after arriving
  }
}

// Request state of the node ip = QHostAddress(INADDR_ANY)
int AdminPing::StateRequest( QHostAddress ip)
{
  GHEADER hdr;
  if ( ip == QHostAddress(INADDR_ANY))
    hdr.UnUsed_1 = SECURITY_REQUEST_ALL;
  else
    hdr.UnUsed_1 = SECURITY_REQUEST_SRC;
  hdr.Size = sizeof(GHEADER);
  hdr.Count = 0;
  hdr.Src.Object = ip.toIPv4Address();
  hdr.Src.Abonent = 0;
  return MsgWrite( (BYTE*)&hdr);
}

int AdminPing::OnDisconnect(int ErrCode)
{
    ErrCode = ErrCode; // make compiler happy ;)
//    qDebug( QString::number( ErrCode));
    return TYPE_OK;
}
