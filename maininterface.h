#ifndef MAININTERFACE_H
#define MAININTERFACE_H

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include <QToolBar>
#include <QTabWidget>
#include <QDebug>
#include <QRadioButton>
#include "dialogmessager.h"
#include "logdialog.h"
#include "ServiceStructures.h"
#include "tabwidget.h"
#include "netdevicethreading.h"

// ============================================================================
#include "searchdeviceswidget.h"
// ============================================================================



class MainInterface : public QMainWindow
{
    Q_OBJECT


public:
    MainInterface(QWidget *parent = 0);
    void load_customDeviceTypeInterfaces(); // для сплеш-скрина при загрузке сделаем отдельным паблик-методом главного класса
    void load_preDefinedDeviceTypeInterfacesImagesReload(); // аналогично. Т.к. на линуксе картинки не самозагружаются, придется их перезагрузить
    ~MainInterface();


protected:
    bool windowMaximizesOnStart = true;
    bool ignoreTabWidgetContentChanged = false; // пропустить ли первый пришедший tabWidgetContentChanged

    ServiceStructures::ProgramSettings programSettings;

    DialogMessager *dialogMessager;
    LogDialog *logDialog;
    NetDeviceThreading *pingThread;

    QTabWidget *tabWidget;

    QAction *newFileAct;
    QAction *openFileAct;
    QAction *saveFileAct;
    QAction *saveFileAsAct;
    QAction *closeFileAct;
    QAction *printFileAct;
    QAction *closeProgramAct;
    QAction *aboutQtAct;
    QAction *aboutProgramAct;
    QAction *whatsThisAct;
    QAction *showLogAct;
    QAction *clearLogAct;
    QAction *scrollLogOutputAct;
    QAction *saveLogFileBeforeCloseAct;
    QAction *editNetDeviceInterfacesAct;
    QAction *addNewNetDevice;
    QAction *askBeforeDeviceDeletion;
    QAction *limitingNumberOfConnections;
    QAction *timeToRePingAct;
    QAction *timeToWaitForPingReplyModifyerAct;

    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *settingsMenu;
    QMenu *helpMenu;

    QToolBar *fileToolBar;

    // ============================================================================
    SearchDevicesWidget* search;
    QAction* searchNewDevices;
    // ============================================================================

    void createActions();
    void createMenus();
    void createToolbars();
    void settingsRead();
    void settingsWrite();
    void connectComponents();
    void setupComponents();

    bool tabWidgetsHaveUnsavedChanges();
    void enableTabWidgetActions(const bool &enable); // вкл/выкл QAction'ы, которые должны быть вкл только при наличии открытых вкладок
    void saveCurrentTab(TabWidget *currentTab);
    void connectTabWidget(TabWidget *connectingTab);

    void closeEvent(QCloseEvent *event);

protected slots:
    void whatsThisAct_slot();
    void showLogAct_slot();
    void logDialogWasClosed();
    void scrollLogOutputAct_slot();
    void saveLogFileBeforeCloseAct_slot();
    TabWidget *newFileAct_slot(); // возвращает созданный виджет
    void closeFileAct_slot();
    void tabWidgetContentChanged(QWidget *sigSender); // изменили содержимое какой-либо вкладки
    void addNewNetDevice_slot();
    void saveFileAct_slot();
    void saveFileAsAct_slot();
    void askBeforeDeviceDeletion_slot();
    void openFileAct_slot();
    void openFile(const QString &fileName);
    void warnFromDevice_slot(const QString &warnText, const ServiceStructures::LogMessageType &messageType);
    void limitingNumberOfConnections_slot();
    void tabWidgetIndexChanged(const int &newIndex);
    void timeToRePing_slot();
    void timeToWaitForPingReplyModifyer_slot();

    // ============================================================================
    void searchNewDevice_slot();
    void addSearchDevices_slot(QVector<QString>* dev, int type);
//    void changePingType(bool adminType);
    // ============================================================================

signals:
    void setAskBeforeDeviceDeletion(bool value);
    void setTimeToRePing(int value);
    void setTimeToWaitForPingReplyModifyer(int value);


protected:
    enum localDefs
    {
        MAX_THREAD_COUNT = 9999,    // максимальное количество тредов
        DEFAULT_TIME_TO_RE_PING = 5,  // дефолтное значение для timeToRePing (см program settings)
        DEFAULT_TIME_TO_WAIT_FOR_PING_REPLY = 3, // дефолтное значение для timeToWaitForPingReply (см program settings)
        MAX_TIME_TO_RE_PING = 300,    // максимальное кол-во секунд для timeToRePing
        MAX_TIME_TO_WAIT_FOR_PING_REPLY = 30 // максимальное timeToWaitForPingReply
    };

};

#endif // MAININTERFACE_H
