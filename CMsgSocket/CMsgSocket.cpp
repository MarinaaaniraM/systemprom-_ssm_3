//////////////////////////////////////////////////////////////////////////////////
/// CMsgSocket - ����� �������� ������ ��������� ��������� ����� �����
//////////////////////////////////////////////////////////////////////////////////
#include "CMsgSocket.h"

// CRC32 ////////////////////////////////////////////////////////////////////////
#ifdef _WITH_CRC32

unsigned long* crctab = NULL; // ������� ��� �������� CRC
#define CRCPOLY 0xEDB88320 // ������� CRC-32

////////////////////////////////////////////////////////////////////////////////
/// ������������� ������� ��� �������� CRC /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void CRCInit()
{// ������� ������� ������ 1 ���
  if(crctab != NULL)
    return;
// ������� ��� �� �������
  crctab = new unsigned long[256];
// ��������� ������� CRC
  for (int i = 0; i < 256; i++) // ��� ������� �������� T
  {
    crctab[i]=i;
    for (int j = 8; j > 0; j--) // ��� ������� ����
      if(crctab[i] & 1)         // ���� ������� ���
        crctab[i] = (crctab[i]>>1) ^ CRCPOLY;
      else
        crctab[i] >>= 1;
  }
}

////////////////////////////////////////////////////////////////////////////////
/// ������� CRC ��������� buff ������ bufflen  /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
unsigned long CountCRC(char* buff, size_t bufflen)
{
  unsigned long crc = 0xFFFFFFFF;
  while(bufflen--)
    crc = crctab[(crc ^ *buff++) & 0xFF] ^ (crc >> 8);
// ������������� CRC
  crc = ~crc;
  return crc;
}

#endif  // CRC-32

// ������ ����� (����)
#define ACTIVE_MSG_ID 0
#define ACTIVE_MSG_PRT 0
// ���� Packet
#define ACTIVE_MSG_REC 0xFFFF
#define ACTIVE_MSG_REP 0
#define MAX_ACTIVE_DELAYS 3

// �����������
CMsgSocket::CMsgSocket():
rptid(0),
wptid(0),
inList(0),
outList(0),
allowed(INADDR_ANY),
buf(NULL),
bufPos(0),
needReConnect(0),
tout(DEF_CLIENT_TIMEOUT),
is_active(0)
{
	memset( &hdr, 0, MSG_HEADER);
#ifdef _WITH_CRC32
	CRCInit();
#endif
}

// ����������
CMsgSocket::~CMsgSocket()
{

    Close();
}

void CMsgSocket::Close()
{
	allowed = INADDR_NONE; // ���� ���� ������� ����������, ��� ����������
	Disconnect( TYPE_OK); // �������������� ����� ERS_NOCON - by Roman 09-11-05 // EXIT_THREAD - Roman 26-03-10
// ���� ������ ����������, ������� ���� id
	if(wptid)
	{
	   pthread_t w = wptid; // ��������� id ����
	   pthread_kill( wptid, SIGUSR1); // �������� ������
	   sched_yield(); // ����
	   if(!needReConnect) // Roman 09-11-05 ���� isConnected()
	   {
//		  pthread_kill( wptid, SIGUSR1);// //???
		  pthread_join( w, NULL); // ������� ����� ����
		 }
	   else
	   {
   	  pthread_cancel(w); // �������� ����, ������� ���� �� ���������
	   	pthread_join( w, NULL);
     }
   	 wptid = 0;
	}
	sched_yield();
	if(rptid)
	{
	   pthread_cancel(rptid);
	   pthread_join( rptid, NULL); //???
 	   rptid = 0;
	}
}

// �������� �����
void* MainRThread(void* a)
{
  int rval = TYPE_OK;
  CMsgSocket *chn = (CMsgSocket*)a;
  if(chn) {
//    sigset_t blocked;
//    sigemptyset(&blocked);
//    sigaddset(&blocked,SIGINT);
//    pthread_sigmask( SIG_BLOCK, &blocked, NULL);
    rval = chn->MainRCycle();
  } else
    rval = NULL_POINTER;
// Roman. Warning aware 20-03-2013  
  return NULL; //((void*)-&rval);
}

// ������� �����
void* MainWThread(void* a)
{
  int rval = TYPE_OK;
  CMsgSocket *chn = (CMsgSocket*)a;
  if(chn) {
//    sigset_t blocked;
//    sigemptyset(&blocked);
//    sigaddset(&blocked,SIGINT);
//    pthread_sigmask( SIG_BLOCK, &blocked, NULL);
    rval = chn->MainWCycle();
  } else
    rval = NULL_POINTER;
// Roman. Warning aware 20-03-2013  
  return NULL; //((void*)-&rval);
}

/// ���������� ���������� ����������. �������� ����� Create!
/// \param ip - ����� � network order.
void CMsgSocket::SetLocalAdr(UINT ip)
{
  sock.ladr.sin_addr.s_addr = ip;
}

// �������� ����������. ��-��������� - ����������
// ����� � server=1 ������ ������������ �����
// hname ����� ����: "127.0.0.1" ��� "localhost", ���� NULL ������������� INADDR_ANY
// sname: "25" ��� "smtp"
int CMsgSocket::Create(char *hname, char *sname, BYTE prior, CBaseMessageList *inL, CBaseMessageList *outL, BOOL server)
{
  if(!inL /*|| !outL*/ )
    return NULL_POINTER;

  inList = inL;
  outList = outL;

//  int rval;
  return( sock.Create(hname, sname, prior, server ));
}

// ip � port - ����� � network order
int CMsgSocket::Create(UINT ip, PORT port, BYTE prior, CBaseMessageList *inL, CBaseMessageList *outL, BOOL server )
{
  if(!inL /*|| !outL */)
    return NULL_POINTER;

  inList = inL;
  outList = outL;

//  int rval;
  return( sock.Create(ip, port, prior, server ));
}

/////////////////////////////////////////////////////////////////////////////////
// ���������� ����������
// �� ���� ������ ������� ������������� �����
int CMsgSocket::Connect( BYTE reCon, UINT ip )
{
  if(isConnected()){
    return ERS_INUSE;
  }
  needReConnect = reCon;
  allowed = ip;
// ������� �����-����������
  if(pthread_create( &rptid, NULL, &MainRThread, (void*)this )!=0)
    return EXIT_THREAD;
  sched_yield();
  return TYPE_OK;
}

/////////////////////////////////////////////////////////////////////////////////
// ������ ��������� � �����
/////////////////////////////////////////////////////////////////////////////////
int CMsgSocket::MsgWrite(BYTE* msg)
{
// ��� ����������
 if ( !isConnected())
   return ERS_NOCON;
// �������� ���������
 if ( !msg)
   return NULL_POINTER;

// ���� ������ �� ������
 if ( outList)
   return(outList->AddMessage(msg));
// ��� ������ �� ������
 GHEADER* msghead = (GHEADER*)msg;
// �������� �� �������� ���������, �����
 if( msghead->Size < MSG_HEADER || msghead->Size > (sizeof(Addr)*msghead->Count+MAX_TRANSPORT_PACKET_SIZE+MSG_HEADER) )
   return ZERO_SIZE;

#ifdef _WITH_CRC32
// ���������� ������������ ���� � ���� CRC-32
 msghead->UnUsed_2 = CRCPOLY;
 msghead->UnUsed_1 = CountCRC( (char*)&msghead->UnUsed_2, MSG_HEADER-sizeof(ULONG));
#endif
// ������ ������ � �����
 return( sock.Write(msg, msghead->Size) );	// ������ ������, ���� ��� ������
}

// ��������� ����������
int CMsgSocket::Disconnect( int ErrCode )
{// �� ���� WaitForEvent � ������ ������ ������ ������� ������, ����� ���� ����� ����������
	int rval;
// ���� ������ ������������� - ������� �����������. �� ��������� ���� ���� �� ���������
  if(ErrCode==TYPE_OK) // Roman 30/01/2008
    needReConnect = 0;
	is_active = 0;
// ������ TYPE_OK ��� ������ ������ (�������������� ������)
	rval = sock.Disconnect();

//	printf("sock return %d\n",rval);
//	if(buf && inList){
//	    inList->FreeMessage( buf);
//	    buf=0;
//	    bufPos=0;
//	}

// ��� ������� ������������� ���� ����������� ���������� �������
	if(ErrCode==TYPE_OK){ // Roman 30/01/2008
	    allowed = INADDR_NONE;
      sched_yield();
  }
//  printf("==> #1\n");
// � �������� �������� ������� ������ ��� TYPE_OK
	if(rval==TYPE_OK) // ������ ���� ������������� ������
	{
	  OnDisconnect( ErrCode);
//  printf("==> #2\n");
	  
	}
//  printf("==> #3\n");

// ���� ����� ��� ��� ����������, �� � rval ����� ������
	return rval;
}

// ������ ����������
int CMsgSocket::isConnected()	// ������ ���������� 1=����
{
	return sock.isConnected();
}

/////////////////////////////////////////////////////////////////////////////////
// �������� ���� ������ ��������� ��������� �� ������
/////////////////////////////////////////////////////////////////////////////////
int CMsgSocket::MainRCycle()
{
  int rval = TYPE_OK;
  do
  {// �����������
    if ( !isConnected() )
    {// ���� ��������� ����������, �� sock.Connect ��� ���������������� �����
     // ���� ����� ������� "������" � ��� ������ ������� ����������
      rval = sock.Connect(allowed); // allowed=INADDR_ANY
//      if(needReConnect) // ���� ��������� ���������� ������������� �����
        while( needReConnect && rval!=TYPE_OK) // ���� ��� �� �����������
        {
//          OnDisconnect(rval); ��� ��� ������ �������
          sock.Disconnect();	// ������������ ������� �����
          if(!sock.isServer()) {	// ���� ������- �������� �������
  	      //  sleep( (tout>0)?(tout):(DEF_CLIENT_TIMEOUT));
  	        fd_set rmask, wmask, emask;
  	        struct timeval tv;
            tv.tv_sec = (tout>0)?(tout):(DEF_CLIENT_TIMEOUT);
            tv.tv_usec = 0;
  	        FD_ZERO(&rmask); FD_ZERO(&wmask); FD_ZERO(&emask);
  	        select( 0, &rmask, &wmask, &emask, &tv);
  	      }
          rval = sock.Connect(allowed); // allowed=INADDR_ANY
        }
//      else
      if( rval!=TYPE_OK)	// ��������� ������� ��� ����� �� ������
      {
      	continue;	// ����� �� while( Disconnect... � ������ �� �����
      }
    }
    // ������� ����� �� ������ ������ ���� ��������� ������ �� ������� �� ��������!
    if(outList)
      if(pthread_create( &wptid, NULL, &MainWThread, (void*)this )!=0)
      {
        rval = EXIT_THREAD;
        continue;	// ����� �� while( Disconnect...
      }
    // ����������� ������� - ��������� � ����������
    OnConnect();
    is_active=0;
    // ����� ���������� ��� ����� ����, ���� ������� �� ������
    // ����� ������� ��������� ������� � ����� 1 ��� ���������� ������� w
    // Roman 27-07-2007 �������� ������� ����� ���������� ������� ����������
    while( 1)
    {
      BYTE r=1, w=0, e=1; // ����� �������������� �������; �� ������ - ��������� ����
      if(tout>0)
      {
        struct timeval tv;
        tv.tv_sec = tout;
        tv.tv_usec = 0;
        rval=sock.WaitForEvent(r, w, e, &tv); // ����� ����� �� ����-����
      } else
        rval=sock.WaitForEvent(r, w, e, 0);   // ����� ������ �� ����� ��� ������

// ������ ��������: ��� ��������<MAX_ACTIVE_DELAYS ������� ���� ���������� � ��� ��������� ����������
      if(rval == ERS_TIMEOUT && is_active < MAX_ACTIVE_DELAYS)
      {
        rval = ActiveMsg( ACTIVE_MSG_REC);  // ������
        is_active++;
        continue;
      }
// ������ ��������: ���� ������� >= MAX_ACTIVE_DELAYS ��� �� ����������� ��������� ��� ��. ������
// - ����� �� ����� ������ � ��������
      if(rval == ERS_INUSE) //{        printf("SIGNAL !\n");
        continue;
      if(rval != TYPE_OK) //        printf("TIMEOUT !\n");
        break;

// ���� ���������� �� ������
    	if(r)
    	{
    		ULONG rd;
    		BYTE* writeHere;
    		is_active = 0; // ������� �����! �Ѩ ��
// ���� ��������� ��� ������!
    	  	if( bufPos >= MSG_HEADER)
    	  	{
// ����� ������������� ������ ���� ��� ������� buf (���������� ����� �� ��������� ���������)
// ���������� ������� �������� ������� (rd)
    	  	  rd = hdr.Size - bufPos;
// ���������� ���� � ���������� ����� ������ c ������ ��� ��������� ����
		        writeHere = buf + bufPos;
    	  	}
    	  	else
    	  	{
// ���� ��������� ��� �� ������ - ������������ rd
    	  	  rd = MSG_HEADER - bufPos;
// ���������� ���� � ����� ��������� c ������ ��� ��������� ����
		        writeHere = (BYTE*)&hdr + bufPos;
    	  	}

// ���� ���� ��� ���-�� ������ (�� ���� ������� �� ����� ����)
    		if(rd>0)
    		{ // ������ (� rd ����� ������� ������� ������)
      	  	  rval = sock.Read( writeHere, rd);
      	  	  if ( rval != TYPE_OK ) // ������ ������ ��� ���������� � ������
      	  	  {    // ����� ������������� � Disconnect()
		    if( rval == ERS_AGAIN)
		      continue;
		    else
      	  	      break;
      	  	  }
      	  	  if ( rd == 0 ) // ������ �� ������� ��� ���������� �� ������
      	  	  {// ����� ������������� � Disconnect()
      	  	    rval = ERS_NOCON; // ���������� ���������
      	  	    break;
      	  	  }
      	 }

// ��� ��� �������� ���-�� ������, ��� ����� ��� ��� �������� (�� ������ ����!)
      	 if(rd >= 0)
      	 {
// �������� ����� �������� ����
    		  bufPos += rd;
// ��������, ��� ������ ���������
    		  if( bufPos >= MSG_HEADER)
    		  {// ��������, ������� �� ���-�� ������ ��������� (������� �� ��� �����)
    		    if(!buf)
    		    { // printf("NEW HEADER REC! SIZE=%d\n", hdr.Size);
#ifdef _WITH_CRC32
// ������ �� ���������� �������
              if(hdr.UnUsed_2!=CRCPOLY || hdr.UnUsed_1!=CountCRC((char*)&hdr.UnUsed_2, MSG_HEADER-sizeof(ULONG)) )
              {
                rval = ERROR_PACKET;
                break;
              }
    		    	// �������� ����������� ����
              hdr.UnUsed_1 = 0, hdr.UnUsed_2 = 0;
#endif
// ������ �� ������������ ������� (� ���� ����� Size)     // Roman! 25-01-2006
// ���� ������ ����� � ������������ ����� Size
      	  	  if( hdr.Size > (sizeof(Addr)*hdr.Count + MAX_TRANSPORT_PACKET_SIZE + MSG_HEADER) ||
      	  	      hdr.Size < (MSG_HEADER+sizeof(Addr)*hdr.Count) )
      	  	  {// ������! ����� �� ���������� / ������������� � Disconnect()/
      	  	    rval = ERROR_SIZE;
    //  	  	    printf("INVALID SIZE=%d bufPos=%d rd=%d buf=%d\n",hdr.Size,bufPos,rd,buf);
      	  	    break;
      	  	  }
// �������� ������� ���������� Roman 27-07-2007
              if( IsActiveMsg(&hdr))
              { // ����� ���������� - �� �������� � ���������
                bufPos = 0; // ���� ���������� ����������� ��� ������ ����� ����
                if(hdr.Packet == ACTIVE_MSG_REC)
                  ActiveMsg( ACTIVE_MSG_REP);  // �����
              } else
              { // �������� ����� ��� ���� ����� ����� � ������������ � ���� �������� �����
    		    	  if( !( buf = inList->GetFreeBuffer( hdr.Size)))
    		    	  { // printf("ERROR ALLOCATE BUFFER\n");
    		    	    rval = NULL_POINTER;
    		    	    break; // continue; // ������ �� ���������
    		    	  }
    		    	  memcpy( buf, &hdr, MSG_HEADER);
    		    	}
    		    }

    		    // ������ ��� ���������??? (������ ������ ��� ����� ���������!)
    		    if( bufPos>=hdr.Size)
    		    {// ������� ��� ���������
    		      // printf("ADDMESSAGE\n");
    		      if( OnReceive(buf)!=true) // ���� ������ �� ����������
    		        inList->AddMessage(buf, 1); // ������ ��������� � �������

    		      buf = NULL; // �� ������� ����������
    		      bufPos = 0; // ���� � ���������� ���������
      	  	 } // if( bufPos>=hdr.Size)
      	  	} // if( bufPos >= MSG_HEADER)
      	   } // if(rd >= 0) // ���-�� �������
      	  } // if(r) // ���������� �� ������

      	if(e)
      	{// ������ ������
      	  rval = NULL_POINTER;
      	  break;
      	}
// ������� ����� ��������� �������
//      	r=1, w=0, e=1; � ������ �����!
    }//while sock.WaitForEvent

  	// ��������� ������
  	if(buf && inList){
	    inList->FreeMessage( buf);
	    buf=0;
	}
	bufPos=0; // Roman clear it in all case! 10-12-2010


    // ������� ������� ����
    if(wptid)
    {
      pthread_kill( wptid, SIGUSR1);
      wptid=0;
    }

// ���� Disconnect ������ �� ������ ��� (�.�. �� ��� ������ �� ����� ���������� ��� ������� ����������)
// �� ��������� ����� ������ ������ || ����� ������� ������ �� ������������� ����������
  } while( Disconnect( rval) == TYPE_OK && needReConnect );
  rptid = 0; // indicate completion of the thread
  return rval;
}

// �������� ���� �� �������
void ProcUsr1(int signo)
{
	pthread_exit(NULL);
}
/////////////////////////////////////////////////////////////////////////////////
// ��������������� ���� ������ ��������� ��������� �� ������
// ����������� ������ ��� ������ �� ������� �� ������ ///////////////////////////
int CMsgSocket::MainWCycle()
{
  ULONG cnt = 0;
  // �� ���� ��� ������� ������� ������ ��������� �� WaitForEvent
  // � ��� ���� isConnected ��� ������ ���� 0
  signal(SIGUSR1, ProcUsr1);
  while(isConnected()){
// �.�. ������ ���������� �������� �� ������ - ��� ��������� ����������
// ������� �������� ������� ������ �� ������
    if(outList->GetCount(&cnt) != TYPE_OK )
	    break;
	  if( cnt>0 )
	  {
	    BYTE* msg = NULL;
	    int rval = outList->GetMessage(&msg); // �������� ���������
    	if ( rval == TYPE_OK && msg)
    	{//������ ������� �� ������ �� ������ �� ����������
	      GHEADER* head = (GHEADER*) msg; // ���������
	      BYTE r=0, e=0, w=1;
    	  // � ��� ���� ������ ��� ��������
    	  // ���� ���������� ������
    	  // ������� ����� ���� ������� � ��������� select �� �������
    	  while( ( rval = sock.WaitForEvent(r, w, e, 0)) == ERS_INUSE);

    	  // ������ ������ ������ - ������ ����������, �����
    	  if ( rval !=TYPE_OK )
    	  {
    	    delete[] msg;
    	    break;
    	  }

#ifdef _WITH_CRC32
	      // ���������� ������������ ���� � ���� CRC-32
        head->UnUsed_2 = CRCPOLY;
        head->UnUsed_1 = CountCRC( (char*)&head->UnUsed_2, MSG_HEADER-sizeof(ULONG));
#endif
    	  // ����� w ����� =1, ����� ���� �� �� TYPE_OK
    	  rval = sock.Write(msg, head->Size ); // ��������
    	  //delete msg;
    	  delete[] msg; // outList->FreeMessage(msg);
    	  //txReady = 0; ������ �� �����
    	  if ( rval !=TYPE_OK )
    	    break;// ������ ������ ��� ���������� � ������ - ������ ����������, �����
    	}
// ����� �� ����� �������� ��������� ����� ������?
//    	if(outList->GetCount(&cnt) != TYPE_OK )
//	      break;
	  } else // ��� ������� ������
	    outList->WaitForEvent(DATA_MESSAGE_LIST,1);

  } // while connected
  wptid = 0; // indicate completion of the thread
  return TYPE_OK;
}

UINT CMsgSocket::SetClientTimeout(UINT sec)
{
  UINT old_tout = tout;
  tout = sec;

  return( old_tout);
}

///////////////////////////////////////////////////////////////////////////////
// ����� ���������� (���: ACTIVE_MSG_REC/ACTIVE_MSG_REP) ////////////////////////
///////////////////////////////////////////////////////////////////////////////
int CMsgSocket::ActiveMsg( WORD type)
{
  GHEADER AMsg;
  AMsg.UnUsed_1=0;       // ���������������
  AMsg.UnUsed_2=0;       // ���������������
  AMsg.ID=ACTIVE_MSG_ID;    // ID ������
  AMsg.Size = MSG_HEADER;	// ����� ������
  AMsg.Src.Object = 0, AMsg.Src.Abonent=0;   // ����� ���������
  AMsg.Protocol = ACTIVE_MSG_PRT; // ��� ���������
  AMsg.Packet = type;	  // ��� ������
  AMsg.Rezerv = 0;        // ���������������
  AMsg.Priority = 1;	    // ���������
  AMsg.Count = 0;	        // ����� ���������

  return MsgWrite( (BYTE*)&AMsg);
}

///////////////////////////////////////////////////////////////////////////////
// ����� ����������                            ////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
int CMsgSocket::IsActiveMsg(GHEADER* Msg)
{
  if(!Msg)
    return false;
  if( Msg->UnUsed_1!=0)       // ���������������
    return false;
  if( Msg->UnUsed_2!=0)       // ���������������
    return false;
  if( Msg->ID!=ACTIVE_MSG_ID)    // ID ������
    return false;
  if( Msg->Size != MSG_HEADER)	// ����� ������
    return false;
  if( Msg->Src.Object != 0 || Msg->Src.Abonent!=0)  // ����� ���������
    return false;
  if( Msg->Protocol != ACTIVE_MSG_PRT) // ��� ���������
    return false;
  if( Msg->Packet != ACTIVE_MSG_REC && Msg->Packet != ACTIVE_MSG_REP)  // ��� ������
    return false;
  if( Msg->Rezerv != 0)     // ���������������
    return false;
  if( Msg->Priority != 1)	    // ���������
    return false;
  if( Msg->Count != 0)	        // ����� ���������
    return false;

  return true;
}

#pragma off (unreferenced);

int CMsgSocket::OnReady(void)
{
 return(TYPE_OK);
}
BOOL CMsgSocket::OnReceive(BYTE* msg)
{
 return(FALSE);
}
int CMsgSocket::OnConnect(void)
{
 return(TYPE_OK);
}
int CMsgSocket::OnDisconnect(int ErrCode)
{
 return(TYPE_OK);
}

#pragma on (unreferenced);
