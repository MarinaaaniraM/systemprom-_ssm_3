///////////////////////////////////////////////////////////////////////
// CClientCommServer - ��������� �������� �������������� � �������� �����, author:Sergey Nikulin
// ClientCommServer.cpp: implementation of the CClientCommServer class.
///////////////////////////////////////////////////////////////////////

#include "BaseTypes.h"
#include "CClientCommServer.h"
#include "CBaseMessageList.h"

// Potok na chtenie iz ocheredi
void* Handler(void* Ptr)
{
	CClientCommServer*	Parent = (CClientCommServer*)Ptr;
	int                     RetCode = TYPE_OK;
	BYTE*                   Buf = NULL;


	if(Parent == NULL)
	{
		Parent->m_ThreadIdHandler = 0;
		return NULL;
	}
	if(Parent->m_InputList == NULL)
	{
		Parent->m_ThreadIdHandler = 0;
		return NULL;
	}


	for(;;)
	{
		Parent->m_InputList->WaitForEvent(PRIORITY_MESSAGE_LIST);
		while((RetCode = Parent->m_InputList->GetMessage(&Buf)) == TYPE_OK)
		{
		    if(Buf != NULL)
			{// esli ne pustoe to razbiraem
                            Parent->MessageHandlers(Buf);
			}
		}

	} // of for(;;)

	return NULL;
}



// Potok na chtenie iz ocheredi
void* HandlerData(void* Ptr)
{
	CClientCommServer*	    Parent = (CClientCommServer*)Ptr;
	int                         RetCode = TYPE_OK;
	BYTE*                       Buf = NULL;


	if(Parent == NULL)
	{
		Parent->m_ThreadIdHandlerData = 0;
		return NULL;
	}
	if(Parent->m_InputList == NULL)
	{
		Parent->m_ThreadIdHandlerData = 0;
		return NULL;
	}


	for(;;)
	{
		Parent->WaitForEvent();
		while((RetCode = Parent->GetMessage(&Buf)) == TYPE_OK)
		{
		    if(Buf != NULL)
			{// esli ne pustoe to razbiraem
                           Parent->OnReceivePacket(Buf);
                           delete[] Buf;
			}
		}

	} // of for(;;)

	return NULL;
}

/// �����������.
/// �������� �������� ��������
CClientCommServer::CClientCommServer()
{
	m_InputList = NULL;
	m_OutputList = NULL;
	m_ThreadIdHandler = 0;
	m_ThreadIdHandlerData = 0;
        m_FlagData = TRUE;
        m_FlagCommand = TRUE;
        m_Connect = FALSE;
}

/// ����������.
/// ��� ������������� ��������� ����������
CClientCommServer::~CClientCommServer()
{
	Clear();
}

/// ������ ������ � �������� �����.
/// \param IP_Server ����� ������� ����� (� ���� ������)
/// \param ServerPort ���������� ���� ������� ����� (� ���� ������)
/// \param ID_Abonent ������������� �������� ��� �������������� �� ������� �����
/// \param Flag ������� ����� ��������� ��������� ����������
int CClientCommServer::Initialization(char* IP_Server, WORD ServerPort,  ULONG ID_Abonent, BYTE Flag)
{
	in_addr    Sadr;

	if(IP_Server == NULL)
		return NULL_POINTER;

	if(strlen(IP_Server) < 7)
		return ERROR_SIZE;

	if((Sadr.s_addr = inet_addr(IP_Server)) == -1)
		return ERROR_SIZE;

	return Initialization(Sadr.s_addr, ServerPort, ID_Abonent, Flag);

}

int CClientCommServer::Initialization(ULONG IP_Server, WORD ServerPort,  ULONG ID_Abonent, BYTE Flag)
{
	int  RetCode = TYPE_OK;

	if(ID_Abonent == 0)
		return NULL_POINTER;


	m_Address.Abonent = ID_Abonent;
	m_Address.Object = 0;

    if(Flag & HANDLER_COMMAND)
       m_FlagCommand = TRUE;
    else
       m_FlagCommand = FALSE;

    if(Flag & HANDLER_DATA)
       m_FlagData = TRUE;
    else
       m_FlagData = FALSE;



	// ������ ������� �������� �������
    if((m_InputList = new CPriorityMessageList) == NULL)
		return EMPTY_MEMORY;

	if((RetCode = m_InputList->Create(TM_CLIENT_PROTOKOL_TYPE)) != TYPE_OK)
	{
		Clear();
		return RetCode;
	}

	// ������ ������� ��������� �������
	if((m_OutputList = new CMessageList) == NULL)
	{
		Clear();
		return EMPTY_MEMORY;
	}

	if((RetCode = m_OutputList->Create()) != TYPE_OK)
	{
		Clear();
		return RetCode;
	}

	// ������ �����
  if((RetCode = Create(IP_Server, ServerPort, 0, m_InputList, m_OutputList, FALSE)) != TYPE_OK)
	{
		Clear();
	  // printf("Error...\n");
		return RetCode;
	}


  if(m_FlagCommand)
	    pthread_create( &m_ThreadIdHandler, NULL, &Handler, (void*)this );


  if(m_FlagData)
	    pthread_create( &m_ThreadIdHandlerData, NULL, &HandlerData, (void*)this );

	if(Flag & CONNECT_SERVER)
	{
	   Connect(Flag & RECONNECT_SERVER);
	  // printf("Connect to server...\n");
	}

	return TYPE_OK;
}

int CClientCommServer::Start(BOOL Recon )
{
	return Connect(Recon);
}



int CClientCommServer::OnConnect()
{
  return MSG_GetConnect();
}

int CClientCommServer::OnDisconnect(int ErrCode)
{
	return TYPE_OK;
}

void CClientCommServer::OnConnectServer()
{
}

void CClientCommServer::OnServerDisConnect()
{
}

void CClientCommServer::OnServerStop()
{
}

void CClientCommServer::OnQueueSize()
{
}

void	CClientCommServer::OnSendLock(ULONG Code , in_addr IpNet)
{
}

void	CClientCommServer::OnSendUnLock(ULONG Code, in_addr IpNet )
{
}

void	CClientCommServer::OnSendError(ULONG IdPacket, int ErrCode, in_addr addr)
{
}

void	CClientCommServer::OnParameters(ULONG Count, LINE_INFO* ChannelInfo)
{

}

void CClientCommServer::Clear()
{
    //printf("**#1**\n");
//       if(m_Connect)
//       {
//          MSG_DisConnect();
//          sleep(3);
//        }
//    printf("**#2**\n");

        CMsgSocket::Disconnect();  //Roman. Was Close()
//    printf("**#3**\n");

	if(m_ThreadIdHandler != 0)
	{
		pthread_cancel(m_ThreadIdHandler);
//		pthread_join(m_ThreadIdHandler, NULL);
	}

//    printf("**#4**\n");

	if(m_ThreadIdHandlerData != 0)
	{
		pthread_cancel(m_ThreadIdHandlerData);
//		pthread_join(m_ThreadIdHandlerData, NULL);
	}
//    printf("**#5**\n");

	if(m_InputList != NULL)
	{
		delete m_InputList;
		m_InputList = NULL;
	}
//    printf("**#6**\n");

	if(m_OutputList != NULL)
	{
		delete m_OutputList;
		m_OutputList = NULL;
	}
//    printf("**#7**\n");

}

void CClientCommServer::WaitForEvent()
{

	m_InputList->WaitForEvent(DATA_MESSAGE_LIST);

	return;
}

ULONG CClientCommServer::GetCount()
{
	ULONG Count;
	int RetCode = TYPE_OK;

	Count = DATA_MESSAGE_LIST;

	RetCode = m_InputList->GetCount(&Count);
	if(RetCode == TYPE_OK)
	  return Count;
	else
	  return 0;
}


int CClientCommServer::GetMessage(BYTE **Msg)
{
	int RetCode = TYPE_OK;

	if(Msg == NULL)
		return NULL_POINTER;

	RetCode = m_InputList->GetDataMessage(Msg);

	return RetCode;
}

int CClientCommServer::SendMessage(BYTE* Msg, BOOL Flag)
{
	int RetCode = TYPE_OK;

  if(!m_Connect )
  {

        return ERS_NOCON;
	}
  else
  {
 	return  m_OutputList->AddMessage(Msg, Flag);
	}
}



int CClientCommServer::SendMessageEx(PACKET *Packet)
{
	int        RetCode = TYPE_OK;
	BYTE      *Paket = NULL;
	ULONG      Size  = 0;
	GHEADER   *Header = NULL;
	Addr      *addr = NULL;
        in_addr   adr;

	if(Packet == NULL)
    	     return NULL_POINTER;
	if((Packet->m_Abonent == NULL) ||
	   (Packet->m_Header.Count == 0) ||
	   (Packet->m_Header.Size  == 0) )
    	        return NULL_POINTER;


        if(!m_Connect )
            return ERS_NOCON;

	Size = sizeof(GHEADER) + Packet->m_Header.Size + (Packet->m_Header.Count * sizeof(Addr));

	if((Paket = (BYTE*) new BYTE[Size]) == NULL)
    	    return NULL_POINTER;

	Header = (GHEADER*)Paket;
	adr.s_addr = m_Address.Object;

	Header->UnUsed_1    = Packet->m_Header.UnUsed_1;
	Header->UnUsed_2    = Packet->m_Header.UnUsed_2;
        Header->ID          = Packet->m_Header.ID;
        Header->Size        = Size;
        Header->Src.Abonent = m_Address.Abonent;
        Header->Src.Object  = m_Address.Object;
        Header->Protocol    = Packet->m_Header.Protocol;
        Header->Packet      = Packet->m_Header.Packet;
        Header->Rezerv      = Packet->m_Header.Rezerv;
        Header->Priority    = Packet->m_Header.Priority;
        Header->Count       = Packet->m_Header.Count;

	addr = (Addr*)( Paket + sizeof(GHEADER) );
	memcpy((void*)addr, (void*)Packet->m_Abonent,(Packet->m_Header.Count * sizeof(Addr)));

	Paket = Paket + sizeof(GHEADER) + (Packet->m_Header.Count * sizeof(Addr));

	if(Packet->m_Buf != NULL)
	    memcpy((void*)Paket, (void*)Packet->m_Buf, Packet->m_Header.Size );

 	if((RetCode = m_OutputList->AddMessage((BYTE*)Header, TRUE)) != TYPE_OK)
		delete[] Paket;

	return RetCode;
}




BOOL CClientCommServer::OnReceive(BYTE* msg)
{
	GHEADER*	Header = (GHEADER*)msg;

	if(!m_FlagCommand)
        {
	       if(Header->Protocol != TM_CLIENT_PROTOKOL_TYPE)
		    return FALSE;
                else
                {
                       MessageHandlers(msg);
					   delete[] msg;
					   msg = NULL;
                       return TRUE;
                }
	}

	return FALSE;

}




void CClientCommServer::OnReceivePacket(BYTE* msg)
{
}





void CClientCommServer::MessageHandlers(BYTE* Packet)
{
	GHEADER*    Header = (GHEADER*)Packet;
	in_addr     addr;

	if(Header == NULL)
		return ;

	if(Header->Protocol != TM_CLIENT_PROTOKOL_TYPE)
		 return ;
	// ��� �����
	switch(Header->Packet)
	{
	case TM_CONNECTION_OK:
		{
                        m_Connect = TRUE;
			OnConnectServer();
			break;
		}
	case TM_CLOSE_CONNECTION_OK:
		{
                        m_Connect = FALSE;
	//	        printf("Receive CloseConnect from server...\n");
			OnServerDisConnect();
			break;
		}
	case TM_QUEUE_SIZE:
		{
			OnQueueSize();
			break;
		}
	case TM_SEND_LOCK:
		{
                        addr.s_addr = Header->Src.Object;
			OnSendLock(Header->Src.Abonent, addr);
			break;
		}
	case TM_SEND_UNLOCK:
		{
                        addr.s_addr = Header->Src.Object;
			OnSendUnLock(Header->Src.Abonent, addr);
			break;
		}
	case TM_ERROR_CODE:
		{
                        addr.s_addr = Header->Src.Object;
			OnSendError(Header->ID, (int)Header->Rezerv, addr);
			break;
		}
	case TM_PARAMETERS:
		{
		        if(Header->Count == 0)
			    OnParameters(Header->Count, NULL);
			else    
			{
                          LINE_INFO* ChannelInfo = (LINE_INFO*)(Packet + sizeof(GHEADER));
			  OnParameters(Header->Count, ChannelInfo);
			}
			break;
		}
	case TM_CLOSE:
		{
                        m_Connect = FALSE;
			OnServerStop();
			break;
		}
	default:
	  { return ;}
	}

	return;


}

int CClientCommServer::MSG_GetConnect()
{
    int        RetCode = TYPE_OK;
	GHEADER*   Header = NULL;

	if((Header = new GHEADER) == NULL)
	{
		return EMPTY_MEMORY;
	}

	Header->UnUsed_1    = 0;                       // ���������������
	Header->UnUsed_2    = 0;                       // ���������������
        Header->ID          = 0;	               // ID ������
        Header->Size        = sizeof(GHEADER);         // ����� ������
        Header->Src.Abonent = m_Address.Abonent;              // ����� ���������
        Header->Src.Object  = m_Address.Object;                  // ����� ���������
        Header->Protocol    = TM_CLIENT_PROTOKOL_TYPE; // ��� ���������
        Header->Packet      = CLIENT_GET_CONNECTION;        // ��� ������
        Header->Rezerv      = 0;     	               // ���������������
        Header->Priority    = 3;	               // ���������
        Header->Count       = 0;		       // ����� ���������


	RetCode =  m_OutputList->AddMessage((BYTE*)Header, TRUE);

        return RetCode;
}


int CClientCommServer::MSG_DisConnect()
{
        int        RetCode = TYPE_OK;
	GHEADER*   Header = NULL;

	if((Header = new GHEADER) == NULL)
	{
		return EMPTY_MEMORY;
	}

	Header->UnUsed_1    = 0;                       // ���������������
	Header->UnUsed_2    = 0;                       // ���������������
        Header->ID          = 0;	               // ID ������
        Header->Size        = sizeof(GHEADER);         // ����� ������
        Header->Src.Abonent = m_Address.Abonent;              // ����� ���������
        Header->Src.Object  = m_Address.Object;                  // ����� ���������
        Header->Protocol    = TM_CLIENT_PROTOKOL_TYPE; // ��� ���������
        Header->Packet      = CLIENT_CLOSE_CONNECTION;        // ��� ������
        Header->Rezerv      = 0;     	               // ���������������
        Header->Priority    = 3;	               // ���������
        Header->Count       = 0;		       // ����� ���������


	RetCode =  m_OutputList->AddMessage((BYTE*)Header, TRUE);

//	printf("Send DisConnect to server...\n");
//        sleep(1);
        return RetCode;
}

int CClientCommServer::GetParameters()
{
  return  MSG_GetParameters();
}


int CClientCommServer::DisConnect()
{
  return  MSG_DisConnect();
}


int CClientCommServer::SetLogFile(WORD TypeProtocol)
{
	return MSG_SetLogFile(TypeProtocol);
}


int CClientCommServer::MSG_SetLogFile(WORD TypeProtocol)
{
    int        RetCode = TYPE_OK;
	GHEADER*   Header = NULL;

	if((Header = new GHEADER) == NULL)
	{
		return EMPTY_MEMORY;
	}

	    Header->UnUsed_1    = 0;                       // ���������������
	    Header->UnUsed_2    = 0;                       // ���������������
        Header->ID          = 0;	               // ID ������
        Header->Size        = sizeof(GHEADER);         // ����� ������
        Header->Src.Abonent = m_Address.Abonent;              // ����� ���������
        Header->Src.Object  = m_Address.Object;                  // ����� ���������
        Header->Protocol    = TM_CLIENT_PROTOKOL_TYPE; // ��� ���������
        Header->Packet      = CLIENT_SET_LOGFILE;        // ��� ������
        Header->Rezerv      = 0;     	               // ���������������
        Header->Priority    = 3;	               // ���������
        Header->Count       = TypeProtocol;		       // ����� ���������


	RetCode =  m_OutputList->AddMessage((BYTE*)Header, TRUE);

        return RetCode;
}



int CClientCommServer::MSG_GetParameters()
{
    int        RetCode = TYPE_OK;
	GHEADER*   Header = NULL;

	if((Header = new GHEADER) == NULL)
	{
		return EMPTY_MEMORY;
	}

	Header->UnUsed_1    = 0;                       // ���������������
	Header->UnUsed_2    = 0;                       // ���������������
        Header->ID          = 0;	               // ID ������
        Header->Size        = sizeof(GHEADER);         // ����� ������
        Header->Src.Abonent = m_Address.Abonent;              // ����� ���������
        Header->Src.Object  = m_Address.Object;                  // ����� ���������
        Header->Protocol    = TM_CLIENT_PROTOKOL_TYPE; // ��� ���������
        Header->Packet      = CLIENT_GET_PARAMETERS;        // ��� ������
        Header->Rezerv      = 0;     	               // ���������������
        Header->Priority    = 3;	               // ���������
        Header->Count       = 0;		       // ����� ���������


	RetCode =  m_OutputList->AddMessage((BYTE*)Header, TRUE);

        return RetCode;
}





