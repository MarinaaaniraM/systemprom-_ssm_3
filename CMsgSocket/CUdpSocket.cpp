/////////////////////////////////////////////////////////////////////////////////
// ����� CUdpSocket - ������� ������ � ������� ����������� �� udp/ip
/////////////////////////////////////////////////////////////////////////////////
#include "CUdpSocket.h"

/////////////////////////////////////////////////////////////////////////////////
// �����������
/////////////////////////////////////////////////////////////////////////////////
CUdpSocket::CUdpSocket():
  s(-1),
  con(0),
  srv(0),
  ip_tos(0),
  rxBytes(0),
  txBytes(0)
  {
  ladr.sin_addr.s_addr = INADDR_ANY;
  ladr.sin_port = 0;
  ladr.sin_family = AF_INET;
  radr.sin_addr.s_addr = INADDR_ANY;
  radr.sin_port = 0;
  radr.sin_family = AF_INET;
  }

/////////////////////////////////////////////////////////////////////////////////
// ����������
/////////////////////////////////////////////////////////////////////////////////
CUdpSocket::~CUdpSocket()
{
	if(s!=-1)
	{
	  Disconnect();
	  close(s);
	  s=-1;
	}
}

/////////////////////////////////////////////////////////////////////////////////
// ������ ����������
/////////////////////////////////////////////////////////////////////////////////
int CUdpSocket::Disconnect()
{
// ���� ���������� ��� ���
	if(s<0 )
	  return ERS_NOCON;

	  shutdown(s,1);
    close(s);
	  s = -1;
    con = 0;
	return TYPE_OK;
}

/////////////////////////////////////////////////////////////////////////////////
// ����������� ���������� ������
/////////////////////////////////////////////////////////////////////////////////
int CUdpSocket::Create(char* hname, char *sname, BYTE prior, int server )
{
  int rval;
  struct sockaddr_in iadr;
// ������ ������ � �����
  rval = set_address( hname, sname, &iadr, "udp");
  if(rval != TYPE_OK )
    return rval;

  return Create(iadr.sin_addr.s_addr, iadr.sin_port, prior, server );
}

/////////////////////////////////////////////////////////////////////////////////
// Function description
/////////////////////////////////////////////////////////////////////////////////
int CUdpSocket::Create(UINT ip, PORT port, BYTE prior, BOOL server )
{
  int rval;
  srv = server;
// ����������� �������������� ������
  s = socket( AF_INET, SOCK_DGRAM, 0);
  if( s<0 ) {
    switch(errno) { // ��� �� ������?
    	case EACCES : rval = ERS_ACCESS; break; // ��� ���� �� ��������
    	case EPROTONOSUPPORT : rval = ERS_NOPROT; break; // ����������v� ��������
    	default: rval = EMPTY_MEMORY; // ��� ������ - ��-�� ���������� �������� ������v
    }
    return rval;
  }

  rxBytes=0, txBytes=0;
// ���� ������, ������������ ��������� �����
  if(server) { // ���������� ���������� ����� ��� ���������
    ladr.sin_addr.s_addr = ip;
    ladr.sin_port = port;
  } else { // ���������� ��������� ����� �� ��� ������, ���� ����� ��� ����� ������������� ��� ����������
    ladr.sin_addr.s_addr = INADDR_ANY;
    ladr.sin_port = 0;
  }
  ladr.sin_family = AF_INET;
// �������� ����� ��� ������� �������
    if( bind(s, ( struct sockaddr * )&ladr, sizeof(ladr)) ){
      switch(errno) { // ��� �� ������?
      	case EADDRINUSE :
      	case EINVAL : rval = ERS_INUSE; break;
      	case EFAULT :
      	case EADDRNOTAVAIL : rval = ERS_NOHOST; break;
      	case EACCES : rval = ERS_ACCESS; break;
      	default: rval = EMPTY_MEMORY;
      }
      return rval;
    }
  if( !server) { // client
// ���������� ���������� ����� ��� ���������
  	radr.sin_addr.s_addr = ip;
	  radr.sin_port = port;
	  radr.sin_family = AF_INET;
// ������������� ����� LINGER (�������� ���� ������ ����� ���������)
    struct linger lng;
    lng.l_onoff = 1;
    lng.l_linger = 0;
    setsockopt( s, SOL_SOCKET, SO_LINGER, (char*)&lng, sizeof(struct linger));
  }
// ��������� ����������
  ip_tos = prior;
  int opt = ip_tos;
  setsockopt(s,0,IP_TOS,&opt,sizeof(opt));
// ���� �������� ������ � �������� ���������
  fcntl( s, F_SETFD, FD_CLOEXEC);
// ���������� ����� ����� ������� ������� � ������
  return TYPE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// ���������� ��������� ���������� ////////////////////////////////////////////
// ����� ���� ������� ������������!!! /////////////////////////////////////////
// ip - ����� �� �������� ��������� ������ (��� ��������� ����������)
// ��� ����� �������� ������� ������������ (��� ����������)
int CUdpSocket::Connect(UINT ip , PORT prt )
{
  int rval;
  if(ip!=INADDR_ANY && prt != 0) // �������� ��������� ���������� �����
  {
    radr.sin_addr.s_addr = ip;
    radr.sin_port = prt;
  }
// ���� ����� �� �����, �� ������ ����� ���������� � Create
// � ��� ������� ������ ������!
// �������� ��� ����� ���������������
  if( s<0 ) {
    switch(errno) { // ��� �� ������?
    	case EACCES : rval = ERS_ACCESS; break; // ��� ���� �� ��������
    	case EPROTONOSUPPORT : rval = ERS_NOPROT; break; // ����������v� ��������
    	default: rval = NULL_POINTER; // ��� ������ - ��-�� ���������� �������� �������
    }
    return rval;
  }

// "�����������" � ��������, � ��������

    // �������� ���������� ���������� (��� ����� �������������)
    if( ( rval = connect( s, ( struct sockaddr * )&radr, sizeof(radr)) ) )
    {
      switch(errno) { // ��� �� ������?
      	case EADDRINUSE :
      	case EISCONN :
      	case EALREADY  : rval = ERS_INUSE; break;
      	case EFAULT :
      	case EADDRNOTAVAIL :
      	case EAFNOSUPPORT   : rval = ERS_NOHOST; break;
      	case ENETUNREACH :
      	case EHOSTUNREACH : rval = ERS_NOROUTE; break;
      	case ETIMEDOUT : rval = ERS_TIMEOUT; break;
      	case ECONNREFUSED : rval = ERS_REFUSED ; break;
      	default: rval = EMPTY_MEMORY;
      }
      return rval;
    }
    // ��� ����� �������� , ��� ���������� �����������
    con = 1;  // ������� ����������

  return TYPE_OK;
}

/////////////////////////////////////////////////////////////////////////////////
// �������� ���������� ����������
/////////////////////////////////////////////////////////////////////////////////
//int CUdpSocket::WaitForConnect(UINT ip = INADDR_ANY )
//{
//  if ( con ) // ��� �����������
//    return ERS_INUSE;
//
//
//  SOCKET sc = s;
//  UINT rlen = sizeof(radr);
//
//  s = accept(sc,(struct sockaddr *) &radr, &rlen);
//  if( !isvalidsock(s) ) {
//    int rval;
//    switch(errno) { // ��� �� ������?
//    	case EACCES : rval = ERS_ACCESS; break; // ��� ���� �� ��������
//    	case EPROTONOSUPPORT : rval = ERS_NOPROT; break; // ����������v� ��������
//    	default: rval = EMPTY_MEMORY; // ��� ������ - ��-�� ���������� �������� ������v
//    }
//    return rval;
//  }
//
//  if( ip != INADDR_ANY && ip != radr.sin_addr.s_addr) {
//  	Disconnect();
//  	s = sc;
//  	return ERS_REFUSED;
//  }
//// ��������� �����
//  int opt = ip_tos;
//  struct linger lng;
//  lng.l_onoff = 1;
//  lng.l_linger = 0;
//  setsockopt( s, SOL_SOCKET, SO_LINGER, (char*)&lng, sizeof(struct linger));
//  setsockopt( s, 0, IP_TOS, &opt, sizeof(opt));
//// �������� � �������� ��������
//  fcntl( s, F_SETFD, FD_CLOEXEC);
//
//  con = 1;
//  shutdown(sc,2);
//  close(sc);
//  return TYPE_OK;
//}

/////////////////////////////////////////////////////////////////////////////////
// ����������� ������ (len ���������� ���-�� ��������� ����)
/////////////////////////////////////////////////////////////////////////////////
int CUdpSocket::Read(BYTE *msg, ULONG &len)
{
  int rval = TYPE_OK;
  socklen_t radrlen = sizeof(radr);
  len = recvfrom(s, msg, len, 0, (sockaddr*)&radr, &radrlen);
  if (len == 0)
     rval = EMPTY_MEMORY;

  rxBytes += len;
  return rval;
}

/////////////////////////////////////////////////////////////////////////////////
// ������ � ����� (��� ����� ������ �������� - ������� Connect)
/////////////////////////////////////////////////////////////////////////////////
int CUdpSocket::Write(BYTE *msg, ULONG len)
{
  int sent = 0, rval = TYPE_OK;
  sent = send(s, msg, len, 0);
  if ( sent < 0 )
    switch(errno) {
    	case EDESTADDRREQ : rval = ERS_NOHOST; break;
    	case EFAULT   : rval = NULL_POINTER; break;
    	default : rval = EMPTY_MEMORY;
    }
  txBytes += sent;
  return rval;
}

/////////////////////////////////////////////////////////////////////////////////
// ������ � ����� �� ������������ �����
/////////////////////////////////////////////////////////////////////////////////
int CUdpSocket::WriteTo(BYTE *msg, ULONG len, UINT ip, PORT prt)
{
  int rval = TYPE_OK;
  if( con) // ����� ��������
    rval = Write(msg, len);
  else
  {
    int sent = 0;
    radr.sin_addr.s_addr = ip;
    radr.sin_port = prt;
    sent = sendto(s, msg, len, 0, (sockaddr*)&radr, sizeof(radr));
    if ( sent < 0 )
    switch(errno) {
    	case EDESTADDRREQ : rval = ERS_NOHOST; break;
    	case EFAULT   : rval = NULL_POINTER; break;
    	default : rval = EMPTY_MEMORY;
    }
  }

  return rval;
}

/////////////////////////////////////////////////////////////////////////////////
// �������� ������� (�� ������ select)
// �������� ����� - ����� ������� ���� (������/������/������) � ��������� �� ��������� ������� �� ��������� �������
// ��������� ����� - ����� ������� ���������. to=NULL - ��� ��������, ����� �������
/////////////////////////////////////////////////////////////////////////////////
int CUdpSocket::WaitForEvent(BYTE &rd, BYTE &wr, BYTE &ex, struct timeval *to)
{
  fd_set rmask, wmask, emask, *rs = 0, *ws = 0, *es = 0;
  int n;

//  printf( "CUdpSocket::WaitForEvent(%d,%d,%d)\n", rd, wr, ex);

  if ( (!rd && !wr && !ex) || s==-1 )
    return NULL_POINTER;

  FD_ZERO(&rmask);
  FD_ZERO(&wmask);
  FD_ZERO(&emask);

  if (rd) {
  	FD_SET( s, &rmask);
  	rs = &rmask;
  }
  if (wr) {
  	FD_SET( s, &wmask);
  	ws = &wmask;
  }
  if (ex) {
  	FD_SET( s, &emask);
  	es = &emask;
  }
// 07-08-2006 �������� ������� ������ ���������� ����
  if( s==-1 )
    return ERS_NOCON;
// �������� ������ �� ������� �������
  n=select( (s+1), rs, ws, es, to);

//  printf( "CUdpSocket::WaitForEvent->select()=%d\n", n);

  rd=0;
  wr=0;
  ex=0;
// ��������� ���������� ��������
  if (n<0)
    switch(errno) { // Roman 25-01-2006 ���� ������ "return EMPTY_MEMORY"
    	case EBADF: //One of the descriptor sets specified an invalid descriptor.
		return EMPTY_MEMORY;
	case EFAULT: //One of the pointers given in the call referred to a nonexistent portion of the address space for the process.
		return NULL_POINTER;
	case EINTR : //A signal was delivered before any of the selected events occurred, or before the time limit expired.
		return ERS_INUSE;
	default: //EINVAL - A component of the pointed-to time limit is outside the acceptable range
		return ERS_NOCON;
	}
// �������� ������ to
  if (n==0) return ERS_TIMEOUT;
// ������ �������
  if( rs && FD_ISSET( s, rs) )
    rd = 1;
  if( ws && FD_ISSET( s, ws) )
    wr = 1;
  if( es && FD_ISSET( s, es) )
    ex = 1;
  return TYPE_OK;
}
/////////////////////////////////////////////////////////////////////////////////
// ��������� ����������
/////////////////////////////////////////////////////////////////////////////////
int CUdpSocket::isConnected()
{return con;}
/////////////////////////////////////////////////////////////////////////////////
// ����� ���������� �����
/////////////////////////////////////////////////////////////////////////////////
ULONG CUdpSocket::GetRemoteAdr()
{
	return radr.sin_addr.s_addr;
}
// � ����
PORT CUdpSocket::GetRemotePrt()
{
	return radr.sin_port;
}
