/////////////////////////////////////////////////////////////////////////////////
// ����� CSocket - ������� ������ � ������� ����������� �� tcp/ip
/////////////////////////////////////////////////////////////////////////////////
#include "CSocket.h"
// ������� �� �������� ������ connect
#define DEF_CONNECT_TOUT 5 // 5 ���
/////////////////////////////////////////////////////////////////////////////////
// ���������� ��������� ������ �� �����/������ ����� � �����/������ �����
/////////////////////////////////////////////////////////////////////////////////

int set_address(char *hname, char *sname, struct sockaddr_in *sap, char *protocol )
{
 struct servent *sp;
 struct hostent *hp;
 char *endptr;
 PORT port;

 bzero ( sap, sizeof(*sap) );
 sap->sin_family = AF_INET;
 if( hname != NULL )
 {
 	if( !inet_aton(hname, &sap->sin_addr) )
 	{
 		hp = gethostbyname( hname );
 		if ( hp == NULL )
 		  return ERS_NOHOST;
 		sap->sin_addr = *(struct in_addr * )hp->h_addr;
 	}
 } else
	sap->sin_addr.s_addr = htonl( INADDR_ANY );
 port = strtol( sname, &endptr, 0);
 if( *endptr == '\0' )
   sap->sin_port = htons( port );
 else {
 	sp = getservbyname ( sname, protocol );
 	if( sp==NULL )
 	  return ERS_NOPORT;
 	sap->sin_port = sp->s_port;
 }
 return TYPE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// �������� ������� ������ �� �������������� //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
int isconnected( SOCKET s, fd_set *rd, fd_set *wr, fd_set *ex )
{
  int err;
  socklen_t len = sizeof( err );
  errno =0; /* ������������, ��� ������ ���. */
  if ( !FD_ISSET( s, rd ) && !FD_ISSET( s, wr ) )
    return 0; // ���������� �� ������ � ������ - ��� ����������
  if ( getsockopt( s, SOL_SOCKET, SO_ERROR, &err, &len ) < 0 )
    return 0;  // ������ ������ � �������
  errno = err; // ������ ����������
  return (err == 0); // ���������� ������ = ������� ����������
}

/////////////////////////////////////////////////////////////////////////////////
// �����������
/////////////////////////////////////////////////////////////////////////////////
CSocket::CSocket():
  s(-1),
  con(0),
  srv(0),
  ip_tos(0),
  rxBytes(0),
  txBytes(0)
  {
  ladr.sin_addr.s_addr = INADDR_ANY;
  ladr.sin_port = 0;
  ladr.sin_family = AF_INET;
  radr.sin_addr.s_addr = INADDR_ANY;
  radr.sin_port = 0;
  radr.sin_family = AF_INET;
  }

/////////////////////////////////////////////////////////////////////////////////
// ����������
/////////////////////////////////////////////////////////////////////////////////
CSocket::~CSocket()
{
// Roman 09-11-05
	if(s!=-1)
	{
// Old code
	  Disconnect();
// Roman 09-11-05
	  close(s);
	  s=-1;
	}
}

/////////////////////////////////////////////////////////////////////////////////
// ������ ����������
/////////////////////////////////////////////////////////////////////////////////
int CSocket::Disconnect()
{// ���� ���������� ��� ��� ��� ��� ��� ������ Disconnect (!con) � ������ �������
// ������ ������� - ��� ���� ���� ���� ������ ������� �� ���� �� ���������
// ��� ���������� �����������
	if(!isvalidsock(s) /*|| (!con && srv)*/)
	  return ERS_NOCON;

	if(!con)
	{// ��� ����� ��� ��������������!!!
	  close(s);
	  s=-1;
	}
	else
	{
	  con = 0;
	  shutdown(s,1);
	  if(srv) // - ������ ��������� ��������� �� �������
	  {// ��� ����� ��� ��������������!!!
	    close(s);
	    s = -1;
	  }
	}

	return TYPE_OK;
}

/////////////////////////////////////////////////////////////////////////////////
// �������� ������ �� ������ ������������ ���������� (�����)
/////////////////////////////////////////////////////////////////////////////////
int CSocket::Create(char* hname, char *sname, BYTE prior, int server )
{
  int rval;
  struct sockaddr_in iadr;
// ������ ������ � �����
  rval = set_address( hname, sname, &iadr, "tcp");
  if(rval != TYPE_OK )
    return rval;
// �������� �������, ���������� � �������
  return Create(iadr.sin_addr.s_addr, iadr.sin_port, prior, server );
}

/////////////////////////////////////////////////////////////////////////////////
// �������� ������ �� ������ ������������ ���������� (�����)
/////////////////////////////////////////////////////////////////////////////////
int CSocket::Create(UINT ip, PORT port, BYTE prior, BOOL server )
{
  int rval;
  const int on = 1;
  s = socket( AF_INET, SOCK_STREAM, 0);
  if( !isvalidsock(s) ) {
    switch(errno) { // ��� �� ������?
    	case EACCES : rval = ERS_ACCESS; break; // ��� ���� �� ��������
    	case EPROTONOSUPPORT : rval = ERS_NOPROT; break; // ����������v� ��������
    	default: rval = EMPTY_MEMORY; // ��� ������ - ��-�� ���������� �������� ������v
    }
    return rval;
  }

  rxBytes=0, txBytes=0;
// ���� ������, ������������ ��������� �����
  if(server) { // ���������� ���������� ����� ��� ���������
    ladr.sin_addr.s_addr = ip;
    ladr.sin_port = port;
  } else { // ���������� ��������� ����� �� ��� ������, ���� ����� ��� ����� ������������� ��� ����������
    ladr.sin_addr.s_addr = INADDR_ANY;
    ladr.sin_port = 0;
  }
  ladr.sin_family = AF_INET;
// ��������� ��� ������������ �������� � ��� �������� � ��� ��������
  if( setsockopt( s, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on)) )
    return EMPTY_MEMORY;
// �������� � ���������� ������ �������� ��� �������� � ��������
  if( bind( s, ( struct sockaddr * )&ladr, sizeof(ladr)) )
    {
      switch(errno) { // ��� �� ������?
      	case EADDRINUSE :
      	case EINVAL : rval = ERS_INUSE; break;
      	case EFAULT :
      	case EADDRNOTAVAIL : rval = ERS_NOHOST; break;
      	case EACCES : rval = ERS_ACCESS; break;
      	default: rval = EMPTY_MEMORY;
      }
      return rval;
    }
// �������� ������� ���������� �����
  socklen_t slen = sizeof(ladr);
  getsockname( s, ( struct sockaddr * )&ladr, &slen);
  if( !server) { // client
// ���������� ���������� ����� ��� ���������
  	radr.sin_addr.s_addr = ip;
	  radr.sin_port = port;
	  radr.sin_family = AF_INET;
// ������������� ����� LINGER (�������� ���� ������ ����� ���������)
    struct linger lng;
    lng.l_onoff = 1;
    lng.l_linger = 0;
    setsockopt( s, SOL_SOCKET, SO_LINGER, (char*)&lng, sizeof(struct linger));
  }

  ip_tos = prior;
  int opt = ip_tos;
  setsockopt(s,0,IP_TOS,&opt,sizeof(opt));

  srv = server;
  return TYPE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// ���������� ��������� ���������� ////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// ip - ����� �� �������� ��������� ������ (��� ��������� ����������)
int CSocket::Connect(UINT ip )
{
  int rval;
  struct sockaddr_in* adr; // ���� � ������ ���������� �������� ������ �����
  if(srv)
    adr = &ladr;
  else
    adr = &radr;

  if(adr->sin_port != 0 && !isvalidsock(s)){ // ������ ����� ���������
      rval=Create(adr->sin_addr.s_addr, adr->sin_port, ip_tos, srv);
      if(rval != TYPE_OK)
        return rval;
  }

// �������� ��� ����� ���������������
  if( !isvalidsock(s) ) {
    switch(errno) { // ��� �� ������?
    	case EACCES : rval = ERS_ACCESS; break; // ��� ���� �� ��������
    	case EPROTONOSUPPORT : rval = ERS_NOPROT; break; // ����������v� ��������
    	default: rval = NULL_POINTER; // ��� ������ - ��-�� ���������� �������� �������
    }
    return rval;
  }

// �����������
  if( !srv )
  { // ��� ������
    // ������� � ��������������� ����� ��� �������� �������� ��������
    rval = 1;
    ioctl( s, FIONBIO, &rval);
    // ������� ����������
    if( ( rval = connect( s, ( struct sockaddr * )&radr, sizeof(radr)) )
        && errno != EINPROGRESS )
    {
      switch(errno) { // ��� �� ������?
      	case EADDRINUSE :
      	case EISCONN :
      	case EALREADY  : rval = ERS_INUSE; break;
      	case EFAULT :
      	case EADDRNOTAVAIL :
      	case EAFNOSUPPORT   : rval = ERS_NOHOST; break;
      	case ENETUNREACH :
      	case EHOSTUNREACH : rval = ERS_NOROUTE; break;
      	case ETIMEDOUT : rval = ERS_TIMEOUT; break;
      	case ECONNREFUSED : rval = ERS_REFUSED ; break;
      	default: rval = EMPTY_MEMORY;
      }
      return rval;
    }
    // ���� ���������� �� ����������� ����� (���� ��������)
    if( rval != 0)
    { // ������� �������� �������� select
      fd_set rdevents, wrevents, exevents;
      timeval tv;
      FD_ZERO( &rdevents ); // �������� ����� ������������ ������������
      FD_SET( s, &rdevents ); // ������� ���� ��� �����
      wrevents = rdevents;  // ������� ���� ��� �����
      exevents = rdevents;  // ������� ���� ��� �����
      tv.tv_sec = DEF_CONNECT_TOUT;
      tv.tv_usec = 0;
      // ���� ����������
      rval = select( s + 1, &rdevents, &wrevents, &exevents, &tv );
      // �������� ���� ��������
      if ( rval < 0 ) // ������ ������ select
        return EMPTY_MEMORY; // ���������� ��������
      else if ( rval == 0 )  // ����� ����-��� connect
        return ETIMEDOUT;    // ������ �� ��������
      else if ( !isconnected( s, &rdevents, &wrevents, &exevents ) ) // ����������� �������
      { // isconnected ��������� ���������� errno, ��� �� ������?
        switch(errno) {
        	case EADDRINUSE :
        	case EISCONN :
        	case EALREADY  : rval = ERS_INUSE; break;
        	case EFAULT :
        	case EADDRNOTAVAIL :
        	case EAFNOSUPPORT   : rval = ERS_NOHOST; break;
        	case ENETUNREACH :
        	case EHOSTUNREACH : rval = ERS_NOROUTE; break;
        	case ETIMEDOUT : rval = ERS_TIMEOUT; break;
        	case ECONNREFUSED : rval = ERS_REFUSED ; break;
        	default: rval = EMPTY_MEMORY;
        }
        return rval;
      }
    }
    // ���������� ����������� ����� ��� �� select
    rval = 0; // ������� ���� ���������������
    ioctl( s, FIONBIO, &rval);
    con = 1;  // ������� ����������
    fcntl( s, F_SETFD, FD_CLOEXEC); // ���� �������� ������ � �������� ���������
    socklen_t slen = sizeof(ladr);
// �������� ������� ���������� �����
    getsockname( s, ( struct sockaddr * )&ladr, &slen);
  } else
  { // ��� ������
    rval=WaitForConnect(ip);
    if(rval!=TYPE_OK)
	    return rval;
  }

  return TYPE_OK;
}

/////////////////////////////////////////////////////////////////////////////////
// �������� ���������� ����������
/////////////////////////////////////////////////////////////////////////////////
int CSocket::WaitForConnect(UINT ip )
{
  if ( con )
    return ERS_INUSE;

  if( listen( s, 1) )
    return EMPTY_MEMORY;

  SOCKET sc = s;
  UINT rlen = sizeof(radr);

  s = accept(sc,(struct sockaddr *) &radr, &rlen);
  if( !isvalidsock(s) ) {
    int rval;
    switch(errno) { // ��� �� ������?
    	case EACCES : rval = ERS_ACCESS; break; // ��� ���� �� ��������
    	case EPROTONOSUPPORT : rval = ERS_NOPROT; break; // ����������v� ��������
    	default: rval = EMPTY_MEMORY; // ��� ������ - ��-�� ���������� �������� ������v
    }
    return rval;
  }

  if( ip != INADDR_ANY && ip != radr.sin_addr.s_addr) {
  	Disconnect();
  	s = sc;
  	return ERS_REFUSED;
  }
// ��������� �����
  int opt = ip_tos;
  struct linger lng;
  lng.l_onoff = 1;
  lng.l_linger = 0;
  setsockopt( s, SOL_SOCKET, SO_LINGER, (char*)&lng, sizeof(struct linger));
  setsockopt( s, 0, IP_TOS, &opt, sizeof(opt));
// �������� � �������� ��������
  fcntl( s, F_SETFD, FD_CLOEXEC);
// �������� ������� ���������� �����
  socklen_t slen = sizeof(ladr);
  getsockname( s, ( struct sockaddr * )&ladr, &slen);

  con = 1;
  shutdown(sc,2);
  close(sc);
  return TYPE_OK;
}

/////////////////////////////////////////////////////////////////////////////////
// ����������� ������ (len ���������� ���-�� ��������� ����)
/////////////////////////////////////////////////////////////////////////////////
int CSocket::Read(BYTE *msg, ULONG &len)
{
  int rval = TYPE_OK;
  len = recv(s, msg, len, 0);
  if (len <= 0)
  {
    switch(errno) {
    	case ENOTCONN : rval = ERS_NOCON; break;
    	case EFAULT   : rval = NULL_POINTER; break;
    	case ECONNREFUSED   : rval = ERS_REFUSED; break;
    	case EAGAIN   : rval = ERS_AGAIN; break;
    	default : rval = EMPTY_MEMORY;
    }
    len = 0;
  }
  rxBytes += len;
  return rval;
}

/////////////////////////////////////////////////////////////////////////////////
// ������ � �����
/////////////////////////////////////////////////////////////////////////////////
int CSocket::Write(BYTE *msg, ULONG len)
{
  int sent, rval = TYPE_OK;
  sent = send(s, msg, len, 0);
  if ( sent < 0 )
    switch(errno) {
    	case EDESTADDRREQ : rval = ERS_NOHOST; break;
    	case EFAULT   : rval = NULL_POINTER; break;
    	case ECONNREFUSED   : rval = ERS_REFUSED; break;
	default : rval = EMPTY_MEMORY;
    }
  txBytes += sent;
  return rval;
}

/////////////////////////////////////////////////////////////////////////////////
// �������� ������� (�� ������ select)
// �������� ����� - ����� ������� ���� (������/������/������) � ��������� �� ��������� ������� �� ��������� �������
// ��������� ����� - ����� ������� ���������. to=NULL - ��� ��������, ����� �������
/////////////////////////////////////////////////////////////////////////////////
int CSocket::WaitForEvent(BYTE &rd, BYTE &wr, BYTE &ex, struct timeval *to)
{
  fd_set rmask, wmask, emask, *rs,*ws,*es;
  int n;

  if ( (!rd && !wr && !ex) || !isvalidsock(s) )
    return NULL_POINTER;

  FD_ZERO(&rmask);
  FD_ZERO(&wmask);
  FD_ZERO(&emask);

  rs = 0;
  ws = 0;
  es = 0;

  if (rd) {
  	FD_SET( s, &rmask);
  	rs = &rmask;
  }
  if (wr) {
  	FD_SET( s, &wmask);
  	ws = &wmask;
  }
  if (ex) {
  	FD_SET( s, &emask);
  	es = &emask;
  }
// 07-08-2006 �������� ������� ������ ���������� ����
  if( !isvalidsock(s) || con==0)
    return ERS_NOCON;
// �������� ������ �� ������� �������
  n=select( (s+1), rs, ws, es, to);
  rd=0;
  wr=0;
  ex=0;
// ��������� ���������� ��������
  if (n<0)
    switch(errno) { // Roman 25-01-2006 ���� ������ "return EMPTY_MEMORY"
    	case EBADF: //One of the descriptor sets specified an invalid descriptor.
		return EMPTY_MEMORY;
	case EFAULT: //One of the pointers given in the call referred to a nonexistent portion of the address space for the process.
		return NULL_POINTER;
	case EINTR : //A signal was delivered before any of the selected events occurred, or before the time limit expired.
		return ERS_INUSE; // Roman EXIT_THREAD;
	default: //EINVAL - A component of the pointed-to time limit is outside the acceptable range
		return ERS_NOCON; // Roman ERROR_SIZE;
	}
//Roman 01-12-11 �������� ������ �� �������������
  int err = 0;
  socklen_t len = sizeof( err );
  if ( getsockopt( s, SOL_SOCKET, SO_ERROR, &err, &len ) < 0 || err!=0)
    return ERS_NOCON; // Roman ERROR_SIZE;
// �������� ������ to
  if (n==0) return ERS_TIMEOUT;
// ������ �������
  if( rs && FD_ISSET( s, rs) )
    rd = 1;
  if( ws && FD_ISSET( s, ws) )
    wr = 1;
  if( es && FD_ISSET( s, es) )
    ex = 1;
  return TYPE_OK;
}
/////////////////////////////////////////////////////////////////////////////////
// ��������� ����������
/////////////////////////////////////////////////////////////////////////////////
int CSocket::isConnected()
{return con;}
/////////////////////////////////////////////////////////////////////////////////
// ����� ���������� �����
/////////////////////////////////////////////////////////////////////////////////
//ULONG CSocket::GetRemoteAdr()
//{
//	return radr.sin_addr.s_addr;
//}
