#include <QVBoxLayout>
#include <QTextCursor>
#include <QDateTime>
#include <QFile>
#include <QTextStream>
#include <QTextCharFormat>
#include <QTextImageFormat>
#include <QCloseEvent>
#include "logdialog.h"
#include "GlobalConstants.h"




LogDialog::LogDialog(QWidget *parent) :
    QDialog(parent)
{
    editor = new QTextEdit(this);
    editor->setReadOnly(true);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(editor);
    setLayout(layout);

    restoreTextFromLog();
    setWindowTitle(tr("Протокол работы"));

    this->setMinimumWidth(LogDialogMinimalWidth);

    this->show();

    appendText(tr("Окно лога загружено"), ServiceStructures::LogMessageType::success);
}
///////////////////////////////////////////
void LogDialog::renameLogFileBeforeClose()
{
    // содержимое файла считаем уже сохраненным, т.к. предварительно вызвали close

    if (!saveLogFileOnClose)
    {
        if (!QFile::remove(GlobalConstants::logFileName))
        {
            emit warning(tr("Невозможно удалить файл лога"));
            appendText(tr("Попытка удалить файл лога закончилась неудачно"), ServiceStructures::LogMessageType::error);
        }
        return;
    }

    if (!QFile::copy(GlobalConstants::logFileName, QString("log_" + QDateTime::currentDateTime().toString("dd_MM_yyyy_hh_mm_ss") + ".txt")))
    {
        emit warning(tr("Невозможно скопировать файл лога"));
        appendText(tr("Попытка скопировать файл лога закончилась неудачно"), ServiceStructures::LogMessageType::error);
    }
    else
    {
        // а пусть он сохранит log.txt, коли не смог скопировать!
        if (!QFile::remove(GlobalConstants::logFileName))
        {
            emit warning(tr("Невозможно удалить файл лога"));
            appendText(tr("Попытка удалить файл лога закончилась неудачно"), ServiceStructures::LogMessageType::error);
        }
    }
}
///////////////////////////////////////////
void LogDialog::setSaveLogFileOnClose(const bool &shoudBeSaved)
{
    saveLogFileOnClose = shoudBeSaved;
}
///////////////////////////////////////////
void LogDialog::appendText(const QString &textToAppend, const ServiceStructures::LogMessageType &messageType)
{
    QTextCharFormat curFormat;
    QTextCursor curCursor = editor->textCursor();
    QTextImageFormat insertedImageFormat;

    curCursor.movePosition(QTextCursor::End);
    insertedImageFormat.setHeight(LogMessageIconSquareSize);
    insertedImageFormat.setWidth(LogMessageIconSquareSize);

    curCursor.insertText(QString(tr("\n") + QDateTime::currentDateTime().toString("[dd.MM.yyyy hh:mm:ss.zzz]: ")), curFormat);

    switch(messageType)
    {
    case ServiceStructures::LogMessageType::error:
        insertedImageFormat.setName(":/Resources/logMessage/logMessage_error.png");
        curCursor.insertImage(insertedImageFormat);
        curCursor.insertText("   ");

        curFormat.setFontItalic(true);
        curFormat.setFontUnderline(true);
        curFormat.setUnderlineColor(Qt::red);
        curFormat.setUnderlineStyle(QTextCharFormat::WaveUnderline);
        break;
    case ServiceStructures::LogMessageType::success:
        insertedImageFormat.setName(":/Resources/logMessage/logMessage_success.png");
        curCursor.insertImage(insertedImageFormat);
        curCursor.insertText("   ");

        curFormat.setFontUnderline(true);
        curFormat.setUnderlineColor(Qt::green);
        curFormat.setUnderlineStyle(QTextCharFormat::SingleUnderline);
        break;
    case ServiceStructures::LogMessageType::warning:
        insertedImageFormat.setName(":/Resources/logMessage/logMessage_warning.png");
        curCursor.insertImage(insertedImageFormat);
        curCursor.insertText("   ");

        curFormat.setFontItalic(true);
        break;
    case ServiceStructures::LogMessageType::normal:
        break; // using default style
    }

    curCursor.insertText(textToAppend, curFormat);

    if (outputScroll)
        scrollTextToEnd();
}
///////////////////////////////////////////
void LogDialog::setOutputScroll(const bool &scroll)
{
    outputScroll = scroll;
}
///////////////////////////////////////////
void LogDialog::clearLog()
{
    saveTextToLog();
    editor->clear();
}
///////////////////////////////////////////
void LogDialog::scrollTextToEnd()
{
    QTextCursor curCursor = editor->textCursor();
    curCursor.movePosition(QTextCursor::End);
    editor->setTextCursor(curCursor);
}
//////////////////////////////////////////
void LogDialog::saveTextToLog()
{
    QFile logFile(GlobalConstants::logFileName, this);
    if (!logFile.open(QIODevice::Append | QIODevice::Text))
    {
        emit warning(tr("Невозможно записать файл лога"));
        appendText(tr("Попытка записать файл лога закончилась неудачно"), ServiceStructures::LogMessageType::error);
        return;
    }

    QTextStream stream(&logFile);
    QString text = editor->toPlainText();
    text.remove(QChar(0xFFFC)); // сохраняем в текстовый файл, так что удалим все иконки сообщений
    stream << text;
}
//////////////////////////////////////////
void LogDialog::restoreTextFromLog()
{
    QFile logFile(GlobalConstants::logFileName, this);
    if (!logFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return; // пока не кидаем никаких ворнингов и текстов - ибо не предусмотрена проверка на первый запуск
    }

    QTextStream stream(&logFile);
    QString currentText = editor->toPlainText(); // вдруг нам уже успело что-то прийти?
    editor->setPlainText(stream.readAll() + currentText);
}
///////////////////////////////////////////
void LogDialog::closeEvent(QCloseEvent *event)
{
    if (this->isVisible())
        appendText(tr("Окно лога закрыто"), ServiceStructures::LogMessageType::warning);
    saveTextToLog();
    setVisible(false); // на самом деле мы не закрываем диалог, а просто его прячем
    emit closeEventHappened();
    event->ignore();
}













