#ifndef PING_H
#define PING_H


#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <istream>
#include <iostream>

#include <QObject>
#include <QString>
#include <QMultiMap>
#include <QList>
#include <QDebug>

#include "ServiceStructures.h"

#include "ICMP_boost/icmp_header.h"
#include "ICMP_boost/ipv4_header.h"


//using boost::asio::ip::icmp;
using boost::asio::deadline_timer;
namespace posix_time = boost::posix_time;




namespace ServiceStructures
{

struct DevicePingCharacteristics
{
    QString deviceUUID;
    deadline_timer *deviceTimer;
    std::size_t numberOfReplies;
    unsigned short sequenceNumber;
    boost::asio::ip::icmp::endpoint endpoint;
    posix_time::ptime sendTime;


    DevicePingCharacteristics(boost::asio::io_service *io_service, QString uuid);
    DevicePingCharacteristics(const DevicePingCharacteristics &other);

    virtual ~DevicePingCharacteristics();
};

}

class Pinger : public QObject
{
    Q_OBJECT


public:
    Pinger(boost::asio::io_service& io_service,
           int timeToRePing, int timeToWaitForPingReplyModifyer, QObject *parent);
    void startWorking();


protected:   
    void handle_timeout();
    void handleConcreteDeviceTimeout(ServiceStructures::DevicePingCharacteristics *deviceCharacteristics,
                                     const boost::system::error_code &ec = boost::system::error_code());
    void handle_receive(std::size_t length);
    static unsigned short get_identifier();
    void sheduleForDeletion(ServiceStructures::DevicePingCharacteristics *toDelete);
    void performDeletion(deadline_timer *dt, ServiceStructures::DevicePingCharacteristics *toDelete);

    boost::asio::ip::icmp::resolver resolver_;
    boost::asio::ip::icmp::endpoint destination_;
    boost::asio::ip::icmp::socket socket_;
    deadline_timer timer_;
    unsigned short sequence_number_;
    posix_time::ptime time_sent_;
    boost::asio::streambuf reply_buffer_;
    std::size_t num_replies_;

    QMultiMap<boost::asio::ip::icmp::endpoint, QString> destinationIPList; // key - IP устройства, value - его UUID
    QMultiMap<QString, ServiceStructures::DevicePingCharacteristics *> deviceCharacteristicsList;
                        // key - IP устройства

    int time_rePing; // время, через которое пингуем заново
    int time_waitForReplyModifyer; // time_waitForReplyModifyer * time_rePing == время, через которое считаем устройство выкл. при отсутствии ответа
    boost::asio::ip::icmp::endpoint dummyPoint;
    boost::asio::io_service *usedService;

    virtual ~Pinger();


signals:
    void ping_successfully(bool success, CustomQStrList uuids);
    // ========================================================================
    void pingedIP(bool success, boost::asio::ip::icmp::endpoint endpoint);
    // ========================================================================


public slots:
    void setTime_rePing(const int &value);
    void setTime_waitForReplyModifyer(const int &value);
    void addPingIP(const QString &uuid, QString ip);
    void removePingIP(const QString &uuid);


protected slots:
    void start_send();
    void start_receive();
    void startSendConcreteDevice(ServiceStructures::DevicePingCharacteristics *deviceCharacteristics,
                                 const boost::system::error_code &ec = boost::system::error_code());


};





#endif // PING_H
