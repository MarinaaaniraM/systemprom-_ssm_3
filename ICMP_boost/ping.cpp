//
// ping.cpp
// ~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//


#include "ping.h"
#include "netdevicethreading.h"
#include <QMapIterator>
#include <QListIterator>



Pinger::Pinger(boost::asio::io_service& io_service,
               int timeToRePing, int timeToWaitForPingReplyModifyer, QObject *parent) :
         QObject(0), resolver_(io_service), socket_(io_service, boost::asio::ip::icmp::v4()),
             timer_(io_service), sequence_number_(0), num_replies_(0),
             time_rePing(timeToRePing), time_waitForReplyModifyer(timeToWaitForPingReplyModifyer),
             usedService(&io_service)
{
    boost::asio::ip::icmp::resolver::query query(boost::asio::ip::icmp::v4(), "127.0.0.1", ""); // dummy - иначе мы высыпаемся с сегфолтом
    dummyPoint = *resolver_.resolve(query);

    socket_.set_option(boost::asio::ip::udp::socket::reuse_address(true));
}
////////////////////////////////////////////
void Pinger::startWorking()
{
    start_send();
    start_receive();
}
////////////////////////////////////////////
void Pinger::startSendConcreteDevice(ServiceStructures::DevicePingCharacteristics *deviceCharacteristics, const boost::system::error_code &ec)
{
    if (ec == boost::asio::error::operation_aborted)
        return;

    std::string body("\"Hello!\" from Asio ping.");

    // Create an ICMP header for an echo request.
    icmp_header echo_request;
    echo_request.type(icmp_header::echo_request);
    echo_request.code(0);
    echo_request.identifier(get_identifier());
    echo_request.sequence_number(++deviceCharacteristics->sequenceNumber);
    compute_checksum(echo_request, body.begin(), body.end());

    // Encode the request packet.
    boost::asio::streambuf request_buffer;
    std::ostream os(&request_buffer);
    os << echo_request << body;

    // Send the request.
    deviceCharacteristics->sendTime = posix_time::microsec_clock::universal_time();
    socket_.send_to(request_buffer.data(), deviceCharacteristics->endpoint);

    // Wait up to five seconds for a reply.
    deviceCharacteristics->numberOfReplies = 0;
    deviceCharacteristics->deviceTimer->expires_at(deviceCharacteristics->sendTime +
                                                   posix_time::seconds(time_waitForReplyModifyer * time_rePing));
    deviceCharacteristics->deviceTimer->async_wait(boost::bind(&Pinger::handleConcreteDeviceTimeout, this,
                                                               deviceCharacteristics, boost::asio::placeholders::error));
}
////////////////////////////////////////////
void Pinger::start_send()
{
    time_sent_ = posix_time::microsec_clock::universal_time();

    // используем dummy на 127.0.0.1
    std::string body("\"Hello!\" from Asio ping.");

    // Create an ICMP header for an echo request.
    icmp_header echo_request;
    echo_request.type(icmp_header::echo_request);
    echo_request.code(0);
    echo_request.identifier(get_identifier());
    echo_request.sequence_number(++sequence_number_);
    compute_checksum(echo_request, body.begin(), body.end());

    // Encode the request packet.
    boost::asio::streambuf request_buffer;
    std::ostream os(&request_buffer);
    os << echo_request << body;

    // Send the request.

    socket_.send_to(request_buffer.data(), dummyPoint);
    // Wait up to five seconds for a reply.
    num_replies_ = 0;

    timer_.expires_at(time_sent_ + posix_time::seconds(time_waitForReplyModifyer * time_rePing));
    timer_.async_wait(boost::bind(&Pinger::handle_timeout, this));
}
////////////////////////////////////////////
void Pinger::handle_timeout()
{
    time_sent_ = posix_time::microsec_clock::universal_time();

    // Requests must be sent no less than one second apart.
    timer_.expires_at(time_sent_ + posix_time::seconds(time_rePing));
    timer_.async_wait(boost::bind(&Pinger::start_send, this));
}
////////////////////////////////////////////
void Pinger::handleConcreteDeviceTimeout(ServiceStructures::DevicePingCharacteristics *deviceCharacteristics,
                                         const boost::system::error_code &ec)
{
    if (ec == boost::asio::error::operation_aborted)
        return;

    if (deviceCharacteristics->numberOfReplies == 0)
    {
        emit ping_successfully(false, destinationIPList.values(deviceCharacteristics->endpoint));
        // ========================================================================
        emit pingedIP(false, deviceCharacteristics->endpoint);
        // ========================================================================

    }

    deviceCharacteristics->sendTime = posix_time::microsec_clock::universal_time();

    // Requests must be sent no less than one second apart.
    deviceCharacteristics->deviceTimer->expires_at(deviceCharacteristics->sendTime + posix_time::seconds(time_rePing));
    deviceCharacteristics->deviceTimer->async_wait(boost::bind(&Pinger::startSendConcreteDevice, this, deviceCharacteristics,
                                                               boost::asio::placeholders::error));
}
////////////////////////////////////////////
void Pinger::start_receive()
{
    // Discard any data already in the buffer.
    reply_buffer_.consume(reply_buffer_.size());

    // Wait for a reply. We prepare the buffer to receive up to 64KB.
    socket_.async_receive(reply_buffer_.prepare(65536),
                          boost::bind(&Pinger::handle_receive, this, _2));
}
////////////////////////////////////////////
void Pinger::handle_receive(std::size_t length)
{
    // The actual number of bytes received is committed to the buffer so that we
    // can extract it using a std::istream object.
    reply_buffer_.commit(length);

    // Decode the reply packet.
    std::istream is(&reply_buffer_);
    ipv4_header ipv4_hdr;
    icmp_header icmp_hdr;
    is >> ipv4_hdr >> icmp_hdr;

    // We can receive all ICMP packets received by the host, so we need to
    // filter out only the echo replies that match the our identifier and
    // expected sequence number.
    if (is && icmp_hdr.type() == icmp_header::echo_reply
          && icmp_hdr.identifier() == get_identifier())
    {
        QString obtainedAddress = QString::fromStdString(ipv4_hdr.source_address().to_string());
        QList<ServiceStructures::DevicePingCharacteristics *> possibleDevices =
                deviceCharacteristicsList.values(obtainedAddress);

        if (!possibleDevices.isEmpty())
        {
            // т.к. они на одном IP, то теоретически должно быть однохренственно
            QListIterator<ServiceStructures::DevicePingCharacteristics *> iter(possibleDevices);
            ServiceStructures::DevicePingCharacteristics * curCharacteristics;

            while(iter.hasNext())
            {
                curCharacteristics = iter.next();

                // If this is the first reply, interrupt the five second timeout.
                if (curCharacteristics->numberOfReplies++ == 0)
                {
                    curCharacteristics->deviceTimer->cancel();

                    curCharacteristics->sendTime = posix_time::microsec_clock::universal_time();

                    // Requests must be sent no less than one second apart.
                    curCharacteristics->deviceTimer->expires_at(curCharacteristics->sendTime + posix_time::seconds(time_rePing));
                    curCharacteristics->deviceTimer->async_wait(boost::bind(&Pinger::startSendConcreteDevice, this, curCharacteristics,
                                                                               boost::asio::placeholders::error));
                }
            }

            // т.к. IP у них один, то и endpoint должен быть один
            emit ping_successfully(true, destinationIPList.values(curCharacteristics->endpoint));
            // ========================================================================
            emit pingedIP(true, curCharacteristics->endpoint);
            // ========================================================================
        }
    }

    start_receive();
}
////////////////////////////////////////////
unsigned short Pinger::get_identifier()
{
#if defined(BOOST_ASIO_WINDOWS)
    return static_cast<unsigned short>(::GetCurrentProcessId());
#else
    return static_cast<unsigned short>(::getpid());
#endif
}
////////////////////////////////////////////
void Pinger::sheduleForDeletion(ServiceStructures::DevicePingCharacteristics *toDelete)
{
    deadline_timer *killTimer = new deadline_timer(*usedService);
    killTimer->expires_at(posix_time::microsec_clock::universal_time() +
                          posix_time::seconds(time_waitForReplyModifyer * time_rePing * 3)); // assuming it's enough
    killTimer->async_wait(boost::bind(&Pinger::performDeletion, this, killTimer, toDelete));
}
////////////////////////////////////////////
void Pinger::performDeletion(boost::asio::deadline_timer *dt, ServiceStructures::DevicePingCharacteristics *toDelete)
{
    dt->cancel();
    delete dt;
    delete toDelete;
}
////////////////////////////////////////////
void Pinger::setTime_rePing(const int &value)
{
    time_rePing = value;
}
////////////////////////////////////////////
void Pinger::setTime_waitForReplyModifyer(const int &value)
{
    time_waitForReplyModifyer = value;
}
////////////////////////////////////////////
void Pinger::addPingIP(const QString &uuid, QString ip)
{
    //icmp::resolver::query query(icmp::v4(), (ip.toStdString().c_str()), "");
    //icmp::endpoint endpoint = *resolver_.resolve(query);
    boost::asio::ip::icmp::endpoint endpoint;

    endpoint.address(boost::asio::ip::address_v4::from_string(ip.toStdString().c_str()));

    destinationIPList.insert(endpoint, uuid);

    ServiceStructures::DevicePingCharacteristics *characteristics =
            new ServiceStructures::DevicePingCharacteristics(usedService, uuid);
    characteristics->endpoint = endpoint;
    deviceCharacteristicsList.insert(ip, characteristics);

    startSendConcreteDevice(characteristics);
}
////////////////////////////////////////////
void Pinger::removePingIP(const QString &uuid)
{
    QMapIterator<boost::asio::ip::icmp::endpoint, QString> iter(destinationIPList);
    while (iter.hasNext())
    {
        iter.next();
        if (iter.value() == uuid)
        {
            destinationIPList.remove(iter.key(), uuid);
        }
    }

    QMapIterator<QString, ServiceStructures::DevicePingCharacteristics *> chIter(deviceCharacteristicsList);
    while (chIter.hasNext())
    {
        chIter.next();
        if (chIter.value()->deviceUUID == uuid)
        {
            ServiceStructures::DevicePingCharacteristics *found = chIter.value();
            found->deviceTimer->cancel();
            sheduleForDeletion(found);

            deviceCharacteristicsList.remove(chIter.key(), chIter.value());
        }
    }

    if (destinationIPList.isEmpty() || deviceCharacteristicsList.isEmpty())
    {
        destinationIPList.clear();
        deviceCharacteristicsList.clear();
    }
}
////////////////////////////////////////////
Pinger::~Pinger()
{
    destinationIPList.clear();

    socket_.close();

    QMapIterator<QString, ServiceStructures::DevicePingCharacteristics *> chIter(deviceCharacteristicsList);
    while (chIter.hasNext())
    {
        chIter.next();
        chIter.value()->deviceTimer->cancel();
        delete chIter.value();
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////



ServiceStructures::DevicePingCharacteristics::DevicePingCharacteristics(boost::asio::io_service *io_service, QString uuid) :
    deviceUUID(uuid), numberOfReplies(0), sequenceNumber(0)
{
    deviceTimer = new deadline_timer(*io_service);
}
////////////////////////////////////////////
ServiceStructures::DevicePingCharacteristics::DevicePingCharacteristics(const ServiceStructures::DevicePingCharacteristics &other)
{
    deviceUUID = other.deviceUUID;
    deviceTimer = other.deviceTimer;
    numberOfReplies = other.numberOfReplies;
    sequenceNumber = other.sequenceNumber;
    endpoint = other.endpoint;
    sendTime = other.sendTime;
}
////////////////////////////////////////////
ServiceStructures::DevicePingCharacteristics::~DevicePingCharacteristics()
{
    delete deviceTimer;
}






































