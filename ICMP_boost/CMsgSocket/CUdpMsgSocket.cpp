//////////////////////////////////////////////////////////////////////////////////
/// CUdpMsgSocket - ����� �������� ������ ��������� ��������� ����� UDP-�����
//////////////////////////////////////////////////////////////////////////////////
#include "CUdpMsgSocket.h"

// �����������
CUdpMsgSocket::CUdpMsgSocket():
rptid(0),
wptid(0),
inList(0),
outList(0),
a_ip(INADDR_ANY),
a_prt(0),
needReConnect(0)
//tout(DEF_CLIENT_TIMEOUT),
//is_active(0)
{
}

// ����������
CUdpMsgSocket::~CUdpMsgSocket()
{
    Close();
}

void CUdpMsgSocket::Close()
{
	a_ip = INADDR_NONE; // ���� ���� ������� ����������, ��� ����������
	Disconnect( TYPE_OK); // �������������� ����� ERS_NOCON - by Roman 09-11-05 // EXIT_THREAD - Roman 26-03-10

	a_prt = 0;
	if(wptid)
	{
		 pthread_kill( wptid, SIGUSR1);// //???
		 pthread_join( wptid, NULL);
   	 wptid = 0;
	}
	sched_yield();
	if(rptid)
	{
	   pthread_cancel(rptid);
	   pthread_join( rptid, NULL); //???
 	   rptid = 0;
	}
}

// �������� �����
void* MainUDPRThread(void* a)
{
  int rval = TYPE_OK;
  CUdpMsgSocket *chn = (CUdpMsgSocket*)a;
  if(chn){
//    sigset_t blocked;
//    sigemptyset(&blocked);
//    sigaddset(&blocked,SIGINT);
//    pthread_sigmask( SIG_BLOCK, &blocked, NULL);
    rval = chn->MainRCycle();
  } else
    rval = NULL_POINTER;

// Roman. Warning aware 20-03-2013  
  return NULL; //((void*)-&rval);
}

//// ������� �����
void* MainUDPWThread(void* a)
{
  int rval = TYPE_OK;
  CUdpMsgSocket *chn = (CUdpMsgSocket*)a;
  if(chn){
//    sigset_t blocked;
//    sigemptyset(&blocked);
//    sigaddset(&blocked,SIGINT);
//    pthread_sigmask( SIG_BLOCK, &blocked, NULL);
    rval = chn->MainWCycle();
  } else
    rval = NULL_POINTER;
// Roman. Warning aware 20-03-2013  
  return NULL; //((void*)-&rval);
}

/// ���������� ���������� ����������. �������� ����� Create!
/// \param ip - ����� � network order.
void CUdpMsgSocket::SetLocalAdr(UINT ip)
{
  sock.ladr.sin_addr.s_addr = ip;
}

// �������� ����������. ��-��������� - ����������
// ����� � server=1 ������ ������������ �����
// hname ����� ����: "127.0.0.1" ��� "localhost", ���� NULL ������������� INADDR_ANY
// sname: "25" ��� "smtp"
int CUdpMsgSocket::Create(char *hname, char *sname, BYTE prior, CBaseMessageList *inL, CBaseMessageList *outL, BOOL server)
{
  if(!inL )
    return NULL_POINTER;

  inList = inL;
  outList = outL;

  int rval;
  struct sockaddr_in iadr;
// ������ ������ � �����
  rval = set_address( hname, sname, &iadr, "udp");
  if(rval != TYPE_OK )
    return rval;

  rval = sock.Create(iadr.sin_addr.s_addr, iadr.sin_port, prior, server );
  a_prt = iadr.sin_port;
  a_ip = iadr.sin_addr.s_addr;
  return rval;
}

// ip � port - ����� � network order
int CUdpMsgSocket::Create(UINT ip, PORT port, BYTE prior, CBaseMessageList *inL, CBaseMessageList *outL, BOOL server )
{
  if(!inL )
    return NULL_POINTER;

  inList = inL;
  outList = outL;

  int rval;
  rval = sock.Create(ip, port, prior, server );
  a_ip = ip;
  a_prt = port;
  return rval;
}

/////////////////////////////////////////////////////////////////////////////////
// ���������� ���������� � ������ ������
// ������ ������, ���� ������� �� ������� ip � port, �� ��������� �����������
// ���� ����������� �� �����, ������ ������� ������������� �����
// reCon - ���� ������������� ������������� ������
int CUdpMsgSocket::Connect( BYTE reCon, UINT ip , PORT port)
{
  int rval = TYPE_OK;
  if(isConnected()){
    return ERS_INUSE;
  }
  needReConnect = reCon;
//  a_ip = ip;
//  a_prt = port;
  if(needReConnect)
  { // Connect ������ ����������� �������, �.�. ������� �� ������������� ������� ����������
    rval = sock.Connect( ip, port);
    if(rval!=TYPE_OK)
      return rval;
  }
// ������� �����-����������
  if(rptid==0)
   if(pthread_create( &rptid, NULL, &MainUDPRThread, (void*)this )!=0)
    return EXIT_THREAD;
  sched_yield();
  return rval;
}

/////////////////////////////////////////////////////////////////////////////////
// ������ ��������� � �����
/////////////////////////////////////////////////////////////////////////////////
int CUdpMsgSocket::MsgWrite(BYTE* msg, UINT ip , PORT port)
{
// ��� ���������� - ��������� �������� ��������
 if ( !isConnected() && (ip == INADDR_ANY || port == 0) )
   return ERS_NOCON;
// �������� ���������
 if ( !msg)
   return NULL_POINTER;

// ���� ������ �� ������ (����� ����������� ������ ��� ����������)
// if ( outList)
//   return(outList->AddMessage(msg));
// ��� ������ �� ������
 GHEADER* msghead = (GHEADER*)msg;
// �������� �� �������� ���������, �����
 if( msghead->Size < MSG_HEADER || msghead->Size > (sizeof(Addr)*msghead->Count+MAX_TRANSPORT_PACKET_SIZE+MSG_HEADER) )
   return ZERO_SIZE;

// ������ ������ � �����
 return( sock.WriteTo(msg, msghead->Size, ip, port) );	// ������ ������, ���� ��� ������
}

// ��������� ����������
int CUdpMsgSocket::Disconnect( int ErrCode )
{// �� ���� WaitForEvent � ������ ������ ������ ������� ������, ����� ���� ����� ����������
	int rval;
// ���� ������ ������������� - ������� �����������. �� ��������� ���� ���� �� ���������
  if(ErrCode==TYPE_OK) // Roman 30/01/2008
    needReConnect = 0;
// ������ TYPE_OK ��� ������ ������ (�������������� ������)
	rval = sock.Disconnect();

// ��� ������� ������������� ���� ����������� ���������� �������
	if(ErrCode==TYPE_OK){ // Roman 30/01/2008
	    a_ip = INADDR_NONE;
	    a_prt = 0;
      sched_yield();
  }
// � �������� �������� ������� ������ ��� TYPE_OK
	if(rval==TYPE_OK) // ������ ���� ������������� ������
	  OnDisconnect( ErrCode);

// ���� ����� ��� ��� ����������, �� � rval ����� ������
	return rval;
}

// ������ ����������
int CUdpMsgSocket::isConnected()	// ������ ���������� 1=����
{
	return sock.isConnected();
}

/////////////////////////////////////////////////////////////////////////////////
// �������� ���� ������ ��������� ��������� �� ������
/////////////////////////////////////////////////////////////////////////////////
int CUdpMsgSocket::MainRCycle()
{
  int rval = TYPE_OK;
 // ����������� �� �����
    // ������� ����� �� ������ ������ ���� ��������� ������ �� ������� �� ��������!
    if(outList)
      if(pthread_create( &wptid, NULL, &MainUDPWThread, (void*)this )!=0)
      {
        rval = EXIT_THREAD;
        Disconnect( rval);
        rptid = 0; // indicate completion of the thread
        return rval;
      }
    // ���� ������� �� ������
    // ����� ������� ��������� ������� � ����� 1 ��� ���������� ������� w
    while( 1)
    {
      BYTE r=1, w=0, e=1; // ����� �������������� �������; �� ������ - ��������� ����
      rval=sock.WaitForEvent(r, w, e, 0);   // ����� ������ �� ����� ��� ������

// ������ ��������: ����� �� ����� ������ � ��������
      if(rval == ERS_INUSE)
        continue;
      if(rval != TYPE_OK)
        break;

// ���� ���������� �� ������
    	if(r)
    	{
    		ULONG rd = MAX_UDP_LENGTH;
// ������ ������ ���������
      	rval = sock.Read( hdr, rd);
      	if ( rval != TYPE_OK ) // ������ ������ ��� ���������� � ������
      	{
      	   //printf("Read -> [%d], rd=%d!\n",rval,rd);
      	   break;
      	}
 	  	  if ( rd < MSG_HEADER ) // ������ �� ������� ��� ���������� �� ������ ��� ����� ������ ������������ ���������
 	  	  {
  	  	    rval = ERS_NOCON; // ���������� ���������
   	  	    break;
 	  	  }

// ��������, ��� ������ ���������
    		  if( rd >= MSG_HEADER) // ������ ���� �� �����, �� �� ������ ������
    		  {// ��������, ������� �� ���-�� ������ ���������
    		    GHEADER* head = (GHEADER*)hdr;
            //printf("NEW HEADER REC! SIZE=%d\n", hdr->Size);

// ������ �� ������������ ������� (� ���� ����� Size)     // Roman! 25-01-2006
// ���� ������ ����� � ������������ ����� Size
      	  	if( head->Size > MAX_UDP_LENGTH ||
      	  	      head->Size < (MSG_HEADER+sizeof(Addr)*head->Count) )
      	  	{// ������! ����� �� ���������� / ������������� � Disconnect()/
      	  	    rval = ERROR_SIZE;
      	  	    // printf("INVALID SIZE=%d\n",hdr.Size);
      	  	    break;
      	  	}

    		    // ������ ��� ���������??? (������ ����� ������ ��� ���������!)
    		    if( rd >= head->Size)
    		    {// ������� ��� ���������
//    		      printf("CUdpMsgSocket: ADDMESSAGE\n");
    		      if( OnReceive(hdr) != true) // ���� ������ �� ����������
    		        inList->AddMessage(hdr, 0); // ������ ������ � �������

      	  	} // if( rd>=hdr.Size)
      	  } // if( rd >= MSG_HEADER)
      	} // if(r) // ���������� �� ������

      	if(e)
      	{// ������ ������
      	  rval = NULL_POINTER;
      	  break;
      	}
// ������� ����� ��������� �������
//      	r=1, w=0, e=1; � ������ �����!
    }//while sock.WaitForEvent

    // ������� ������� ����
    if(wptid)
    {
      pthread_kill( wptid, SIGUSR1);
      wptid=0;
    }

// ���� Disconnect ������ �� ������ ��� (�.�. �� ��� ������ �� ����� ���������� ��� ������� ����������)
// �� ��������� ����� ������ ������ || ����� ������� ������ �� ������������� ����������
//  printf("CUdpMsgSocket: Disconnect %d\n", rval);
  Disconnect( rval);
  rptid = 0; // indicate completion of the thread
  return rval;
}

// �������� ���� �� �������
void ProcUDPUsr1(int signo)
{
	pthread_exit(NULL);
}
/////////////////////////////////////////////////////////////////////////////////
// ��������������� ���� ������ ��������� ��������� �� ������
// ����������� ������ ��� ������ �� ������� �� ������ ///////////////////////////
int CUdpMsgSocket::MainWCycle()
{
  ULONG cnt = 0;
  // �� ���� ��� ������� ������� ������ ��������� �� WaitForEvent
  // � ��� ���� isConnected ��� ������ ���� 0
  signal(SIGUSR1, ProcUDPUsr1);
  while( !(needReConnect!=0 && !isConnected()) ){
// �.�. ������ ���������� �������� �� ������ - ��� ��������� ����������
// ������� �������� ������� ������ �� ������
    if(outList->GetCount(&cnt) != TYPE_OK )
	    break;
	  if( cnt > 0 )
	  {
	    BYTE* msg=NULL;
	    int rval = outList->GetMessage(&msg); // �������� ���������
    	if ( rval == TYPE_OK && msg)
    	{//������ ������� �� ������ �� ������ �� ����������
	      GHEADER* head = (GHEADER*) msg; // ���������
	      BYTE r=0, e=0, w=1;
    	  // � ��� ���� ������ ��� ��������
    	  // ���� ���������� ������
    	  // ������� ����� ���� ������� � ��������� select �� �������
    	  while( ( rval = sock.WaitForEvent(r, w, e, 0)) == ERS_INUSE);

    	  // ������ ������ - ������
    	  if ( rval !=TYPE_OK )
    	  {
    	    delete[] msg;
    	    break;
    	  }

    	  // ����� w ����� =1, ����� ���� �� �� TYPE_OK
    	  rval = sock.Write(msg, head->Size ); // ��������

    	  delete[] msg; // outList->FreeMessage(msg);

    	  if ( rval !=TYPE_OK )
    	    break;// ������ ������ ��� ���������� � ������
    	}
    	// ����� �� ����� �������� ��������� ����� ������?
//    	if(outList->GetCount(&cnt) != TYPE_OK )
//	      break;
	  } else // ��� ������� ������
	    outList->WaitForEvent();
  }//while connected
  wptid = 0; // indicate completion of the thread
  return TYPE_OK;
}

//UINT CUdpMsgSocket::SetClientTimeout(UINT sec)
//{
//  UINT old_tout = tout;
//  tout = sec;
//
//  return( old_tout);
//}



#pragma off (unreferenced);

//int CUdpMsgSocket::OnReady(void)
//{
// return(TYPE_OK);
//}
BOOL CUdpMsgSocket::OnReceive(BYTE* msg)
{
 return(FALSE);
}
//int CUdpMsgSocket::OnConnect(void)
//{
// return(TYPE_OK);
//}
int CUdpMsgSocket::OnDisconnect(int ErrCode)
{
 return(TYPE_OK);
}

#pragma on (unreferenced);
