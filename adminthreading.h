// Этот файл является частью проекта ПС "Конструктор".
// Время создания: Птн Сен 16 13:37:52 2011
// Реализация класса взаимодействия с сервером безопасности

#ifndef ADMINTHREADING_H
#define ADMINTHREADING_H

#include <QObject>
#include <QThread>
#include <QHostAddress>
#include <QVector>
#include <QDebug>

#include <adminping.h>

#define ADMIN_ADDRESS "127.0.0.1"
#define ADMIN_PORT 0


class AdminThreading : public QThread
{
    Q_OBJECT
public:
    explicit AdminThreading(QObject *parent = 0);
    void addDeviceToPing (QHostAddress addr)
    {
        devicesOnline->append(addr);
    }
    void deleteDeviceToPing (QHostAddress addr)
    {
        devicesOnline->remove(devicesOnline->indexOf(addr));
    }
private slots:
    void deviceIsOnline(QHostAddress addr);

private:
    AdminPing* adminPing;
    QVector <QHostAddress>* devicesOnline;
    void run();

};

#endif // ADMINTHREADING_H
