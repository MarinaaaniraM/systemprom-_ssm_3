#-------------------------------------------------
#
# Project created by QtCreator 2014-01-16T13:02:19
#
#-------------------------------------------------

QT       += core gui network

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SSM_3
TEMPLATE = app


#INCLUDEPATH for boost should be reconfigured for each desktop-builder
INCLUDEPATH += /home/marina/boost_1_55_0

#also for LIBS path
LIBS += -L/home/marina/boost_1_55_0/stage/lib -lboost_system \
        -L/home/marina/ssm_3/lib -lCMsgSocket \
        -L/home/marina/ssm_3/lib -lBaseTransportServer


RC_FILE += Icon.rc

SOURCES += main.cpp\
        maininterface.cpp \
    logdialog.cpp \
    dialogmessager.cpp \
    tabwidget.cpp \
    NetLogicalDevices.cpp \
    GraphicalItems.cpp \
    devicetypeinterfaceaddwidget.cpp \
    xmloperator.cpp \
    addnewnetdevicewidget.cpp \
    ip4validator.cpp \
    ServiceStructures.cpp \
    netdevicethreading.cpp \
    ICMP_boost/ping.cpp \
    GlobalConstants.cpp \
    searchdeviceswidget.cpp \
    pingavalialedevices.cpp \
    broadcastping_linux.cpp \
    adminthreading.cpp \
    adminping.cpp



HEADERS  += maininterface.h \
    VersionInfo.h \
    ServiceStructures.h \
    logdialog.h \
    GlobalConstants.h \
    dialogmessager.h \
    tabwidget.h \
    NetLogicalDevices.h \
    GraphicalItems.h \
    devicetypeinterfaceaddwidget.h \
    xmloperator.h \
    addnewnetdevicewidget.h \
    ip4validator.h \
    netdevicethreading.h \
    ICMP_boost/icmp_header.h \
    ICMP_boost/ipv4_header.h \
    ICMP_boost/ping.h \
    searchdeviceswidget.h \
    pingavalialedevices.h \
    broadcastping_linux.h \
    adminthreading.h \
    adminping.h


RESOURCES += \
    Resources.qrc

FORMS += \
    searchdeviceswidget.ui
