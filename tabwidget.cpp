#include <QMenu>
#include <QContextMenuEvent>
#include <QHBoxLayout>
#include <QFileInfo>
#include "tabwidget.h"
#include "dialogmessager.h"
#include "xmloperator.h"


TabWidget::TabWidget(const QList<ServiceStructures::DeviceTypeInterface> *usersDTIs, QRectF sceneBaseRect, QWidget *parent) :
    QWidget(parent), customDTIs(usersDTIs)
{
    graphicsScene = new QGraphicsScene(this);
    //graphicsScene->setSceneRect(this->geometry());

    graphicsView = new QGraphicsView(graphicsScene, this);

    createActions();
    connectComponents();

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(graphicsView);

    setLayout(layout);

    if (sceneBaseRect != QRectF(0, 0, 0, 0))
    {
        graphicsScene->setSceneRect(sceneBaseRect);
        graphicsView->adjustSize();
        //adjustSize();
    }
    else
        graphicsScene->setSceneRect(geometry());

    adminType = false;
}
////////////////////////////////////////////
QString TabWidget::getFileName() const
{
    return fileName;
}
////////////////////////////////////////////
QString TabWidget::getShortenFileName() const
{
    if (fileName.isEmpty())
        return fileName;

    // раз мы тут, то файл должен существовать
    // хотя, по идее, даже с несуществующим это должно прокатить
    QFileInfo fileInfo(fileName);
    return fileInfo.fileName();
}
////////////////////////////////////////////
void TabWidget::setFileName(const QString &value)
{
    fileName = value;
}
////////////////////////////////////////////
bool TabWidget::checkIfDocumentWasChanged() const
{
    return documentWasChanged;
}
///////////////////////////////////////////
void TabWidget::setDocumentToBeChanged(const bool &value)
{
    documentWasChanged = value;
    if (value)
        emit documentChanged(this);
}
///////////////////////////////////////////
void TabWidget::addNewDeviceAct_fromMenu()
{
    posWhereMenuWasCalledFrom = graphicsView->viewport()->geometry().center();

    addNewDeviceAct_slot();
}


// ============================================================================
void TabWidget::addSearchDevices_fromMenu(QVector<QString>* dev, int type)
{
    posWhereMenuWasCalledFrom = graphicsView->viewport()->geometry().center();

    addSearchlDevices(dev, type);
}
// ============================================================================


///////////////////////////////////////////
bool TabWidget::saveToXML()
{
    if (fileName.isEmpty())
        return false;
    // ========================================================================
    QString type;
    if (setAdminTypeAct->isChecked())
        type = "admin";
    else
        type = "icmp";
    // ========================================================================

    if (XMLOperator::saveTabWidget(type, fileName, graphicsScene->sceneRect(),
                                      netDevicesList, netDeviceLinksList))
    {
        documentWasChanged = false;
        return true;
    }

    return false;
}
///////////////////////////////////////////
void TabWidget::createActions()
{
    addNewDeviceAct = new QAction(tr("Добавить устройство"), this);
    addNewDeviceAct->setStatusTip(tr("Добавить новое устройство"));
    addNewDeviceAct->setWhatsThis(tr("Добавление нового устройства"));
    connect(addNewDeviceAct, &QAction::triggered, this, &TabWidget::addNewDeviceAct_slot);
    // ========================================================================
    setAdminTypeAct = new QAction(tr("Режим опроса 'admin'"), this);
    setAdminTypeAct->setStatusTip(tr("Опрос через сервер безопасности 'admin'"));
    setAdminTypeAct->setWhatsThis(tr("Режим опроса через сервер безопасности 'admin'"));
    setAdminTypeAct->setCheckable(true);
    connect(setAdminTypeAct, &QAction::triggered, this, &TabWidget::setAdminTypeAct_slot);
    // ========================================================================

}
////////////////////////////////////////////
void TabWidget::connectComponents()
{
    connect(graphicsScene, &QGraphicsScene::changed, this, &TabWidget::sceneChanged);
}
////////////////////////////////////////////
bool TabWidget::canBeResized(const QSize &newSize, const QList<QGraphicsItem *> &graphicsItems) const
{
    if (graphicsItems.isEmpty())
        return true;

    static qreal xMax = 0;
    static qreal yMax = 0;

    if (graphicsSceneWasChanged) // оптимизация - чтобы не бегать каждый раз по листу
    {
        foreach(QGraphicsItem *item, graphicsItems)
        {
            if ((item->x() + item->boundingRect().width()) > xMax)
                xMax = item->x() + item->boundingRect().width();

            if ((item->y() + item->boundingRect().height()) > yMax)
                yMax = item->y() + item->boundingRect().height();
        }
    }

    return ((newSize.height() > yMax) && (newSize.width() > xMax));
}
////////////////////////////////////////////
AddNewNetDeviceWidget *TabWidget::createAddNewNetDeviceWidget()
{
    AddNewNetDeviceWidget *addNewDeviceWidget = new AddNewNetDeviceWidget(customDTIs, this);
    connect(addNewDeviceWidget, &AddNewNetDeviceWidget::createNewNetDevice, this, &TabWidget::obtainNewDevice_slot);
    connect(addNewDeviceWidget, &AddNewNetDeviceWidget::warning, this, &TabWidget::warning);

    return addNewDeviceWidget;
}
////////////////////////////////////////////
void TabWidget::addNewDeviceAct_slot()
{
    AddNewNetDeviceWidget *addNewDeviceWidget = createAddNewNetDeviceWidget();

    addNewDeviceWidget->exec();
}


// ============================================================================
void TabWidget::setAdminTypeAct_slot()
{
    if (setAdminTypeAct->isChecked())
        adminType = true;
    else
        adminType = false;

    emit pingTypeWasChanged(adminType);
}


// ----------------------------------------------------------------------------
void TabWidget::setAdminType(bool b)
{
    setAdminTypeAct->setChecked(b);
}

// ============================================================================


////////////////////////////////////////////
void TabWidget::obtainNewDevice_slot(NetDevice *newDevice, int x, int y)
{
    obtainNewDevice(newDevice);

    NetDeviceGraphicalItem *graphicalItem = newDevice->getGraphicalItem();

//    QPointF graphicalItemPos = graphicsView->mapToScene(
//                graphicsView->mapFromParent(posWhereMenuWasCalledFrom));

//    graphicalItem->setPos(graphicalItemPos.x(), graphicalItemPos.y());

//    graphicalItem->getTextItem()->setPos(graphicalItemPos.x(),
//                                         (graphicalItemPos.y() + graphicalItem->boundingRect().height()));

    // ============================================================================
    graphicalItem->setPos(20 * x, 0);
    graphicalItem->getTextItem()->setPos(20 * x, graphicalItem->boundingRect().height());
    // ============================================================================

    emit newDeviceWasAdded(QString(tr("Добавлено новое устройство: ") + newDevice->getName() +
                                   tr(", тип: ") + newDevice->getDTI()->name));
}
////////////////////////////////////////////
void TabWidget::obtainNewDevice(NetDevice *newDevice)
{
    netDevicesList.append(newDevice);

    NetDeviceGraphicalItem *graphicalItem = newDevice->getGraphicalItem();

    graphicsScene->addItem(graphicalItem);
    graphicsScene->addItem(graphicalItem->getTextItem());

    connect(newDevice, &NetDevice::requestToEditDevice, this, &TabWidget::userRequestedToEditDevice);
    connect(newDevice, &NetDevice::requestToConnectDevices, this, &TabWidget::userRequestedToConnectDevices);
    connect(newDevice, &NetDevice::graphicalItemIsSelected, this, &TabWidget::userSelectedDevice);
    connect(newDevice, &NetDevice::requestToDeleteDevice, this, &TabWidget::userRequestedToDeleteDevice);
    connect(newDevice, &NetDevice::requestToOpenMultiMap, this, &TabWidget::userRequestedToOpenMultiMap);
    connect(newDevice, &NetDevice::warning, this, &TabWidget::warningFromDevice);
    connect(newDevice, &NetDevice::startPingingMe, this, &TabWidget::startPingingDevice);
    connect(newDevice, &NetDevice::stopPingingMe, this, &TabWidget::stopPingingDevice);
    connect(newDevice, &NetDevice::deviceOnlineStatusChanged_writeToLog, this, &TabWidget::addTextToLog);

    newDevice->suspendThread(false);
}
////////////////////////////////////////////
void TabWidget::setDevicePos(NetDevice *device, const qreal &x, const qreal &y)
{
    NetDeviceGraphicalItem *graphicalItem = device->getGraphicalItem();

    graphicalItem->setPos(x, y);
    graphicalItem->getTextItem()->setPos(x, (y + graphicalItem->boundingRect().height()));
}
////////////////////////////////////////////
NetDevice *TabWidget::getDeviceByUUID(const QString &uuid, bool &ok)
{
    ok = false;

    foreach(NetDevice *device, netDevicesList)
    {
        if (device->getUUID() == uuid)
        {
            ok = true;
            return device;
        }
    }

    ok = true;
    return 0;
}
////////////////////////////////////////////
void TabWidget::suspendThreads(const bool &suspend)
{
    for (int i = 0; i < netDevicesList.size(); i++)
        netDevicesList[i]->suspendThread(suspend);
}
////////////////////////////////////////////
void TabWidget::sceneChanged(const QList<QRectF> &region)
{
    static bool firstRun = true; // потому что приконнекченный сигнал эмитится и при самой первой отрисовке виджета

    if (firstRun)
    {
        firstRun = false;
    }
    else
    {
        documentWasChanged = true;
        emit documentChanged(this);
    }

    graphicsView->updateScene(region);

    update();

    graphicsSceneWasChanged = true;
}
////////////////////////////////////////////
void TabWidget::userRequestedToEditDevice(NetDevice *editedDevice)
{
    AddNewNetDeviceWidget *addNewNetDeviceWidget = createAddNewNetDeviceWidget();
    addNewNetDeviceWidget->setDeviceToEdit(editedDevice);

    addNewNetDeviceWidget->exec();
}
/////////////////////////////////////////////
void TabWidget::userRequestedToConnectDevices(NetDevice *firstDevice)
{
    firstDeviceToBeConnected = firstDevice;
    firstDeviceToBeConnectedIsSelected = true;
}
////////////////////////////////////////////
void TabWidget::obtainNewDeviceLink(NetDeviceLink *newLink)
{
    netDeviceLinksList.append(newLink);

    newLink->paintElements(graphicsScene);

    connect(newLink, &NetDeviceLink::requestToDeleteDevice, this, &TabWidget::userRequestedToDeleteLink);
}
////////////////////////////////////////////
void TabWidget::userSelectedDevice(NetDevice *selectedDevice)
{
    if (firstDeviceToBeConnectedIsSelected)
    {
        // образуем новую связь!
        NetDeviceLink *netDeviceLink = new NetDeviceLink(firstDeviceToBeConnected, selectedDevice);

        obtainNewDeviceLink(netDeviceLink);

        firstDeviceToBeConnectedIsSelected = false;

        emit newLinkWasAdded(QString(tr("Добавлена связь между устройством ") + netDeviceLink->getFirstDevice()->getName() +
                                     tr(" и устройством ") + netDeviceLink->getSecondDevice()->getName()));
    }
}
////////////////////////////////////////////
void TabWidget::userRequestedToDeleteDevice(NetDevice *selectedDevice)
{
    if (askBeforeDeviceDeletion)
    {
        if (!(DialogMessager::selectedGraphicsItemCanBeDeleted()))
            return;
    }

    foreach(NetDeviceLink *link, netDeviceLinksList)
    {
        if ((link->getFirstDevice() == selectedDevice) || (link->getSecondDevice() == selectedDevice))
        {
            // нужно удалить связанную с удаляемым девайсом связь
            netDeviceLinksList.removeOne(link);
            delete link;
        }
    }

    netDevicesList.removeOne(selectedDevice);

    emit deviceWasDeleted(QString(tr("Устройство ") + selectedDevice->getName() +
                                  tr(" было удалено")));

    delete selectedDevice;
}
////////////////////////////////////////////
void TabWidget::userRequestedToDeleteLink(NetDeviceLink *selectedLink)
{
    if (askBeforeDeviceDeletion)
    {
        if (!(DialogMessager::selectedGraphicsItemCanBeDeleted()))
            return;
    }

    netDeviceLinksList.removeOne(selectedLink);

    emit linkWasDeleted(QString(tr("Связь между усройством ") + selectedLink->getFirstDevice()->getName() +
                                tr(" и устройством ") + selectedLink->getSecondDevice()->getName() +
                                tr(" была удалена")));

    delete selectedLink;
}
////////////////////////////////////////////
void TabWidget::userRequestedToOpenMultiMap(NetDevice *selectedDevice)
{
    if (firstDeviceToBeConnectedIsSelected)
        return; // это просто выбор второго девайса для коннекта, отработается в другом слоте

    if (selectedDevice->isMultiLevel())
        emit openAnotherTabWidget(selectedDevice->getMultiMapName());
}
////////////////////////////////////////////
void TabWidget::setAskBeforeDeviceDeletion(const bool &value)
{
    askBeforeDeviceDeletion = value;
}
////////////////////////////////////////////
void TabWidget::setDevicesOnline(const bool &online, const CustomQStrList &uuids)
{
    if (uuids.isEmpty())
        return;

    for (int i = 0; i < netDevicesList.size(); i++)
    {
        if (uuids.contains(netDevicesList.at(i)->getUUID()))
        {
            netDevicesList[i]->getGraphicalItem()->setDeviceStatus(online);
        }
    }
}
////////////////////////////////////////////
void TabWidget::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);

    menu.addAction(addNewDeviceAct);
    // ========================================================================
    menu.addAction(setAdminTypeAct);
    // ========================================================================

    posWhereMenuWasCalledFrom = event->pos();

    menu.exec(event->globalPos());
}
///////////////////////////////////////////
void TabWidget::resizeEvent(QResizeEvent *event)
{
    if (canBeResized(event->size(), graphicsScene->items()))
    {
        //graphicsScene->setSceneRect(graphicsView->geometry());
        graphicsScene->setSceneRect(graphicsView->viewport()->geometry()); // иначе получается хаута с вечным скроллбаром

        QWidget::resizeEvent(event);
    }
    else
        event->ignore();

    graphicsSceneWasChanged = false;
}
///////////////////////////////////////////
TabWidget::~TabWidget()
{
    while(!netDevicesList.isEmpty())
    {
        NetDevice *netDevice = netDevicesList.takeFirst();
        delete netDevice;
    }

    while (!netDeviceLinksList.isEmpty())
    {
        NetDeviceLink *netDeviceLink = netDeviceLinksList.takeFirst();
        delete netDeviceLink;
    }
}

// ============================================================================
void TabWidget::addSearchlDevices(QVector<QString>* dev, int type)
{
    for (int i = 0; i < dev->size(); ++i)
    {
        AddNewNetDeviceWidget *addNewDeviceWidget = createAddNewNetDeviceWidget();
        addNewDeviceWidget->searchDevices(dev->at(i), type, i, i);
    }
}
// ============================================================================
































