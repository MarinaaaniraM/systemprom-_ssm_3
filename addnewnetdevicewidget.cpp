#include <QGridLayout>
#include <QFileDialog>
#include <QPalette>
#include "addnewnetdevicewidget.h"
#include "GlobalConstants.h"
#include "ip4validator.h"

AddNewNetDeviceWidget::AddNewNetDeviceWidget(const QList<ServiceStructures::DeviceTypeInterface> *customInterfaces, QWidget *parent) :
    QDialog(parent), customDTIs(customInterfaces)
{
    createElements();

    initComboBoxContent();

    setupLayout();

    connectComponents();

    setWindowTitle(tr("Добавление/изменение устройства"));

    possibleDTIs_currentIndexChanged_slot(0); // в первый раз дернем ручками, т.к. при программном выставлении индекса,
                                              // которое происходит в initComboBoxContent(), сигнал-менялка не эмитится
    setAttribute(Qt::WA_DeleteOnClose);

}
//////////////////////////////////////////////
void AddNewNetDeviceWidget::setDeviceToEdit(NetDevice *deviceToEdit)
{
    device = deviceToEdit;

    usingDeviceThatAlreadyExist = true;

    if (device->isMultiLevel())
        multiMapName->setText(device->getMultiMapName());

    pictureLabel->setPixmap(QPixmap::fromImage(device->getDTI()->onlineImage));
    deviceName->setText(device->getName());
    aboutDevice->setPlainText(device->getAboutDeviceText());
    deviceAddress->setText(device->getNetworkAddressString());
    possibleDTIs->setCurrentText(device->getDTI()->name);
    shouldBePinged->setChecked(device->network_isBeingPinged());
}
//////////////////////////////////////////////
void AddNewNetDeviceWidget::createElements()
{
    okButton = new QPushButton(tr("ОK"), this);
    okButton->setStatusTip(tr("Добавить элемент"));
    okButton->setWhatsThis(tr("Добавление нового элемента с установленными свойствами на карту"));
    okButton->setAutoDefault(false);

    cancelButton = new QPushButton(tr("Отмена"), this);
    cancelButton->setStatusTip(tr("Не добавлять элемент"));
    cancelButton->setWhatsThis(tr("Не добавлять нового элемента и закрыть диалог"));
    cancelButton->setAutoDefault(true); // от дурака же

    openFileToMultiMapButton = new QPushButton(QIcon("://Resources/actionIcons/OpenFile.png"), "", this);
    openFileToMultiMapButton->setStatusTip(tr("Открыть файл карты сети..."));
    openFileToMultiMapButton->setWhatsThis(tr("Открыть файл карты сети, прикрепленный к данному устройству"));
    openFileToMultiMapButton->setAutoDefault(false);

    pictureLabel = new QLabel(this);
    pictureLabel->setStatusTip(tr("Изображение включенного устройства"));
    pictureLabel->setWhatsThis(tr("Изображение, которое показывается, если устройство данного типа включено"));

    possibleDTIs = new QComboBox(this);
    possibleDTIs->setInsertPolicy(QComboBox::NoInsert);

    deviceName = new QLineEdit(this);
    deviceName->setStatusTip(tr("Имя устройства"));
    deviceName->setWhatsThis(tr("Поле ввода используемого имени устройства"));

    multiMapName = new QLineEdit(this);
    multiMapName->setStatusTip(tr("Путь к прикреплённой карте сети"));
    multiMapName->setWhatsThis(tr("Если к устройству прикреплена другая карта сети, то в это поле вводится путь к файлу карты сети"));

    deviceAddress = new QLineEdit(this);
    deviceAddress->setStatusTip(tr("IP-адрес устройства"));
    deviceAddress->setWhatsThis(tr("IP-адрес устройства на карте"));
    deviceAddress->setInputMask("000.000.000.000; ");
    deviceAddress->setValidator(new IP4Validator(deviceAddress));

    aboutDevice = new QPlainTextEdit(this);
    aboutDevice->setDocumentTitle(tr("Описание устройства"));
    aboutDevice->setStatusTip(tr("Описание устройства"));
    aboutDevice->setWhatsThis(tr("Поле для ввода описания добавляемого устройства"));

    shouldBePinged = new QCheckBox(tr("Опрашивать"), this);
    shouldBePinged->setStatusTip(tr("Проводить ли опрос устройства"));
    shouldBePinged->setWhatsThis(tr("Проводить ли постоянный опрос устройства по указанному адресу?"));
}
//////////////////////////////////////////////
void AddNewNetDeviceWidget::initComboBoxContent()
{
    const int preDefinedListSize = PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size();

    for (int i = 0; i < preDefinedListSize; i++)
    {
        possibleDTIs->insertItem(i, PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.at(i).name);
    }

    for (int i = 0; i < customDTIs->size(); i++)
    {
        possibleDTIs->insertItem((i + preDefinedListSize), customDTIs->at(i).name);
    }

    possibleDTIs->setCurrentIndex(0); // ну и чтобы сразу какой-то конкретный индекст стоял - запихиваем
}
//////////////////////////////////////////////
void AddNewNetDeviceWidget::setupLayout()
{
    QLabel *addressText = new QLabel(tr("IP адрес"), this);
    QLabel *typeText = new QLabel(tr("Тип устройства"), this);
    QLabel *nameText = new QLabel(tr("Название устройства"), this);
    QLabel *mapText = new QLabel(tr("Карта устройства"), this);
    QLabel *aboutText = new QLabel(tr("Описание устройства"), this);

    QGridLayout *mainLayout = new QGridLayout(this);

    mainLayout->addWidget(nameText, 0, 0, 1, 1);
    mainLayout->addWidget(deviceName, 0, 1, 1, 3);
    mainLayout->addWidget(typeText, 1, 0, 1, 1);
    mainLayout->addWidget(possibleDTIs, 1, 1, 1, 2);
    mainLayout->addWidget(pictureLabel, 1, 3, 1, 1);
    mainLayout->addWidget(addressText, 2, 0, 1, 1);
    mainLayout->addWidget(deviceAddress, 2, 1, 1, 3);
    mainLayout->addWidget(mapText, 3, 0, 1, 1);
    mainLayout->addWidget(multiMapName, 3, 1, 1, 2);
    mainLayout->addWidget(openFileToMultiMapButton, 3, 3, 1, 1);
    mainLayout->addWidget(aboutText, 4, 0, 1, 1);
    mainLayout->addWidget(aboutDevice, 4, 1, 1, 3);
    mainLayout->addWidget(shouldBePinged, 5, 1, 1, 1);
    mainLayout->addWidget(okButton, 6, 1, 1, 1);
    mainLayout->addWidget(cancelButton, 6, 2, 1, 1);

    pictureLabel->setMaximumWidth(GridLayoutWidgetMinimalWidth / 1.5);
    addressText->setMaximumWidth(GridLayoutWidgetMinimalWidth * 2.7);
    typeText->setMaximumWidth(GridLayoutWidgetMinimalWidth * 2.7);
    nameText->setMaximumWidth(GridLayoutWidgetMinimalWidth * 2.7);
    mapText->setMaximumWidth(GridLayoutWidgetMinimalWidth * 2.7);
    openFileToMultiMapButton->setMaximumWidth(GridLayoutWidgetMinimalWidth / 1.5);
    aboutText->setMaximumWidth(GridLayoutWidgetMinimalWidth * 2.7);
    mainLayout->setSizeConstraint(QLayout::SetFixedSize);
    setLayout(mainLayout);
}
//////////////////////////////////////////////
void AddNewNetDeviceWidget::connectComponents()
{
    connect(cancelButton, &QPushButton::clicked, this, &AddNewNetDeviceWidget::close);
    connect(openFileToMultiMapButton, &QPushButton::clicked, this, &AddNewNetDeviceWidget::openFileToMultiMapButton_slot);
    connect(possibleDTIs, SELECT<int>::OVERLOAD_OF(&QComboBox::currentIndexChanged),
            this, &AddNewNetDeviceWidget::possibleDTIs_currentIndexChanged_slot);
    connect(okButton, &QPushButton::clicked, this, &AddNewNetDeviceWidget::okButton_slot);
}
//////////////////////////////////////////////
void AddNewNetDeviceWidget::bad_deviceName()
{
    // Застенчиво покраснеем полем ввода
    QPalette badPalette;
    badPalette.setColor(QPalette::Base, Qt::red);
    deviceName->setPalette(badPalette);

    connect(deviceName, &QLineEdit::textChanged, this, &AddNewNetDeviceWidget::deviceName_textChanged_slot);
    // фактически одноразовый коннект - при первом изменении перекрасим обратно в белый
}
//////////////////////////////////////////////
void AddNewNetDeviceWidget::bad_deviceAddress()
{
    // Застенчиво покраснеем полем ввода
    QPalette badPalette;
    badPalette.setColor(QPalette::Base, Qt::red);
    deviceAddress->setPalette(badPalette);

    connect(deviceAddress, &QLineEdit::textChanged, this, &AddNewNetDeviceWidget::deviceAddress_textChanged_slot);
    // фактически одноразовый коннект - при первом изменении перекрасим обратно в белый
}
//////////////////////////////////////////////
void AddNewNetDeviceWidget::openFileToMultiMapButton_slot()
{
    // создадим свой вместо дефолтного для большей кастомизации
    QFileDialog *fileDialog = new QFileDialog(this);
    fileDialog->setFileMode(QFileDialog::ExistingFile);
    fileDialog->setNameFilter(tr("Карты SSM (*.ssmm)"));

    if (fileDialog->exec())
    {
        QStringList strList = fileDialog->selectedFiles();
        if (!strList.isEmpty())
            multiMapName->setText(strList.last()); // т.к. мы в режиме QFileDialog::ExistingFile, то там может быть лишь один элемент
    }
}
//////////////////////////////////////////////
void AddNewNetDeviceWidget::possibleDTIs_currentIndexChanged_slot(const int &index)
{
    if (index < PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size())
    {
        // имеем дело с пре-дефайненным типом
        pictureLabel->setPixmap(QPixmap::fromImage(
                                    PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.at(index).onlineImage));
    }
    else
    {
        // пользовательский тип, шайтанама
        pictureLabel->setPixmap(QPixmap::fromImage(
                            customDTIs->at(index - PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size()).onlineImage));
    }
}
/////////////////////////////////////////////
void AddNewNetDeviceWidget::okButton_slot()
{
    // вначале проверим валидность данных
    if (deviceName->text().isEmpty())
    {
        // ну имя-то нам точно нужно!
        emit warning(tr("Необходимо ввести имя устройства"));
        bad_deviceName();

        return;
    }

    QString deviceAddressString = deviceAddress->text();
    deviceAddressString.remove(' ');
    QHostAddress netAddress;

    if (!netAddress.setAddress(deviceAddressString))
    {
        // не можем распарсить адрес
        emit warning(tr("Необходимо ввести корректный IP-адрес устройства"));
        bad_deviceAddress();

        return;
    }

    if (usingDeviceThatAlreadyExist)
    {
        // закончили редактировать, сохраним изменения в существующем девайсе
        device->setName(deviceName->text());
    }
    else
    {
        // создадим новый девайс и выставим ему свойства
        device = new NetDevice(deviceName->text());
    }
    // а дальше общее для обоих вариантов

    const ServiceStructures::DeviceTypeInterface *usedDTI = 0;

    if (possibleDTIs->currentIndex() < PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size())
    {
        // используется стандартный тип
        usedDTI = &PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.at(possibleDTIs->currentIndex());
    }
    else
    {
        // кастомный тип
        usedDTI = &customDTIs->at(possibleDTIs->currentIndex() - PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size());
    }

    device->setDeviceTypeInterface(usedDTI);
    device->setAboutDeviceText(aboutDevice->toPlainText());
    device->setMultiMapName(multiMapName->text());
    device->network_setNetworkAddress(netAddress);
    device->network_setPinging(shouldBePinged->isChecked());

    if (!usingDeviceThatAlreadyExist)
        emit createNewNetDevice(device, 0, 0); // а иначе мы просто отредактили его по указателю
    else
    {
        if (device->network_isBeingPinged())
            device->resetThread();
    }

    close();
}
////////////////////////////////////////////
void AddNewNetDeviceWidget::deviceName_textChanged_slot()
{
    QPalette goodPalette;
    goodPalette.setColor(QPalette::Base, Qt::white);
    deviceName->setPalette(goodPalette);

    // мавр сделал свое дело, мавра можно дисконнектнуть
    disconnect(deviceName, &QLineEdit::textChanged, this, &AddNewNetDeviceWidget::deviceName_textChanged_slot);
}
///////////////////////////////////////////
void AddNewNetDeviceWidget::deviceAddress_textChanged_slot()
{
    QPalette goodPalette;
    goodPalette.setColor(QPalette::Base, Qt::white);
    deviceAddress->setPalette(goodPalette);

    // мавр сделал свое дело, мавра можно дисконнектнуть
    disconnect(deviceAddress, &QLineEdit::textChanged, this, &AddNewNetDeviceWidget::deviceAddress_textChanged_slot);
}


// ============================================================================
void AddNewNetDeviceWidget::searchDevices(QString dev, int type, int x, int y)
{
    device = new NetDevice(dev);

    const ServiceStructures::DeviceTypeInterface *usedDTI = 0;

    if (type < PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size())
    {
        // используется стандартный тип
        usedDTI = &PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.at(type);
    }
    else
    {
        // кастомный тип
        usedDTI = &customDTIs->at(type - PreDefinedDeviceTypeInterface::preDefinedDeviceTypeInterfaceList.size());
    }

    device->setDeviceTypeInterface(usedDTI);
    device->network_setNetworkAddress(dev);
    device->network_setPinging(true);

    emit createNewNetDevice(device, x, y);
    close();
}
// ============================================================================




























