#include "adminthreading.h"

AdminThreading::AdminThreading(QObject *parent) :
    QThread(parent)
{
    adminPing = new AdminPing();
    devicesOnline = new QVector <QHostAddress>();
    adminPing->Create(QHostAddress(ADMIN_ADDRESS), ADMIN_PORT);

    connect(adminPing, &AdminPing::devicePingedSuccesessful, this, &AdminThreading::deviceIsOnline);
}


// ----------------------------------------------------------------------------
void AdminThreading::run()
{
    for (int i = 0; i < devicesOnline->size(); ++i)
    {
        adminPing->StateRequest(devicesOnline->at(i));
    }
}


// ----------------------------------------------------------------------------
void AdminThreading::deviceIsOnline(QHostAddress addr)
{
    qDebug() << addr.toString();
}
